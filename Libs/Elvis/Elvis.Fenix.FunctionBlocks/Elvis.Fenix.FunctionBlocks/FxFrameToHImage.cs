﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HalconDotNet;
using Elvis.Fenix.Hardware;
using System.ComponentModel;

namespace Elvis.Fenix.FunctionBlocks
{
    public class FxFrameToHImage : FxBlock
    {
        public FxFrame InputImage { get; set; }
        public HalconDotNet.HImage _image;

        public HalconDotNet.HImage OutputImage
        {
            get { return _image; }
            set
            {
                _image = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("OutputImage"));
            }
        }

        public FxFrameToHImage() { }

        public override void Run()
        {
            if(InputImage != null)
            {
                try 
                {
                   OutputImage = new HImage("byte", InputImage.Width, InputImage.Height, InputImage.Data);
                }
                catch(Exception ex)
                {
                    throw new Exception("Failed to convert FxFrame to HImage");
                }         
            }
        }

        public override void Clear()
        {
            if(OutputImage != null)
            {
                OutputImage.Dispose();
            }
        }
    }
}
