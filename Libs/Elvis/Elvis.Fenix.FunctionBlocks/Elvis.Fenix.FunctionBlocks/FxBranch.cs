﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elvis.Fenix.FunctionBlocks
{
    public enum Expression
    {
        Equal,
        NotEqual,
        LowerThan,
        LowerThanOrEqual,
        HigherThan,
        HigherThanOrEqual
    }

    public class FxBranch: FxBlock
    {
        [Browsable(false), BlockNameAttribute]
        public override string Name { get; set; }

        private double _ArgA;
        private double _ArgB;
        private Expression _Expression;
        private bool _Condition;

        public FxBranch()
        {
        }

        public FxBranch(string name)
        {
            Name = name;
            Icon = Resource.Default;
            Expression = Expression.Equal;
        }

        public override void Run()
        {
            Judgement = true;
            DateTime startTime = DateTime.Now;

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            Compare();

            if(Condition)
            {
                NextFxBlock = PosRef;
            }
            else
            {
                NextFxBlock = NegRef;
            }

            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }

        private void Compare()
        {
            switch(Expression)
            {
                case Expression.LowerThan:
                    Condition = ArgA < ArgB ? true : false;
                    break;

                case Expression.LowerThanOrEqual:
                    Condition = ArgA <= ArgB ? true : false;
                    break;

                case Expression.HigherThan:
                    Condition = ArgA > ArgB ? true : false;
                    break;

                case Expression.HigherThanOrEqual:
                    Condition = ArgA >= ArgB ? true : false;
                    break;

                case Expression.Equal:
                    Condition = ArgA == ArgB ? true : false;
                    break;

                case Expression.NotEqual:
                    Condition = ArgA != ArgB ? true : false;
                    break;
            }            
        }

        public override void Clear()
        {
            
        }

        [DisplayName("A")]
        public double ArgA
        {
            get
            {
                return _ArgA;
            }

            set
            {
                _ArgA = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ArgA"));
            }
        }

        [DisplayName("B")]
        public double ArgB
        {
            get
            {
                return _ArgB;
            }

            set
            {
                _ArgB = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ArgB"));
            }
        }

        public Expression Expression
        {
            get
            {
                return _Expression;
            }

            set
            {
                _Expression = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Expression"));
            }
        }

        [Browsable(false)]
        public FxBlock PosRef { set; get; }

        [Browsable(false)]
        public FxBlock NegRef { set; get; }

        [DisplayName("On possitive")]
        public string PosRefBlock
        {
            get { return PosRef.Name; }
        }

        [DisplayName("On negative")]
        public string NegRefBlock
        {
            get { return NegRef.Name; }
        }


        [Browsable(false)]
        public bool Condition
        {
            get
            {
                return _Condition;
            }

            set
            {
                _Condition = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Condition"));
            }
        }

        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;
    }
}
