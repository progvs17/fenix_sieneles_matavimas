﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.ComponentModel;


namespace Elvis.Fenix.FunctionBlocks
{
    public enum SynchMode
    {
        SingleShot,
        ContinuesHigh,
        ContinuesLow
    }

    public class FxDISynch : FxBlock, IDisposable
    {        
        private Fenix.Hardware.FxGpio _gpio;
        private int _Input;
        private SynchMode _SynchMode; 
        private bool _isChanged = true;
        private ManualResetEventSlim _exit = new ManualResetEventSlim(false);
        private bool _Disposed { get; set; }

        public FxDISynch(string name, Fenix.Hardware.FxGpio gpio)
        {
            Name = name;
            _gpio = gpio;
            Icon = Resource.IO_sync;
        }

        public override void Dispose()
        {
            Dispose(true);           
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_Disposed)
                return;

            if (disposing)
            {
                _exit.Set();
                Thread.Sleep(10);
            }
            _Disposed = true;
        }

        public override void Run()
        {
            try
            {
                Judgement = true;
                _exit.Reset();

                while (!_exit.IsSet)
                {
                    int val = 0;

                    if(_gpio != null)
                    {
                        val = _gpio.GetPin(0, Input);
                    }                    

                    if (SynchMode == SynchMode.SingleShot)
                    {
                        if (val > 0 && _isChanged)
                        {
                            _isChanged = false;
                            _exit.Set();
                            break;
                        }
                        else if (val == 0)
                        {
                            _isChanged = true;
                        }
                    }
                    else if (SynchMode == SynchMode.ContinuesHigh && val == 1)
                    {
                        _exit.Set();
                    }
                    else if (SynchMode == SynchMode.ContinuesLow && val == 0)
                    {
                        _exit.Set();
                    }
                }

                ElapsedTime = 0;
            }
            catch(Exception ex)
            {
                Judgement = false;
            }            
        }

        public override void Clear()
        {
           
        }

        public override void Exit()
        {
            _exit.Set();
        }

        public void SoftwareSynch()
        {
            _exit.Set();            
        }

        [DisplayName("Mode")]
        public SynchMode SynchMode
        {
            get
            {
                return _SynchMode;
            }

            set
            {
                _SynchMode = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SynchMode"));
            }
        }


        [DisplayName("Digital Input")]
        public int Input
        {
            get
            {
                return _Input;
            }

            set
            {
                _Input = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Input"));
            }
        }

        
    }
}
