﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elvis.Fenix.FunctionBlocks
{
    public class FxEnd : FxBlock
    {
        [Browsable(false), BlockNameAttribute]
        public override string Name { get; set; }

        public FxEnd() { }

        public FxEnd(string name)
        {
            Name = name;
            Icon = Resource.Default;
        }

        public override void Run()
        {
            Judge = Judgement = true;

            if (onTaskStarted != null)
                onTaskStarted(this);
 
            // Empty process      

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }

        public override void Clear()
        {
            // Empty process            
        }


        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;
    }
}
