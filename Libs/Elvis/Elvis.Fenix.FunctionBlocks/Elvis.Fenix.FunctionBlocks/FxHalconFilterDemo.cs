﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Elvis.Fenix.FunctionBlocks
{
    public class FxHalconFilterDemo : FxBlock
    {
        static Object _lock = new Object();

        private HImage inputImage;
        private int roiCircleDiameter;        
        private int roiTop;
        private int roiLeft;
        private double threshold;
        private double emty;
        private int roiStartX;
        private int roiStartY;
        private int lastDiam;
        private int sx, sy, dx, dy;
        private bool IsDiamBusy;
        private bool IsPosBusy;

        private bool IsMouseDown { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage InputImage
        {
            get
            {
                return inputImage;
            }
            set
            {
                lock (_lock)
                {
                    inputImage = value != null ? value.CopyImage() : null;
                }                
            }
        }


        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage OutputImage {get; set; }

        [XmlIgnoreAttribute]
        public override Bitmap Icon { set; get; }

        public override string Name { set; get; }

        public FxHalconFilterDemo()
        {      
        }

        public FxHalconFilterDemo(string name)
        {
            Name = name;
            Icon = Resource.Default;
            IsMarkAble = true;
            Marks = new List<MarkObject>();
            Text = new List<TextObject>();
        }
        
       public override void Run()
       {
           Judgement = true;
           DateTime startTime = DateTime.Now;

           if(Marks != null)
           {
               Marks.Clear();
               Text.Clear();
           }

           if (onTaskStarted != null)
           {
               onTaskStarted(this);
           }

           if(InputImage != null)
           {
               try
               {
                   HObject cont_;
                   HObject resize_mark_;
                   HalconDotNet.HOperatorSet.GenCircleContourXld(out cont_, ROITop, ROILeft, ROICircleDiameter, 0, 6.28318, "positive", 1);
                   HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out resize_mark_, ROITop, ROILeft + ROICircleDiameter, 0, 8, 8);

                   HTuple corr, homo, cont;
                   HalconDotNet.HRegion circle = new HRegion((double)ROITop, ROILeft, ROICircleDiameter);

                   InputImage.CoocFeatureImage(circle, 6, 0, out corr, out homo, out cont);

                   Judgement = cont.ToDArr()[0] >= Threshold || cont.ToDArr()[0] <= Emty ? false : true;
                   string color = !Judgement ? "red" : "green";

                   if (cont.ToDArr()[0] <= Emty) color = "yellow";

                   Marks.Add(new MarkObject { Name = "Contour", Color = color, Object = cont_ });
                   Marks.Add(new MarkObject { Name = "Contour", Color = "yellow", Object = resize_mark_ });
                   //Text.Add(new TextObject { Text = corr.ToDArr()[0].ToString("#.##"), Color = color, Font = "-Courier New-18-*-*-*-*-1-", Position = new Point(ROITop, ROILeft) });
                   //Text.Add(new TextObject { Text = homo.ToDArr()[0].ToString("#.##"), Color = color, Font = "-Courier New-18-*-*-*-*-1-", Position = new Point(ROITop + 20, ROILeft) });
                   Text.Add(new TextObject { Text = cont.ToDArr()[0].ToString("#.##"), Color = color, Font = "-Courier New-18-*-*-*-*-1-", Position = new Point(ROITop + ROICircleDiameter, ROILeft) });
                   Text.Add(new TextObject { Text = Name, Color = color, Font = "-Courier New-18-*-*-*-*-1-", Position = new Point(ROITop - ROICircleDiameter - 25, ROILeft - 40) });
               }
               catch(Exception ex)
               {
                   Judgement = false;
               }
               

           }

           ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

           if (onTaskCompleted != null)
           {
               onTaskCompleted(this);
           }
       }
       public override void Clear()
       {
          // if (OutputImage != null) OutputImage.Dispose();
          // if (InputImage != null) InputImage.Dispose();
       }

       public void HOnMouseDown(object sender, HMouseEventArgs e)
       {
           if(IsFocused)
           {
               sx = (int)e.X;
               sy = (int)e.Y;
               roiStartX = ROILeft;
               roiStartY = ROITop;
               lastDiam = ROICircleDiameter;
           }                     
       }

       public void HOnMouseUp(object sender, HMouseEventArgs e)
       {
           IsDiamBusy = false;
           IsPosBusy = false;
       }

       public void HOnMouseMove(object sender, HMouseEventArgs e)
       {
           if (IsFocused)
           {

               dx = (int)e.X - sx;
               dy = (int)e.Y - sy;               

               if(e.Button == System.Windows.Forms.MouseButtons.Left )
               {
                   if (!IsPosBusy)
                   {                       
                       double vr1 = (double)e.X - (double)ROILeft - (double)ROICircleDiameter;
                       double vr2 = (double)e.Y - (double)ROITop;
                       double vr3 = Math.Sqrt((vr1 * vr1) + (vr2 * vr2));

                       if (vr3 < 8 || IsDiamBusy)
                       {
                           ROICircleDiameter = lastDiam + dx;
                           IsDiamBusy = true;
                       }                       
                   }


                   if(!IsDiamBusy)
                   {                       
                       double v1 = (double)e.X - (double)ROILeft;
                       double v2 = (double)e.Y - (double)ROITop;
                       double v3 = Math.Sqrt((v1 * v1) + (v2 * v2));

                       if (v3 < ROICircleDiameter || IsPosBusy)
                       {
                           ROILeft = roiStartX + dx;
                           ROITop = roiStartY + dy;
                           IsPosBusy = true;
                       }                       
                   }                 
                  
               }               
           } 
       }

       public void HOnMouseWheel(object sender, HMouseEventArgs e)
       {
           if (IsFocused)
           {               
               if(e.Delta < 0)
               {
                   ROICircleDiameter--;
               }
               else if(e.Delta >0 )
               {
                   ROICircleDiameter++;
               }                         
           }
       }

       

       [Description("All images with heigher energy will be considered as negative.")]
       [DisplayName("Defect threshold")]
       public double Threshold
       {
           get
           {
               return threshold;
           }
           set
           {
               threshold = value;
               OnPropertyChanged(this, new PropertyChangedEventArgs("Threshold"));
           }
       }

        [Description("Images with lower energy will be considered as emty images.")]
       [DisplayName("No mask threshold")]
       public double Emty
       {
           get
           {
               return emty;
           }
           set
           {
               emty = value;
               OnPropertyChanged(this, new PropertyChangedEventArgs("Emty"));
           }
       }

       [Description("Region of Interest top position.")]
       [DisplayName("ROI top")]
       public int ROITop
       {
           get
           {
               return roiTop;
           }
           set
           {
               roiTop = value;
               OnPropertyChanged(this, new PropertyChangedEventArgs("ROITop"));
           }
       }

       [Description("Region of Interest left position.")]
       [DisplayName("ROI left")]
       public int ROILeft
       {
           get
           {
               return roiLeft;
           }
           set
           {
               roiLeft = value;
               OnPropertyChanged(this, new PropertyChangedEventArgs("ROILeft"));
           }
       }

       [Description("Circle's diameter of Region of Interest")]
       [DisplayName("ROI diameter")]
       public int ROICircleDiameter
       {
           get
           {
               return roiCircleDiameter;
           }
           set
           {
               roiCircleDiameter = value;
               OnPropertyChanged(this, new PropertyChangedEventArgs("ROICircleDiameter"));
           }
       }

       public override event TaskStartHandler onTaskStarted;
       public override event TaskCompleteHandler onTaskCompleted;
       public override event MessageHandler onMessage;
     }
}
