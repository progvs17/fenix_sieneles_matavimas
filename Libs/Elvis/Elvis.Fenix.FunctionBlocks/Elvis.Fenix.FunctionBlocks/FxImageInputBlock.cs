﻿using Elvis.Fenix.Hardware;
using System;
using System.Drawing;

namespace Elvis.Fenix.FunctionBlocks
{
    public abstract class FxImageInput : FxBlock
    {
        public virtual FxFrame OutputImage { get; set; }
    }
}
