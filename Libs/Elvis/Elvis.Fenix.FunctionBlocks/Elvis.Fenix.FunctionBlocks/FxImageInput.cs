﻿using Elvis.Fenix.Hardware;
using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Elvis.Fenix.IO;
using System.Xml.Serialization;

namespace Elvis.Fenix.FunctionBlocks
{
    public class FxImageInput : FxBlock
    {
        private ManualResetEventSlim _stop = new ManualResetEventSlim(false);
        private ManualResetEventSlim _exit = new ManualResetEventSlim(false);

        private FxCamera _camera;
        private GPIO _gpio;
        private FxImageLoader _loader;
        private FxFrame _frame;
        private Thread thread;

        private bool _isChanged = true;
        private int _Input;
        private SynchMode _SynchMode;
        private bool _Disposed { get; set; }
        private bool _singleShotRequest = false;

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public override Bitmap Icon { set; get; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage _hImage { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage HImage 
        {
            get 
            {
                return _hImage;
            }

            set
            {
                _hImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HImage"));
            }
        }

        public FxImageInput()
        {
        }

        [Browsable(false)]
        public FxImageInput(string name, FxCamera camera, GPIO gpio, FxImageLoader loader)
        {
            Name = name;
            Icon = Resource.Camera;
            _camera = camera;
            _loader = loader;
            _gpio = gpio;
        }

        public override void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_Disposed)
                return;

            if (disposing)
            {
                _exit.Set();
                Thread.Sleep(10);
            }
            _Disposed = true;
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public FxFrame OutputImage
        {
            get { return _frame; }
        }

        private void GrabAction()
        {
            if (_loader.IsEnabled)
            {
                HImage = _loader.CurrentImage != null ? _loader.CurrentImage.CopyImage() : null;

                if (HImage == null)
                {
                    Judgement = false;
                }
            }
            else
            {
                _frame = null;
                _camera.Connect();
                _camera.StartGrab();
                _frame = _camera.GrabFrame() as FxFrame;

                if (_frame != null && _frame.Data != null)
                {
                    HImage img = new HImage();
                    img.GenImage1Extern("byte", _frame.Width, _frame.Height, _frame.Data, IntPtr.Zero);
                    HImage = img;
                }
                else
                {
                    Judgement = false;
                }
            }
        }

        public override void Run()
        {
            Judgement = true;
            //DateTime startTime = DateTime.Now;

            if(onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            try
            {
                _exit.Reset();

                while (!_exit.IsSet)
                {
                    int val = 0;
                    if (_gpio != null)
                    {
                        val = _gpio.GetPin(0, Input);
                    }

                    if(_singleShotRequest)
                    {
                        _singleShotRequest = false;
                        GrabAction();
                        _exit.Set();
                        break;
                    }


                    if (SynchMode == SynchMode.SingleShot)
                    {
                        if (val > 0 && _isChanged)
                        {
                            _isChanged = false;
                            GrabAction();
                            _exit.Set();
                        }
                        else if (val == 0)
                        {
                            _isChanged = true;
                        }                   

                    }
                    else if (SynchMode == SynchMode.ContinuesHigh && val == 1)
                    {
                        GrabAction();
                        _exit.Set();
                    }
                    else if (SynchMode == SynchMode.ContinuesLow && val == 0)
                    {
                        GrabAction();
                        _exit.Set();
                    }
                    else if (_singleShotRequest)
                    {
                        _singleShotRequest = false;
                        GrabAction();
                        _exit.Set();
                    }

                    ElapsedTime = 0;// (DateTime.Now - startTime).TotalMilliseconds;

                    if (onTaskCompleted != null)
                    {
                        onTaskCompleted(this);
                    }
                }
            }
            catch (Exception ex)
            {
                HImage = null;
                Judgement = false;

                if (onMessage != null)
                {
                    onMessage(this, "Error onFxImageInput block.");
                }
            }                    
        }

        public override void Clear()
        {
            //_camera.ReleaseFrame(_frame);
            //if (HImage != null) HImage.Dispose();
            //HImage = null;
        }

        public override void Stop()
        {
            _exit.Set();
            //_camera.StopGrab();
            //_camera.Disconnect();
        }

        public void SoftwareSynch()
        {
            _singleShotRequest = true;
        }
        
        [Browsable(false)]
        public int Width
        {
            get
            {
                return _camera != null && _camera.IsConnected ? _camera.Width : 0;
            }
            set
            {
                if (_camera != null && _camera.IsConnected)
                {
                    bool restart = _camera.IsGrabbing;
                    if (restart)
                    {
                        _camera.StopGrab();
                        _camera.Width = value;
                        _camera.StartGrab();
                    }
                    else
                    {
                        _camera.Width = value;
                    }
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("Width"));
            }
        }

        [Browsable(false)]
        public int Height
        {
            get
            {
                return _camera != null && _camera.IsConnected ? _camera.Height : 0;
            }
            set
            {
                if (_camera != null && _camera.IsConnected)
                {
                    bool restart = _camera.IsGrabbing;
                    if(restart)
                    {
                        _camera.StopGrab();
                        _camera.Height = value;
                        _camera.StartGrab();
                    }
                    else
                    {
                        _camera.Height = value;
                    } 
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("Height"));
            }
        }


        public int Exposure { 
            get
            {
                return _camera != null && _camera.IsConnected ? _camera.Exposure : 0;
            }
            set
            {
                if (_camera != null && _camera.IsConnected)
                {
                    _camera.Exposure = value;
                }                
                OnPropertyChanged(this, new PropertyChangedEventArgs("Exposure"));
            }
        }

        [DisplayName("Mode")]
        public SynchMode SynchMode
        {
            get
            {
                return _SynchMode;
            }

            set
            {
                _SynchMode = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SynchMode"));
            }
        }

        [DisplayName("Digital Input")]
        public int Input
        {
            get
            {
                return _Input;
            }

            set
            {
                _Input = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Input"));
            }
        }

        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;

    }
}
