﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elvis.Fenix.FunctionBlocks
{
    public class FxHPlasticHolesBlots : FxBlock
    {
        List<HImage> list = new List<HImage>();

        private HImage inputImage;
        private double maxSizeBlots = 15;
        private double maxSizeHoles = 80;
        private double minSizeBlots = 2;
        private double minSizeHoles = 5;

        double holesNumber;
        double blotsNumber;
        double defectsQuantity;


        [Browsable(false)]
        public HImage InputImage
        {
            get
            {
                return inputImage;
            }
            set
            {
                inputImage = value != null ? value.CopyImage() : null;
            }
        }

        [Browsable(false)]
        public HImage _hImage { get; set; }

        [Browsable(false)]
        public HImage HImage
        {
            get
            {
                return _hImage;
            }
            set
            {
                _hImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HImage"));
            }
        }

        [Browsable(false)]
        public List<HImage> InputImages { get; set; }

        [Browsable(false)]
        public List<HImage> OutputImages { get; set; }

        [Browsable(false)]
        public HImage OutputImage { get; set; }

        public override Bitmap Icon { set; get; }
        public override string Name { set; get; }

        public FxHPlasticHolesBlots(string name)
        {
            Name = name;
            Icon = Resource.Default;
            IsMarkAble = true;
            Marks = new List<MarkObject>();
            Text = new List<TextObject>();
        }

        public override void Run()
        {
            Judgement = true;
            DateTime startTime = DateTime.Now;

            if (Marks != null)
            {
                Marks.Clear();
                Text.Clear();
            }

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            //if (InputImages != null)//Image List
            if (InputImage != null)
            {
                //OutputImages = new List<HImage>();
                try
                {
                    //foreach (HImage image in InputImages)
                    //{
                    HRegion region = new HRegion();
                    HRegion holesCont;
                    HRegion blotsCont;
                    HRegion region2;
                    HRegion rectRegion = new HRegion();
                    HImage rectImage;
                    HImage imgReduce;
                    HImage imgMean;
                    HXLDCont edges;
                    HRegion holesRegion;

                    if (maxSizeBlots <= minSizeBlots) maxSizeBlots = minSizeBlots + 1;
                    if (maxSizeHoles <= minSizeHoles) maxSizeHoles = minSizeHoles + 1;

                    region.GenRectangle1((double)0, 0, 370, 650);
                    rectImage = InputImage.ReduceDomain(region);

                    region = InputImage.Threshold((double)40, 190);
                    region = region.Connection();
                    region = region.FillUp();
                    region = region.SelectShape("area", "and", (double)35000, 100000);
                    imgReduce = rectImage.ReduceDomain(region);

                    imgMean = imgReduce.MeanImage((int)3, 3);
                    
                    region2 = imgMean.Threshold((double)160, 220);
                    region2 = region2.FillUp();
                    region2 = region2.Connection();
                    region2 = region2.SelectShape("area", "and", (double)150, 1000);
                    region2 = region2.DilationCircle(3.5);
                    region2 = region.Difference(region2);
                    imgMean = imgMean.ReduceDomain(region2);


                    edges = imgMean.EdgesSubPix("canny", (double)1, 10, 30);
                    edges = edges.SelectShapeXld("area", "and", minSizeHoles, maxSizeHoles);
                    edges = edges.SelectShapeXld("circularity", "and", 0.3, 1);
                    holesRegion = edges.GenRegionContourXld("filled");
                    holesCont = holesRegion.Boundary("outer");

                    region = imgMean.Threshold((double)85, 220);
                    region = region.Connection();
                    region = region.SelectShape("area", "and", minSizeBlots, maxSizeBlots);
                    blotsCont = region.Boundary("inner");

                    try
                    {
                        //Counted the number of holes.
                        holesRegion = holesRegion.Connection();
                        holesRegion = holesRegion.SelectShape("area", "and", (double)0, 80);
                        holesNumber = holesRegion.CountObj();
                        
                        //Counted the number of blots.
                        region = region.Connection();
                        region = region.SelectShape("area", "and", (double)0, 80);
                        blotsNumber = region.CountObj();
                        defectsQuantity = holesNumber + blotsNumber;
                    }
                    catch
                    {
                        defectsQuantity = 0;
                    }

                    Judgement = defectsQuantity > 0 || !Judgement ? false : true;
                    string color = !Judgement ? "red" : "green";

                    Marks.Add(new MarkObject { Name = "PlasticHoles", Color = "red", Object = holesCont });
                    Marks.Add(new MarkObject { Name = "Blots", Color = "red", Object = blotsCont });
                     
                    Text.Add(new TextObject { Text = "Blots:" + blotsNumber.ToString(), Color = color, Font = "-Courier New-18-*-*-*-*-1-", Position = new Point((int)10, (int)50) });
                    Text.Add(new TextObject { Text = "Holes:" + holesNumber.ToString(), Color = color, Font = "-Courier New-18-*-*-*-*-1-", Position = new Point((int)30, (int)50) });

                }
                catch (Exception ex)
                {
                    Judgement = false;
                }
            }

            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }

        public override void Clear()
        {
            if (OutputImage != null) OutputImage.Dispose();
            if (InputImage != null) InputImage.Dispose();
        }


        [Description("All images with smaller blots will be considered as negative.")]
        [DisplayName("Maximum blot size")]
        public double MaxSizeBlots
        {
            get
            {
                return maxSizeBlots;
            }
            set
            {
                maxSizeBlots = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxSizeBlots"));
            }
        }

        [Description("All images with smaller holes will be considered as negative.")]
        [DisplayName("Maximum hole size")]
        public double MaxSizeHoles
        {
            get
            {
                return maxSizeHoles;
            }
            set
            {
                maxSizeHoles = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxSizeHoles"));
            }
        }

        [Description("All images with bigger blots will be considered as negative.")]
        [DisplayName("Minimum blot size")]
        public double MinSizeBlots
        {
            get
            {
                return minSizeBlots;
            }
            set
            {
                minSizeBlots = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinSizeBlots"));
            }
        }

        [Description("All images with bigger holes will be considered as negative.")]
        [DisplayName("Minimum hole size")]
        public double MinSizeHoles
        {
            get
            {
                return minSizeHoles;
            }
            set
            {
                minSizeHoles = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinSizeHoles"));
            }
        }

        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessgage;
    }
}