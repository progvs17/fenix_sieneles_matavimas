﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace Elvis.Fenix.FunctionBlocks
{
    public class ContourFragmentationSettings
    {
        #region Constructors

        public ContourFragmentationSettings()
        {
            AngleStart = 0;
            AngleExtent = 0.1;
            Greediness = 0.9;
            NumLevels = 0;
            DefectThreshold = 30;
            ABSThreshold = 30;
            ScaleRMin = 1;
            ScaleRMax = 1;
            ScaleCMin = 1;
            ScaleCMax = 1;
            MinScore = 0.7;
            MaxOverlap = 0.3;
        }

        #endregion


        #region Public properties

        public double AngleStart
        { get; set; }

        public double AngleExtent
        { get; set; }

        public double NumLevels
        { get; set; }

        public double Greediness
        { get; set; }

        public double DefectThreshold
        { get; set; }

        public double ABSThreshold
        { get; set; }
        
        public double ScaleRMin
        { get; set; }

        public double ScaleRMax
        { get; set; }

        public double ScaleCMin
        { get; set; }

        public double ScaleCMax
        { get; set; }

        public double MinScore
        { get; set; }

        public double MaxOverlap
        { get; set; }

        public string DeformableModelID
        { get; set; }

        public string VariationModelID
        { get; set; }

        #endregion
    }

    public class FxHContourFragmentation : FxBlock
    {
        #region Private Members

        List<HImage> list = new List<HImage>();

        private HImage inputImage;

        HTuple area;
        HTuple ra;
        HTuple row2, column2;
        HTuple rb, phi;
        HTuple smoothness = 25;

        List<ContourFragmentationSettings> _contourFragmentationSettings;
        int _operationIndex = 0;

        FxRotateAndGrabImage _imageGrabBlock;

        #endregion


        #region Constructors

        public FxHContourFragmentation() { }

        public FxHContourFragmentation(string name, FxRotateAndGrabImage imageGrabBlock)
        {
            Name = name;
            Icon = Resource.Default;
            IsMarkAble = true;
            Marks = new List<MarkObject>();
            Text = new List<TextObject>();

            BlockSettings = new List<ContourFragmentationSettings>();

            _imageGrabBlock = imageGrabBlock;
        }

        #endregion


        #region Public Methods

        public override void Run()
        {
            Judgement = true;
            DateTime startTime = DateTime.Now;

            if (Marks != null)
            {
                Marks.Clear();
                Text.Clear();
            }

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            if (_imageGrabBlock.BlockOperationMode == FxRotateAndGrabImage.OperationMode.SingleImageOperation)
            {
                if (_imageGrabBlock.HImage != null)
                {
                    DrawRectangles();

                    if (!updateRegion)
                    {
                        ROITopRow = BlockOperations[_imageGrabBlock.OperationIndex].TopRow;
                        ROIBotRow = BlockOperations[_imageGrabBlock.OperationIndex].BottomRow;
                        ROITopCol = BlockOperations[_imageGrabBlock.OperationIndex].TopCollumn;
                        ROIBotCol = BlockOperations[_imageGrabBlock.OperationIndex].BottomCollumn;
                    }
                    UpdateSettings(_imageGrabBlock.OperationIndex);
                    FilterImage(BlockOperations[_imageGrabBlock.OperationIndex].Image, _imageGrabBlock.OperationIndex);
                }
                else
                {
                    _imageGrabBlock.Run();
                }
            }
            else
            {
                if (_imageGrabBlock.BlockOperations != null)
                {
                    int oper = 0;

                    foreach (var operation in _imageGrabBlock.BlockOperations)
                    {
                        if (operation.Image != null)
                        {
                            ROITopRow = operation.TopRow;
                            ROIBotRow = operation.BottomRow;
                            ROITopCol = operation.TopCollumn;
                            ROIBotCol = operation.BottomCollumn;

                            UpdateSettings(oper);
                            FilterImage(operation.Image, oper);
                            oper++;
                        }
                    }
                }
            }

            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }

        public override void Clear()
        {
            //if (OutputImage != null) OutputImage.Dispose();
            if (InputImage != null) InputImage.Dispose();
        }

        #endregion


        #region Private methods

        void FilterImage(HImage image, int operationIndex)
        {
            try
            {
                HRegion rect_ = new HRegion();
                HRegion region = new HRegion();
                HRegion connectedRegions = new HRegion();
                HRegion selectedRegions = new HRegion();
                HObject cont_ = new HObject();
                HXLDCont contEllipse = new HXLDCont();
                HXLDCont deformedContours;
                HImage imageRectified;
                HImage imgReduce;
                HImage vectorField;
                HTuple pointOrder = new HTuple();
                HVariationModel variationModelID = new HVariationModel();
                HDeformableModel modelID = new HDeformableModel();
                
                HTuple score;
                HTuple row1, column1;
                HTuple ones = new HTuple();

                int number;
                string color = !Judgement ? "red" : "green";

                HImage = image;
                
                rect_.GenRectangle1(ROITopRow, ROITopCol, ROIBotRow, ROIBotCol);
                imgReduce = image.ReduceDomain(rect_);

                HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow,
                    (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);

                Marks.Add(new MarkObject { Name = "Image", Color = "red", Object = imgReduce });
                //Process images
                Judgement = true;

                try
                {
                    variationModelID.ReadVariationModel(BlockSettings[_operationIndex].VariationModelID);
                    modelID.ReadDeformableModel(BlockSettings[_operationIndex].DeformableModelID);
                }
                catch { }

                imageRectified = imgReduce.FindLocalDeformableModel(
                    out vectorField,    //VectorField - Vector field of the rectification transformation.
                    out deformedContours,   //DeformedContours - Contours of the found instances of the model.
                    modelID,  //ModelID - Handle of the model.
                    (BlockSettings[_operationIndex].AngleStart * Math.PI) / 180,    //AngleStart - Smallest rotation of the model.
                    (BlockSettings[_operationIndex].AngleExtent * Math.PI) / 180,   //AngleExtent - Extent of the rotation angles.
                    BlockSettings[_operationIndex].ScaleRMin,  //ScaleRMin - Minimum scale of the model in row direction.
                    BlockSettings[_operationIndex].ScaleRMax,  //ScaleRMax - Maximum scale of the model in row direction.
                    BlockSettings[_operationIndex].ScaleCMin,  //ScaleCMin - Minimum scale of the model in column direction.
                    BlockSettings[_operationIndex].ScaleCMax,  //ScaleCMax - Maximum scale of the model in column direction.
                    BlockSettings[_operationIndex].MinScore,    //MinScore - Minumum score of the instances of the model to be found.
                    1,  //NumMatches - Number of instances of the model to be found (or 0 for all matches).
                    BlockSettings[_operationIndex].MaxOverlap,    //MaxOverlap - Maximum overlap of the instances of the model to be found.
                    BlockSettings[_operationIndex].NumLevels,  //NumLevels - Number of pyramid levels used in the matching.
                    BlockSettings[_operationIndex].Greediness, //Greediness - “Greediness” of the search heuristic (0: safe but slow; 1: fast but matches may be missed).
                    ((new HTuple("image_rectified")).TupleConcat("vector_field")).TupleConcat("deformed_contours"), //ResultType - Switch for requested iconic result.
                    ((new HTuple("deformation_smoothness")).TupleConcat("expand_border")).TupleConcat("subpixel"),  //ParamName - The general parameter names.
                    smoothness.TupleConcat((new HTuple(0)).TupleConcat(1)), //ParamValue - Values of the general parameters.
                    out score,  //Score - Scores of the found instances of the model.
                    out row1,   //Row - Row coordinates of the found instances of the model.
                    out column1);   //Column - Column coordinates of the found instances of the model.
                region = imageRectified.CompareVariationModel(variationModelID);

                if (score > 0)
                {
                    connectedRegions = region.Connection();
                    selectedRegions = connectedRegions.SelectShape("area", "and", BlockSettings[_operationIndex].DefectThreshold, 99999);

                    number = selectedRegions.CountObj();

                    if (number > 0)
                    {
                        area = selectedRegions.AreaCenter(out row2, out column2);
                        ra = selectedRegions.EllipticAxis(out rb, out phi);
                        HalconDotNet.HOperatorSet.TupleGenConst(number, 1, out ones);

                        for (int Idx = 0; Idx < number; Idx++)
                        {
                            pointOrder = pointOrder.TupleConcat("positive");
                        }

                        Judgement = false;
                        score = 0;
                    }
                }

                color = !Judgement ? "red" : "green";
                Marks.Add(new MarkObject { Name = "Contour", Color = color, Object = cont_ });

                HImage = image.Clone();

                if (!Judgement)
                {
                    contEllipse.GenEllipseContourXld(row2 + ROITopRow, column2 + ROITopCol, phi, ra + 10, rb + 10, 0 * ones, 6.28318 * ones, pointOrder, 1.5);
                    Marks.Add(new MarkObject { Name = "ShowEdge", Color = "red", Object = contEllipse });
                }

                if (operationIndex >= 0)
                {
                    Text.Add(new TextObject { Text = "Operation: #" + operationIndex, Color = color, Font = "-Courier New-14-*-*-*-*-1-", Position = new Point((int)ROITopRow - 20, (int)ROITopCol + 10) });
                }
            }

            catch (Exception ex)
            {
                Judgement = false;
            }
        }

        public void UpdateSettings(int index)
        {
            if (BlockSettings != null && BlockSettings.Count > 0 && BlockSettings.Count > index)
            {
                AbsThreshold = BlockSettings[_operationIndex].ABSThreshold;
                MinDefectThreshold = BlockSettings[_operationIndex].DefectThreshold;
                SmallAngleStart = BlockSettings[_operationIndex].AngleStart;
                RotationAngleExtent = BlockSettings[_operationIndex].AngleExtent;
                GreedinessSize = BlockSettings[_operationIndex].Greediness;
                NumberLevels = BlockSettings[_operationIndex].NumLevels;
                ScaleRMinimum = BlockSettings[_operationIndex].ScaleRMin;
                ScaleRMaximum = BlockSettings[_operationIndex].ScaleRMax;
                ScaleCMinimum = BlockSettings[_operationIndex].ScaleCMin;
                ScaleCMaximum = BlockSettings[_operationIndex].ScaleCMax;
                MinimumScore = BlockSettings[_operationIndex].MinScore;
                MaximumOverlap = BlockSettings[_operationIndex].MaxOverlap;
                VarModelID = BlockSettings[_operationIndex].VariationModelID;
                DefModelID = BlockSettings[_operationIndex].DeformableModelID;
            }
        }

        #endregion


        #region UI mouse clicks interaction

        private double roiTopCol = 200;
        private double roiTopRow = 150;
        private double roiBotCol = 400;
        private double roiBotRow = 315;
        private double roiStartRow = 200;
        private double roiStartCol = 150;
        private double roiEndRow = 315;
        private double roiEndCol = 400;
        private double sx, sy, dx, dy;
        private bool IsDiamBusy;
        private bool updateRegion = false;

        private void DrawRectangles()
        {
            HRegion rect_ = new HRegion();
            HRegion region = new HRegion();
            HObject cont_;

            HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow, (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);

            Marks.Add(new MarkObject { Name = "Contour", Color = "blue", Object = cont_ });
        }


        public void UpdateROI()
        {
            if (updateRegion)
            {
                BlockOperations[_imageGrabBlock.OperationIndex].TopRow = ROITopRow;
                BlockOperations[_imageGrabBlock.OperationIndex].BottomRow = ROIBotRow;
                BlockOperations[_imageGrabBlock.OperationIndex].TopCollumn = ROITopCol;
                BlockOperations[_imageGrabBlock.OperationIndex].BottomCollumn = ROIBotCol;
            }
        }

        public void HOnMouseDown(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                sx = (int)e.X;
                sy = (int)e.Y;

                if (sx > (ROITopCol - 15) && sx < (ROITopCol + 15) && sy > (ROITopRow - 15) && sy < (ROITopRow + 15))
                {
                    roiStartRow = ROITopRow;
                    roiStartCol = ROITopCol;
                    IsDiamBusy = false;
                }
                else
                {
                    roiEndRow = ROIBotRow;
                    roiEndCol = ROIBotCol;
                    IsDiamBusy = true;
                }

                updateRegion = true;
            }
        }

        public void HOnMouseUp(object sender, HMouseEventArgs e)
        {
            UpdateROI();
            updateRegion = false;

        }

        public void HOnMouseMove(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {

                dx = (int)e.X - sx;
                dy = (int)e.Y - sy;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    if (!IsDiamBusy)
                    {
                        ROITopCol = roiStartCol + dx;
                        ROITopRow = roiStartRow + dy;
                    }
                    else
                    {
                        ROIBotCol = roiEndCol + dx;
                        ROIBotRow = roiEndRow + dy;
                    }

                }
            }
        }

        public void HOnMouseWheel(object sender, HMouseEventArgs e)
        {

        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top row position.")]
        [DisplayName("ROI top row")]
        public double ROITopRow
        {
            get
            {
                return roiTopRow;
            }
            set
            {
                roiTopRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot row position.")]
        [DisplayName("ROI bot row")]
        public double ROIBotRow
        {
            get
            {
                return roiBotRow;
            }
            set
            {
                roiBotRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top col position.")]
        [DisplayName("ROI top col")]
        public double ROITopCol
        {
            get
            {
                return roiTopCol;
            }
            set
            {
                roiTopCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopCol"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot col position.")]
        [DisplayName("ROI bot col")]
        public double ROIBotCol
        {
            get
            {
                return roiBotCol;
            }
            set
            {
                roiBotCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotCol"));
            }
        }

        #endregion


        #region Public properties

        //[Browsable(false)]
        [XmlIgnoreAttribute]
        public List<OperationList> BlockOperations
        {
            get
            {
                return _imageGrabBlock.BlockOperations;
            }
            set
            {
                _imageGrabBlock.BlockOperations = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage InputImage
        {
            get
            {
                return inputImage;
            }
            set
            {
                inputImage = value != null ? value.CopyImage() : null;
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage _hImage { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage HImage
        {
            get
            {
                return _hImage;
            }
            set
            {
                _hImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HImage"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<HImage> InputImages { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<HImage> OutputImages { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage OutputImage { get; set; }

        [XmlIgnoreAttribute]
        [Browsable(false)]
        public override Bitmap Icon { set; get; }

        [Browsable(false)]
        public override string Name { set; get; }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Calibration")]
        [Description("This parameter determines the minimum amount of gray levels by which the image of the current object must differ from the image of the ideal object.")]
        [DisplayName("ABS threshold")]
        public double AbsThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[_operationIndex].ABSThreshold;
                }
                catch
                {
                    return -1;

                }
            }
            set
            {
                try
                {
                    BlockSettings[_operationIndex].ABSThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    ContourFragmentationSettings newItem = new ContourFragmentationSettings();
                    BlockSettings.Add(newItem);
                    AbsThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("AbsThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Calibration")]
        [Description("The minimum value of the threshold of the defect.")]
        [DisplayName("Defect threshold")]
        public double MinDefectThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[_operationIndex].DefectThreshold;
                }
                catch
                {
                    return -1;

                }
            }
            set
            {
                try
                {
                    BlockSettings[_operationIndex].DefectThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    ContourFragmentationSettings newItem = new ContourFragmentationSettings();
                    BlockSettings.Add(newItem);
                    MinDefectThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinDefectThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Calibration")]
        [Description("Smallest rotation of the model.")]
        [DisplayName("Angle start")]
        public double SmallAngleStart
        {
            get
            {
                try
                {
                    return BlockSettings[_operationIndex].AngleStart;
                }
                catch
                {
                    return -1;

                }
            }
            set
            {
                try
                {
                    BlockSettings[_operationIndex].AngleStart = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    ContourFragmentationSettings newItem = new ContourFragmentationSettings();
                    BlockSettings.Add(newItem);
                    SmallAngleStart = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("SmallAngleStart"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Calibration")]
        [Description("Extent of the rotation angles.")]
        [DisplayName("Angle extent")]
        public double RotationAngleExtent
        {
            get
            {
                try
                {
                    return BlockSettings[_operationIndex].AngleExtent;
                }
                catch
                {
                    return -1;

                }
            }
            set
            {
                try
                {
                    BlockSettings[_operationIndex].AngleExtent = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    ContourFragmentationSettings newItem = new ContourFragmentationSettings();
                    BlockSettings.Add(newItem);
                    RotationAngleExtent = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("RotationAngleExtent"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Calibration")]
        [Description("Greediness of the search heuristic.")]
        [DisplayName("Greediness")]
        public double GreedinessSize
        {
            get
            {
                try
                {
                    return BlockSettings[_operationIndex].Greediness;
                }
                catch
                {
                    return -1;

                }
            }
            set
            {
                try
                {
                    BlockSettings[_operationIndex].Greediness = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    ContourFragmentationSettings newItem = new ContourFragmentationSettings();
                    BlockSettings.Add(newItem);
                    GreedinessSize = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("GreedinessSize"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Calibration")]
        [Description("Number of pyramid levels used in the matching.")]
        [DisplayName("NumLevels")]
        public double NumberLevels
        {
            get
            {
                try
                {
                    return BlockSettings[_operationIndex].NumLevels;
                }
                catch
                {
                    return -1;

                }
            }
            set
            {
                try
                {
                    BlockSettings[_operationIndex].NumLevels = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    ContourFragmentationSettings newItem = new ContourFragmentationSettings();
                    BlockSettings.Add(newItem);
                    NumberLevels = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("NumberLevels"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Calibration")]
        [Description("Minimum scale of the model in row direction.")]
        [DisplayName("ScaleRMin")]
        public double ScaleRMinimum
        {
            get
            {
                try
                {
                    return BlockSettings[_operationIndex].ScaleRMin;
                }
                catch
                {
                    return -1;

                }
            }
            set
            {
                try
                {
                    BlockSettings[_operationIndex].ScaleRMin = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    ContourFragmentationSettings newItem = new ContourFragmentationSettings();
                    BlockSettings.Add(newItem);
                    ScaleRMinimum = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("ScaleRMinimum"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Calibration")]
        [Description("Maximum scale of the model in row direction.")]
        [DisplayName("ScaleRMax")]
        public double ScaleRMaximum
        {
            get
            {
                try
                {
                    return BlockSettings[_operationIndex].ScaleRMax;
                }
                catch
                {
                    return -1;

                }
            }
            set
            {
                try
                {
                    BlockSettings[_operationIndex].ScaleRMax = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    ContourFragmentationSettings newItem = new ContourFragmentationSettings();
                    BlockSettings.Add(newItem);
                    ScaleRMaximum = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("ScaleRMaximum"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Calibration")]
        [Description("Minimum scale of the model in column direction.")]
        [DisplayName("ScaleCMin")]
        public double ScaleCMinimum
        {
            get
            {
                try
                {
                    return BlockSettings[_operationIndex].ScaleCMin;
                }
                catch
                {
                    return -1;

                }
            }
            set
            {
                try
                {
                    BlockSettings[_operationIndex].ScaleCMin = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    ContourFragmentationSettings newItem = new ContourFragmentationSettings();
                    BlockSettings.Add(newItem);
                    ScaleCMinimum = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("ScaleCMinimum"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Calibration")]
        [Description("Maximum scale of the model in column direction.")]
        [DisplayName("ScaleCMax")]
        public double ScaleCMaximum
        {
            get
            {
                try
                {
                    return BlockSettings[_operationIndex].ScaleCMax;
                }
                catch
                {
                    return -1;

                }
            }
            set
            {
                try
                {
                    BlockSettings[_operationIndex].ScaleCMax = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    ContourFragmentationSettings newItem = new ContourFragmentationSettings();
                    BlockSettings.Add(newItem);
                    ScaleCMaximum = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("ScaleCMaximum"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Calibration")]
        [Description("Minimum score of the instances of the model to be found.")]
        [DisplayName("MinScore")]
        public double MinimumScore
        {
            get
            {
                try
                {
                    return BlockSettings[_operationIndex].MinScore;
                }
                catch
                {
                    return -1;

                }
            }
            set
            {
                try
                {
                    BlockSettings[_operationIndex].MinScore = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    ContourFragmentationSettings newItem = new ContourFragmentationSettings();
                    BlockSettings.Add(newItem);
                    MinimumScore = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinimumScore"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Calibration")]
        [Description("Maximum overlap of the instances of the model to be found.")]
        [DisplayName("MaxOverlap")]
        public double MaximumOverlap
        {
            get
            {
                try
                {
                    return BlockSettings[_operationIndex].MaxOverlap;
                }
                catch
                {
                    return -1;

                }
            }
            set
            {
                try
                {
                    BlockSettings[_operationIndex].MaxOverlap = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    ContourFragmentationSettings newItem = new ContourFragmentationSettings();
                    BlockSettings.Add(newItem);
                    MaximumOverlap = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaximumOverlap"));
            }
        }

        //[Browsable(false)]
        [CategoryAttribute("Calibration")]
        [Description("Region uneven settigs")]
        public List<ContourFragmentationSettings> BlockSettings
        {
            get
            {
                return _contourFragmentationSettings;
            }
            set
            {
                _contourFragmentationSettings = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockSettings"));
            }
        }

        [XmlIgnoreAttribute]
        [Browsable(false)]
        [CategoryAttribute("Calibration")]
        [Description("")]
        public int OperationIndex
        {
            get
            {
                return _operationIndex;
            }
            set
            {
                _operationIndex = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("OperationIndex"));
            }
        }

        [XmlIgnoreAttribute]
        [Browsable(false)]
        public string DefModelID
        {
            get
            {
                try
                {
                    return BlockSettings[_operationIndex].DeformableModelID;
                }
                catch
                {
                    return "null";

                }
            }
            set
            {
                try
                {
                    BlockSettings[_operationIndex].DeformableModelID = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    ContourFragmentationSettings newItem = new ContourFragmentationSettings();
                    BlockSettings.Add(newItem);
                    DefModelID = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("DefModelID"));
            }
        }

        [XmlIgnoreAttribute]
        [Browsable(false)]
        public string VarModelID
        {
            get
            {
                try
                {
                    return BlockSettings[_operationIndex].VariationModelID;
                }
                catch
                {
                    return "null";

                }
            }
            set
            {
                try
                {
                    BlockSettings[_operationIndex].VariationModelID = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    ContourFragmentationSettings newItem = new ContourFragmentationSettings();
                    BlockSettings.Add(newItem);
                    VarModelID = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("VarModelID"));
            }
        }

        #endregion


        #region Buttons

        [ButtonControl]
        public void NextImage()
        {
            if (_imageGrabBlock.BlockOperationMode == FxRotateAndGrabImage.OperationMode.SingleImageOperation)
            {
                _imageGrabBlock.NextOperation();

                if (BlockOperations[_imageGrabBlock.OperationIndex].Image == null)
                {
                    _imageGrabBlock.Run();
                }
                _operationIndex = _operationIndex < BlockOperations.Count - 1 ? _operationIndex + 1 : _operationIndex;
            }
        }

        [ButtonControl]
        public void PreviousImage()
        {
            if (_imageGrabBlock.BlockOperationMode == FxRotateAndGrabImage.OperationMode.SingleImageOperation)
            {
                _imageGrabBlock.PreviousOperation();

                if (BlockOperations[_imageGrabBlock.OperationIndex].Image == null)
                {
                    _imageGrabBlock.Run();
                }
                _operationIndex = _operationIndex > 0 ? _operationIndex - 1 : _operationIndex;
            }
        }

        [ButtonControl]
        public void FilterTraining()
        {
            using (System.Windows.Forms.OpenFileDialog selectImage = new System.Windows.Forms.OpenFileDialog())
            {

                if (selectImage.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    HImage image = new HImage();
                    image.ReadImage(selectImage.FileName);

                    HRegion ROI = new HRegion();
                    HImage modelImage = new HImage();
                    HImage edgeAmplitude = new HImage();
                 
                    HVariationModel variationModelID = new HVariationModel();
                    HDeformableModel modelID = new HDeformableModel();
                    int width;
                    int height;
                    int size = 5;
                    string varModelDirectory = "C:/Fenix/Mask Configs/Size 1/Fragmentation filter variation models";
                    string defModelDirectory = "C:/Fenix/Mask Configs/Size 1/Fragmentation filter deformable models";

                    try
                    {
                        if (!Directory.Exists(varModelDirectory))
                            Directory.CreateDirectory(varModelDirectory);

                        if (!Directory.Exists(defModelDirectory))
                            Directory.CreateDirectory(defModelDirectory);

                        modelImage = image.CropPart((int)ROITopRow, (int)ROITopCol, ((int)ROIBotCol - (int)ROITopCol), ((int)ROIBotRow - (int)ROITopRow));
                        modelImage.GetImageSize(out width, out height);

                        //Create variation model
                        edgeAmplitude = modelImage.SobelAmp("thin_max_abs", size);
                        variationModelID.CreateVariationModel(width, height, "byte", "direct");
                        variationModelID.PrepareDirectVariationModel(modelImage, edgeAmplitude, BlockSettings[_operationIndex].ABSThreshold, 1.5);
                        variationModelID.WriteVariationModel(BlockSettings[_operationIndex].VariationModelID =
                            String.Format("C:/Fenix/Mask Configs/Size 1/Fragmentation filter variation models/variationModelID_{0}.var", _operationIndex));

                        //Create locally deformable model
                        modelID = modelImage.CreateLocalDeformableModel("auto", (new HTuple(-10)).TupleRad(), (new HTuple(10)).TupleRad(),
                            "auto", 0.9, 1.1, "auto", 0.9, 1.1, "auto", "none", "use_polarity", "auto", "auto", new HTuple(), new HTuple());
                        modelID.WriteDeformableModel(BlockSettings[_operationIndex].DeformableModelID =
                            String.Format("C:/Fenix/Mask Configs/Size 1/Fragmentation filter deformable models/modelID_{0}.dfm", _operationIndex));
                    }
                    catch { }
                }
            }
        }

        #endregion


        #region Events

        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessgage;

        #endregion
    }
}