﻿using Elvis.Fenix.Hardware;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Elvis.Fenix.FunctionBlocks
{
    public enum InputCondition
    {
        Low,
        Heigh
    }

    public class FxDIBranch : FxBlock
    {
        private bool _Disposed;
        private FxGpio _gpio;
        private ManualResetEventSlim _exit = new ManualResetEventSlim(false);
        private int _input;
        private InputCondition _condition;
          
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public FxBlock PosRef { set; get; }
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public FxBlock NegRef { set; get; }

        public FxDIBranch()
        {
        }

        public FxDIBranch(string name, FxGpio gpio)
        {
            Name = name;
            Icon = Resource.DI_branch;
            _gpio = gpio;
        }

        public override void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_Disposed)
                return;

            if (disposing)
            {
                _exit.Set();
            }
            _Disposed = true;
        }

        public override void Run()
        {
            Judgement = true;

            if (_gpio == null || !_gpio.IsOpen)
            {
                Judgement = false;
                NextFxBlock = NegRef;   
            }
            else
            {
                int val = _gpio.GetPin(0, Input);

                if ( (val == 1 && Condition == InputCondition.Heigh) || (val == 0 && Condition == InputCondition.Low))
                {
                    NextFxBlock = PosRef;
                }
                else
                {
                    NextFxBlock = NegRef;                   
                }
            }
        }

        public override void Clear()
        {
            // base.Clear();
        }


        #region Properties

        public InputCondition Condition
        {
            get
            {
                return _condition;
            }
            set
            {
                _condition = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Condition"));
            }
        }

        public int Input
        {
            get
            {
                return _input;
            }

            set
            {
                _input = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Input"));
            }
        }

        [DisplayName("POS Direction")]
        [XmlIgnoreAttribute]
        public string Direction1
        {
            get
            {
                return PosRef.Name;
            }
        }

        [DisplayName("NG Direction")]
        [XmlIgnoreAttribute]
        public string Direction2
        {
            get
            {
                return NegRef.Name;
            }
        }


        #endregion

        
    }
}
