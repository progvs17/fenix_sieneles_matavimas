﻿using HalconDotNet;
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Threading;

namespace Elvis.Fenix.FunctionBlocks
{
    public class BlobsSettings
    {
        #region Constructors

        public BlobsSettings()
        {
            MinimumRegionThreshold = 40;
            MaximumRegionThreshold = 180;
            ErosionCircleRadius = 1;
            MinimumRegionShape = 3000;
            MaximumRegionShape = 50000;
            DilationCircleRadius1 = 2;
            DilationCircleRadius2 = 2;
            DefectThreshold = 80;
        }

        #endregion

        #region Public properties

        public double MinimumRegionThreshold
        { get; set; }

        public double MaximumRegionThreshold
        { get; set; }

        public double ErosionCircleRadius
        { get; set; }

        public double MinimumRegionShape
        { get; set; }

        public double MaximumRegionShape
        { get; set; }
        
        public double DilationCircleRadius1
        { get; set; }

        public double DilationCircleRadius2
        { get; set; }

        public double DefectThreshold
        { get; set; }

        #endregion
    }

    public class FxHBlobs : FxBlock
    {
        #region Private Members

        List<HImage> list = new List<HImage>();

        private HImage inputImage;

        private bool showRegion = false;
        private bool showUsefulRegion = false;
        private bool showDefects = false;
        private bool _runBlockOnce = false;

        private List<OperationList> _operationsList;
        private int _operationIndex;
        private OperationMode _operationMode;
        private string _dir;
        private string _sceneName;

        HObject cont_;

        int number;

        List<BlobsSettings> _blobsSettings;

        #endregion

        #region Constructors

        public FxHBlobs() {}

        public FxHBlobs(string name)
        {
            Name = name;
            Icon = Resource.Default;
            IsMarkAble = true;
            Marks = new List<MarkObject>();
            Text = new List<TextObject>();

            BlockSettings = new List<BlobsSettings>();
        }

        #endregion

        #region Public Methods

        [FilterClass]
        public override void Run()
        {
            Judgement = true;
            Judge = true;

            DateTime startTime = DateTime.Now;

            if (Marks != null)
            {
                Marks.Clear();
                Text.Clear();
            }

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            if (BlockOperationMode == OperationMode.SingleImageOperation)
            {
                if (BlockOperations != null && BlockOperations.Count > 0 && BlockOperations[OperationIndex].Image != null && BlockOperations[OperationIndex].Filter == FilterList.ContourBreak)
                {
                    DrawRectangles();

                    if (!updateRegion)
                    {
                        ROITopRow = BlockOperations[OperationIndex].TopRow;
                        ROIBotRow = BlockOperations[OperationIndex].BottomRow;
                        ROITopCol = BlockOperations[OperationIndex].TopCollumn;
                        ROIBotCol = BlockOperations[OperationIndex].BottomCollumn;
                    }

                    FilterImage(BlockOperations[OperationIndex].Image, OperationIndex);
                    HImage = BlockOperations[OperationIndex].Image;
                }
                else
                {
                    Text.Add(new TextObject { Text = "Operation: #" + OperationIndex + "image does not exist", Color = "red", Font = "-Courier New-16-*-*-*-*-1-", Position = new Point(20, 20) });
                }
            }
            else
            {
                if (BlockOperations != null && BlockOperations.Count > 0)
                {
                    int oper = 0;

                    foreach (var operation in BlockOperations)
                    {
                        OperationIndex = oper;

                        if (operation.Image != null && operation.Filter == FilterList.ContourBreak)
                        {
                            Judge = true;
                            ROITopRow = operation.TopRow;
                            ROIBotRow = operation.BottomRow;
                            ROITopCol = operation.TopCollumn;
                            ROIBotCol = operation.BottomCollumn;

                            FilterImage(operation.Image, oper);
                        }
                        oper++;
                    }
                }
            }

            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }

        public override void Clear()
        {
            if (InputImage != null) InputImage.Dispose();
        }

        #endregion

        #region Private Methods

        void FilterImage(HImage image, int operationIndex)
        {
            string color = !Judge ? "red" : "green";

            try
            {
                HRegion region = new HRegion();
                HRegion rectRegion = new HRegion();
                HImage rectImage;
                HImage imgReduce;
                //HObject cont_;
                HRegion RegionContour;

                HXLDCont Border;
                HRegion RegionBorder;

                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                UpdateThreshold(operationIndex);
                BlockOperations[operationIndex].Marks = new List<MarkObject>();
                BlockOperations[operationIndex].Text = new List<TextObject>();

                region.GenRectangle1(ROITopRow, ROITopCol, ROIBotRow, ROIBotCol);
                rectImage = image.ReduceDomain(region);
                HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow, (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);

                region = rectImage.Threshold(BlockSettings[operationIndex].MinimumRegionThreshold, BlockSettings[operationIndex].MaximumRegionThreshold);

                if (ShowRegion)
                {
                    Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = region });
                    BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = region });
                }

                region = region.Connection();
                region = region.ErosionCircle(BlockSettings[operationIndex].ErosionCircleRadius);
                region = region.DilationCircle(BlockSettings[operationIndex].DilationCircleRadius1);
                region = region.FillUp();
                region = region.SelectShapeStd("max_area", (double)10);//BlockSettings[operationIndex].MaximumRegionShape);

                region = region.SelectShape("area", "and", BlockSettings[operationIndex].MinimumRegionShape, BlockSettings[operationIndex].MaximumRegionShape);

                if(region.Area.D > 0)
                {
                    if (ShowUsefulRegion)
                    {
                        Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = region });
                        BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = region });
                    }

                    imgReduce = rectImage.ReduceDomain(region);
                    HTuple rows, cols;
                    region.GetRegionPoints(out rows, out cols);

                    if (region.Area.D > 0)//rows > 0 && cols > 0)
                    {
                        Border = imgReduce.ThresholdSubPix(BlockSettings[operationIndex].DefectThreshold);
                        RegionContour = Border.GenRegionContourXld("filled");
                        region = RegionContour.DilationCircle(BlockSettings[operationIndex].DilationCircleRadius2);

                        RegionBorder = region.Boundary("outer");
                        if (ShowDefects)
                        {
                            Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = RegionBorder });
                            BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = RegionBorder });
                        }

                        try
                        {
                            number = RegionBorder.CountObj();
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    else
                    {
                        number = 0;
                    }

                    //Damage Recognition
                    if (number > 0)
                    {
                        Judgement &= false;
                        Judge = false;
                    }
                }
                else
                {
                    Judgement &= false;
                    Judge = false;
                }

            }
            catch (Exception ex)
            {
                FxSessionTimeClass.LogErrorMessage(this.Name, ex.Message);
                
                Judgement &= false;
                Judge = false;
            }
            BlockOperations[operationIndex].Judgement = Judge;

            color = !Judge ? "red" : "green";
            Marks.Add(new MarkObject { Name = "Contour", Color = color, Object = cont_ });
            BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Contour", Color = color, Object = cont_ });
        }

        public void UpdateThreshold(int index)
        {
            if (BlockSettings != null && BlockSettings.Count > 0 && BlockSettings.Count > index)
            {
                MinRegionThreshold = BlockSettings[index].MinimumRegionThreshold;
                MaxRegionThreshold = BlockSettings[index].MaximumRegionThreshold;
                RegionDilationCircle1 = BlockSettings[OperationIndex].DilationCircleRadius1;
                RegionDilationCircle2 = BlockSettings[OperationIndex].DilationCircleRadius2;
                RegionErosionCircle = BlockSettings[OperationIndex].ErosionCircleRadius;
                DefThreshold = BlockSettings[OperationIndex].DefectThreshold;
                MaxRegionShape = BlockSettings[index].MaximumRegionShape;
                MinRegionShape = BlockSettings[OperationIndex].MinimumRegionShape;
            }
        }

        #endregion

        #region UI mouse clicks interaction

        private double roiTopCol = 200;
        private double roiTopRow = 150;
        private double roiBotCol = 400;
        private double roiBotRow = 315;
        private double roiStartRow = 200;
        private double roiStartCol = 150;
        private double roiEndRow = 315;
        private double roiEndCol = 400;
        private double sx, sy, dx, dy;
        private bool IsDiamBusy;

        private bool updateRegion = false;

        private void DrawRectangles()
        {
            HRegion rect_ = new HRegion();
            HRegion region = new HRegion();
            HObject cont_;

            HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow, (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);

            Marks.Add(new MarkObject { Name = "Contour", Color = "blue", Object = cont_ });
        }


        public void UpdateROI()
        {
            if (updateRegion)
            {
                BlockOperations[OperationIndex].TopRow = ROITopRow;
                BlockOperations[OperationIndex].BottomRow = ROIBotRow;
                BlockOperations[OperationIndex].TopCollumn = ROITopCol;
                BlockOperations[OperationIndex].BottomCollumn = ROIBotCol;
            }
        }

        public void HOnMouseDown(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                sx = (int)e.X;
                sy = (int)e.Y;

                if (sx > (ROITopCol - 15) && sx < (ROITopCol + 15) && sy > (ROITopRow - 15) && sy < (ROITopRow + 15))
                {
                    roiStartRow = ROITopRow;
                    roiStartCol = ROITopCol;
                    IsDiamBusy = false;
                }
                else
                {
                    roiEndRow = ROIBotRow;
                    roiEndCol = ROIBotCol;
                    IsDiamBusy = true;
                }

                updateRegion = true;
            }
        }

        public void HOnMouseUp(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                UpdateROI();
                updateRegion = false;
            }
        }

        public void HOnMouseMove(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {

                dx = (int)e.X - sx;
                dy = (int)e.Y - sy;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    if (!IsDiamBusy)
                    {
                        ROITopCol = roiStartCol + dx;
                        ROITopRow = roiStartRow + dy;
                    }
                    else
                    {
                        ROIBotCol = roiEndCol + dx;
                        ROIBotRow = roiEndRow + dy;
                    }

                }
            }
        }

        public void HOnMouseWheel(object sender, HMouseEventArgs e)
        {

        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top row position.")]
        [DisplayName("ROI top row")]
        public double ROITopRow
        {
            get
            {
                return roiTopRow;
            }
            set
            {
                roiTopRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot row position.")]
        [DisplayName("ROI bot row")]
        public double ROIBotRow
        {
            get
            {
                return roiBotRow;
            }
            set
            {
                roiBotRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top col position.")]
        [DisplayName("ROI top col")]
        public double ROITopCol
        {
            get
            {
                return roiTopCol;
            }
            set
            {
                roiTopCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopCol"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot col position.")]
        [DisplayName("ROI bot col")]
        public double ROIBotCol
        {
            get
            {
                return roiBotCol;
            }
            set
            {
                roiBotCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotCol"));
            }
        }

        #endregion

        #region Public Properties

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage InputImage
        {
            get
            {
                return inputImage;
            }
            set
            {
                inputImage = value != null ? value.CopyImage() : null;
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage _hImage { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage HImage
        {
            get
            {
                return _hImage;
            }
            set
            {
                _hImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HImage"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<HImage> InputImages { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public override Bitmap Icon { set; get; }

        [Browsable(false), BlockNameAttribute]
        public override string Name { set; get; }



        #region Properties from image grab block


        [CategoryAttribute("Block Settings")]
        [Description("Switch between single & multiple image grabbing")]
        [Browsable(false)]
        public OperationMode BlockOperationMode
        {
            get
            {
                return _operationMode;
            }
            set
            {
                _operationMode = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperationMode"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("Currently displayed operation (image)")]
        [XmlIgnoreAttribute, Browsable(false)]
        public int OperationIndex
        {
            get
            {
                return _operationIndex;
            }
            set
            {
                _operationIndex = value;

                OnPropertyChanged(this, new PropertyChangedEventArgs("OperationIndex"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("List of operations")]
        [XmlIgnoreAttribute]
        public List<OperationList> BlockOperations
        {
            get
            {
                return _operationsList;
            }
            set
            {
                _operationsList = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [XmlIgnoreAttribute, Browsable(false)]
        public bool RunBlockOnce
        {
            get
            {
                return _runBlockOnce;
            }
            set
            {
                _runBlockOnce = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("RunBlockOnce"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Show calibration region")]
        [DisplayName("1. Show region")]
        public bool ShowRegion
        {
            get
            {
                return showRegion;
            }
            set
            {
                showRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowRegion"));
            }
        }


        #endregion

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("The minimum threshold for the analyzed region.")]
        [DisplayName("2. Min region threshold")]
        public double MinRegionThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumRegionThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new BlobsSettings());
                    return BlockSettings[OperationIndex].MinimumRegionThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumRegionThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new BlobsSettings());
                    MinRegionThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinRegionThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("The maximum threshold for the analyzed region.")]
        [DisplayName("3. Max region threshold")]
        public double MaxRegionThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumRegionThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new BlobsSettings());
                    return BlockSettings[OperationIndex].MaximumRegionThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumRegionThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new BlobsSettings());
                    MaxRegionThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxRegionThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Useful region edges dilation circle radius.")]
        [DisplayName("4. Region dilation circle radius")]
        public double RegionDilationCircle1
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].DilationCircleRadius1;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new BlobsSettings());
                    return BlockSettings[OperationIndex].DilationCircleRadius1;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].DilationCircleRadius1 = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new BlobsSettings());
                    RegionDilationCircle1 = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("RegionDilationCircle1"));
            }
        }


        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Useful region edges erosion circle radius.")]
        [DisplayName("4. Region erosion circle radius")]
        public double RegionErosionCircle
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].ErosionCircleRadius;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new BlobsSettings());
                    return BlockSettings[OperationIndex].ErosionCircleRadius;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].ErosionCircleRadius = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new BlobsSettings());
                    RegionErosionCircle = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("RegionErosionCircle"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Show useful region")]
        [DisplayName("5. Show useful region")]
        public bool ShowUsefulRegion
        {
            get
            {
                return showUsefulRegion;
            }
            set
            {
                showUsefulRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowUsefulRegion"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Filterable size of the region.")]
        [DisplayName("6. Min region size")]
        public double MinRegionShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumRegionShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new BlobsSettings());
                    return BlockSettings[OperationIndex].MinimumRegionShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumRegionShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new BlobsSettings());
                    MinRegionShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinRegionShape"));
            }
        }
        
        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Filterable size of the region.")]
        [DisplayName("7. Max region size")]
        public double MaxRegionShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumRegionShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new BlobsSettings());
                    return BlockSettings[OperationIndex].MaximumRegionShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumRegionShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new BlobsSettings());
                    MaxRegionShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxRegionShape"));
            }
        }
        

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find PET blobs")]
        [Description("Threshold of the defect.")]
        [DisplayName("1. Defect threshold")]
        public double DefThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].DefectThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new BlobsSettings());
                    return BlockSettings[OperationIndex].DefectThreshold;
                }
            }
            set
            {
                try
                {
                    BlockSettings[OperationIndex].DefectThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new BlobsSettings());
                    DefThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("DefThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find PET blobs")]
        [Description("Useful region edges dilation circle radius.")]
        [DisplayName("2. Region dilation circle radius")]
        public double RegionDilationCircle2
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].DilationCircleRadius2;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new BlobsSettings());
                    return BlockSettings[OperationIndex].DilationCircleRadius2;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].DilationCircleRadius2 = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new BlobsSettings());
                    RegionDilationCircle2 = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("RegionDilationCircle2"));
            }
        }


        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find PET blobs")]
        [Description("Show PET defects")]
        [DisplayName("3. Show defects")]
        public bool ShowDefects
        {
            get
            {
                return showDefects;
            }
            set
            {
                showDefects = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowDefects"));
            }
        }

        [CategoryAttribute("Calibration")]
        [Description("Blobs settings")]
        public List<BlobsSettings> BlockSettings
        {
            get
            {
                return _blobsSettings;
            }
            set
            {
                _blobsSettings = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockSettings"));
            }
        }

        #endregion


        #region Buttons


        [ButtonControl]
        public void NextImage()
        {
            if (BlockOperationMode == OperationMode.SingleImageOperation && BlockOperations != null && BlockOperations.Count > 0)
            {
                OperationIndex = OperationIndex < BlockOperations.Count - 1 ? OperationIndex + 1 : OperationIndex;
                RunBlockOnce = true;
            }
        }

        [ButtonControl]
        public void PreviousImage()
        {
            if (BlockOperationMode == OperationMode.SingleImageOperation && BlockOperations != null && BlockOperations.Count > 0)
            {
                OperationIndex = OperationIndex > 0 ? OperationIndex - 1 : OperationIndex;
                RunBlockOnce = true;
            }
        }

        #endregion

        #region Events
        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;


        #endregion
    }
}
