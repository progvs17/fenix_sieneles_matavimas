﻿using System;
using System.ComponentModel;
using System.Timers;
using System.Threading;

using Elvis.Fenix.Hardware;

namespace Elvis.Fenix.FunctionBlocks
{
	public class JudgeDOBlock : FxBlock
	{
		private FxGpio _gpio;
		private bool _SceneJudgement;
		private bool _EnableTimer;
		private int _DoneOutput;
		private int _DefectOutput;
		private int _Duration;
		private System.Timers.Timer _autoOffTimer;

		private ManualResetEventSlim _wait = new ManualResetEventSlim(false);

		
		
		
		public JudgeDOBlock(string name, FxGpio gpio)
		{
			Name = name;
			Icon = Resource.IO_sync;
			_autoOffTimer = new System.Timers.Timer();
			_autoOffTimer.Elapsed += new ElapsedEventHandler(onTimeElapsed);
			_gpio = gpio;
		}

		public override void Dispose()
		{
			base.Dispose();
		}

		public override void Run()
		{
			Judgement = true;

			try
			{
				if(!SceneJudgement)
				{
					//Judgement = true;
					_gpio.SetPin(DefectOutput, 1);
					_gpio.SetPin(DoneOutput, 1);                    
				}
				else 
				{
				   // Judgement = false;
					_gpio.SetPin(DoneOutput, 1);
					_gpio.SetPin(DefectOutput, 0);                
				}

				if(EnableTimer)
				{
					_wait.Reset();
					_autoOffTimer.Enabled = EnableTimer;
					while (!_wait.IsSet);
				}


				_autoOffTimer.Enabled = EnableTimer;
			}
			catch(Exception ex)
			{
				Judgement = false;
			}
		}

		public override void Clear()
		{
						
		}

		public void Reset(Object sender)
		{
			if(_gpio != null)
			{
				_gpio.SetPin(DefectOutput, 0);
				_gpio.SetPin(DoneOutput, 0);
			}			
		}

		private void onTimeElapsed(object sender, ElapsedEventArgs e)
		{
			_autoOffTimer.Enabled = false;
			if (_gpio != null)
			{
				_gpio.SetPin(DefectOutput, 0);
				_gpio.SetPin(DoneOutput, 0);
			}
			_wait.Set();
		}


		[Browsable(false)]
		public bool SceneJudgement
		{
			get
			{
				return _SceneJudgement;
			}

			set
			{
				_SceneJudgement = value;
				OnPropertyChanged(this, new PropertyChangedEventArgs("SceneJudgement"));
			}
		}

		[DisplayName("Defect Output")]
		[Description("The number of output signal pin")]
		public int DefectOutput
		{
			get
			{
				return _DefectOutput;
			}
			set
			{
				_DefectOutput = value;
				OnPropertyChanged(this, new PropertyChangedEventArgs("DefectOutput"));
			}
		}
		[DisplayName("Done Output")]
		[Description("The number of output signal pin.")]
		public int DoneOutput
		{
			get
			{
				return _DoneOutput;
			}
			set
			{
				_DoneOutput = value;
				OnPropertyChanged(this, new PropertyChangedEventArgs("DoneOutput"));
			}
		}

		[DisplayName("Signal Lenght")]
		[Description("Signal lenght until auto reset")]
		public int Duration
		{
			get
			{
				return _Duration;
			}

			set
			{
				_Duration = value > 2000 ? 2000 : value;
				_Duration = value <= 0 ? 10 : value;
				_autoOffTimer.Interval = _Duration;
				OnPropertyChanged(this, new PropertyChangedEventArgs("Duration"));
			}
		}
		[DisplayName("Auto Off")]
		[Description("Enable auto reset timer")]
		public bool EnableTimer
		{
			get
			{
				return _EnableTimer;
			}
			set
			{
				_EnableTimer = value;
				OnPropertyChanged(this, new PropertyChangedEventArgs("EnableTimer"));
			}
		}

	}
}
