﻿using System;
using Elvis.Fenix.General;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace Elvis.Fenix.FunctionBlocks
{
    public delegate void TaskStartHandler(Object sender);
    public delegate void TaskCompleteHandler(Object sender);
    public delegate void MessageHandler(Object sender, string msg);

    /// <summary>
    /// Attribute to ease blocks name search
    /// </summary>
    public class BlockNameAttribute : System.Attribute
    {
        public BlockNameAttribute()
        {

        }
    }

    /// <summary>
    /// Attribute for block rellations
    /// </summary>
    public class BlockRellationsAttribute : System.Attribute
    {
        public BlockRellationsAttribute()
        {

        }
    }

    /// <summary>
    /// Attribute to indicate a filter
    /// </summary>
    public class FilterClassAttribute : System.Attribute
    {
        public FilterClassAttribute()
        {
        }
    }


    public class ButtonControlAttribute : System.Attribute
    {
        public ButtonControlAttribute()
        {

        }
    }

    ///// <summary>
    ///// Attribute to mark start block
    ///// </summary>
    //public class StartBlockAttribute : System.Attribute
    //{
    //    public StartBlockAttribute() { }
    //}

    /// <summary>
    /// Halcon mark object class
    /// </summary>
    public class MarkObject
    {
        public string Name { get; set; }
        //public HObject Object { get; set; }
        public string Color { get; set; }
    }

    /// <summary>
    /// Halcon text object class
    /// </summary>
    public class TextObject
    {
        public string Text { get; set; }
        public string Font { get; set; }
        public string Color { get; set; }
        public System.Drawing.Point Position { get; set; }
    }

    public enum OperationMode
    {
        SingleImage,
        MultipleImages
    }

    /// <summary>
    /// Case structure for Function Blocks
    /// </summary>
    public abstract class FxBlock : IFxBindable, IDisposable
    {
        #region Public Properties

        /// <summary>
        /// Block name
        /// </summary>
        [Browsable(false), BlockNameAttribute]
        public virtual string Name { set; get; }

        /// <summary>
        /// Block icon
        /// </summary>
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public virtual System.Drawing.Bitmap Icon { get; set; }

        /// <summary>
        /// Stores reference to next binded FxBlock object
        /// </summary>
        [Browsable(false)]
        [XmlIgnoreAttribute, BlockRellationsAttribute]
        public FxBlock NextFxBlock { get; set; }

        [Description("True - measure in mm; False - measure in px")]
        [DisplayName("Measurement mode (mm : px)")]
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public bool MeasurementMode { get; set; }

        [Description("Measurement coefficient (mm/px)")]
        [DisplayName("Measurement coefficient (mm/px)")]
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public int MeasurementCoefficient { get; set; }

        /// <summary>
        /// Block runs once or continously
        /// </summary>
        [CategoryAttribute("Block Settings")]
        [Description("Block continous or single operation option")]
        [DisplayName("Run block once")]
        public bool SingleRun 
        { 
            get 
            { return _singleRun; 
            } 
            set 
            { 
                _singleRun = value;
                OnPropertyChanged(new PropertyChangedEventArgs("SingleRun"));
            } 
        }

        /// <summary>
        /// Start block property
        /// </summary>
        [CategoryAttribute("Block Settings")]
        [Description("Provides information either block is start block in scene or not")]
        [XmlIgnoreAttribute, Browsable(false)]
        public bool IsStartBlock
        {
            get
            {
                return _isStartBlock;
            }
            set
            {
                _isStartBlock = value;
                OnPropertyChanged(new PropertyChangedEventArgs("IsStartBlock"));
            }
        }

        /// <summary>
        /// Block judgement result
        /// </summary>
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public bool Judgement
        {
            get
            {
                return _Judgement;
            }
            set
            {
                _Judgement = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Judgement"));
            }
        }

        /// <summary>
        /// Scene judgement result
        /// </summary>
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public bool Judge
        {
            get
            {
                return _Judge;
            }
            set
            {
                _Judge = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Judge"));
            }
        }

        /// <summary>
        /// Block execution time result
        /// </summary>
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public double ElapsedTime
        {
            get
            {
                return _ElapsedTime;
            }
            set
            {
                _ElapsedTime = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ElapsedTime"));
            }
        }

        /// <summary>
        /// Flag is true if class supports Halcon drawing (Regions, Countours, Strings)
        /// </summary>
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public bool IsMarkAble { get; set; }
        
        /// <summary>
        /// List for Halcon drawing objects (Regions, Countours)
        /// </summary>
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<MarkObject> Marks { set; get; }

        /// <summary>
        /// List for Halcon string objects
        /// </summary>
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<TextObject> Text { set; get; }


        /// <summary>
        /// Flag for events control
        /// </summary>
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public bool IsFocused { set; get; }

        [CategoryAttribute("Block Settings")]
        public bool UseBlock
        {
            set
            {
                _useBlock = value;

            }
            get
            {
                return _useBlock;
            }
        }

        #endregion


        #region Private members       

        /// <summary>
        /// Block judgement result
        /// </summary>
        private bool _Judgement;

        /// <summary>
        ///Scene judgement result
        /// </summary>
        private bool _Judge;

        /// <summary>
        /// Block elapsed time result
        /// </summary>
        private double _ElapsedTime;  

        /// <summary>
        /// Block continous/single action
        /// </summary>
        private bool _singleRun = true;

        /// <summary>
        /// Indicates if block is StartBlock in scene
        /// </summary>
        private bool _isStartBlock = false;

        private bool _useBlock = true;

        #endregion


        #region Public Methods

        public virtual void Dispose()
        {

        }

        /// <summary>
        /// Main run method
        /// </summary>
        public virtual void Run() { throw new NotImplementedException(); }

        /// <summary>
        /// Memory clean method
        /// </summary>
        public virtual void Clear() { throw new NotImplementedException(); }


        public virtual void Stop() { }



        public virtual void Exit() {  }


        /*
        public abstract void OnMouseDown(object sender, HMouseEventArgs e)
        {
                      
        }
         * */

        #endregion


        #region Events

        public virtual event TaskStartHandler onTaskStarted;
        public virtual event TaskCompleteHandler onTaskCompleted;
        public virtual event MessageHandler onMessage;

        #endregion
    }
}
