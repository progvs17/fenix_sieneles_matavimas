﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

using System.Drawing;

using HalconDotNet;
using Elvis.Fenix.Hardware;
using Elvis.Fenix.Hardware.Camera.AsyncGigE;





namespace Elvis.Fenix.FunctionBlocks
{
    public class FxGrabImage : FxBlock
    {
        #region Private Fields

        private FxCamera _camera;
        private FxFrame _frame;
        List<OperationList> _operationsList;
        int _operationIndex = 0;
        OperationMode _operationMode;


        private bool _imageLoaderEnabled;
        private bool _useSingleImage;
        private bool _saveGrabbedImages = false;

        private string _imageDirectory;
        private string _sceneName;
        private string _sessionImagesDir;
        private string _prefix;
        private string _fileName;

        private bool _runBlockOnce = false;
        private ManualResetEventSlim _stop = new ManualResetEventSlim(false);

        #endregion


        #region Public Properties
        /// <summary>
        /// Block name
        /// </summary>
        [Browsable(false), BlockNameAttribute]
        public override string Name { set; get; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public override Bitmap Icon { set; get; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage _hImage { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage HImage
        {
            get
            {
                return _hImage;
            }

            set
            {
                _hImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HImage"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<FxFrame> FrameList 
        { 
            get
            {
                return _frameList;
            }
            set
            {
                _frameList = value;
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("List of operations")]
        public List<OperationList> BlockOperations
        {
            get
            {
                return _operationsList;
            }
            set
            {
                _operationsList = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("Currently displayed operation (image)")]
        [XmlIgnoreAttribute]
        public int OperationIndex
        {
            get
            {
                return _operationIndex;
            }
            set
            {
                _operationIndex = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("OperationIndex"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("Switch between single & multiple image grabbing")]
        public OperationMode BlockOperationMode
        {
            get
            {
                return _operationMode;
            }
            set
            {
                _operationMode = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperationMode"));
            }
        }

        [XmlIgnoreAttribute, Browsable(false)]
        public bool RunBlockOnce
        {
            get { return _runBlockOnce; }
            set
            {
                if (value)
                {
                    Run();
                }
                _runBlockOnce = false;
                OnPropertyChanged(this, new PropertyChangedEventArgs("RunBlockOnce"));
            }
        }

        [XmlIgnoreAttribute]
        public bool TriggerOn
        {
            get
            {
                return (_camera as AsyncPylonCamera).TriggerOn;
            }
            set
            {
                (_camera as AsyncPylonCamera).TriggerOn = value;
            }
        }

        [XmlIgnoreAttribute]
        public int Exposure
        {
            get
            {
                return _camera.Exposure;
            }
            set
            {
                _camera.Exposure = value;
            }
        }

        #region Props for image save/load block

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public bool ImageLoaderEnabled
        {
            get
            {
                return _imageLoaderEnabled;
            }
            set
            {
                _imageLoaderEnabled = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ImageLoaderEnabled"));
            }
        }

        [Description("Use only 1 image")]
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public bool UseSingleImage
        {
            get
            {
                return _useSingleImage;
            }
            set
            {
                _useSingleImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("UseSingleImage"));
            }
        }

        [Description("Session location to load images from")]
        [Browsable(false)]
        public string SelectedSessionImagesDir
        {
            get
            {
                return _sessionImagesDir;
            }

            set
            {
                _sessionImagesDir = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SelectedSessionImagesDir"));
            }
        }

        [Description("Location where grabbed images are stored")]
        [Browsable(false)]
        public string FilePath
        {
            get
            {
                return _imageDirectory;
            }

            set
            {
                _imageDirectory = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("FilePath"));
            }
        }

        [Description("Current scene name")]
        [Browsable(false)]
        public string SceneName
        {
            get
            {
                return _sceneName;
            }
            set
            {
                _sceneName = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SceneName"));
            }
        }

        [Description("Saved image prefix (name)")]
        [Browsable(false)]
        public string Prefix
        {
            get
            {
                return _prefix;
            }
            set
            {
                _prefix = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Prefix"));
            }
        }

        [Description("Saved image prefix (name)")]
        [Browsable(false)]
        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("FileName"));
            }
        }

        [Description("Save grabbed images?")]
        public bool SaveGrabbedImages
        {
            get
            {
                return _saveGrabbedImages;
            }
            set
            {
                _saveGrabbedImages = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SaveGrabbedImages"));
            }
        }

        #endregion

        #endregion


        #region UI mouse clicks interaction

        private double roiStartRow = 200;
        private double roiStartCol = 150;
        private double roiEndRow = 315;
        private double roiEndCol = 400;
        private double sx, sy, dx, dy;
        private bool IsDiamBusy;

        private bool updateRegion = true;

        private void DrawRectangles()
        {
            HRegion rect_ = new HRegion();
            HRegion region = new HRegion();
            HObject cont_;

            HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow, (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);
            Marks.Add(new MarkObject { Name = "Contour", Color = "blue", Object = cont_ });
        }

        public void HOnMouseDown(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                sx = (int)e.X;
                sy = (int)e.Y;

                if (sx > (ROITopCol - 15) && sx < (ROITopCol + 15) && sy > (ROITopRow - 15) && sy < (ROITopRow + 15))
                {
                    roiStartRow = ROITopRow;
                    roiStartCol = ROITopCol;
                    IsDiamBusy = false;
                }
                else
                {
                    roiEndRow = ROIBotRow;
                    roiEndCol = ROIBotCol;
                    IsDiamBusy = true;
                }
                updateRegion = true;
            }
        }

        public void HOnMouseUp(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                updateRegion = false;
            }
        }

        public void HOnMouseMove(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                dx = (int)e.X - sx;
                dy = (int)e.Y - sy;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    if (!IsDiamBusy)
                    {
                        ROITopCol = roiStartCol + dx;
                        ROITopRow = roiStartRow + dy;
                    }
                    else
                    {
                        ROIBotCol = roiEndCol + dx;
                        ROIBotRow = roiEndRow + dy;
                    }
                }
            }
        }

        public void HOnMouseWheel(object sender, HMouseEventArgs e)
        {

        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top row position.")]
        [DisplayName("ROI top row")]
        public double ROITopRow
        {
            get
            {
                try
                {
                    return BlockOperations[OperationIndex].TopRow;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Operation does not exist");
                    return 0;
                }
            }
            set
            {
                try
                {
                    if (updateRegion)
                        BlockOperations[OperationIndex].TopRow = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Operation does not exist");
                    BlockOperations.Add(new OperationList());
                    ROITopRow = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopRow"));
            }

        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot row position.")]
        [DisplayName("ROI bot row")]
        public double ROIBotRow
        {
            get
            {
                try
                {
                    return BlockOperations[OperationIndex].BottomRow;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Operation does not exist");
                    return 0;
                }
            }
            set
            {
                try
                {
                    if (updateRegion)
                        BlockOperations[OperationIndex].BottomRow = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Operation does not exist");
                    BlockOperations.Add(new OperationList());
                    ROIBotRow = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top col position.")]
        [DisplayName("ROI top col")]
        public double ROITopCol
        {
            get
            {
                try
                {
                    return BlockOperations[OperationIndex].TopCollumn;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Operation does not exist");
                    return 0;
                }
            }
            set
            {
                try
                {
                    if (updateRegion)
                        BlockOperations[OperationIndex].TopCollumn = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Operation does not exist");
                    BlockOperations.Add(new OperationList());
                    ROITopCol = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopCol"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot col position.")]
        [DisplayName("ROI bot col")]
        public double ROIBotCol
        {
            get
            {
                try
                {
                    return BlockOperations[OperationIndex].BottomCollumn;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Operation does not exist");
                    return 0;
                }
            }
            set
            {
                try
                {
                    if (updateRegion)
                        BlockOperations[OperationIndex].BottomCollumn = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Operation does not exist");
                    BlockOperations.Add(new OperationList());
                    ROIBotCol = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotCol"));
            }
        }


        #endregion


        #region Constructors

        public FxGrabImage() 
        {
        }

        public FxGrabImage(string name, FxCamera camera)
        {
            Name = name;
            _camera = camera;
            Icon = Resource.Camera;

            IsMarkAble = true;
            Marks = new List<MarkObject>();
            Text = new List<TextObject>();

            FrameList = new List<FxFrame>();
            BlockOperations = new List<OperationList>();

        }


        #endregion


        #region Public Methods

        public override void Dispose()
        {
            Exit();
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Main run method
        /// </summary>
        public override void Run() 
        {
            Judgement = true;
            DateTime startTime = DateTime.Now;

            if (Marks != null)
            {
                Marks.Clear();
                Text.Clear();
            }

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            _stop.Reset();

            try
            {
                //int idx = 0;
                // update variable for filter blocks
                UseSingleImage = BlockOperationMode == OperationMode.SingleImageOperation ? true : false;

                if (ImageLoaderEnabled)
                {
                    if (SelectedSessionImagesDir != null)
                        LoadImagesFromFile();
                }
                else
                {

                    switch (BlockOperationMode)
                    {
                        case OperationMode.SingleImageOperation:

                            if (_camera.IsConnected && _camera.IsGrabbing)
                            {
                                _camera.Exposure = BlockOperations[OperationIndex].Exposure;

                                //while (_frame == null && !_stop.IsSet)
                                _frame = _camera.RetrieveFrame();
                            }

                            if (_frame != null && _frame.Data != null)
                            {
                                try
                                {
                                    HImage img = new HImage();
                                    img.GenImage1Extern("byte", _frame.Width, _frame.Height, _frame.Data, IntPtr.Zero);

                                    HImage = img.Clone();
                                    BlockOperations[OperationIndex].Image = img.Clone();

                                    img.Dispose();
                                    //if (onImageGrabbed != null) onImageGrabbed(this);


                                    //_camera.ReleaseFrame(_frame);
                                    _frame = null;
                                }

                                catch
                                {
                                    Judgement = false;
                                }
                            }
                            else
                            {
                                Judgement = false;
                            }

                            break;

                        case OperationMode.MultipleImagesOperations:

                            if (BlockOperations.Count >= 1)
                            {
                                foreach (var operation in BlockOperations)
                                {

                                    if (_camera.IsConnected && _camera.IsGrabbing)
                                    {
                                        _camera.Exposure = operation.Exposure;

                                        //while (_frame == null && !_stop.IsSet)
                                        _frame = _camera.RetrieveFrame();
                                    }


                                    if (_frame != null && _frame.Data != null)
                                    {
                                        try
                                        {
                                            HImage img = new HImage();
                                            img.GenImage1Extern("byte", _frame.Width, _frame.Height, _frame.Data, IntPtr.Zero);

                                            HImage = img.Clone();
                                            operation.Image = img.Clone();

                                            img.Dispose();
                                            //if (onImageGrabbed != null) onImageGrabbed(this);


                                            //_camera.ReleaseFrame(_frame);
                                            _frame = null;
                                        }

                                        catch
                                        {
                                            Judgement = false;
                                        }
                                    }
                                    else
                                    {
                                        Judgement = false;
                                    }

                                }
                            }

                            break;
                    }


                    //if (!ImageLoaderEnabled)
                    //{
                    //    _camera.ReleaseFrame(_frame);
                    //}

                    if (SaveGrabbedImages)
                    {
                        SaveImage();
                    }

                    
                }
                

            }
            catch (OutOfMemoryException)
            {
                GC.Collect();
                FxSessionTimeClass.LogErrorMessage("(" + Name + ") ", "Out of memory");
            }
            catch (Exception ex)
            {
                Judgement = false;

                if (onMessage != null)
                {
                    onMessage(this, "FxGrabImage block error: " + ex.Message);
                }

                FxSessionTimeClass.LogErrorMessage("(" + Name + ") ", ex.Message);
            }

            // update block operations
            OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));

            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

            Judge = Judgement;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }


        /// <summary>
        /// Memory clean method
        /// </summary>
        public override void Clear() 
        {
            GC.SuppressFinalize(this); 
        }


        public override void Stop() 
        {
            _stop.Set();

            //if (_camera.IsGrabbing)
            //    _camera.StopGrab();
        }



        public override void Exit() 
        {
            Stop();
            //if (_camera.IsGrabbing)
            //    _camera.StopGrab();

            //if (_camera.IsConnected)
            //    _camera.Disconnect();
        }

        #endregion


        #region Private Methods

        public void LoadImagesFromFile()
        {
            try
            {
                string[] files = Directory.GetFiles(SelectedSessionImagesDir + @"\" + Name, "*.*");
                HImage img = new HalconDotNet.HImage();

                switch (BlockOperationMode)
                {
                    case OperationMode.MultipleImagesOperations:

                        int i = 0;

                        foreach (var operation in BlockOperations)
                        {
                            OperationIndex = i;
                            img.ReadImage(files[i++]);
                            operation.Image = HImage = img.Clone();

                            //if (onImageGrabbed != null)
                            //{
                            //    onImageGrabbed(this);
                            //    Thread.Sleep(100);
                            //}
                        }

                        break;

                    case OperationMode.SingleImageOperation:

                        img.ReadImage(files[OperationIndex]);
                        BlockOperations[OperationIndex].Image = img.Clone();
                        HImage = img.Clone();

                        DrawRectangles();

                        Text.Add(new TextObject { Text = "Operation: #" + (OperationIndex), Color = "red", Font = "-Courier New-14-*-*-*-*-1-", Position = new Point(20, 10) });

                        break;
                }
            }
            catch (Exception ex)
            {
                if (onMessage != null)
                    onMessage(this, "Wrong path selected");

                FxSessionTimeClass.LogErrorMessage("(" + Name + ") ", ex.Message);


                Judgement = false;
                //throw new Exception(ex.Message);
            }
        }


        /// <summary>
        /// function to save grabbed image(s)
        /// </summary>
        void SaveImage()
        {
            string path = FilePath + @"\" + DateTime.Now.ToString("yyyy-MM-dd") + @"\" + SceneName + @"\Session-" + FxSessionTimeClass.SessionStartTime + @"\" + Name;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string imageNumber;

            switch (BlockOperationMode)
            {
                case OperationMode.MultipleImagesOperations:

                    int i = 0;

                    if (BlockOperations != null && BlockOperations.Count > 0)
                    {
                        foreach (var operation in BlockOperations)
                        {
                            if (i < 10)
                            {
                                imageNumber = "0" + i.ToString();
                            }
                            else
                            {
                                imageNumber = i.ToString();
                            }

                            operation.Image.WriteImage("tiff", 0, path + @"\" + Prefix + " (" + imageNumber + ")");
                            i++;
                        }
                    }

                    break;

                case OperationMode.SingleImageOperation:

                    if (BlockOperations != null && BlockOperations[OperationIndex].Image != null)
                    {
                        
                        if(OperationIndex < 10)
                        {
                            imageNumber = "0" + OperationIndex.ToString();
                        }
                        else
                        {
                            imageNumber = OperationIndex.ToString();
                        }

                        BlockOperations[OperationIndex].Image.WriteImage("tiff", 0, path + @"\" + Prefix + " (" + imageNumber + ")");
                    }

                    break;
            }
        }

        #endregion


        #region Buttons

        [ButtonControl]
        public void ResetCamera()
        {
            //if (_camera.IsGrabbing)
            _camera.StopGrab();

            //if (_camera.IsConnected)
            _camera.Disconnect();

            System.Threading.Thread.Sleep(500);

            _camera.Connect();
            _camera.StartGrab();

        }

        [ButtonControl]
        public void DuplicateLastOperation()
        {
            if (BlockOperations != null && BlockOperations.Count > 0)
            {
                Stop();

                int lastIdx = BlockOperations.Count - 1;
                OperationList newItem = new OperationList();

                foreach (var property in newItem.GetType().GetProperties())
                {
                    newItem.GetType().GetProperty(property.Name).SetValue(newItem, BlockOperations[lastIdx].GetType().GetProperty(property.Name).GetValue(BlockOperations[lastIdx]));
                }

                BlockOperations.Add(newItem);
                NextOperation();
            }
        }

        [ButtonControl]
        public void NextOperation()
        {
            if (BlockOperations != null && BlockOperations.Count > 0)
            {
                Stop();

                OperationIndex = OperationIndex < BlockOperations.Count - 1 ? OperationIndex + 1 : OperationIndex;
            }
        }

        [ButtonControl]
        public void PreviousOperation()
        {
            if (BlockOperations != null && BlockOperations.Count > 0)
            {
                Stop();

                OperationIndex = OperationIndex > 0 ? OperationIndex - 1 : OperationIndex;
            }
        }

        #endregion


        


        #region Events

        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;
        private List<FxFrame> _frameList;

        #endregion

    }
}
