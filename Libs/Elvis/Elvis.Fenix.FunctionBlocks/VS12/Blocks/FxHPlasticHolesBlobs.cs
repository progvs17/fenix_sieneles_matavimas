﻿using HalconDotNet;
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using System.Diagnostics;
using System.Threading;

namespace Elvis.Fenix.FunctionBlocks
{
    public class HolesBlobsSettings
    {
        #region Constructors

        public HolesBlobsSettings()
        {
            MinimumLinesThreshold = 40;
            MaximumLinesThreshold = 180;
            MinimumObjectThreshold = 40;
            MaximumObjectThreshold = 180;
            MinimumRegionThreshold = 40;
            MaximumRegionThreshold = 180;
            MinimumRegionShape = 3000;
            MaximumRegionShape = 25000;
            MinimumLinesShape = 3000;
            MaximumLinesShape = 50000;
            MinimumObjectSize = 300;
            MaximumObjectSize = 3000;
            MinimumRegionThresholdHoles = 160;
            MaximumRegionThresholdHoles = 220;
            MinimumRegionThresholdBlobs = 85;
            MaximumRegionThresholdBlobs = 220;
            ObjectDilationCircleRadius = 5;
            ErrorBotRowSize = 0;
            ErrorBotColSize = 0;
            ErrorTopColSize = 0;
            ObjectErosionCircleRadius = 1;
            DilationCircleRadius = 2;
            RegionDilationCircleRadius = 2;
            ErosionCircleRadius = 1.5;
            HolesLowAccuracy = 10;
            HolesHighAccuracy = 20;
        }

        #endregion

        #region Public properties

        public double MinimumLinesThreshold
        { get; set; }

        public double MaximumLinesThreshold
        { get; set; }

        public double MinimumObjectThreshold
        { get; set; }

        public double MaximumObjectThreshold
        { get; set; }

        public double MinimumRegionThreshold
        { get; set; }

        public double MaximumRegionThreshold
        { get; set; }

        public double MinimumRegionShape
        { get; set; }

        public double MaximumRegionShape
        { get; set; }

        public double MinimumLinesShape
        { get; set; }

        public double MaximumLinesShape
        { get; set; }

        public double DilationCircleRadius
        { get; set; }

        public double ErosionCircleRadius
        { get; set; }

        public double MinimumObjectSize
        { get; set; }

        public double MaximumObjectSize
        { get; set; }

        public double MinimumRegionThresholdHoles
        { get; set; }

        public double MaximumRegionThresholdHoles
        { get; set; }

        public double MinimumRegionThresholdBlobs
        { get; set; }

        public double MaximumRegionThresholdBlobs
        { get; set; }

        public double ObjectDilationCircleRadius
        { get; set; }

        public double ErrorBotRowSize
        { get; set; }

        public double ErrorBotColSize
        { get; set; }

        public double ErrorTopColSize
        { get; set; }

        public double ObjectErosionCircleRadius
        { get; set; }

        public double RegionDilationCircleRadius
        { get; set; }

        public double HolesLowAccuracy
        { get; set; }

        public double HolesHighAccuracy
        { get; set; }

        #endregion


    }


    public class FxHPlasticHolesBlobs : FxBlock
    {
        #region Private Members

        private HImage inputImage;

        private double maxSizeBlobs = 15;
        private double maxSizeHoles = 80;
        private double minSizeBlobs = 2;
        private double minSizeHoles = 5;

        private bool showRegion = false;
        private bool showRegionHoles = false;
        private bool showRegionBlobs = false;
        private bool showUsefulRegion = false;
        private bool showLinesRegion = false;
        private bool showObjectsRegion = false;
        private bool errorRegion = false;

        private bool _runBlockOnce = false;


        double holesNumber;
        double blobsNumber;
        double defectsQuantity;

        private List<OperationList> _operationsList;
        private int _operationIndex;
        private OperationMode _operationMode;

        List<HolesBlobsSettings> _holesBlobsSettings;

        #endregion


        #region Constructors

        public FxHPlasticHolesBlobs() { }

        public FxHPlasticHolesBlobs(string name)
        {
            Name = name;
            Icon = Resource.Default;
            IsMarkAble = true;
            Marks = new List<MarkObject>();
            Text = new List<TextObject>();

            BlockSettings = new List<HolesBlobsSettings>();
        }

        #endregion

        #region Public Methods

        [FilterClass]
        public override void Run()
        {
            Judgement = true;
            Judge = true;
            DateTime startTime = DateTime.Now;

            if (Marks != null)
            {
                Marks.Clear();
                Text.Clear();
            }

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }


            if (BlockOperationMode == OperationMode.SingleImageOperation)
            {
                if (BlockOperations != null && BlockOperations.Count > 0 && BlockOperations[OperationIndex].Image != null && BlockOperations[OperationIndex].Filter == FilterList.HolesBlobs)
                {
                    DrawRectangles();

                    if (!updateRegion)
                    {

                        ROITopRow = BlockOperations[OperationIndex].TopRow;
                        ROIBotRow = BlockOperations[OperationIndex].BottomRow;
                        ROITopCol = BlockOperations[OperationIndex].TopCollumn;
                        ROIBotCol = BlockOperations[OperationIndex].BottomCollumn;
                    }

                    FilterImage(BlockOperations[OperationIndex].Image, OperationIndex);
                }
                else
                {
                    Text.Add(new TextObject { Text = "Operation: #" + OperationIndex + " image does not exist", Color = "red", Font = "-Courier New-16-*-*-*-*-1-", Position = new Point(20, 20) });
                    //RunBlockOnce = true;
                }
            }
            else
            {
                if (BlockOperations != null && BlockOperations.Count > 0)
                {
                    int oper = 0;

                    foreach (var operation in BlockOperations)
                    {
                        OperationIndex = oper;

                        if (operation.Image != null && operation.Filter == FilterList.HolesBlobs) //operation.Filter == FilterList.HolesBlots && operation.Image != null)
                        {
                            ROITopRow = operation.TopRow;
                            ROIBotRow = operation.BottomRow;
                            ROITopCol = operation.TopCollumn;
                            ROIBotCol = operation.BottomCollumn;

                            FilterImage(operation.Image, oper);

                        }
                        oper++;
                        /*else
                        {
                            RunBlockOnce = true;
                        }*/
                    }

                }
            }

            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }

        public override void Clear()
        {
            if (InputImage != null) InputImage.Dispose();
        }

        #endregion


        #region Private methods

        void FilterImage(HImage image, int operationIndex)
        {
            try
            {
                
                HRegion region = new HRegion();
                HRegion regionFillUp = new HRegion();
                HRegion linesRegion = new HRegion();
                HRegion objectRegion = new HRegion();
                HRegion temporaryRegion = new HRegion();
                HRegion holesCont = new HRegion();
                HRegion blobsCont = new HRegion();
                HRegion region2 = new HRegion();
                HRegion rectRegion = new HRegion();
                HImage rectImage;
                HImage imgReduce;
                HImage imgMean;
                HXLDCont edges;
                HRegion holesRegion = new HRegion();
                HRegion regionDilation = new HRegion();
                HRegion regionErosion = new HRegion();
                HObject cont_;

               holesNumber = 0;
               blobsNumber = 0;

               Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                    UpdateThreshold(operationIndex);
                    BlockOperations[operationIndex].Marks = new List<MarkObject>();
                    BlockOperations[operationIndex].Text = new List<TextObject>();

                    if (maxSizeBlobs <= minSizeBlobs) maxSizeBlobs = minSizeBlobs + 1;
                    if (maxSizeHoles <= minSizeHoles) maxSizeHoles = minSizeHoles + 1;

                    region.GenRectangle1(ROITopRow, ROITopCol, ROIBotRow, ROIBotCol);
                    rectImage = image.ReduceDomain(region);
                    HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow, (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);


                    //**************************2015-01-14*******************************
                    region = rectImage.Threshold(BlockSettings[operationIndex].MinimumRegionThreshold, BlockSettings[operationIndex].MaximumRegionThreshold);
                    regionFillUp = region.FillUp();
                    region = regionFillUp.Difference(region);


                    if (errorRegion)
                    {
                        imgReduce = rectImage.ReduceDomain(region);
                        region.GenRectangle1(ROITopRow, ROITopCol + BlockSettings[operationIndex].ErrorTopColSize, ROIBotRow - BlockSettings[operationIndex].ErrorBotRowSize, ROIBotCol - BlockSettings[operationIndex].ErrorBotColSize);
                        rectImage = imgReduce.ReduceDomain(region);
                        region = rectImage.Threshold((double)0, 255);
                    }


                    region = region.DilationCircle(BlockSettings[operationIndex].RegionDilationCircleRadius);
                    region = region.ErosionCircle(BlockSettings[operationIndex].ObjectErosionCircleRadius);
                    region = region.Connection();
                    region = region.SelectShape("area", "and", BlockSettings[operationIndex].MinimumRegionShape, BlockSettings[operationIndex].MaximumRegionShape);
                    //**************************2015-01-14*******************************

                    if (onMessage != null) onMessage(this, "1");

                    if (ShowRegion)
                    {
                        Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = region });
                        BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = region });
                    }

                    imgReduce = rectImage.ReduceDomain(region);
                    imgMean = imgReduce.MeanImage((int)3, 3);

                    if (onMessage != null) onMessage(this, "2");
               

                try
                {
                    //Unnecessary region
                    linesRegion = imgMean.Threshold(BlockSettings[operationIndex].MinimumLinesThreshold, BlockSettings[operationIndex].MaximumLinesThreshold);
                    linesRegion = linesRegion.ErosionCircle(BlockSettings[operationIndex].ErosionCircleRadius);
                    linesRegion = linesRegion.Connection();
                    linesRegion = linesRegion.DilationCircle(BlockSettings[operationIndex].DilationCircleRadius);
                    linesRegion = linesRegion.SelectShape("area", "and", BlockSettings[operationIndex].MinimumLinesShape, BlockSettings[operationIndex].MaximumLinesShape);

                    if (onMessage != null) onMessage(this, "3");

                    if (ShowLinesRegion)
                    {
                        Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = linesRegion });
                        BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = linesRegion });
                    }

                    objectRegion = imgMean.Threshold(BlockSettings[operationIndex].MinimumObjectThreshold, BlockSettings[operationIndex].MaximumObjectThreshold);
                    objectRegion = objectRegion.Connection();
                    objectRegion = objectRegion.SelectShape("area", "and", BlockSettings[operationIndex].MinimumObjectSize, BlockSettings[operationIndex].MaximumObjectSize);
                    objectRegion = objectRegion.DilationCircle(BlockSettings[operationIndex].ObjectDilationCircleRadius);
                    objectRegion = objectRegion.FillUp();

                    if (onMessage != null) onMessage(this, "4");

                    if (ShowObjectsRegion)
                    {
                        Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = objectRegion });
                        BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = objectRegion });
                    }

                    objectRegion = objectRegion.Union2(linesRegion);
                }
                catch(HOperatorException exception)
                {
                    FxSessionTimeClass.LogErrorMessage("(HHoles Blobs filter - Stage II) ", exception.GetErrorText());
                }

                try
                {
                    objectRegion = region.Difference(objectRegion);

                    if (onMessage != null) onMessage(this, "5");

                    if (ShowUsefulRegion)
                    {
                        Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = objectRegion });
                        BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = objectRegion });
                    }

                    imgMean = imgMean.ReduceDomain(objectRegion);
                }
                catch(HOperatorException exception)
                {
                    FxSessionTimeClass.LogErrorMessage("(HHoles Blobs filter - Stage III) ", exception.GetErrorText());
                }

                try
                {
                    //Holes analysis
                    edges = imgMean.EdgesSubPix("canny", (double)1, BlockSettings[operationIndex].HolesLowAccuracy, BlockSettings[operationIndex].HolesHighAccuracy);//10, 30);

                    //px -> mm
                    if (MeasurementMode)
                        edges = edges.SelectShapeXld("area", "and", minSizeHoles * MeasurementCoefficient, maxSizeHoles * MeasurementCoefficient);
                    else
                        edges = edges.SelectShapeXld("area", "and", minSizeHoles, maxSizeHoles);


                    edges = edges.SelectShapeXld("circularity", "and", 0.3, 1);
                    holesRegion = edges.GenRegionContourXld("filled");
                    holesCont = holesRegion.Boundary("outer");

                    if (onMessage != null) onMessage(this, "6");

                    if (ShowRegionHoles)
                    {
                        Marks.Add(new MarkObject { Name = "ShowRegion", Color = "blue", Object = holesCont });
                        BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "ShowRegion", Color = "blue", Object = holesCont });
                    }
                }
                catch (HOperatorException exception)
                {
                    FxSessionTimeClass.LogErrorMessage("(HHoles Blobs filter - Stage IV) ", exception.GetErrorText());
                }

                try
                {
                    //Blobs analysis
                    try
                    {
                        region = imgMean.Threshold(MinRegionThresholdBlobs, MaxRegionThresholdBlobs);
                        region = region.Connection();

                        //px -> mm
                        if (MeasurementMode)
                            region = region.SelectShape("area", "and", minSizeBlobs * MeasurementCoefficient, maxSizeBlobs * MeasurementCoefficient);
                        else
                            region = region.SelectShape("area", "and", minSizeBlobs, maxSizeBlobs);

                        regionDilation = region.DilationCircle((double)2);

                        if (onMessage != null) onMessage(this, "7");
                    }
                    catch { }

                    if (ShowRegionBlobs)
                    {
                        Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = regionDilation });
                        BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = regionDilation });
                    }
                }
                catch(HOperatorException exception)
                {
                    FxSessionTimeClass.LogErrorMessage("(HHoles Blobs filter - Stage V) ", exception.GetErrorText());
                }

                try
                {
                    //Counted the number of holes.
                    holesRegion = holesRegion.Connection();
                    holesRegion = holesRegion.SelectShape("area", "and", (double)0, 80);
                    holesNumber = holesRegion.CountObj();
                }
                catch { }

                try
                {
                    //Counted the number of blobs.
                    region = region.Connection();
                    region = region.SelectShape("area", "and", (double)0, 80);
                    blobsNumber = region.CountObj();
                }
                catch { }

                defectsQuantity = holesNumber + blobsNumber;

                Judgement &= defectsQuantity > 0 || !Judgement ? false : true;
                Judge = defectsQuantity > 0 ? false : true;
                string color = !Judge ? "red" : "green";

                Marks.Add(new MarkObject { Name = "Contour", Color = color, Object = cont_ });
                BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Contour", Color = color, Object = cont_ });

                //Temporary variables
                Text.Add(new TextObject { Text = "Blobs:" + blobsNumber.ToString(), Color = "red", Font = "-Courier New-18-*-*-*-*-1-", Position = new Point((int)10, (int)50) });
                BlockOperations[operationIndex].Text.Add(new TextObject { Text = "Blobs:" + blobsNumber.ToString(), Color = "red", Font = "-Courier New-18-*-*-*-*-1-", Position = new Point((int)10, (int)50) });
                Text.Add(new TextObject { Text = "Holes:" + holesNumber.ToString(), Color = "blue", Font = "-Courier New-18-*-*-*-*-1-", Position = new Point((int)30, (int)50) });
                BlockOperations[operationIndex].Text.Add(new TextObject { Text = "Holes:" + holesNumber.ToString(), Color = "blue", Font = "-Courier New-18-*-*-*-*-1-", Position = new Point((int)30, (int)50) });

                HImage = image.Clone();

                if (operationIndex >= 0)
                {
                    Text.Add(new TextObject { Text = "Operation: #" + operationIndex, Color = color, Font = "-Courier New-14-*-*-*-*-1-", Position = new Point((int)ROITopRow - 20, (int)ROITopCol + 10) });
                    BlockOperations[operationIndex].Text.Add(new TextObject { Text = "Operation: #" + operationIndex, Color = color, Font = "-Courier New-14-*-*-*-*-1-", Position = new Point((int)ROITopRow - 20, (int)ROITopCol + 10) });
                }
                
                stopwatch.Stop();
                Text.Add(new TextObject { Text = "Elapsed time: " + stopwatch.ElapsedMilliseconds + "ms", Color = color, Font = "-Courier New-14-*-*-*-*-1-", Position = new Point((int)200, 50) });
                BlockOperations[operationIndex].Text.Add(new TextObject { Text = "Elapsed time: " + stopwatch.ElapsedMilliseconds + "ms", Color = color, Font = "-Courier New-14-*-*-*-*-1-", Position = new Point((int)200, 50) });
            }
            catch (HOperatorException exception)
            {
                FxSessionTimeClass.LogErrorMessage("(HHoles Blobs filter - Stage I) ", exception.GetErrorText());
                Judgement = false;
                Judge = false;
            }
            BlockOperations[operationIndex].Judgement = Judge;
        }


        public void UpdateThreshold(int index)
        {
            if (BlockSettings != null && BlockSettings.Count > 0 && BlockSettings.Count > index)
            {
                MinRegionThreshold = BlockSettings[index].MinimumRegionThreshold;
                MaxRegionThreshold = BlockSettings[index].MaximumRegionThreshold;
                MinLinesThreshold = BlockSettings[index].MinimumLinesThreshold;
                MaxLinesThreshold = BlockSettings[index].MaximumLinesThreshold;
                MinObjectThreshold = BlockSettings[index].MinimumObjectThreshold;
                MaxObjectThreshold = BlockSettings[index].MaximumObjectThreshold;
                MinObjectSize = BlockSettings[index].MinimumObjectSize;
                MaxObjectSize = BlockSettings[index].MaximumObjectSize;
                LinesDilationCircleRadius = BlockSettings[index].DilationCircleRadius;
                LinesErosionCircleRadius = BlockSettings[index].ErosionCircleRadius;
            }
        }



        #endregion


        #region UI mouse clicks interaction

        private double roiTopCol = 200;
        private double roiTopRow = 150;
        private double roiBotCol = 400;
        private double roiBotRow = 315;
        private double roiStartRow = 200;
        private double roiStartCol = 150;
        private double roiEndRow = 315;
        private double roiEndCol = 400;
        private double sx, sy, dx, dy;
        private bool IsDiamBusy;

        private bool updateRegion = false;

        private void DrawRectangles()
        {
            HRegion rect_ = new HRegion();
            HRegion region = new HRegion();
            HObject cont_;

            HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow, (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);

            Marks.Add(new MarkObject { Name = "Contour", Color = "blue", Object = cont_ });
        }


        public void UpdateROI()
        {
            if (updateRegion)
            {
                BlockOperations[OperationIndex].TopRow = ROITopRow;
                BlockOperations[OperationIndex].BottomRow = ROIBotRow;
                BlockOperations[OperationIndex].TopCollumn = ROITopCol;
                BlockOperations[OperationIndex].BottomCollumn = ROIBotCol;
            }
        }

        public void HOnMouseDown(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                sx = (int)e.X;
                sy = (int)e.Y;

                if (sx > (ROITopCol - 15) && sx < (ROITopCol + 15) && sy > (ROITopRow - 15) && sy < (ROITopRow + 15))
                {
                    roiStartRow = ROITopRow;
                    roiStartCol = ROITopCol;
                    IsDiamBusy = false;
                }
                else
                {
                    roiEndRow = ROIBotRow;
                    roiEndCol = ROIBotCol;
                    IsDiamBusy = true;
                }

                updateRegion = true;
            }
        }

        public void HOnMouseUp(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                UpdateROI();
                updateRegion = false;
            }
        }

        public void HOnMouseMove(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {

                dx = (int)e.X - sx;
                dy = (int)e.Y - sy;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    if (!IsDiamBusy)
                    {
                        ROITopCol = roiStartCol + dx;
                        ROITopRow = roiStartRow + dy;
                    }
                    else
                    {
                        ROIBotCol = roiEndCol + dx;
                        ROIBotRow = roiEndRow + dy;
                    }

                }
            }
        }

        public void HOnMouseWheel(object sender, HMouseEventArgs e)
        {

        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top row position.")]
        [DisplayName("ROI top row")]
        public double ROITopRow
        {
            get
            {
                return roiTopRow;
            }
            set
            {
                roiTopRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot row position.")]
        [DisplayName("ROI bot row")]
        public double ROIBotRow
        {
            get
            {
                return roiBotRow;
            }
            set
            {
                roiBotRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top col position.")]
        [DisplayName("ROI top col")]
        public double ROITopCol
        {
            get
            {
                return roiTopCol;
            }
            set
            {
                roiTopCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopCol"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot col position.")]
        [DisplayName("ROI bot col")]
        public double ROIBotCol
        {
            get
            {
                return roiBotCol;
            }
            set
            {
                roiBotCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotCol"));
            }
        }


        #endregion


        #region Public Properties

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage InputImage
        {
            get
            {
                return inputImage;
            }
            set
            {
                inputImage = value != null ? value.CopyImage() : null;
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage _hImage { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage HImage
        {
            get
            {
                return _hImage;
            }
            set
            {
                _hImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HImage"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<HImage> InputImages { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public override Bitmap Icon { set; get; }

        [Browsable(false), BlockNameAttribute]
        public override string Name { set; get; }



        #region Properties from image grab block


        [CategoryAttribute("Block Settings")]
        [Description("Switch between single & multiple image grabbing")]
        [Browsable(false)]
        public OperationMode BlockOperationMode
        {
            get
            {
                return _operationMode;
            }
            set
            {
                _operationMode = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperationMode"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("Currently displayed operation (image)")]
        [XmlIgnoreAttribute, Browsable(false)]
        public int OperationIndex
        {
            get
            {
                return _operationIndex;
            }
            set
            {
                _operationIndex = value;

                OnPropertyChanged(this, new PropertyChangedEventArgs("OperationIndex"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("List of operations")]
        [XmlIgnoreAttribute]
        public List<OperationList> BlockOperations
        {
            get
            {
                return _operationsList;
            }
            set
            {
                _operationsList = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [XmlIgnoreAttribute, Browsable(false)]
        public bool RunBlockOnce
        {
            get { return _runBlockOnce; }
            set
            {
                _runBlockOnce = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("RunBlockOnce"));
            }
        }

        


        #endregion

        [CategoryAttribute("Stage V: find blobs")]
        [Description("All images with smaller blobs will be considered as negative.")]
        [DisplayName("4. Maximum blob size")]
        public double MaxSizeBlobs
        {
            get
            {
                return maxSizeBlobs;
            }
            set
            {
                maxSizeBlobs = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxSizeBlobs"));
            }
        }

        [CategoryAttribute("Stage IV: find holes")]
        [Description("All images with smaller holes will be considered as negative.")]
        [DisplayName("4. Maximum hole size")]
        public double MaxSizeHoles
        {
            get
            {
                return maxSizeHoles;
            }
            set
            {
                maxSizeHoles = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxSizeHoles"));
            }
        }

        [CategoryAttribute("Stage V: find blobs")]
        [Description("All images with bigger blobs will be considered as negative.")]
        [DisplayName("3. Minimum blob size")]
        public double MinSizeBlobs
        {
            get
            {
                return minSizeBlobs;
            }
            set
            {
                minSizeBlobs = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinSizeBlobs"));
            }
        }

        [CategoryAttribute("Stage IV: find holes")]
        [Description("All images with bigger holes will be considered as negative.")]
        [DisplayName("3. Minimum hole size")]
        public double MinSizeHoles
        {
            get
            {
                return minSizeHoles;
            }
            set
            {
                minSizeHoles = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinSizeHoles"));
            }
        }




        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("The minimum threshold for the analyzed region.")]
        [DisplayName(" 1. Min region threshold")]
        public double MinRegionThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumRegionThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].MinimumRegionThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumRegionThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    MinRegionThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinRegionThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("The maximum threshold for the analyzed region.")]
        [DisplayName(" 2. Max region threshold")]
        public double MaxRegionThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumRegionThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].MaximumRegionThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumRegionThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    MaxRegionThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxRegionThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Show calibration region")]
        [DisplayName(" 7. Show region")]
        public bool ShowRegion
        {
            get
            {
                return showRegion;
            }
            set
            {
                showRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowRegion"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage III: unuseful object region")]
        [Description("Show useful region.")]
        [DisplayName("7. Show useful region")]
        public bool ShowUsefulRegion
        {
            get
            {
                return showUsefulRegion;
            }
            set
            {
                showUsefulRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowUsefulRegion"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: unuseful lines region")]
        [Description("Show lines region.")]
        [DisplayName("7. Show lines region")]
        public bool ShowLinesRegion
        {
            get
            {
                return showLinesRegion;
            }
            set
            {
                showLinesRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowLinesRegion"));
            }
        }

        [CategoryAttribute("Stage I: useful region")]
        [Description("Parameter is used to reduce the region.")]
        [DisplayName(" 8. Use the reduced area")]
        public bool ErrorRegion
        {
            get
            {
                return errorRegion;
            }
            set
            {
                errorRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("errorRegion"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage III: unuseful object region")]
        [Description("Show objects region.")]
        [DisplayName("6. Show objects region")]
        public bool ShowObjectsRegion
        {
            get
            {
                return showObjectsRegion;
            }
            set
            {
                showObjectsRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowObjectsRegion"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: unuseful lines region")]
        [Description("Minimum limit unnecessary lines.")]
        [DisplayName("1. Min lines threshold")]
        public double MinLinesThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumLinesThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].MinimumLinesThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumLinesThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    MinLinesThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinLinesThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: unuseful lines region")]
        [Description("Maximum limit unnecessary lines.")]
        [DisplayName("2. Max lines threshold")]
        public double MaxLinesThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumLinesThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].MaximumLinesThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumLinesThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    MaxLinesThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxLinesThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage III: unuseful object region")]
        [Description("Minimum limit unnecessary objects.")]
        [DisplayName("1. Min object threshold")]
        public double MinObjectThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumObjectThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].MinimumObjectThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumObjectThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    MinObjectThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinObjectThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage III: unuseful object region")]
        [Description("Maximum limit unnecessary objects.")]
        [DisplayName("2. Max object threshold")]
        public double MaxObjectThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumObjectThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].MaximumObjectThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumObjectThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    MaxObjectThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxObjectThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Filterable size of the region.")]
        [DisplayName(" 3. Min region size")]
        public double MinRegionShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumRegionShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].MinimumRegionShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumRegionShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    MinRegionShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinRegionShape"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Filterable size of the region.")]
        [DisplayName(" 4. Max region size")]
        public double MaxRegionShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumRegionShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].MaximumRegionShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumRegionShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    MaxRegionShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxRegionShape"));
            }
        }
        
        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Useful region edges erosion circle radius.")]
        [DisplayName(" 6. Region erosion circle radius")]
        public double ObjectErosionCircle
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].ObjectErosionCircleRadius;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].ObjectErosionCircleRadius;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].ObjectErosionCircleRadius = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    ObjectErosionCircle = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("ObjectErosionCircle"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Useful region edges dilation circle radius.")]
        [DisplayName(" 5. Region dilation circle radius")]
        public double RegionDilationCircle
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].RegionDilationCircleRadius;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].RegionDilationCircleRadius;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].RegionDilationCircleRadius = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    RegionDilationCircle = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("RegionDilationCircle"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: unuseful lines region")]
        [Description("Filterable size of the lines region.")]
        [DisplayName("5. Min lines size")]
        public double MinLinesShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumLinesShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].MinimumLinesShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumLinesShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    MinLinesShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinLinesShape"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: unuseful lines region")]
        [Description("Filterable size of the lines region.")]
        [DisplayName("6. Max lines size")]
        public double MaxLinesShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumLinesShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].MaximumLinesShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumLinesShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    MaxLinesShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxLinesShape"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: unuseful lines region")]
        [Description("Unnecessary lines erosion circle radius.")]
        [DisplayName("3. Lines erosion circle radius")]
        public double LinesErosionCircleRadius
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].ErosionCircleRadius;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].ErosionCircleRadius;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].ErosionCircleRadius = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    LinesErosionCircleRadius = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("LinesErosionCircleRadius"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: unuseful lines region")]
        [Description("Unnecessary lines dilation circle radius.")]
        [DisplayName("4. Lines dilation circle radius")]
        public double LinesDilationCircleRadius
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].DilationCircleRadius;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].DilationCircleRadius;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].DilationCircleRadius = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    LinesDilationCircleRadius = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("LinesDilationCircleRadius"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage IV: find holes")]
        [Description("Analyzed for finding holes in the threshold region.")]
        [DisplayName("1. Min holes threshold")]
        public double MinRegionThresholdHoles
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumRegionThresholdHoles;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].MinimumRegionThresholdHoles;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumRegionThresholdHoles = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    MinRegionThresholdHoles = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinRegionThresholdHoles"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage IV: find holes")]
        [Description("Analyzed for finding holes in the threshold region.")]
        [DisplayName("2. Max holes threshold")]
        public double MaxRegionThresholdHoles
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumRegionThresholdHoles;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].MaximumRegionThresholdHoles;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumRegionThresholdHoles = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    MaxRegionThresholdHoles = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxRegionThresholdHoles"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage V: find blobs")]
        [Description("Analyzed for finding blobs in the threshold region.")]
        [DisplayName("1. Min blobs threshold")]
        public double MinRegionThresholdBlobs
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumRegionThresholdBlobs;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].MinimumRegionThresholdBlobs;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumRegionThresholdBlobs = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    MinRegionThresholdBlobs = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinRegionThresholdBlobs"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage V: find blobs")]
        [Description("Analyzed for finding blobs in the threshold region.")]
        [DisplayName("2. Max blobs threshold")]
        public double MaxRegionThresholdBlobs
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumRegionThresholdBlobs;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].MaximumRegionThresholdBlobs;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumRegionThresholdBlobs = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    MaxRegionThresholdBlobs = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxRegionThresholdBlobs"));
            }
        }

        [CategoryAttribute("Stage IV: find holes")]
        [Description("Show holes region")]
        [DisplayName("5. Show holes region")]
        public bool ShowRegionHoles
        {
            get
            {
                return showRegionHoles;
            }
            set
            {
                showRegionHoles = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowRegionHoles"));
            }
        }

        [CategoryAttribute("Stage V: find blobs")]
        [Description("Show blobs region")]
        [DisplayName("5. Show blobs region")]
        public bool ShowRegionBlobs
        {
            get
            {
                return showRegionBlobs;
            }
            set
            {
                showRegionBlobs = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowRegionBlobs"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage III: unuseful object region")]
        [Description("Filterable size of the object.")]
        [DisplayName("3. Min object size")]
        public double MinObjectSize
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumObjectSize;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].MinimumObjectSize;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumObjectSize = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    MinObjectSize = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinObjectSize"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage III: unuseful object region")]
        [Description("Filterable size of the object.")]
        [DisplayName("4. Max object size")]
        public double MaxObjectSize
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumObjectSize;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].MaximumObjectSize;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumObjectSize = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    MaxObjectSize = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxObjectSize"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage III: unuseful object region")]
        [Description("Unnecessary objects dilation circle radius.")]
        [DisplayName("5. Objects dilation circle radius")]
        public double ObjectDilationCircle
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].ObjectDilationCircleRadius;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].ObjectDilationCircleRadius;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].ObjectDilationCircleRadius = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    ObjectDilationCircle = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("ObjectDilationCircle"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Analyzed area reduction. Visual field reduction of the bottom side.")]
        [DisplayName(" 9. Reduction of the area of the bottom (BotCol)")]
        public double BotRowErrorSize
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].ErrorBotRowSize;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].ErrorBotRowSize;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].ErrorBotRowSize = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    BotRowErrorSize = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("BotRowErrorSize"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Analyzed area reduction. Visual field reduction of the right side.")]
        [DisplayName("10. Reduction of the area of the right (BotCol)")]
        public double BotColErrorSize
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].ErrorBotColSize;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].ErrorBotColSize;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].ErrorBotColSize = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    BotColErrorSize = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("BotColErrorSize"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Analyzed area reduction. Visual field reduction of the left side.")]
        [DisplayName("11. Reduction of the area of the left (TopCol)")]
        public double TopColErrorSize
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].ErrorTopColSize;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].ErrorTopColSize;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].ErrorTopColSize = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    TopColErrorSize = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("TopColErrorSize"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage IV: find holes")]
        [Description("Holes in the lower limit of detection accuracy.")]
        [DisplayName("6. Holes accuracy (low)")]
        public double LowerHolesAccuracy
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].HolesLowAccuracy;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].HolesLowAccuracy;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].HolesLowAccuracy = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    LowerHolesAccuracy = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("LowerHolesAccuracy"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage IV: find holes")]
        [Description("Holes in the higher limit of detection accuracy.")]
        [DisplayName("7. Holes accuracy (high)")]
        public double HigherHolesAccuracy
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].HolesHighAccuracy;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    return BlockSettings[OperationIndex].HolesHighAccuracy;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].HolesHighAccuracy = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new HolesBlobsSettings());
                    HigherHolesAccuracy = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("HigherHolesAccuracy"));
            }
        }

        [CategoryAttribute("Calibration")]
        [Description("Holes and blobs settings")]
        public List<HolesBlobsSettings> BlockSettings
        {
            get
            {
                return _holesBlobsSettings;
            }
            set
            {
                _holesBlobsSettings = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockSettings"));
            }
        }

        #endregion


        #region Buttons


        [ButtonControl]
        public void NextImage()
        {
            if (BlockOperationMode == OperationMode.SingleImageOperation && BlockOperations != null && BlockOperations.Count > 0)
            {
                OperationIndex = OperationIndex < BlockOperations.Count - 1 ? OperationIndex + 1 : OperationIndex;
                RunBlockOnce = true;
            }
        }

        [ButtonControl]
        public void PreviousImage()
        {
            if (BlockOperationMode == OperationMode.SingleImageOperation && BlockOperations != null && BlockOperations.Count > 0)
            {
                OperationIndex = OperationIndex > 0 ? OperationIndex - 1 : OperationIndex;
                RunBlockOnce = true;
            }
        }

        #endregion

        #region Events
        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;


        #endregion
    }
}