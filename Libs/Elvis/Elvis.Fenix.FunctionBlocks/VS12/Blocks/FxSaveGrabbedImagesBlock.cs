﻿//using HalconDotNet;
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Drawing.Design;
using System.Globalization;

namespace Elvis.Fenix.FunctionBlocks
{

    //public delegate void ImageLoadHandler(Object sender);

    public class FxSaveGrabbedImagesBlock : FxBlock
    {
        #region Private Members

        string _imageDirectory = @"C:\Fenix\Log Images";
        string _sceneName = ""; // = "NaN";
        string _sessionImagesDir = "";// = "NaN";
        string _prefix = "IMG";
        
        string[] _sessionsList;
        string _fileName = "";
        int currentIdx;
        //HImage _image;

        //public event ImageLoadHandler onImageLoaded;  
        //int initIdx;
        //string _currentSession;
        bool _saveGrabbedImages = false;
        bool _loadImagesFromFile = false;
        bool _useSingleImage;


        #endregion

        #region Constructors

        public FxSaveGrabbedImagesBlock() { }

        public FxSaveGrabbedImagesBlock(string name)
        {
            Name = name;
            Icon = Resource.SaveImage;            
        }

        #endregion

        #region Public Methods

        public override void Dispose()
        {

        }

        /// <summary>
        /// Main run method
        /// </summary>
        public override void Run() 
        {
            Judgement = true;

            DateTime startTime = DateTime.Now;

            if (Marks != null)
            {
                Marks.Clear();
                Text.Clear();
            }

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            try 
            {
                 //if(EnableImageLoader)
                 //{
                 //    LoadSessionFolderByIndex(currentIdx);
                 //}
            }
            catch(Exception ex)
            {
                Judgement = false;
            }

            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }

        /// <summary>
        /// Memory clean method
        /// </summary>
        public override void Clear() 
        { 
            
        }

        public override void Stop() { }



        public override void Exit() { }



        public void LoadSessionFolders()
        {
            if (System.IO.Directory.Exists(SelectedSessionImagesDir))
            {
               
                 _sessionsList = Directory.GetDirectories(SelectedSessionImagesDir);

                //ProcessDirectory(SelectedSessionImagesDir);

                currentIdx = 0;
                LoadSessionFolderByIndex(currentIdx);
            }
        }

        public void LoadNextSessionFolder()
        {
            if (_sessionsList != null)
            {
                currentIdx = currentIdx < _sessionsList.Length - 1 ? currentIdx + 1 : currentIdx;
                LoadSessionFolderByIndex(currentIdx);
            }
        }

        public void LoadPrevSessionFolder()
        {
            if (_sessionsList != null)
            {
                currentIdx = currentIdx > 0 ? currentIdx - 1 : currentIdx;
                LoadSessionFolderByIndex(currentIdx);
            }
        }

        public void LoadSessionFolderByIndex(int index)
        {
            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            if(_sessionsList != null & _sessionsList.Length > index && index >= 0)
            {
                try
                {
                    currentIdx = index;
                    SelectedSessionImagesDir = _sessionsList[currentIdx];
                    FileName = _sessionsList[currentIdx];
                }
                catch (Exception ex)
                {
                    SelectedSessionImagesDir = null;
                }
            }

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }


        public void SaveImages(int sideNumber)
        {
            //try
            //{
            //    string path = FilePath + @"\" + DateTime.Now.ToString("yyyy-MM-dd") + @"\" + SceneName + @"\Session-" + FxSessionTimeClass.SessionStartTime + @"\" + Name;
            //    if (!Directory.Exists(path))
            //    {
            //        Directory.CreateDirectory(path);
            //    }

            //    string imageNumber;

            //    int i = 0;

            //    switch(sideNumber)
            //    {
            //        case 1:
            //            if (BlockOperations1 != null && BlockOperations1.Count > 0)
            //            {
            //                foreach (var operation in BlockOperations1)
            //                {
            //                    if (i < 10)
            //                    {
            //                        imageNumber = "0" + i.ToString();
            //                    }
            //                    else
            //                    {
            //                        imageNumber = i.ToString();
            //                    }

            //                    if (operation.Image != null)
            //                        operation.Image.WriteImage("tiff", 0, path + @"\" + Prefix + " (" + imageNumber + ")");

            //                    i++;
            //                }
            //            }
            //            break;

            //        case 2:
            //            if (BlockOperations2 != null && BlockOperations2.Count > 0)
            //            {
            //                foreach (var operation in BlockOperations2)
            //                {
            //                    if (i < 10)
            //                    {
            //                        imageNumber = "0" + i.ToString();
            //                    }
            //                    else
            //                    {
            //                        imageNumber = i.ToString();
            //                    }

            //                    if (operation.Image != null)
            //                        operation.Image.WriteImage("tiff", 0, path + @"\" + Prefix + " (" + imageNumber + ")");

            //                    i++;
            //                }
            //            }
            //            break;
            //    }

                

                
            //}
            //catch(Exception ex)
            //{
            //    FxSessionTimeClass.LogErrorMessage("(" + Name + ") ", ex.Message);
            //}


        }

        // Process all files in the directory passed in, recurse on any directories  
        // that are found, and process the files they contain. 
        //void ProcessDirectory(string targetDirectory)
        //{
        //    // Process the list of files found in the directory. 
        //    string[] fileEntries = Directory.GetFiles(targetDirectory);
        //    foreach (string fileName in fileEntries)
        //        ProcessFile(fileName);

        //    // Recurse into subdirectories of this directory. 
        //    string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
        //    foreach (string subdirectory in subdirectoryEntries)
        //        ProcessDirectory(subdirectory);
        //}

        //// Insert logic for processing found files here. 
        //void ProcessFile(string path)
        //{
        //    Console.WriteLine("Processed file '{0}'.", path);
        //}

        #endregion


        #region Events

        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;
        //private List<OperationList> _operationsList;
        //private List<OperationList> _operationsList1;
        //private List<OperationList> _operationsList2;
        

        #endregion

        #region Public Properties

        //[XmlIgnoreAttribute]
        //[CategoryAttribute("Block Settings1")]
        //[Description("List of operations side 1")]
        //public List<OperationList> BlockOperations1
        //{
        //    get
        //    {
        //        return _operationsList1;
        //    }
        //    set
        //    {
        //        _operationsList1 = value;
        //        OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations1"));
        //    }
        //}

        //[XmlIgnoreAttribute]
        //[CategoryAttribute("Block Settings2")]
        //[Description("List of operations side 2")]
        //public List<OperationList> BlockOperations2
        //{
        //    get
        //    {
        //        return _operationsList2;
        //    }
        //    set
        //    {
        //        _operationsList2 = value;
        //        OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations2"));
        //    }
        //}

        /// <summary>
        /// Block name
        /// </summary>
        [Browsable(false), BlockNameAttribute]
        public override string Name { set; get; }

        /// <summary>
        /// Block icon
        /// </summary>
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public override System.Drawing.Bitmap Icon { get; set; }

        //[Browsable(false)]
        //[XmlIgnoreAttribute]
        //public HImage CurrentImage
        //{
        //    get { return _image; }
        //    set
        //    {
        //        _image = value;
        //        OnPropertyChanged(this, new PropertyChangedEventArgs("CurrentImage"));
        //    }
        //}


        [Description("Session location to load images from")]
        public string SelectedSessionImagesDir
        {
            get
            {
                return _sessionImagesDir;
            }

            set
            {
                _sessionImagesDir = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SelectedSessionImagesDir"));
            }
        }

        [Description("Location where grabbed images are stored")]
        public string FilePath
        {
            get
            {
                return _imageDirectory;
            }

            set
            {
                _imageDirectory = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("FilePath"));
            }
        }

        [Description("Current scene name")]
        public string SceneName
        {
            get
            {
                return _sceneName;
            }
            set
            {
                _sceneName = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SceneName"));
            }
        }

        [Description("Saved image prefix (name)")]
        public string Prefix
        {
            get
            {
                return _prefix;
            }
            set
            {
                _prefix = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Prefix"));
            }
        }

        [Description("Saved image prefix (name)")]
        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("FileName"));
            }
        }

        //[XmlIgnoreAttribute]
        [Description("Save grabbed images?")]
        public bool SaveGrabbedImages
        {
            get
            {
                return _saveGrabbedImages;
            }
            set
            {
                _saveGrabbedImages = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SaveGrabbedImages"));
            }
        }

        [XmlIgnoreAttribute]
        [Description("Load images from files?")]
        public bool EnableImageLoader
        {
            get
            {
                return _loadImagesFromFile;
            }
            set
            {
                _loadImagesFromFile = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("EnableImageLoader"));
            }
        }

        [Description("Use only 1 image")]
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public bool UseSingleImage
        {
            get
            {
                return _useSingleImage;
            }
            set
            {
                _useSingleImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("UseSingleImage"));
            }
        }

        #endregion


        #region Buttons

        //[ButtonControl]
        //public void SelectLocationToSaveImages()
        //{
        //    FolderBrowserDialog folderBrowser = new FolderBrowserDialog();

        //    folderBrowser.RootFolder = Environment.SpecialFolder.MyComputer;
        //    folderBrowser.SelectedPath = _imageDirectory;
        //    folderBrowser.Description = "Select grabbed images location";

        //    if (folderBrowser.ShowDialog() == DialogResult.OK)
        //    {
        //        _imageDirectory = folderBrowser.SelectedPath;
        //    }
        //}

        //[ButtonControl]
        //public void SelectSessionPath()
        //{
        //    FolderBrowserDialog folderBrowser = new FolderBrowserDialog();

        //    folderBrowser.RootFolder = Environment.SpecialFolder.MyComputer;
        //    folderBrowser.SelectedPath = _imageDirectory;
        //    folderBrowser.Description = "Select session folder";

        //    if (folderBrowser.ShowDialog() == DialogResult.OK)
        //    {
        //        SelectedSessionImagesDir = folderBrowser.SelectedPath;
        //    }

        //}

        #endregion

    }


    
}
