﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using System.Diagnostics;
using System.Threading;

namespace Elvis.Fenix.FunctionBlocks
{
    public class RubberPlasticContactSettings
    {
        #region Constructors

        public RubberPlasticContactSettings()
        {
            LowContourThreshold = 10;
            HighContourThreshold = 15;
            MinContourShape = 3000;
            MaxContourShape = 3500;
            EdgeDilationCircle = 10;
            MinEdgeShape = 450;
            MaxEdgeShape = 3000;
            MinRegionDilationCircle = 5;
            MinDefectsThreshold = 5;
            MaxDefectsThreshold = 12;
            MaxRegionDilationCircle = 5;
            DefectsDilationCircle = 5;
            LowRegionThreshold = 45;
            HighRegionThreshold = 255;
        }

        #endregion


        #region Public properties

        public double LowContourThreshold
        { get; set; }

        public double HighContourThreshold
        { get; set; }

        public double MinContourShape
        { get; set; }

        public double MaxContourShape
        { get; set; }

        public double EdgeDilationCircle
        { get; set; }

        public double MinEdgeShape
        { get; set; }

        public double MaxEdgeShape
        { get; set; }

        public double MinRegionDilationCircle
        { get; set; }

        public double MinDefectsThreshold
        { get; set; }

        public double MaxDefectsThreshold
        { get; set; }

        public double MaxRegionDilationCircle
        { get; set; }

        public double DefectsDilationCircle
        { get; set; }

        public double LowRegionThreshold
        { get; set; }

        public double HighRegionThreshold
        { get; set; }

        #endregion
    }

    public class FxHRubberPlasticContact : FxBlock
    {
        #region Private Members

        List<HImage> list = new List<HImage>();

        private HImage inputImage;

        List<RubberPlasticContactSettings> _rubberPlasticContactSettings;

        private bool _runBlockOnce = false;
        private List<OperationList> _operationsList;
        private int _operationIndex;
        private OperationMode _operationMode;


        private bool showEdge;
        private bool showSelectShape;
        private bool showSelectEdge;
        private bool showMinRegionDilation;
        private bool showMaxRegionDilation;
        private bool showRegionsDifference;
        private bool showSelectedRegion;
        private bool showReducedImage;
        private bool showDefects = true;


        #endregion


        #region Constructors

        public FxHRubberPlasticContact() { }

        public FxHRubberPlasticContact(string name)
        {
            Name = name;
            Icon = Resource.Default;
            IsMarkAble = true;
            Marks = new List<MarkObject>();
            Text = new List<TextObject>();

            BlockSettings = new List<RubberPlasticContactSettings>();
        }

        #endregion


        #region Public Methods

        [FilterClass]
        public override void Run()
        {
            Judgement = true;
            DateTime startTime = DateTime.Now;

            if (Marks != null)
            {
                Marks.Clear();
                Text.Clear();
            }

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            if (BlockOperationMode == OperationMode.SingleImageOperation)
            {
                if (BlockOperations != null && BlockOperations.Count > 0 && BlockOperations[OperationIndex].Image != null)
                {
                    DrawRectangles();

                    if (!updateRegion)
                    {
                        ROITopRow = BlockOperations[OperationIndex].TopRow;
                        ROIBotRow = BlockOperations[OperationIndex].BottomRow;
                        ROITopCol = BlockOperations[OperationIndex].TopCollumn;
                        ROIBotCol = BlockOperations[OperationIndex].BottomCollumn;
                    }

                    FilterImage(BlockOperations[OperationIndex].Image, OperationIndex);
                }
                else
                {
                    Text.Add(new TextObject { Text = "Operation: #" + OperationIndex + " image does not exist", Color = "red", Font = "-Courier New-16-*-*-*-*-1-", Position = new Point(20, 20) });
                    //RunBlockOnce = true;
                }
            }
            else
            {
                if (BlockOperations != null && BlockOperations.Count > 0)
                {
                    int oper = 0;

                    foreach (var operation in BlockOperations)
                    {
                        OperationIndex = oper;

                        if (operation.Image != null)
                        {
                            ROITopRow = operation.TopRow;
                            ROIBotRow = operation.BottomRow;
                            ROITopCol = operation.TopCollumn;
                            ROIBotCol = operation.BottomCollumn;

                            FilterImage(operation.Image, oper);
                            
                        }
                        oper++;
                    }
                }
            }

            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }

        public override void Clear()
        {
            //if (OutputImage != null) OutputImage.Dispose();
            if (InputImage != null) InputImage.Dispose();
        }

        #endregion


        #region Private methods

        void FilterImage(HImage image, int operationIndex)
        {

            try
            {
                HRegion rect_ = new HRegion();
                HRegion rectangle = new HRegion();
                HRegion region = new HRegion();
                HRegion analyzedRegion = new HRegion();
                HRegion regionUneven = new HRegion();
                HRegion edgeRegionBig = new HRegion();
                HRegion edgeRegion = new HRegion();
                HRegion regionDifference = new HRegion();
                HRegion intersectionRegion = new HRegion();
                HObject cont_;
                HXLDCont edges = new HXLDCont();
                HImage imgReduce = new HImage();
                HImage imaDir;
                HImage meanImage;
                HImage edgeImage;
                int number = 0;
                bool judge = true;
                HImage = image;

                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                string color = !Judgement ? "red" : "green";

                UpdateSettings(operationIndex);

                //Stage 1 - Select edge
                meanImage = image.MeanImage((int)8, 8);
                edgeImage = meanImage.EdgesImage(out imaDir, "deriche2", (double)0.5, "nms", BlockSettings[operationIndex].LowContourThreshold, BlockSettings[operationIndex].HighContourThreshold);
                region = imaDir.Threshold((double)0, 255);

                if (ShowEdge) Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = region });

                region = region.Connection();
                region = region.SelectShape("area", "and", BlockSettings[operationIndex].MinContourShape, BlockSettings[operationIndex].MaxContourShape);
                if (ShowSelectShape) Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = region });
                region = region.DilationCircle(BlockSettings[operationIndex].EdgeDilationCircle);
                region = region.Skeleton();
                region = region.Connection();
                region = region.SelectShape("area", "and", BlockSettings[operationIndex].MinEdgeShape, BlockSettings[operationIndex].MaxEdgeShape);

                if (ShowSelectEdge) Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = region });

                //Stage 2 - Find useful region
                edgeRegion = region.DilationCircle(BlockSettings[operationIndex].MinRegionDilationCircle);
                if (ShowMinRegionDilation) Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = edgeRegion });
                edgeRegionBig = region.DilationCircle(BlockSettings[operationIndex].MaxRegionDilationCircle);
                if (ShowMaxRegionDilation) Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = edgeRegionBig });
                regionDifference = edgeRegionBig.Difference(edgeRegion);
                if (ShowRegionsDifference) Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = regionDifference });
                imgReduce = image.ReduceDomain(regionDifference);
                intersectionRegion = imgReduce.Threshold(BlockSettings[operationIndex].LowRegionThreshold, BlockSettings[operationIndex].HighRegionThreshold);
                if (ShowSelectedRegion) Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = intersectionRegion });

                //Stage 3 - Find defects
                intersectionRegion = intersectionRegion.DilationCircle(BlockSettings[operationIndex].DefectsDilationCircle);
                intersectionRegion = intersectionRegion.ErosionCircle(BlockSettings[operationIndex].DefectsDilationCircle);

                imgReduce = image.ReduceDomain(intersectionRegion);
                rect_.GenRectangle1(ROITopRow, ROITopCol, ROIBotRow, ROIBotCol);
                imgReduce = imgReduce.ReduceDomain(rect_);
                imgReduce = imgReduce.EdgesImage(out imaDir, "deriche2", (double)0.5, "nms", BlockSettings[operationIndex].MinDefectsThreshold, BlockSettings[operationIndex].MaxDefectsThreshold);
                region = imaDir.Threshold((double)0, 255);
                region = region.Connection();

                if (ShowDefects) Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = region });

                if (region.Area != 0)
                    number = region.CountObj();

                if (number > 0)
                {
                    judge = false;
                }

                HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow,
                    (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);

                if (!judge & Judgement)
                {
                    Judgement = false;
                }
                else if (!judge)
                {
                    //if (ShowDefects) Marks.Add(new MarkObject { Name = "regionUneven", Color = "red", Object = regionUneven });
                }

                color = !Judgement ? "red" : "green";

                if (operationIndex >= 0)
                {
                    Text.Add(new TextObject { Text = "Operation: #" + operationIndex, Color = color, Font = "-Courier New-14-*-*-*-*-1-", Position = new Point((int)ROITopRow - 20, (int)ROITopCol + 10) });
                }

                Marks.Add(new MarkObject { Name = "Contour", Color = color, Object = cont_ });
                judge = true;

                stopwatch.Stop();
                Text.Add(new TextObject { Text = "Elapsed time: " + stopwatch.ElapsedMilliseconds + "ms", Color = color, Font = "-Courier New-14-*-*-*-*-1-", Position = new Point((int)50, 50) });
            }
            catch (Exception ex)
            {
                Judgement = false;
            }
            BlockOperations[operationIndex].Judgement = Judgement;
        }

        public void UpdateSettings(int index)
        {
            if (BlockSettings != null && BlockSettings.Count > 0 && BlockSettings.Count > index)
            {
                LowerContourThreshold = BlockSettings[index].LowContourThreshold;
                UpperContourThreshold = BlockSettings[index].HighContourThreshold;
                LowerContourShape = BlockSettings[index].MinContourShape;
                UpperContourShape = BlockSettings[index].MaxContourShape;
                EdgeDilationCircleRadius = BlockSettings[index].EdgeDilationCircle;
                LowerEdgeShape = BlockSettings[index].MinEdgeShape;
                UpperEdgeShape = BlockSettings[index].MaxEdgeShape;
                MinRegDilationCircleRadius = BlockSettings[index].MinRegionDilationCircle;
                LowerDefectsThreshold = BlockSettings[index].MinDefectsThreshold;
                UpperDefectsThreshold = BlockSettings[index].MaxDefectsThreshold;
                MaxRegionDilationCircleRadius = BlockSettings[index].MaxRegionDilationCircle;
                LowerRegionThreshold = BlockSettings[index].LowRegionThreshold;
                UpperRegionThreshold = BlockSettings[index].HighRegionThreshold;
                DefectsDilationCircleRadius = BlockSettings[index].DefectsDilationCircle;
            }
        }

        #endregion


        #region UI mouse clicks interaction

        private double roiTopCol = 200;
        private double roiTopRow = 150;
        private double roiBotCol = 400;
        private double roiBotRow = 315;
        private double roiStartRow = 200;
        private double roiStartCol = 150;
        private double roiEndRow = 315;
        private double roiEndCol = 400;
        private double sx, sy, dx, dy;
        private bool IsDiamBusy;

        private bool updateRegion = false;

        private void DrawRectangles()
        {
            HRegion rect_ = new HRegion();
            HRegion region = new HRegion();
            HObject cont_;

            HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow, (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);

            Marks.Add(new MarkObject { Name = "Contour", Color = "blue", Object = cont_ });
            //Text.Add(new TextObject { Text = "Current ROI", Color = "red", Font = "-Courier New-14-*-*-*-*-1-", Position = new Point((int)ROITopRow - 20, (int)ROITopCol + 10) });
        }


        public void UpdateROI()
        {
            if (updateRegion && BlockOperations != null && BlockOperations.Count > OperationIndex)
            {
                BlockOperations[OperationIndex].TopRow = ROITopRow;
                BlockOperations[OperationIndex].BottomRow = ROIBotRow;
                BlockOperations[OperationIndex].TopCollumn = ROITopCol;
                BlockOperations[OperationIndex].BottomCollumn = ROIBotCol;
            }
        }

        public void HOnMouseDown(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                sx = (int)e.X;
                sy = (int)e.Y;

                if (sx > (ROITopCol - 15) && sx < (ROITopCol + 15) && sy > (ROITopRow - 15) && sy < (ROITopRow + 15))
                {
                    roiStartRow = ROITopRow;
                    roiStartCol = ROITopCol;
                    IsDiamBusy = false;
                }
                else
                {
                    roiEndRow = ROIBotRow;
                    roiEndCol = ROIBotCol;
                    IsDiamBusy = true;
                }

                updateRegion = true;
            }
        }

        public void HOnMouseUp(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                UpdateROI();
                updateRegion = false;
            }
        }

        public void HOnMouseMove(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {

                dx = (int)e.X - sx;
                dy = (int)e.Y - sy;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    if (!IsDiamBusy)
                    {
                        ROITopCol = roiStartCol + dx;
                        ROITopRow = roiStartRow + dy;
                    }
                    else
                    {
                        ROIBotCol = roiEndCol + dx;
                        ROIBotRow = roiEndRow + dy;
                    }

                }
            }
        }

        public void HOnMouseWheel(object sender, HMouseEventArgs e)
        {

        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top row position.")]
        [DisplayName("ROI top row")]
        public double ROITopRow
        {
            get
            {
                return roiTopRow;
            }
            set
            {
                roiTopRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot row position.")]
        [DisplayName("ROI bot row")]
        public double ROIBotRow
        {
            get
            {
                return roiBotRow;
            }
            set
            {
                roiBotRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top col position.")]
        [DisplayName("ROI top col")]
        public double ROITopCol
        {
            get
            {
                return roiTopCol;
            }
            set
            {
                roiTopCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopCol"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot col position.")]
        [DisplayName("ROI bot col")]
        public double ROIBotCol
        {
            get
            {
                return roiBotCol;
            }
            set
            {
                roiBotCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotCol"));
            }
        }


        #endregion


        #region Public Properties

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage InputImage
        {
            get
            {
                return inputImage;
            }
            set
            {
                inputImage = value != null ? value.CopyImage() : null;
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage _hImage { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage HImage
        {
            get
            {
                return _hImage;
            }
            set
            {
                _hImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HImage"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<HImage> InputImages { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<HImage> OutputImages { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage OutputImage { get; set; }

        [XmlIgnoreAttribute]
        [Browsable(false)]
        public override Bitmap Icon { set; get; }

        [Browsable(false), BlockNameAttribute]
        public override string Name { set; get; }

        #region Properties from image grab block


        [CategoryAttribute("Block Settings")]
        [Description("Switch between single & multiple image grabbing")]
        [Browsable(false)]
        public OperationMode BlockOperationMode
        {
            get
            {
                return _operationMode;
            }
            set
            {
                _operationMode = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperationMode"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("Currently displayed operation (image)")]
        [XmlIgnoreAttribute, Browsable(false)]
        public int OperationIndex
        {
            get
            {
                return _operationIndex;
            }
            set
            {
                _operationIndex = value;

                OnPropertyChanged(this, new PropertyChangedEventArgs("OperationIndex"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("List of operations")]
        [XmlIgnoreAttribute]
        public List<OperationList> BlockOperations
        {
            get
            {
                return _operationsList;
            }
            set
            {
                _operationsList = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [XmlIgnoreAttribute, Browsable(false)]
        public bool RunBlockOnce
        {
            get { return _runBlockOnce; }
            set
            {
                _runBlockOnce = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("RunBlockOnce"));
            }
        }




        #endregion

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: select edge")]
        [Description("Lower threshold for the hysteresis threshold operation. Contour separation.")]
        [DisplayName(" 1. Lower threshold (contour)")]
        public double LowerContourThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].LowContourThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    //return -1;
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    return BlockSettings[OperationIndex].LowContourThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].LowContourThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    LowerContourThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("LowerContourThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: select edge")]
        [Description("Upper threshold for the hysteresis threshold operation. Contour separation.")]
        [DisplayName(" 2. Upper threshold (contour)")]
        public double UpperContourThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].HighContourThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    //return -1;
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    return BlockSettings[OperationIndex].HighContourThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].HighContourThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    UpperContourThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("UpperContourThreshold"));
            }
        }
        

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: select edge")]
        [Description("Lower limits of the selected region. Contour separation.")]
        [DisplayName(" 4. Lower shape size (contour)")]
        public double LowerContourShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinContourShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    //return -1;
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    return BlockSettings[OperationIndex].MinContourShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinContourShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    LowerContourShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("LowerContourShape"));
            }
        }
    

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: select edge")]
        [Description("Upper limits of the selected region. Contour separation.")]
        [DisplayName(" 5. Upper shape size (contour)")]
        public double UpperContourShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaxContourShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    //return -1;
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    return BlockSettings[OperationIndex].MaxContourShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaxContourShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    UpperContourShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("UpperContourShape"));
            }
        }
        

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: select edge")]
        [Description("Radius of the circular structuring element. Contour separation.")]
        [DisplayName(" 7. Dilation circle radius (contour)")]
        public double EdgeDilationCircleRadius
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].EdgeDilationCircle;
                }
                catch (ArgumentOutOfRangeException)
                {
                    //return -1;
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    return BlockSettings[OperationIndex].EdgeDilationCircle;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].EdgeDilationCircle = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    EdgeDilationCircleRadius = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("EdgeDilationCircleRadius"));
            }
        }
       

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: select edge")]
        [Description("Lower limits of the selected region. Skeleton selection.")]
        [DisplayName(" 8. Lower shape size (skeleton)")]
        public double LowerEdgeShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinEdgeShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    //return -1;
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    return BlockSettings[OperationIndex].MinEdgeShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinEdgeShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    LowerEdgeShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("LowerEdgeShape"));
            }
        }
        

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: select edge")]
        [Description("Upper limits of the selected region. Skeleton selection.")]
        [DisplayName(" 9. Upper shape size (skeleton)")]
        public double UpperEdgeShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaxEdgeShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    //return -1;
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    return BlockSettings[OperationIndex].MaxEdgeShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaxEdgeShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    UpperEdgeShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("UpperEdgeShape"));
            }
        }
       

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find useful region")]
        [Description("Radius of the circular structuring element. Skeleton selection.")]
        [DisplayName(" 1. Dilation circle radius (skeleton)")]
        public double MinRegDilationCircleRadius
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinRegionDilationCircle;
                }
                catch (ArgumentOutOfRangeException)
                {
                    //return -1;
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    return BlockSettings[OperationIndex].MinRegionDilationCircle;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinRegionDilationCircle = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    MinRegDilationCircleRadius = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinRegDilationCircleRadius"));
            }
        }
      

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage III: find defects")]
        [Description("Radius of the circular structuring element. Analyzed area.")]
        [DisplayName(" 2. Lower threshold (defects)")]
        public double LowerDefectsThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinDefectsThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    //return -1;
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    return BlockSettings[OperationIndex].MinDefectsThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinDefectsThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    LowerDefectsThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("LowerDefectsThreshold"));
            }
        }
       

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage III: find defects")]
        [Description("Radius of the circular structuring element. Analyzed area.")]
        [DisplayName(" 3. Upper threshold (defects)")]
        public double UpperDefectsThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaxDefectsThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    //return -1;
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    return BlockSettings[OperationIndex].MaxDefectsThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaxDefectsThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    UpperDefectsThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("UpperDefectsThreshold"));
            }
        }
        

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find useful region")]
        [Description("Radius of the circular structuring element. Skeleton selection.")]
        [DisplayName(" 3. Dilation circle radius (area)")]
        public double MaxRegionDilationCircleRadius
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaxRegionDilationCircle;
                }
                catch (ArgumentOutOfRangeException)
                {
                    //return -1;
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    return BlockSettings[OperationIndex].MaxRegionDilationCircle;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaxRegionDilationCircle = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    MaxRegionDilationCircleRadius = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxRegionDilationCircleRadius"));
            }
        }
       

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find useful region")]
        [Description("Lower threshold for the hysteresis threshold operation. Defect selection.")]
        [DisplayName(" 6. Lower threshold (region)")]
        public double LowerRegionThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].LowRegionThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    //return -1;
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    return BlockSettings[OperationIndex].LowRegionThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].LowRegionThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    LowerRegionThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("LowerRegionThreshold"));
            }
        }
       

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find useful region")]
        [Description("Upper threshold for the hysteresis threshold operation. Defect selection.")]
        [DisplayName(" 7. Upper threshold (region)")]
        public double UpperRegionThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].HighRegionThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    //return -1;
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    return BlockSettings[OperationIndex].HighRegionThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].HighRegionThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    UpperRegionThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("UpperRegionThreshold"));
            }
        }
      

        [CategoryAttribute("Calibration")]
        [Description("Rubber-plastic contact filter settigs")]
        public List<RubberPlasticContactSettings> BlockSettings
        {
            get
            {
                return _rubberPlasticContactSettings;
            }
            set
            {
                _rubberPlasticContactSettings = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockSettings"));
            }
        }

       
        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage III: find defects")]
        [Description("Radius of the circular structuring element. Contour separation.")]
        [DisplayName(" 1. Dilation circle radius (defects)")]
        public double DefectsDilationCircleRadius
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].DefectsDilationCircle;
                }
                catch (ArgumentOutOfRangeException)
                {
                    //return -1;
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    return BlockSettings[OperationIndex].DefectsDilationCircle;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].DefectsDilationCircle = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberPlasticContactSettings());
                    DefectsDilationCircleRadius = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("DefectsDilationCircleRadius"));
            }
        }
        

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: select edge")]
        [Description("Analyzed region edge.")]
        [DisplayName(" 3. Show edge")]
        public bool ShowEdge
        {
            get
            {
                return showEdge;
            }
            set
            {
                showEdge = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowEdge"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: select edge")]
        [Description("Show select shape.")]
        [DisplayName(" 6. Show select shape")]
        public bool ShowSelectShape
        {
            get
            {
                return showSelectShape;
            }
            set
            {
                showSelectShape = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowSelectShape"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: select edge")]
        [Description("Show select edge region.")]
        [DisplayName("10. Show select edge")]
        public bool ShowSelectEdge
        {
            get
            {
                return showSelectEdge;
            }
            set
            {
                showSelectEdge = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowSelectEdge"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find useful region")]
        [Description("Show lower region dilation.")]
        [DisplayName(" 2. Show min region dilation")]
        public bool ShowMinRegionDilation
        {
            get
            {
                return showMinRegionDilation;
            }
            set
            {
                showMinRegionDilation = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowMinRegionDilation"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find useful region")]
        [Description("Show upper region dilation.")]
        [DisplayName(" 4. Show max region dilation")]
        public bool ShowMaxRegionDilation
        {
            get
            {
                return showMaxRegionDilation;
            }
            set
            {
                showMaxRegionDilation = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowMaxRegionDilation"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find useful region")]
        [Description("Show selected region.")]
        [DisplayName(" 8. Show selected region")]
        public bool ShowSelectedRegion
        {
            get
            {
                return showSelectedRegion;
            }
            set
            {
                showSelectedRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowSelectedRegion"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find useful region")]
        [Description("Show regions differences.")]
        [DisplayName(" 5. Show regions differences")]
        public bool ShowRegionsDifference
        {
            get
            {
                return showRegionsDifference;
            }
            set
            {
                showRegionsDifference = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowRegionsDifference"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage III: find defects")]
        [Description("Show defects.")]
        [DisplayName(" 4. Show defects")]
        public bool ShowDefects
        {
            get
            {
                return showDefects;
            }
            set
            {
                showDefects = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowDefects"));
            }
        }

        #endregion


        #region Buttons

        [ButtonControl]
        public void NextImage()
        {
            if (BlockOperationMode == OperationMode.SingleImageOperation && BlockOperations != null && BlockOperations.Count > 0)
            {
                OperationIndex = OperationIndex < BlockOperations.Count - 1 ? OperationIndex + 1 : OperationIndex;
                RunBlockOnce = true;
            }
        }

        [ButtonControl]
        public void PreviousImage()
        {
            if (BlockOperationMode == OperationMode.SingleImageOperation && BlockOperations != null && BlockOperations.Count > 0)
            {
                OperationIndex = OperationIndex > 0 ? OperationIndex - 1 : OperationIndex;
                RunBlockOnce = true;
            }
        }

        #endregion


        #region Events

        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;

        #endregion
    }
}
