﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Drawing;

using LibSVMsharp;
using LibSVMsharp.Helpers;
using LibSVMsharp.Extensions;




namespace Elvis.Fenix.FunctionBlocks
{
    public class FxSVMPredictor : FxBlock
    {
        #region Private Members

        private bool _disposed;
        private ManualResetEventSlim _exit = new ManualResetEventSlim(false);

        private NumberFormatInfo provider = new NumberFormatInfo();
        private SVMProblem trainingSet;
        private SVMProblem testSet;
        private SVMModel model;

        private OperationMode _operationMode;
        private int _operationIndex;

        private string _modelPath;
        private string _trainingFilePath;

        #endregion

        #region Constructors

        public FxSVMPredictor() { }

        public FxSVMPredictor(string name)
        {
            Name = name;
            Icon = Resource.Judgement;

            trainingSet = new SVMProblem();
            testSet = new SVMProblem();

            provider.NumberDecimalSeparator = ".";

        }

        #endregion

        #region Public Methods

        public override void Run()
        {
            Judgement = true;
            DateTime startTime = DateTime.Now;

            double bottle = -1;

            _exit.Reset();

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            try
            {
                // load model for the first time
                if (model == null) LoadSVMModel();

                // block routine
                if(model != null)
                {
                    bottle = Predict(FormatProblem());

                    if (!bottle.Equals(0)) Judgement = false;
                }
                else 
                {
                    AnnounceInfoMessage("Model is not loaded");
                    Judgement = false;
                }

            }
            catch (OutOfMemoryException)
            {
                GC.Collect();
                FxSessionTimeClass.LogErrorMessage("(" + Name + ") ", "Out of memory");
            }
            catch (Exception ex)
            {
                Judgement = false;

                if (onMessage != null)
                {
                    onMessage(this, " block error: " + ex.Message);
                }

                FxSessionTimeClass.LogErrorMessage("(" + Name + ") ", ex.Message);
            }

            Prediction = bottle;
            if (Judgement) UpdateResultImage(true);
            else UpdateResultImage(false);

            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

            Judge = Judgement;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }



        public override void Clear()
        {
            Image = null;

            GC.Collect();
        }

        public override void Exit()
        {
            base.Exit();
        }
        
        public override void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _exit.Set();
            }
            _disposed = true;
        }

        public override void Stop()
        {
            _exit.Set();
        }

        #endregion



        #region Private Methods

        private void TrainSVMModel()
        {
            try
            {
                trainingSet = SVMProblemHelper.Load(_trainingFilePath);

                if (trainingSet != null && trainingSet.Length > 0)
                {
                    // Select the parameter set
                    SVMParameter parameter = new SVMParameter();
                    parameter.Type = SVMType.C_SVC;
                    parameter.Kernel = SVMKernelType.RBF;
                    parameter.Degree = Degree;
                    parameter.C = C;
                    parameter.Gamma = Gamma;

                    // Do cross validation to check this parameter set is correct for the dataset or not
                    double[] crossValidationResults; // output labels
                    int nFold = 5;
                    trainingSet.CrossValidation(parameter, nFold, out crossValidationResults);

                    // Evaluate the cross validation result
                    // If it is not good enough, select the parameter set again
                    double crossValidationAccuracy = trainingSet.EvaluateClassificationProblem(crossValidationResults);

                    // Train the model, If your parameter set gives good result on cross validation
                    model = trainingSet.Train(parameter);
                    AnnounceInfoMessage("Model trained");
                }
            }
            catch (Exception ex)
            {
                AnnounceInfoMessage(ex.Message);
            }
            
        }


        private void SaveSVMModel()
        {
            if (model != null)
            {
                if (!string.IsNullOrEmpty(ModelPath))
                {
                    try
                    {
                        // Save the model
                        SVM.SaveModel(model, ModelPath + @"\model.txt");
                        AnnounceInfoMessage("Model saved");
                    }
                    catch(Exception ex)
                    {
                        AnnounceInfoMessage(ex.Message);
                    }
                }
                else
                {
                    AnnounceInfoMessage("Invalid model path");
                }
            }
            else
            {
                AnnounceInfoMessage("Create a model first!");
            }
        }

        private void LoadSVMModel()
        {
            if (!string.IsNullOrEmpty(ModelPath))
            {
                try
                {
                    // load the model
                    model = SVM.LoadModel(ModelPath + @"\model.txt");
                    AnnounceInfoMessage("Model loaded");
                }
                catch (Exception ex)
                {
                    AnnounceInfoMessage(ex.Message);
                }
            }
            else
            {
                AnnounceInfoMessage("Invalid model path");
            }
        }

        private double Predict(SVMProblem p)
        {
            double[] result;

            try
            {
                result = p.Predict(model);

                if(result.Length > 0)
                    return result[0];
            }
            catch(Exception ex)
            {
                AnnounceInfoMessage(ex.Message);
            }
            return -1;
        }

        private SVMProblem FormatProblem()
        {
            SVMProblem result = new SVMProblem();

            try
            {
                if(!string.IsNullOrEmpty(Problem))
                {
                    List<SVMNode> nodes = new List<SVMNode>();

                    string[] list = Problem.Trim().Split(' ');

                    for (int i = 0; i < list.Length; i++)
                    {
                        string[] temp = list[i].Split(':');
                        SVMNode node = new SVMNode();
                        node.Index = Convert.ToInt32(temp[0].Trim());
                        node.Value = Convert.ToDouble(temp[1].Trim(), provider);
                        nodes.Add(node);
                    }

                    result.Add(nodes.ToArray(), 0);

                    return result;
                }
                else
                {
                    AnnounceInfoMessage("Invalid problem data format");
                }
                
            }
            catch(Exception ex)
            {
                AnnounceInfoMessage(ex.Message);
            }

            return null;
        }

        #endregion


        #region Private Helpers

        /// <summary>
        /// Announce block's info message
        /// </summary>
        /// <param name="msg"></param>
        private void AnnounceInfoMessage(string msg)
        {
            if (onMessage != null) onMessage(this, msg);
        }

        /// <summary>
        /// Draw an image of specified width, height and color
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="color"></param>
        /// <returns>image</returns>
        private Bitmap DrawFilledRectangle(int width, int height, Brush color)
        {
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(color, ImageSize);
            }
            return bmp;
        }

        /// <summary>
        /// Draw an image of specified width, height, color and prediction result in the middle
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="color"></param>
        /// <param name="prediction"></param>
        /// <returns>image with number in the middle</returns>
        private Bitmap DrawFilledRectangle(int width, int height, Brush color, double prediction)
        {
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(color, ImageSize);

                using (Font arialFont = new Font("Arial", 10))
                {
                    PointF loc = new PointF((float)(width / 2.0), (float)(height / 2.0));
                    graph.DrawString(prediction.ToString(), arialFont, Brushes.Black, loc);
                }
            }
            return bmp;
        }

        /// <summary>
        /// Update an image that is corresponding block's result and draw result number in the middle
        /// </summary>
        /// <param name="ok"></param>
        /// <param name="prediction"></param>
        private void UpdateResultImage(bool ok, double prediction)
        {
            if(ok)
            {
                Image = DrawFilledRectangle(100, 100, Brushes.Green, prediction);
            }
            else
            {
                Image = DrawFilledRectangle(100, 100, Brushes.Red, prediction);
            }
        }

        /// <summary>
        /// Update an image that is corresponding block's result
        /// </summary>
        /// <param name="ok"></param>
        private void UpdateResultImage(bool ok)
        {
            if (ok)
            {
                Image = DrawFilledRectangle(100, 100, Brushes.Green);
            }
            else
            {
                Image = DrawFilledRectangle(100, 100, Brushes.Red);
            }
        }

        #endregion


        #region Public Properties

        [XmlIgnoreAttribute]
        [Browsable(false)]
        public Bitmap Image { get; set; }


        [XmlIgnoreAttribute]
        [CategoryAttribute("Block Settings")]
        public string Problem { get; set; }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Block Settings")]
        public double Prediction { get; set; }



        [CategoryAttribute("SVM settings")]
        public string DecimalSeparator
        {
            get
            {
                return provider.NumberDecimalSeparator;
            }
            set
            {
                try
                {
                    provider.NumberDecimalSeparator = value;
                }
                catch { }
            }
        }

        //[Browsable(false)]
        //[XmlIgnoreAttribute]
        [CategoryAttribute("SVM settings")]
        [Description("SVM Polynomial kernel degree")]
        [DisplayName("SVM Function Degree")]
        public int Degree { get; set; }

        [CategoryAttribute("SVM settings")]
        [Description("Optimization problem parameter")]
        [DisplayName("C Parameter")]
        public double C { get; set; }

        [CategoryAttribute("SVM settings")]
        [Description("Kernel's gamma value")]
        [DisplayName("Gamma Parameter")]
        public double Gamma { get; set; }

        [CategoryAttribute("SVM settings")]
        public string ModelPath 
        { 
            get
            {
                return _modelPath;
            }
            set
            {
                _modelPath = value;

                if (!string.IsNullOrEmpty(_modelPath) && model != null)
                {
                    try
                    {
                        LoadSVMModel();
                    }
                    catch (Exception ex)
                    {
                        AnnounceInfoMessage("Failed to load model: " + ex.Message);
                    }
                }
            }
        }

        [CategoryAttribute("SVM settings")]
        public string TrainingFilePath
        {
            get
            {
                return _trainingFilePath;
            }
            set
            {
                _trainingFilePath = value;
            }
        }

        [CategoryAttribute("Block Settings")]
        public OperationMode BlockOperationMode
        {
            get
            {
                return _operationMode;
            }
            set
            {
                _operationMode = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperationMode"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("Currently displayed operation (image)")]
        [XmlIgnoreAttribute]
        public int OperationIndex
        {
            get
            {
                return _operationIndex;
            }
            set
            {
                _operationIndex = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("OperationIndex"));
            }
        }

        #endregion


        #region Events

        public override event MessageHandler onMessage;
        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        
        #endregion

        #region Buttons

        [ButtonControl]
        public void TrainSVM()
        {
            TrainSVMModel();
        }

        [ButtonControl]
        public void SaveModel()
        {
            SaveSVMModel();
        }

        [ButtonControl]
        public void LoadModel()
        {
            LoadSVMModel();
        }


        #endregion
    }
}
