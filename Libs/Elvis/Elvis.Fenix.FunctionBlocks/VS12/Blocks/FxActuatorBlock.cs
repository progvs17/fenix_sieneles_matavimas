﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

using Elvis.Fenix.Hardware;

namespace Elvis.Fenix.FunctionBlocks
{
    #region Hardware attributes

    public class HardwareIdAtrribute : System.Attribute
    {
        public HardwareIdAtrribute()
        {

        }
    }

    public class HardwareAtrribute : System.Attribute
    {
        public HardwareAtrribute()
        {

        }
    }

    public struct HardwareObject
    {
        public string Name { get; set; }
        public object Obj { get; set; }
    }

    #endregion


    public class FxActuatorBlock : FxBlock 
    {

        #region Private members

        ManualResetEventSlim _stop = new ManualResetEventSlim(false);

        /// <summary>
        /// Actuator movement type
        /// </summary>
        private bool _absolutePosition = false;

        /// <summary>
        /// Actuator movement indicator
        /// </summary>
        private bool _actuatorStopped = false;

        /// <summary>
        /// Actuator movement position in scene
        /// </summary>
        private double _scenePosition = 0;

        /// <summary>
        /// Loading images from file(s) variable
        /// </summary>
        private bool _imageLoaderEnabled = false;

        /// <summary>
        /// Assigned hardware ID
        /// </summary>
        private string _hardwareId = "NaN";

        /// <summary>
        /// variable to indicate if properties should be updated
        /// </summary>
        private bool _updateProperties = true;

        [XmlIgnore]
        public HardwareObject hw = new HardwareObject();


        double _currentPos; // = 0;
        int _speed; // = 100;
        int _acc; // = 2;
        int _decc; // = 2;

        /// <summary>
        /// Assigned actuator type
        /// </summary>
        private FxActuator _actuator;


        #endregion


        #region Constructors

        /// <summary>
        /// Empty constructor, needed to save block to xml file
        /// </summary>
        public FxActuatorBlock()
        {

        }

        /// <summary>
        /// Constructor with block name only, hardware id must be assigned later
        /// </summary>
        /// <param name="name"></param>
        public FxActuatorBlock(string name)
        {
            Name = name;
            Icon = Resource.Default;
        }

        /// <summary>
        /// constructor with block name & hardware id, that is used to assign hardware element
        /// </summary>
        /// <param name="name"></param>
        /// <param name="hardwareId"></param>
        public FxActuatorBlock(string name, string hardwareId)
        {
            Name = name;
            Icon = Resource.Default;
            HardwareId = hardwareId;
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Method used to dispose block & it's hardware
        /// </summary>
        public override void Dispose()
        {
            Stop();
            _actuator.onActuatorStop -= _actuator_onActuatorStop;
            _actuator.onActuatorStart -= _actuator_onActuatorStart;

            _actuator = null;
        }

        public override void Stop()
        {
            _stop.Set();

            //Thread.Sleep(50);

            if(!_actuatorStopped)
                _actuator.Stop();

            _actuatorStopped = true;
        }

         /// <summary>
        /// Main run method
        /// </summary>
        public override void Run() 
        {

            Judgement = true;
            DateTime startTime = DateTime.Now;

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            try
            {
                _stop.Reset();

                // check if image loading from files is not enabled
                if (!ImageLoaderEnabled)
                {
                    if (_actuator != null && IsConnected)
                    {
  
                        if (_absolutePosition)
                            _actuator.MoveTo(_scenePosition);
                        else
                            _actuator.Move(_scenePosition);

                        while (!_actuatorStopped && !_stop.IsSet) ;

                    }
                    else
                    {
                        FxSessionTimeClass.LogErrorMessage("(" + Name + ") ", "Actuator not connected!");
                    }
                }
           
            }
            catch (Exception ex)
            {
                Judgement = false;

                if(onMessage != null)
                {
                    onMessage(this, "FxActuator block error: " + ex.Message);
                }

                FxSessionTimeClass.LogErrorMessage("(" + Name + ") ", ex.Message);

            }

            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

            Judge = Judgement;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }

        }


        public override void Clear() 
        {
            //Stop();
            //Console.WriteLine("Actuator action finished"); 
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Exit() 
        {
            Dispose();
        }

        /// <summary>
        /// Method to assign hardware and subscribe to it's events
        /// </summary>
        /// <param name="hwObj">Hardware object & it's name from hardware list</param>
        [HardwareAtrribute]
        public void SetHw(HardwareObject hwObj)
        {
            hw = new HardwareObject();
            hw = hwObj;

            //if (hw.Obj != null)
            //{
            //    if (HardwareId == hw.Name)
            //    {
            //        _actuator = (FxActuator)hw.Obj;
            //        _actuator.onActuatorStop += _actuator_onActuatorStop;
            //        _actuator.onActuatorStart += _actuator_onActuatorStart;


            //        if (_actuator.GetType().Equals(new Elvis.Fenix.Hardware.FxActuators.IAILinearActuator().GetType()) && _actuator.MaxPosition > 200) // mask
            //        {
            //            Icon = Properties.Resources.LinearActuator;
            //        }
            //        else if (_actuator.GetType().Equals(new Elvis.Fenix.Hardware.FxActuators.IAILinearActuator().GetType()) && _actuator.MaxPosition <= 200) // camera
            //        {
            //            Icon = Properties.Resources.CameraLinearActuator;
            //        }
            //        else if(_actuator.GetType().Equals(new Elvis.Fenix.Hardware.FxActuators.ARDKD().GetType()))
            //        {
            //            Icon = Properties.Resources.RotateActuator;
            //        }
            //        else if (_actuator.GetType().Equals(new Elvis.Fenix.Hardware.FxActuators.CRD514KD().GetType()))
            //        {
            //            Icon = Properties.Resources.ReductionActuator;
            //        }
            //    }
            //}
        }

        #endregion

        /// <summary>
        /// Private method to update properties if any changes are present before running block
        /// </summary>
        //public void UpdateProps()
        //{
        //    _actuator.Speed = _speed;
        //    _actuator.Acceleration = _acc;
        //    _actuator.Decceleration = _decc;
        //}


        #region Public Properties

        /// <summary>
        /// Block name
        /// </summary>
        [Browsable(false), BlockNameAttribute]
        public override string Name { set; get; }

        
        /// <summary>
        /// Block icon
        /// </summary>
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public override System.Drawing.Bitmap Icon { get; set; }


        
        [Browsable(false)]
        [HardwareIdAtrribute]
        public string HardwareId 
        { 
            get
            {
                return _hardwareId;
            }
            set
            {
                _hardwareId = value;
                OnPropertyChanged(new PropertyChangedEventArgs("HardwareId"));

            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public bool ImageLoaderEnabled 
        { 
            get
            {
                return _imageLoaderEnabled;
            }
            set
            {
                _imageLoaderEnabled = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ImageLoaderEnabled"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("Actuator moves to an absolute position or makes a step")]
        [DisplayName("Move to absolute position")]
        public bool AbsolutePosition
        {
            get
            {
                return _absolutePosition;
            }
            set
            {
                _absolutePosition = value;
                OnPropertyChanged(new PropertyChangedEventArgs("AbsolutePosition"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("Actuator position (step) for operation")]
        [DisplayName("Movement position")]
        public double ScenePosition
        {
            get
            {
                return _scenePosition;
            }
            set
            {
                _scenePosition = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ScenePosition"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Actuator Information")]
        [Description("True - is moving, false - stacionary")]
        [DisplayName("Moving")]
        public bool IsMoving
        {
            get
            {
                bool moving = false;
                try
                {
                    moving = _actuator.IsMoving;
                }
                catch { }

                return moving;
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Actuator Information")]
        [Description("True - is connected, false - no")]
        [DisplayName("Connected")]
        public bool IsConnected
        {
            get
            {
                bool conn = false;
                try
                {
                    conn = _actuator.IsConnected;
                }
                catch { }

                return conn;
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Actuator Information")]
        [Description("Returns information about actuator")]
        [DisplayName("Status")]
        public string Status
        {
            get
            {
                string stat = "Fail";
                try
                {
                    stat = _actuator.Status;
                }
                catch { }

                return stat;
            }
        }


        [CategoryAttribute("Actuator Settings")]
        [Description("Information about actuator's current position")]
        [DisplayName("Actuator current position")]
        [XmlIgnoreAttribute]
        public double CurrentPosition
        {
            get
            {
                try
                {
                    _currentPos = _actuator.CurrentPosition;
                }
                catch { }

                return _currentPos;
            }
            set
            {
                try
                {
                    _currentPos = value;
                    _actuator.CurrentPosition = value;
                    OnPropertyChanged(new PropertyChangedEventArgs("CurrentPosition"));
                }
                catch { }
            }
        }

        //[CategoryAttribute("Actuator Settings")]
        //[Description("Information about actuator's speed")]
        //[DisplayName("Actuator speed")]
        //public int Speed
        //{
        //    get
        //    {
        //        try
        //        {
        //            //if (!_updateProperties)
        //                _speed = _actuator.Speed;
        //        }
        //        catch { }

        //        return _speed;
        //    }
        //    set
        //    {
        //        try
        //        {
        //            _actuator.Speed = _speed = value;
        //            OnPropertyChanged(new PropertyChangedEventArgs("Speed"));
        //        }
        //        catch { }
        //        //_speed = value;
        //        //_updateProperties = true;
        //    }
        //}

        //[CategoryAttribute("Actuator Settings")]
        //[Description("Information about actuator's acceleration")]
        //[DisplayName("Actuator acceleration")]
        //public int Acceleration
        //{
        //    get
        //    {
        //        try
        //        {
        //            //if (!_updateProperties)
        //                _acc = _actuator.Acceleration;
        //        }
        //        catch { }

        //        return _acc;
        //    }
        //    set
        //    {
        //        try
        //        {
        //            _actuator.Acceleration = _acc = value;
        //            OnPropertyChanged(new PropertyChangedEventArgs("Acceleration"));
        //        }
        //        catch { }
        //        //_acc = value;
        //        //_updateProperties = true;
        //    }
        //}

        //[CategoryAttribute("Actuator Settings")]
        //[Description("Information about actuator's decceleration")]
        //[DisplayName("Actuator decceleration")]
        //public int Decceleration
        //{
        //    get
        //    {
        //        try
        //        {
        //           // if (!_updateProperties)
        //                _decc = _actuator.Decceleration;
        //        }
        //        catch { }

        //        return _decc;
        //    }
        //    set
        //    {
        //        try
        //        {
        //            _actuator.Decceleration = _decc = value;
        //            OnPropertyChanged(new PropertyChangedEventArgs("Decceleration"));
        //        }
        //        catch { }
        //        //_decc = value;
        //        //_updateProperties = true;
        //    }
        //}


        #endregion


        #region Events

        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;


        void _actuator_onActuatorStop(object sender)
        {
            _actuatorStopped = true;
        }

        void _actuator_onActuatorStart(object sender)
        {
            _actuatorStopped = false;
        }

        #endregion


        #region buttons

        [ButtonControl]
        public void StepForward()
        {
            if(_actuator != null)
                _actuator.Move(5);
        }

        [ButtonControl]
        public void StepBackwards()
        {
            if (_actuator != null)
               _actuator.Move(-5);
        }

        [ButtonControl]
        public void HomePosition()
        {
            if (_actuator != null)
                _actuator.MoveToHomePosition();
        }

        #endregion
    }
}
