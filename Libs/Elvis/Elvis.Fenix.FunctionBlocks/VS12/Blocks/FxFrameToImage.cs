﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Elvis.Fenix.Hardware;

using Emgu.CV;
using System.ComponentModel;
using System.Drawing;
using Emgu.CV.Structure;

namespace Elvis.Fenix.FunctionBlocks
{
    public class FxFrameToImage : FxBlock
    {
        private FxFrame _input;
        private Emgu.CV.Image<Emgu.CV.Structure.Rgba, Byte> _output;


        public FxFrameToImage(string name)
        {
            Name = name;
            Icon = Resource.Default;
        }

        public override void Dispose()
        {
            
        }

        public override void Run()
        {
            Judgement = true;
            if(Input != null)
            {
                var temp = new Image<Emgu.CV.Structure.Gray, Byte>(Input.Width, Input.Height, Input.Width, Input.Data);
                Output = new Image<Emgu.CV.Structure.Rgba, Byte>(Input.Width, Input.Height);
                CvInvoke.cvCvtColor(temp, Output, Emgu.CV.CvEnum.COLOR_CONVERSION.CV_GRAY2RGBA);
                CvInvoke.cvCircle(Output, new Point(100, 100), 100, new MCvScalar(255, 0, 0), 1, Emgu.CV.CvEnum.LINE_TYPE.EIGHT_CONNECTED, 0);
                temp.Dispose();          
            }
            else
            {
                Judgement = false;
            }
        }

        public override void Clear()
        {
        }

        public FxFrame Input
        {
            get
            {
                return _input;
            }

            set
            {
                _input = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Input"));
            }
        }

        public Image<Emgu.CV.Structure.Rgba, Byte> Output
        {
            get
            {
                return _output;
            }

            set
            {
                _output = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Output"));
            }
        }

    }
}
