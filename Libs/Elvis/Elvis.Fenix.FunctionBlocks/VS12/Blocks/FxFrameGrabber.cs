﻿using Elvis.Fenix.Hardware;
//using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Elvis.Fenix.FunctionBlocks
{
    public class FxFrameGrabber: FxBlock
    {
        private ManualResetEventSlim _stop = new ManualResetEventSlim(false);

        private FxCamera _camera;
        private FxFrame _frame;

        [Browsable(false)]
        public override Bitmap Icon { set; get; }
 
        [Browsable(false)]
        public FxFrameGrabber(string name, FxCamera camera)
        {
            Name = name;
            Icon = Resource.Camera;
            _camera = camera;
        }

        [Browsable(false)]
        public FxFrame Frame
        {
            get { 
                return _frame; 
            }

            set
            {
                _frame = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Frame"));
            }
        }

        public override void Run()
        {
            Judgement = true;
            //DateTime startTime = DateTime.Now;

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            try
            {
                _camera.Connect();
                _camera.StartGrab();

                while(_stop.IsSet || Frame != null)
                {
                    Frame = _camera.RetrieveFrame();
                }

                //Frame = _camera.GrabFrame() as FxFrame;
                

                if (Frame == null)
                {
                    Judgement = false;
                }

                /*
                if (_frame != null && _frame.Data != null)
                {
                    HImage = new HImage();
                    HImage.GenImage1Extern("byte", _frame.Width, _frame.Height, _frame.Data, IntPtr.Zero);
                }
                else
                {
                    Judgement = false;
                }
                 * 
                 */

                ElapsedTime = 0;// (DateTime.Now - startTime).TotalMilliseconds;

                if (onTaskCompleted != null)
                {
                    onTaskCompleted(this);
                }
            }
            catch (Exception ex)
            {
                Judgement = false;

                if (onMessage != null)
                {
                    onMessage(this, "Error onFxImageInput block.");
                }
            }
        }

        public override void Clear()
        {
            _camera.ReleaseFrame(_frame);
        }

        public int Width
        {
            get
            {
                return _camera != null && _camera.IsConnected ? _camera.Width : 0;
            }
            set
            {
                if (_camera != null && _camera.IsConnected)
                {
                    bool restart = _camera.IsGrabbing;
                    if (restart)
                    {
                        _camera.StopGrab();
                        _camera.Width = value;
                        _camera.StartGrab();
                    }
                    else
                    {
                        _camera.Width = value;
                    }
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("Width"));
            }
        }

        public int Height
        {
            get
            {
                return _camera != null && _camera.IsConnected ? _camera.Height : 0;
            }
            set
            {
                if (_camera != null && _camera.IsConnected)
                {
                    bool restart = _camera.IsGrabbing;
                    if (restart)
                    {
                        _camera.StopGrab();
                        _camera.Height = value;
                        _camera.StartGrab();
                    }
                    else
                    {
                        _camera.Height = value;
                    }
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("Height"));
            }
        }


        public int Exposure
        {
            get
            {
                return _camera != null && _camera.IsConnected ? _camera.Exposure : 0;
            }
            set
            {
                if (_camera != null && _camera.IsConnected)
                {
                    _camera.Exposure = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("Exposure"));
            }
        }

        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;

    }
}
