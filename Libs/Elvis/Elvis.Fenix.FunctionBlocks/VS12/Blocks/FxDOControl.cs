﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elvis.Fenix.FunctionBlocks
{
    public enum OutputStatus
    {
        ON,
        OFF,
        DEFAULT
    }

    public struct SynchOutput
    {
        public string Name;
        public OutputStatus OutputStatus;
    }

    public class FxDOControl : FxBlock
    {
        public Hardware.FxGpio _gpio;
        private bool _disposed;
        private SynchOutput[] outs = new SynchOutput[8];

        public OutputStatus Output0 {
            get
            {
                return outs[0].OutputStatus;
            }
            set
            {
                outs[0].OutputStatus = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Output0"));               
            }
        }

        public OutputStatus Output1
        {
            get
            {
                return outs[1].OutputStatus;
            }
            set
            {
                outs[1].OutputStatus = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Output1"));
            }
        }

        public OutputStatus Output2
        {
            get
            {
                return outs[1].OutputStatus;
            }
            set
            {
                outs[1].OutputStatus = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Output2"));
            }
        }

        public OutputStatus Output3
        {
            get
            {
                return outs[1].OutputStatus;
            }
            set
            {
                outs[1].OutputStatus = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Output3"));
            }
        }



        public FxDOControl(string name, Fenix.Hardware.FxGpio gpio)
        {
            Name = name;
            _gpio = gpio;
            Icon = Resource.IO_sync;
            
            outs[0] = new SynchOutput();
            outs[1] = new SynchOutput();
        }


        public override void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            _disposed = true;
        }

        public override void Run()
        {
            int i = 0;
            foreach(var item in outs)
            {
                if(item.OutputStatus != OutputStatus.DEFAULT)
                {
                    int value = item.OutputStatus == OutputStatus.ON ? 1 : 0;
                    _gpio.SetPin(i, value);
                }
                i++;
            }            
        }

        public override void Clear()
        {
            // Empty
        }
       
    }
}
