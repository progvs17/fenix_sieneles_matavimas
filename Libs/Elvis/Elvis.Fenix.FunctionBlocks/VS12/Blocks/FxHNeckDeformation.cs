﻿using HalconDotNet;
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using System.Diagnostics;
using System.Threading;

namespace Elvis.Fenix.FunctionBlocks
{
    public class NeckDeformationSettings
    {
        #region Constructors

        public NeckDeformationSettings()
        {
            MinimumRegionThreshold1 = 0;
            MaximumRegionThreshold1 = 180;
            //MinimumRegionThreshold2 = 180;
            //MaximumRegionThreshold2 = 255;
            MinimumRegionShape = 5000;
            MaximumRegionShape = 100000;
            MinimumRegionThreshold2 = 20;
            MaximumRegionThreshold2 = 255;
            MinimumFillShape = 1;
            MaximumFillShape = 1000;
            //MinimumSelectShape = 10000;
            //MaximumSelectShape = 99999;
            EdgesAlpha = 2;
            EdgesLow = 10;
            EdgesHigh = 15;
            DilationCircle = 3.5;
            TrainingImage = "C:/Fenix/Terekas/PET Configs/Neck_Deformation_(training images)/C ";
            RunTrainingMode = false;
            ClassNumber = 0;
        }

        #endregion

        #region Public properties

        public double MinimumRegionThreshold1
        { get; set; }

        public double MaximumRegionThreshold1
        { get; set; }

        public double MinimumRegionShape
        { get; set; }

        public double MaximumRegionShape
        { get; set; }

        public double MinimumRegionThreshold2
        { get; set; }

        public double MaximumRegionThreshold2
        { get; set; }

        public double MinimumFillShape
        { get; set; }

        public double MaximumFillShape
        { get; set; }

        public double MinimumSelectShape
        { get; set; }

        public double MaximumSelectShape
        { get; set; }

        public double EdgesAlpha
        { get; set; }

        public double EdgesLow
        { get; set; }

        public double EdgesHigh
        { get; set; }

        public double DilationCircle
        { get; set; }

        public string TrainingImage
        { get; set; }

        public bool RunTrainingMode
        { get; set; }

        public int ClassNumber
        { get; set; }

        #endregion
    }

    public class FxHNeckDeformation : FxBlock
    {
        #region Private Members

        private HImage inputImage;

        private bool _runBlockOnce = false;

        private bool showRegion = false;
        private bool showEmphasizeImage = false;
        private bool showNeckRegion = false;
        private bool showConnectedRegion = false;
        private bool showUsefulRegion = false;
        private bool showContour = false;

        HTuple selectFeatureNames;
        HClassKnn KNNHandle = new HClassKnn();

        HObject cont_;


        private bool SaveTrainingData = false;

        HTuple matches = new HTuple();
        int[] sizes = new int[10];
        HTuple derrivative = new HTuple();
        double sort;



        private List<OperationList> _operationsList;
        private int _operationIndex;
        private OperationMode _operationMode;

        List<NeckDeformationSettings> _neckDeformationSettings;

        private string settingsPath = @"C:\Fenix\Terekas\PET Preform\Filter Data\";

        #endregion

        #region Constructors

        public FxHNeckDeformation() { }

        public FxHNeckDeformation(string name)
        {
            Name = name;
            Icon = Resource.Default;
            IsMarkAble = true;
            Marks = new List<MarkObject>();
            Text = new List<TextObject>();

            BlockSettings = new List<NeckDeformationSettings>();

            try
            {
                HOperatorSet.ReadTuple(settingsPath + "FeatureNames", out selectFeatureNames);
                KNNHandle.ReadClassKnn(settingsPath + "TrainingDataKNN");
                KNNHandle.SetParamsClassKnn(new HTuple("method").TupleConcat("k").TupleConcat("max_num_classes"), new HTuple("classes_distance").TupleConcat(32).TupleConcat(1));
            }
            catch
            {
                FxSessionTimeClass.LogErrorMessage(this.Name + " error!", "Path not found: " + settingsPath);
            }

        }

        #endregion

        #region Public Methods

        [FilterClass]
        public override void Run()
        {
            Judgement = true;
            Judge = true;
            DateTime startTime = DateTime.Now;

            if (Marks != null)
            {
                Marks.Clear();
                Text.Clear();
            }

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            if (BlockOperationMode == OperationMode.SingleImageOperation)
            {
                if (BlockOperations != null && BlockOperations.Count > 0 && BlockOperations[OperationIndex].Image != null && BlockOperations[OperationIndex].Filter == FilterList.NeckDeformation)
                {
                    DrawRectangles();
                    if (!updateRegion)
                    {
                        ROITopRow = BlockOperations[OperationIndex].TopRow;
                        ROIBotRow = BlockOperations[OperationIndex].BottomRow;
                        ROITopCol = BlockOperations[OperationIndex].TopCollumn;
                        ROIBotCol = BlockOperations[OperationIndex].BottomCollumn;
                    }
                    FilterImage(BlockOperations[OperationIndex].Image, OperationIndex);
                }
                else
                {
                    Text.Add(new TextObject { Text = "Operation: #" + OperationIndex + " image does not exist", Color = "red", Font = "-Courier New-16-*-*-*-*-1-", Position = new Point(20, 20) });
                }
            }
            else
            {
                if (BlockOperations != null && BlockOperations.Count > 0)
                {
                    int oper = 0;

                    foreach (var operation in BlockOperations)
                    {
                        OperationIndex = oper;

                        if (operation.Image != null && operation.Filter == FilterList.NeckDeformation)
                        {
                            ROITopRow = operation.TopRow;
                            ROIBotRow = operation.BottomRow;
                            ROITopCol = operation.TopCollumn;
                            ROIBotCol = operation.BottomCollumn;

                            FilterImage(operation.Image, oper);

                        }
                        oper++;
                    }

                }
            }

            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }

        public override void Clear()
        {
            if (InputImage != null) InputImage.Dispose();
        }

        #endregion

        #region Private methods

        void FilterImage(HImage image, int operationIndex)
        {
            string color = !Judge ? "red" : "green";

            try
            {
                HRegion rect_ = new HRegion();

                HRegion region = new HRegion();
                HRegion regionFillUp = new HRegion();
                HRegion linesRegion = new HRegion();
                HRegion objectRegion = new HRegion();
                HRegion temporaryRegion = new HRegion();
                HRegion holesCont = new HRegion();
                HRegion blobsCont = new HRegion();
                HRegion region2 = new HRegion();
                HRegion rectRegion = new HRegion();
                HImage rectImage;
                HXLDCont outerCircleCont = new HXLDCont();
                HXLDCont outerCircleCont_test = new HXLDCont();

                double Row, Column, Radius, StartPhi, EndPhi;
                string PointOrder;


                HTuple score;

                HTuple genParamNames = new HTuple();
                HTuple genParamValues = new HTuple();

                HTuple result;
                HTuple rating;
                string[] classNames = { "perfect", "defect" };

                HClassTrainData ClassTrainDataHandle = new HClassTrainData();
                int[] DataSample1 = new int[0];

                HTuple DataSample = new HTuple();
                HTuple DataSample2 = new HTuple();

                int MinParamValue = 200;
                int MaxParamValue = 350;
                int ParameterInc = 10;
                HImage trainImage = new HImage();

                HTuple GrayValueProfile, VertProjection, GrayValueProfileChange, VertProjection1;
                int size = 2 * (MaxParamValue - MinParamValue) / ParameterInc;
                string[] FeatureNames = new string[size];
                int[] FeatureLength = new int[size];
                int i = 0;

                HRegion holesRegion = new HRegion();
                HRegion regionDilation = new HRegion();
                HRegion regionErosion = new HRegion();
                //HObject cont_;

                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                HImage = image.Clone();

                UpdateThreshold(operationIndex);
                BlockOperations[operationIndex].Marks = new List<MarkObject>();
                BlockOperations[operationIndex].Text = new List<TextObject>();


                region.GenRectangle1(ROITopRow, ROITopCol, ROIBotRow, ROIBotCol);
                rectImage = image.ReduceDomain(region);
                HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow, (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);



                //Find bootle mouth
                FindBottleMouth(rectImage, operationIndex, out Row, out Column, out Radius, out StartPhi, out EndPhi, out PointOrder);

                if (Radius > 140)
                {

                    //Training mode
                    if (BlockSettings[operationIndex].RunTrainingMode == true)
                    {

                        //Create a data vector and give names to all subfeatures
                        for (int TestParameter = MinParamValue; TestParameter < MaxParamValue; TestParameter = TestParameter + ParameterInc)
                        {
                            //Calculate the feature with TestParameter as parameter for the resolution of the polar transformation
                            ComputeGrayValueProfile(rectImage, Row, Column, Radius, TestParameter, out VertProjection, out VertProjection1, out GrayValueProfile, out GrayValueProfileChange);

                            FeatureNames[i] = "profile_" + TestParameter + "(" + TestParameter + ")";
                            FeatureLength[i] = TestParameter;
                            i++;
                            FeatureNames[i] = "profile_change_" + TestParameter + "(c" + TestParameter + ")";
                            FeatureLength[i] = TestParameter;
                            i++;

                            DataSample = DataSample.TupleConcat(GrayValueProfile);
                            DataSample = DataSample.TupleConcat(GrayValueProfileChange);
                        }

                        //Pre-Training
                        //*
                        //Generate the whole feature set for all images
                        //*
                        //Create the training data structure for the classifier                
                        ClassTrainDataHandle.CreateClassTrainData(DataSample.Length);
                        ClassTrainDataHandle.SetFeatureLengthsClassTrainData(FeatureLength, FeatureNames);









                        //*************************************************************************************************************************************
                        //Training
                        //The classes of the training data
                        /*if (SaveTrainingData == true)
                        {
                            outerCircleCont.GenCircleContourXld(Row, Column, Radius, 0, 2 * Math.PI, "positive", 1);

                            //Find bootle mouth
                            FindBottleMouth(rectImage, operationIndex, out Row, out Column, out Radius, out StartPhi, out EndPhi, out PointOrder);
                            outerCircleCont_test.GenCircleContourXld(Row, Column, Radius, 0, 2 * Math.PI, "positive", 1);
                            i = 0;
                         

                        
                            for (int TestParameter = MinParamValue; TestParameter < MaxParamValue; TestParameter = TestParameter + ParameterInc)
                            {
                                //Compute gray value profile
                                ComputeGrayValueProfile(rectImage, Row, Column, Radius, TestParameter, out VertProjection, out VertProjection1, out GrayValueProfile, out GrayValueProfileChange);

                                FeatureNames[i] = "profile_" + TestParameter + "(" + TestParameter + ")";
                                FeatureLength[i] = TestParameter;
                                i++;
                                FeatureNames[i] = "profile_change_" + TestParameter + "(c" + TestParameter + ")";
                                FeatureLength[i] = TestParameter;
                                i++;

                                DataSample2 = DataSample2.TupleConcat(GrayValueProfile);
                                DataSample2 = DataSample2.TupleConcat(GrayValueProfileChange);
                            }
                        
                         
                            ClassTrainDataHandle.AddSampleClassTrainData("row", DataSample2, BlockSettings[operationIndex].ClassNumber);
                            ClassTrainDataHandle.WriteClassTrainData("D:/Terekas/PET Preform/App Settings/config 2/ClassTrainData");
                            SaveTrainingData = false;
                        }*/
                        //***********************************************************************************************************************************











                        //Save training data

                        if (SaveTrainingData)
                        {
                            //The classes of the training data
                            int[] ClassNumber = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                                        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                                        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                                        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

                            for (int index = 1; index < 142; index++)
                            {
                                //trainImage.ReadImage(BlockSettings[operationIndex].TrainingImage + index);
                                trainImage.ReadImage(settingsPath + "Training images/PET (" + index + ")");
                                outerCircleCont.GenCircleContourXld(Row, Column, Radius, 0, 2 * Math.PI, "positive", 1);

                                region.GenRectangle1(ROITopRow, ROITopCol, ROIBotRow, ROIBotCol);
                                rectImage = trainImage.ReduceDomain(region);

                                //Find bootle mouth
                                FindBottleMouth(rectImage, operationIndex, out Row, out Column, out Radius, out StartPhi, out EndPhi, out PointOrder);
                                outerCircleCont_test.GenCircleContourXld(Row, Column, Radius, 0, 2 * Math.PI, "positive", 1);
                                i = 0;

                                for (int TestParameter = MinParamValue; TestParameter < MaxParamValue; TestParameter = TestParameter + ParameterInc)
                                {
                                    //Compute gray value profile
                                    ComputeGrayValueProfile(rectImage, Row, Column, Radius, TestParameter, out VertProjection, out VertProjection1, out GrayValueProfile, out GrayValueProfileChange);

                                    FeatureNames[i] = "profile_" + TestParameter + "(" + TestParameter + ")";
                                    FeatureLength[i] = TestParameter;
                                    i++;
                                    FeatureNames[i] = "profile_change_" + TestParameter + "(c" + TestParameter + ")";
                                    FeatureLength[i] = TestParameter;
                                    i++;

                                    DataSample2 = DataSample2.TupleConcat(GrayValueProfile);
                                    DataSample2 = DataSample2.TupleConcat(GrayValueProfileChange);
                                }//endfor

                                //Add the feature vector to the training data
                                ClassTrainDataHandle.AddSampleClassTrainData("row", DataSample2, ClassNumber[index - 1]);
                                ClassTrainDataHandle.WriteClassTrainData(settingsPath + "ClassTrainData");
                                SaveTrainingData = false;
                            }//endfor
                        }//endif


                        //*
                        //Auto-select features
                        //*
                        //Select the best features using a knn classifier
                        //The operator returns not only the best features,
                        //but also a classifier trained with these.
                        ClassTrainDataHandle.ReadClassTrainData(settingsPath + "ClassTrainData");

                        KNNHandle = ClassTrainDataHandle.SelectFeatureSetKnn("greedy", genParamNames, genParamValues, out selectFeatureNames, out score);

                        selectFeatureNames.WriteTuple(settingsPath + "FeatureNames");
                        KNNHandle.WriteClassKnn(settingsPath + "TrainingDataKNN");
                    }

                //Image processing
                    else
                    {

                        matches = selectFeatureNames.TupleRegexpMatch("\\d*");

                        for (int m = 0; m < matches.Length; m++)
                        {
                            Int32.TryParse(matches[m], out sizes[m]);
                        }

                        matches = selectFeatureNames.TupleRegexpMatch("(c)\\d+");

                        for (int t = 0; t < matches.Length; t++)
                        {
                            if (matches[t] == "c")
                            {
                                derrivative[t] = 1;
                            }
                            else
                            {
                                derrivative[t] = 0;
                            }
                        }

                        //Apply the newly trained classifier to the training set
                        FindBottleMouth(rectImage, operationIndex, out Row, out Column, out Radius, out StartPhi, out EndPhi, out PointOrder);

                        outerCircleCont_test.GenCircleContourXld(Row, Column, Radius, 0, 2 * Math.PI, "positive", 1);

                        HTuple feature = new HTuple();

                        for (int indexInner = 0; indexInner < selectFeatureNames.Length; indexInner++)
                        {
                            //Compute gray value profile
                            ComputeGrayValueProfile(rectImage, Row, Column, Radius, sizes[indexInner], out VertProjection, out VertProjection1, out GrayValueProfile, out GrayValueProfileChange);

                            if (derrivative[indexInner] != null)
                            {
                                feature = feature.TupleConcat(GrayValueProfileChange);
                            }
                            else
                            {
                                feature = feature.TupleConcat(GrayValueProfile);
                            }
                        }

                        //Classify
                        //HTuple[] genParamNames= new HTuple["method", "k", "max_num_classes"];

                        result = KNNHandle.ClassifyClassKnn(feature, out rating);


                        outerCircleCont.GenCircleContourXld(Row, Column, Radius, 0, 2 * Math.PI, "positive", 1);

                        switch (classNames[result])
                        {
                            case "defect":
                                Judge = false;
                                break;
                            case "perfect":
                                Judge = true;
                                break;
                        }
                    }//end else

                }//end first if
                else
                {
                    Judgement &= false;
                    Judge = false;
                }
            }
            catch (Exception ex)
            {
                Text.Add(new TextObject { Text = "Operation: #" + operationIndex + " error", Color = "red", Font = "-Courier New-16-*-*-*-*-1-", Position = new Point(20, 20) });
                Text.Add(new TextObject { Text = ex.Message, Color = "red", Font = "-Courier New-16-*-*-*-*-1-", Position = new Point(40, 20) });

                FxSessionTimeClass.LogErrorMessage(this.Name, ex.Message);

                Judgement &= false;
                Judge = false;
            }
            BlockOperations[operationIndex].Judgement = Judgement = Judge;

            color = !Judge ? "red" : "green";
            Marks.Add(new MarkObject { Name = "Contour", Color = color, Object = cont_ });
            BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Contour", Color = color, Object = cont_ });
        }

        public void FindBottleMouth(HImage rectImage, int operationIndex, out double Row, out double Column, out double Radius, out double StartPhi, out double EndPhi, out string PointOrder)
        {
            HRegion region = new HRegion();
            HRegion regionFillUp = new HRegion();
            HRegion linesRegion = new HRegion();
            HRegion objectRegion = new HRegion();
            HImage imgReduce;
            HXLDCont edges;
            double[] length;
            double maxClosureDist = 0, clippingFactor = 2;
            int maxNumPoints = -1, clippingEndPoints = 0, iterations = 3;

            region = rectImage.Threshold(BlockSettings[operationIndex].MinimumRegionThreshold1, BlockSettings[operationIndex].MaximumRegionThreshold1);

            if (ShowRegion)
            {
                Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = region });
                BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = region });
            }

            region = region.Connection();
            region = region.SelectShape("area", "and", BlockSettings[operationIndex].MinimumRegionShape, BlockSettings[operationIndex].MaximumRegionShape);

            if (ShowConnectedRegion)
            {
                Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = region });
                BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = region });
            }

            region = region.FillUp();
            imgReduce = rectImage.ReduceDomain(region);

            region = imgReduce.Threshold(BlockSettings[operationIndex].MinimumRegionThreshold2, BlockSettings[operationIndex].MaximumRegionThreshold2);
            region = region.DilationCircle(BlockSettings[operationIndex].DilationCircle);
            imgReduce = imgReduce.ReduceDomain(region);

            if (ShowNeckRegion)
            {
                Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = region });
                BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = region });
            }

            //The operator emphasize emphasizes high frequency areas of the image (edges and corners). The resulting images appears sharper.
            imgReduce = imgReduce.Emphasize(9, 9, 3);

            if (showEmphasizeImage)
            {
                HImage = imgReduce;
            }

            //region = imgReduce.Threshold(BlockSettings[operationIndex].MinimumRegionThreshold2, BlockSettings[operationIndex].MaximumRegionThreshold2);

            //region = region.Connection();
            //region = region.FillUpShape("area", BlockSettings[operationIndex].MinimumFillShape, BlockSettings[operationIndex].MaximumFillShape);//inner_circle
            //region = region.DilationCircle(BlockSettings[operationIndex].DilationCircle);
            //region = region.SelectShape("area", "and", BlockSettings[operationIndex].MinimumSelectShape, BlockSettings[operationIndex].MaximumSelectShape);

            //imgReduce = imgReduce.ReduceDomain(region);
            /*
            if (ShowUsefulRegion)
            {
                Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = region });
                BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = region });
            }
            */
            edges = imgReduce.EdgesSubPix("canny", BlockSettings[operationIndex].EdgesAlpha, BlockSettings[operationIndex].EdgesLow, BlockSettings[operationIndex].EdgesHigh);

            length = edges.LengthXld();

            double maxValue = length.Max();
            int maxIndex = length.ToList().IndexOf(maxValue);

            edges = edges.SelectObj(maxIndex + 1);

            if (ShowContour)
            {
                Marks.Add(new MarkObject { Name = "ShowEdge", Color = "red", Object = edges });
                BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "ShowEdge", Color = "red", Object = edges });
            }

            edges.FitCircleContourXld("ahuber", maxNumPoints, maxClosureDist, clippingEndPoints, iterations, clippingFactor, out Row, out Column, out Radius, out StartPhi, out EndPhi, out PointOrder);
        }

        public void ComputeGrayValueProfile(HImage image, double Row, double Column, double Radius, int Index, out HTuple VertProjection, out HTuple VertProjection1, out HTuple GrayValueProfile, out HTuple GrayValueProfileChange)
        {
            // This procedure calculates a feature that is based on a polar projection
            // of the grayvalues and their derivatives to the 1 dimensional feature array.

            //The found circle is unfolded to a polar image
            HImage PolarTransImage = image.PolarTransImageExt(Row, Column, Math.PI, 0, Radius - 1, Radius - 6, Index, Index, "bilinear");
            PolarTransImage = PolarTransImage.InvertImage();

            //Project gray values
            GrayValueProfile = PolarTransImage.GrayProjections(PolarTransImage, "simple", out VertProjection);

            //Calculate derivative
            PolarTransImage = PolarTransImage.SobelAmp("x_binomial", 3);
            PolarTransImage = PolarTransImage.AbsImage();

            //Project derivative
            GrayValueProfileChange = PolarTransImage.GrayProjections(PolarTransImage, "simple", out VertProjection1);

            //Scale derivative to a similar scale as the image values.
            GrayValueProfileChange = GrayValueProfileChange * 5;
        }

        public void UpdateThreshold(int index)
        {
            if (BlockSettings != null && BlockSettings.Count > 0 && BlockSettings.Count > index)
            {
                MinRegionThreshold1 = BlockSettings[index].MinimumRegionThreshold1;
                MaxRegionThreshold1 = BlockSettings[index].MaximumRegionThreshold1;
                MinRegionShape = BlockSettings[index].MinimumRegionShape;
                MaxRegionShape = BlockSettings[index].MaximumRegionShape;
                MinRegionThreshold2 = BlockSettings[index].MinimumRegionThreshold2;
                MaxRegionThreshold2 = BlockSettings[index].MaximumRegionThreshold2;
                RegionDilationCircle = BlockSettings[index].DilationCircle;
                //MinSelectShape = BlockSettings[index].MinimumSelectShape;
                //MaxSelectShape=BlockSettings[index].MaximumSelectShape;
                EdgAlpha = BlockSettings[index].EdgesAlpha;
                EdgLow = BlockSettings[index].EdgesLow;
                EdgHigh = BlockSettings[index].EdgesHigh;
                TrainImage = BlockSettings[index].TrainingImage;
                RunTrainMode = BlockSettings[index].RunTrainingMode;
                ClassNum = BlockSettings[index].ClassNumber;
            }
        }

        #endregion

        #region UI mouse clicks interaction

        private double roiTopCol = 200;
        private double roiTopRow = 150;
        private double roiBotCol = 400;
        private double roiBotRow = 315;
        private double roiStartRow = 200;
        private double roiStartCol = 150;
        private double roiEndRow = 315;
        private double roiEndCol = 400;
        private double sx, sy, dx, dy;
        private bool IsDiamBusy;

        private bool updateRegion = false;

        private void DrawRectangles()
        {
            HRegion rect_ = new HRegion();
            HRegion region = new HRegion();
            HObject cont_;

            HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow, (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);

            Marks.Add(new MarkObject { Name = "Contour", Color = "blue", Object = cont_ });
        }


        public void UpdateROI()
        {
            if (updateRegion)
            {
                BlockOperations[OperationIndex].TopRow = ROITopRow;
                BlockOperations[OperationIndex].BottomRow = ROIBotRow;
                BlockOperations[OperationIndex].TopCollumn = ROITopCol;
                BlockOperations[OperationIndex].BottomCollumn = ROIBotCol;
            }
        }

        public void HOnMouseDown(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                sx = (int)e.X;
                sy = (int)e.Y;

                if (sx > (ROITopCol - 15) && sx < (ROITopCol + 15) && sy > (ROITopRow - 15) && sy < (ROITopRow + 15))
                {
                    roiStartRow = ROITopRow;
                    roiStartCol = ROITopCol;
                    IsDiamBusy = false;
                }
                else
                {
                    roiEndRow = ROIBotRow;
                    roiEndCol = ROIBotCol;
                    IsDiamBusy = true;
                }

                updateRegion = true;
            }
        }

        public void HOnMouseUp(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                UpdateROI();
                updateRegion = false;
            }
        }

        public void HOnMouseMove(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {

                dx = (int)e.X - sx;
                dy = (int)e.Y - sy;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    if (!IsDiamBusy)
                    {
                        ROITopCol = roiStartCol + dx;
                        ROITopRow = roiStartRow + dy;
                    }
                    else
                    {
                        ROIBotCol = roiEndCol + dx;
                        ROIBotRow = roiEndRow + dy;
                    }

                }
            }
        }

        public void HOnMouseWheel(object sender, HMouseEventArgs e)
        {

        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top row position.")]
        [DisplayName("ROI top row")]
        public double ROITopRow
        {
            get
            {
                return roiTopRow;
            }
            set
            {
                roiTopRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot row position.")]
        [DisplayName("ROI bot row")]
        public double ROIBotRow
        {
            get
            {
                return roiBotRow;
            }
            set
            {
                roiBotRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top col position.")]
        [DisplayName("ROI top col")]
        public double ROITopCol
        {
            get
            {
                return roiTopCol;
            }
            set
            {
                roiTopCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopCol"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot col position.")]
        [DisplayName("ROI bot col")]
        public double ROIBotCol
        {
            get
            {
                return roiBotCol;
            }
            set
            {
                roiBotCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotCol"));
            }
        }


        #endregion


        #region Public Properties

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage InputImage
        {
            get
            {
                return inputImage;
            }
            set
            {
                inputImage = value != null ? value.CopyImage() : null;
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage _hImage { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage HImage
        {
            get
            {
                return _hImage;
            }
            set
            {
                _hImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HImage"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<HImage> InputImages { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public override Bitmap Icon { set; get; }

        [Browsable(false), BlockNameAttribute]
        public override string Name { set; get; }



        #region Properties from image grab block


        [CategoryAttribute("Block Settings")]
        [Description("Switch between single & multiple image grabbing")]
        [Browsable(false)]
        public OperationMode BlockOperationMode
        {
            get
            {
                return _operationMode;
            }
            set
            {
                _operationMode = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperationMode"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("Currently displayed operation (image)")]
        [XmlIgnoreAttribute, Browsable(false)]
        public int OperationIndex
        {
            get
            {
                return _operationIndex;
            }
            set
            {
                _operationIndex = value;

                OnPropertyChanged(this, new PropertyChangedEventArgs("OperationIndex"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("List of operations")]
        [XmlIgnoreAttribute]
        public List<OperationList> BlockOperations
        {
            get
            {
                return _operationsList;
            }
            set
            {
                _operationsList = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [XmlIgnoreAttribute, Browsable(false)]
        public bool RunBlockOnce
        {
            get { return _runBlockOnce; }
            set
            {
                _runBlockOnce = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("RunBlockOnce"));
            }
        }

        #endregion

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage 0: training mode")]
        [Description("The quality of the object being analyzed. 0 - object perfect, 1 - object with defect.")]
        [DisplayName(" 1. Class number")]
        public int ClassNum
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].ClassNumber;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    return BlockSettings[OperationIndex].ClassNumber;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].ClassNumber = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    ClassNum = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("ClassNum"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Run the training mode.")]
        [DisplayName(" 1. Run training mode")]
        public bool RunTrainMode
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].RunTrainingMode;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    return BlockSettings[OperationIndex].RunTrainingMode;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].RunTrainingMode = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    RunTrainMode = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("RunTrainMode"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Show calibration region")]
        [DisplayName(" 2. Show region")]
        public bool ShowRegion
        {
            get
            {
                return showRegion;
            }
            set
            {
                showRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowRegion"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("The minimum threshold for the bottle mouth region.")]
        [DisplayName(" 3. Min bottle mouth region threshold")]
        public double MinRegionThreshold1
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumRegionThreshold1;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    return BlockSettings[OperationIndex].MinimumRegionThreshold1;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumRegionThreshold1 = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    MinRegionThreshold1 = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinRegionThreshold1"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("The maximum threshold for the bottle mouth region.")]
        [DisplayName(" 4. Max bottle mouth region threshold")]
        public double MaxRegionThreshold1
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumRegionThreshold1;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    return BlockSettings[OperationIndex].MaximumRegionThreshold1;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumRegionThreshold1 = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    MaxRegionThreshold1 = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxRegionThreshold1"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Show connected bootle mouth region")]
        [DisplayName(" 5. Show connected region")]
        public bool ShowConnectedRegion
        {
            get
            {
                return showConnectedRegion;
            }
            set
            {
                showConnectedRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowConnectedRegion"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Size of the mouth region.")]
        [DisplayName(" 6. Min region shape")]
        public double MinRegionShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumRegionShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    return BlockSettings[OperationIndex].MinimumRegionShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumRegionShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    MinRegionShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinRegionShape"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Size of the mouth region.")]
        [DisplayName(" 7. Max region shape")]
        public double MaxRegionShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumRegionShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    return BlockSettings[OperationIndex].MaximumRegionShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumRegionShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    MaxRegionShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxRegionShape"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Show bootle mouth region")]
        [DisplayName(" 8. Show neck region")]
        public bool ShowNeckRegion
        {
            get
            {
                return showNeckRegion;
            }
            set
            {
                showNeckRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowNeckRegion"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Show emphasize bootle mouth image")]
        [DisplayName(" 9. Show emphasize image")]
        public bool ShowEmphasizeImage
        {
            get
            {
                return showEmphasizeImage;
            }
            set
            {
                showEmphasizeImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowEmphasizeImage"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("The minimum threshold for the neck circle region.")]
        [DisplayName("10. Min neck circle region threshold")]
        public double MinRegionThreshold2
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumRegionThreshold2;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    return BlockSettings[OperationIndex].MinimumRegionThreshold2;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumRegionThreshold2 = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    MinRegionThreshold2 = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinRegionThreshold2"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("The maximum threshold for the neck circle region.")]
        [DisplayName("11. Max neck circle region threshold")]
        public double MaxRegionThreshold2
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumRegionThreshold2;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    return BlockSettings[OperationIndex].MaximumRegionThreshold2;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumRegionThreshold2 = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    MaxRegionThreshold2 = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxRegionThreshold2"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Useful region edges dilation circle radius.")]
        [DisplayName("12. Neck region dilation circle radius")]
        public double RegionDilationCircle
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].DilationCircle;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    return BlockSettings[OperationIndex].DilationCircle;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].DilationCircle = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    RegionDilationCircle = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("RegionDilationCircle"));
            }
        }
        /*
        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Show useful bootle mouth region")]
        [DisplayName("13. Show useful neck region")]
        public bool ShowUsefulRegion
        {
            get
            {
                return showUsefulRegion;
            }
            set
            {
                showUsefulRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowUsefulRegion"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Size of the region with the aid of shape features.")]
        [DisplayName("14. Min select shape")]
        public double MinSelectShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumSelectShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    return BlockSettings[OperationIndex].MinimumSelectShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumSelectShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    MinSelectShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinSelectShape"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Size of the region with the aid of shape features.")]
        [DisplayName("15. Max select shape")]
        public double MaxSelectShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumSelectShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    return BlockSettings[OperationIndex].MaximumSelectShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumSelectShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    MaxSelectShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxSelectShape"));
            }
        }
        */
        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Useful neck circle region edges alpha parameter.")]
        [DisplayName("16. Edges alpha parameter")]
        public double EdgAlpha
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].EdgesAlpha;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    return BlockSettings[OperationIndex].EdgesAlpha;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].EdgesAlpha = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    EdgAlpha = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("EdgAlpha"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Useful neck circle region edges low parameter.")]
        [DisplayName("17. Edges low parameter")]
        public double EdgLow
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].EdgesLow;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    return BlockSettings[OperationIndex].EdgesLow;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].EdgesLow = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    EdgLow = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("EdgLow"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Useful neck circle region edges high parameter.")]
        [DisplayName("18. Edges high parameter")]
        public double EdgHigh
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].EdgesHigh;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    return BlockSettings[OperationIndex].EdgesHigh;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].EdgesHigh = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    EdgHigh = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("EdgHigh"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Show bootle mouth contour")]
        [DisplayName("19. Show neck contour")]
        public bool ShowContour
        {
            get
            {
                return showContour;
            }
            set
            {
                showContour = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowContour"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: filter training")]
        [Description("Images for filter training (folder path).")]
        [DisplayName(" 1. Training images")]
        public string TrainImage
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].TrainingImage;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    return BlockSettings[OperationIndex].TrainingImage;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].TrainingImage = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new NeckDeformationSettings());
                    TrainImage = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("TrainImage"));
            }
        }

        [CategoryAttribute("Calibration")]
        [Description("Neck Deformation settings")]
        public List<NeckDeformationSettings> BlockSettings
        {
            get
            {
                return _neckDeformationSettings;
            }
            set
            {
                _neckDeformationSettings = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockSettings"));
            }
        }

        #endregion


        #region Buttons

        [ButtonControl]
        public void NextImage()
        {
            if (BlockOperationMode == OperationMode.SingleImageOperation && BlockOperations != null && BlockOperations.Count > 0)
            {
                OperationIndex = OperationIndex < BlockOperations.Count - 1 ? OperationIndex + 1 : OperationIndex;
                RunBlockOnce = true;
            }
        }

        [ButtonControl]
        public void PreviousImage()
        {
            if (BlockOperationMode == OperationMode.SingleImageOperation && BlockOperations != null && BlockOperations.Count > 0)
            {
                OperationIndex = OperationIndex > 0 ? OperationIndex - 1 : OperationIndex;
                RunBlockOnce = true;
            }
        }

        [ButtonControl]
        public void TrainingMode()
        {
            SaveTrainingData = true;
        }

        #endregion

        #region Events
        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;

        #endregion

    }
}