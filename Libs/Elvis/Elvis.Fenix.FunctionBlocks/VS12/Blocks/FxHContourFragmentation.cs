﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

using System.Threading;
using System.Diagnostics;

namespace Elvis.Fenix.FunctionBlocks
{
    public class ContourFragmentationSettings
    {
        #region Constructors

        public ContourFragmentationSettings()
        {
            AnalyzedRegionMinThreshold = 200;
            AnalyzedRegionMaxThreshold = 255;
            MinRegionSize = 20000;
            MaxRegionSize = 500000;
            RegionDilationCircle = 15;
            LowEdgeThreshold = 20;
            HighEdgeThreshold = 55;
            EdgesAlphaParameter = 5;
            MinContourSize = 1500;
            MaxContourSize = 3000;
            SmoothPointsNumber = 201;
            AnalyzedAreaStep = 4;
            DefectLength = 4;
        }

        #endregion


        #region Public properties

        public double AnalyzedRegionMinThreshold
        { get; set; }

        public double AnalyzedRegionMaxThreshold
        { get; set; }

        public double MinRegionSize
        { get; set; }

        public double MaxRegionSize
        { get; set; }

        public double RegionDilationCircle
        { get; set; }

        public double LowEdgeThreshold
        { get; set; }

        public double HighEdgeThreshold
        { get; set; }

        public double EdgesAlphaParameter
        { get; set; }

        public double MinContourSize
        { get; set; }

        public double MaxContourSize
        { get; set; }

        public int SmoothPointsNumber
        { get; set; }

        public int AnalyzedAreaStep
        { get; set; }

        public double DefectLength
        { get; set; }

        #endregion
    }

    public class FxHContourFragmentation : FxBlock
    {
        #region Private Members

        List<HImage> list = new List<HImage>();

        private HImage inputImage;

        private bool showSelectedRegion;
        private bool showUsefulRegion;
        private bool showEdge;
        private bool showSmoothEdge;
        private bool showDefects;

        private bool _runBlockOnce = false;
   
        private List<OperationList> _operationsList;
        private int _operationIndex;
        private OperationMode _operationMode;
        private string _dir;
        private string _sceneName;

        List<ContourFragmentationSettings> _contourFragmentationSettings;

        #endregion


        #region Constructors

        public FxHContourFragmentation() { }

        public FxHContourFragmentation(string name)
        {
            Name = name;
            Icon = Resource.Default;
            IsMarkAble = true;
            Marks = new List<MarkObject>();
            Text = new List<TextObject>();

            BlockSettings = new List<ContourFragmentationSettings>();
        }

        #endregion


        #region Public Methods

        [FilterClass]
        public override void Run()
        {
            Judgement = true;
            Judge = true;
            DateTime startTime = DateTime.Now;

            if (Marks != null)
            {
                Marks.Clear();
                Text.Clear();
            }

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            if (BlockOperationMode == OperationMode.SingleImageOperation)
            {
                if (BlockOperations != null && BlockOperations.Count > 0 && BlockOperations[OperationIndex].Image != null && BlockOperations[OperationIndex].Filter == FilterList.ContourFragmentation) //ar priklauso filtrui
                {
                    DrawRectangles();

                    if (!updateRegion)
                    {
                        ROITopRow = BlockOperations[OperationIndex].TopRow;
                        ROIBotRow = BlockOperations[OperationIndex].BottomRow;
                        ROITopCol = BlockOperations[OperationIndex].TopCollumn;
                        ROIBotCol = BlockOperations[OperationIndex].BottomCollumn;
                    }

                    FilterImage(BlockOperations[OperationIndex].Image, OperationIndex);
                }
                else
                {
                    Text.Add(new TextObject { Text = "Operation: #" + OperationIndex + " image does not exist", Color = "red", Font = "-Courier New-16-*-*-*-*-1-", Position = new Point(20, 20) });
                    //RunBlockOnce = true;
                }
            }
            else
            {
                if (BlockOperations != null && BlockOperations.Count > 0)
                {
                    int oper = 0;

                    foreach (var operation in BlockOperations)
                    {
                        OperationIndex = oper;
                        if (operation.Image != null && operation.Filter == FilterList.ContourFragmentation) //ar priklauso filtrui
                        {
                            Judge = true;
                            ROITopRow = operation.TopRow;
                            ROIBotRow = operation.BottomRow;
                            ROITopCol = operation.TopCollumn;
                            ROIBotCol = operation.BottomCollumn;

                            FilterImage(operation.Image, oper);
                        }
                        oper++;
                    }
                }
            }
            
            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }

        public override void Clear()
        {
            //if (OutputImage != null) OutputImage.Dispose();
            if (InputImage != null) InputImage.Dispose();
        }

        #endregion


        #region Private methods

        void FilterImage(HImage image, int operationIndex)
        {
            try
            {
                HRegion rect_ = new HRegion();
                HRegion region = new HRegion();
                HRegion LineRegion = new HRegion();
                HRegion Line = new HRegion();
                HRegion defectRegion;
                HXLDCont edges = new HXLDCont();
                HXLDCont smooth = new HXLDCont();
                HTuple Row = new HTuple();
                HTuple Col = new HTuple();
                HTuple Row1, Col1;
                HTuple isOverlapping;
                HTuple edgeLength;
                HObject cont_ = new HObject();
                HImage imgReduce;
                int Length;
                double dx, dy;
                double sin_a;
                double x, y;
                double x1, y1, x2, y2;
                double length;
                double defectLength;

                Stopwatch stopwatch = new Stopwatch();

                string color = !Judge ? "red" : "green";

                UpdateSettings(operationIndex);
                BlockOperations[operationIndex].Marks = new List<MarkObject>();
                BlockOperations[operationIndex].Text = new List<TextObject>();

                HImage = image;
                stopwatch.Start();
                rect_.GenRectangle1(ROITopRow, ROITopCol, ROIBotRow, ROIBotCol);
                imgReduce = image.ReduceDomain(rect_);

                HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow,
                    (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);

                Marks.Add(new MarkObject { Name = "Image", Color = "red", Object = imgReduce });
                BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Image", Color = "red", Object = imgReduce });

                //Process images

                region = imgReduce.Threshold(BlockSettings[operationIndex].AnalyzedRegionMinThreshold, BlockSettings[operationIndex].AnalyzedRegionMaxThreshold);
                region = region.Connection();
                if (ShowSelectedRegion)
                {
                    Marks.Add(new MarkObject { Name = "Region", Color = "green", Object = region });
                    BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Region", Color = "green", Object = region });
                }


                region = region.SelectShape("area", "and", BlockSettings[operationIndex].MinRegionSize, BlockSettings[operationIndex].MaxRegionSize);
                if (ShowUsefulRegion)
                {
                    Marks.Add(new MarkObject { Name = "Region", Color = "green", Object = region });
                    BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Region", Color = "green", Object = region });
                }

                region = region.DilationCircle(BlockSettings[operationIndex].RegionDilationCircle);
                imgReduce = imgReduce.ReduceDomain(region);
                edges = imgReduce.EdgesSubPix("canny", BlockSettings[operationIndex].EdgesAlphaParameter, BlockSettings[operationIndex].LowEdgeThreshold, BlockSettings[operationIndex].HighEdgeThreshold);
                edges = edges.SelectContoursXld("contour_length", BlockSettings[operationIndex].MinContourSize, BlockSettings[operationIndex].MaxContourSize, -0.5, 0.5);
                smooth = edges.SmoothContoursXld(BlockSettings[operationIndex].SmoothPointsNumber);

                //************************************
                //Masks rupture
                edgeLength = edges.LengthXld();
                if (edgeLength.Length > 0)
                {
                    edgeLength = 0;
                }
                else
                {
                    Judgement = false;
                    Judge = false;
                }
                //************************************

                if (ShowEdge)
                {
                    Marks.Add(new MarkObject { Name = "ShowEdge", Color = "blue", Object = edges });
                    BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "ShowEdge", Color = "blue", Object = edges });
                }

                if (ShowSmoothEdge)
                {
                    Marks.Add(new MarkObject { Name = "ShowEdge", Color = "green", Object = smooth });
                    BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "ShowEdge", Color = "green", Object = smooth });
                }

                try
                {
                    smooth.GetContourXld(out Row, out Col);
                }
                catch
                {
                    Judgement &= false;
                    Judge = false;
                }

                LineRegion.GenRegionLine(0, 0, 0, 0);
                Length = Col.TupleLength();

                for (int i = Length - 5; i > BlockSettings[operationIndex].AnalyzedAreaStep; i = i - BlockSettings[operationIndex].AnalyzedAreaStep)
                {
                    dx = Col[i - BlockSettings[operationIndex].AnalyzedAreaStep] - Col[i];
                    dy = Row[i] - Row[i - BlockSettings[operationIndex].AnalyzedAreaStep];

                    sin_a = dx / Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2));

                    double c = 20;
                    y = sin_a * c;
                    x = Math.Sqrt(Math.Pow(c, 2) - Math.Pow(y, 2));

                    if (dy < 0)
                    {
                        x1 = Col[i] - x;
                        y1 = Row[i] + y;
                        x2 = Col[i] + x;
                        y2 = Row[i] - y;
                    }
                    else
                    {
                        x1 = Col[i] - x;
                        y1 = Row[i] - y;
                        x2 = Col[i] + x;
                        y2 = Row[i] + y;
                    }

                    try
                    {
                        HOperatorSet.IntersectionLineContourXld(edges, y1, x1, y2, x2, out Row1, out Col1, out isOverlapping);
                        
                        //****************************************************
                        double rowLength = Row1.TupleLength();
                        HTuple[] subRow = new HTuple[(int)rowLength];
                        HTuple[] subCol = new HTuple[(int)rowLength];
                        HTuple Row0 = 0;
                        HTuple Col0 = 0;

                        try
                        {
                            if (rowLength > 1)
                            {
                                subRow[0] = Row1.TupleMax();
                                subCol[0] = Col1.TupleMax();

                                for (int z = 0; z < rowLength; z++)
                                {
                                    subRow[1] = Math.Abs((double)Row[i] - (double)Row1[z]);
                                    if (subRow[1] < subRow[0])
                                    {
                                        subRow[0] = subRow[1];
                                        Row0 = z;
                                    }

                                    subCol[1] = Math.Abs((double)Col[i] - (double)Col1[z]);
                                    if (subCol[1] < subCol[0])
                                    {
                                        subCol[0] = subCol[1];
                                        Col0 = z;
                                    }
                                }
                                Row1 = Row1[Row0];
                                Col1 = Col1[Col0];
                            }
                        }
                        catch(HOperatorException exception)
                        {
                            FxSessionTimeClass.LogErrorMessage("(Contour Fragmentation filter) ", exception.GetErrorText());
                        }
                        //****************************************************
                        
                        length = Math.Sqrt(Math.Pow(Row1[0] - Row[i], 2) + Math.Pow(Col1[0] - Col[i], 2));

                        //px -> mm
                        if (MeasurementMode)
                            defectLength = BlockSettings[operationIndex].DefectLength * MeasurementCoefficient;
                        else
                            defectLength = BlockSettings[operationIndex].DefectLength;

                        if (length > BlockSettings[operationIndex].DefectLength)
                        {
                            Line.GenRegionLine((double)Row[i], Col[i], Row1[0], Col1[0]);
                            LineRegion = LineRegion.Union2(Line);
                            Judgement &= false;
                            Judge = false;
                        }
                    }
                    catch
                    {
                        Judgement &= false;
                        Judge = false;
                    }
                }

                defectRegion = LineRegion.DilationCircle((double)BlockSettings[operationIndex].AnalyzedAreaStep);
                defectRegion = defectRegion.ErosionCircle((double)BlockSettings[operationIndex].AnalyzedAreaStep);

                bool dontShowRegion = false;
                try
                {
                    if (defectRegion.Area != null && defectRegion.Column != null && defectRegion.Row != null)
                    {
                        dontShowRegion = false;
                    }
                    else
                        dontShowRegion = true;
                }
                catch
                {
                    dontShowRegion = true;
                }

                if (ShowDefects && !dontShowRegion)
                {
                    Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = defectRegion });
                    BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = defectRegion });
                }

                color = !Judge ? "red" : "green";
                Marks.Add(new MarkObject { Name = "Contour", Color = color, Object = cont_ });
                BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Contour", Color = color, Object = cont_ });

                if (operationIndex >= 0)
                {
                    Text.Add(new TextObject { Text = "Operation: #" + operationIndex, Color = color, Font = "-Courier New-14-*-*-*-*-1-", Position = new Point((int)ROITopRow - 20, (int)ROITopCol + 10) });
                    BlockOperations[operationIndex].Text.Add(new TextObject { Text = "Operation: #" + operationIndex, Color = color, Font = "-Courier New-14-*-*-*-*-1-", Position = new Point((int)ROITopRow - 20, (int)ROITopCol + 10) });
                }

                stopwatch.Stop();
                Text.Add(new TextObject { Text = "Elapsed time: " + stopwatch.ElapsedMilliseconds + "ms", Color = color, Font = "-Courier New-14-*-*-*-*-1-", Position = new Point((int)10, 10) });
                BlockOperations[operationIndex].Text.Add(new TextObject { Text = "Elapsed time: " + stopwatch.ElapsedMilliseconds + "ms", Color = color, Font = "-Courier New-14-*-*-*-*-1-", Position = new Point((int)10, 10) });
            }


            catch (HOperatorException exception)
            {
                FxSessionTimeClass.LogErrorMessage("(HContour Fragmentation filter - Stage I) ", exception.GetErrorText());
                Judgement &= false;
                Judge = false;
            }

            BlockOperations[operationIndex].Judgement = Judge;
        }

        public void UpdateSettings(int index)
        {
            if (BlockSettings != null && BlockSettings.Count > 0 && BlockSettings.Count > index)
            {
                AnalyzedRegionMinimumThreshold = BlockSettings[index].AnalyzedRegionMinThreshold;
                AnalyzedRegionMaximumThreshold = BlockSettings[index].AnalyzedRegionMaxThreshold;
                MinRegSize = BlockSettings[index].MinRegionSize;
                MaxRegSize = BlockSettings[index].MaxRegionSize;
                RegDilationCircle = BlockSettings[index].RegionDilationCircle;
                LowerEdgeThreshold = BlockSettings[index].LowEdgeThreshold;
                HigherEdgeThreshold = BlockSettings[index].HighEdgeThreshold;
                MinContSize = BlockSettings[index].MinContourSize;
                MaxContSize = BlockSettings[index].MaxContourSize;
                EdgesAlphaParam = BlockSettings[index].EdgesAlphaParameter;
                AreaStep = BlockSettings[index].AnalyzedAreaStep;
                SmoothPntNmb = BlockSettings[index].SmoothPointsNumber;
                DefLength = BlockSettings[index].DefectLength;
            }
        }

        #endregion


        #region UI mouse clicks interaction

        private double roiTopCol = 200;
        private double roiTopRow = 150;
        private double roiBotCol = 400;
        private double roiBotRow = 315;
        private double roiStartRow = 200;
        private double roiStartCol = 150;
        private double roiEndRow = 315;
        private double roiEndCol = 400;
        private double sx, sy, dx, dy;
        private bool IsDiamBusy;

        private bool updateRegion = false;

        private void DrawRectangles()
        {
            HRegion rect_ = new HRegion();
            HRegion region = new HRegion();
            HObject cont_;

            HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow, (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);

            Marks.Add(new MarkObject { Name = "Contour", Color = "blue", Object = cont_ });
        }


        public void UpdateROI()
        {
            if (updateRegion && BlockOperations != null && BlockOperations.Count > OperationIndex)
            {
                BlockOperations[OperationIndex].TopRow = ROITopRow;
                BlockOperations[OperationIndex].BottomRow = ROIBotRow;
                BlockOperations[OperationIndex].TopCollumn = ROITopCol;
                BlockOperations[OperationIndex].BottomCollumn = ROIBotCol;
            }
        }

        public void HOnMouseDown(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                sx = (int)e.X;
                sy = (int)e.Y;

                if (sx > (ROITopCol - 15) && sx < (ROITopCol + 15) && sy > (ROITopRow - 15) && sy < (ROITopRow + 15))
                {
                    roiStartRow = ROITopRow;
                    roiStartCol = ROITopCol;
                    IsDiamBusy = false;
                }
                else
                {
                    roiEndRow = ROIBotRow;
                    roiEndCol = ROIBotCol;
                    IsDiamBusy = true;
                }

                updateRegion = true;
            }
        }

        public void HOnMouseUp(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                UpdateROI();
                updateRegion = false;
            }
        }

        public void HOnMouseMove(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {

                dx = (int)e.X - sx;
                dy = (int)e.Y - sy;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    if (!IsDiamBusy)
                    {
                        ROITopCol = roiStartCol + dx;
                        ROITopRow = roiStartRow + dy;
                    }
                    else
                    {
                        ROIBotCol = roiEndCol + dx;
                        ROIBotRow = roiEndRow + dy;
                    }

                }
            }
        }

        public void HOnMouseWheel(object sender, HMouseEventArgs e)
        {

        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top row position.")]
        [DisplayName("ROI top row")]
        public double ROITopRow
        {
            get
            {
                return roiTopRow;
            }
            set
            {
                roiTopRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot row position.")]
        [DisplayName("ROI bot row")]
        public double ROIBotRow
        {
            get
            {
                return roiBotRow;
            }
            set
            {
                roiBotRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top col position.")]
        [DisplayName("ROI top col")]
        public double ROITopCol
        {
            get
            {
                return roiTopCol;
            }
            set
            {
                roiTopCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopCol"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot col position.")]
        [DisplayName("ROI bot col")]
        public double ROIBotCol
        {
            get
            {
                return roiBotCol;
            }
            set
            {
                roiBotCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotCol"));
            }
        }


        #endregion


        #region Public properties

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage InputImage
        {
            get
            {
                return inputImage;
            }
            set
            {
                inputImage = value != null ? value.CopyImage() : null;
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage _hImage { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage HImage
        {
            get
            {
                return _hImage;
            }
            set
            {
                _hImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HImage"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<HImage> InputImages { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<HImage> OutputImages { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage OutputImage { get; set; }

        [XmlIgnoreAttribute]
        [Browsable(false)]
        public override Bitmap Icon { set; get; }

        [Browsable(false), BlockNameAttribute]
        public override string Name { set; get; }


        #region Properties from image grab block

        [CategoryAttribute("Block Settings")]
        [Description("Switch between single & multiple image grabbing")]
        [Browsable(false)]
        public OperationMode BlockOperationMode
        {
            get
            {
                return _operationMode;
            }
            set
            {
                _operationMode = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperationMode"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("Currently displayed operation (image)")]
        [XmlIgnoreAttribute, Browsable(false)]
        public int OperationIndex
        {
            get
            {
                return _operationIndex;
            }
            set
            {
                _operationIndex = value;

                OnPropertyChanged(this, new PropertyChangedEventArgs("OperationIndex"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("List of operations")]
        [XmlIgnoreAttribute]
        public List<OperationList> BlockOperations
        {
            get
            {
                return _operationsList;
            }
            set
            {
                _operationsList = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [XmlIgnoreAttribute, Browsable(false)]
        public bool RunBlockOnce
        {
            get { return _runBlockOnce; }
            set
            {
                _runBlockOnce = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("RunBlockOnce"));
            }
        }

        

        #endregion

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find contour")]
        [Description("Minimum analyzed region threshold.")]
        [DisplayName(" 2. Min analyzed region threshold")]
        public double AnalyzedRegionMinimumThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].AnalyzedRegionMinThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    return BlockSettings[OperationIndex].AnalyzedRegionMinThreshold;
                }
            }
            set
            {
                try
                {
                    BlockSettings[OperationIndex].AnalyzedRegionMinThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    AnalyzedRegionMinimumThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("AnalyzedRegionMinimumThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find contour")]
        [Description("Maximum analyzed region threshold.")]
        [DisplayName(" 3. Max analyzed region threshold")]
        public double AnalyzedRegionMaximumThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].AnalyzedRegionMaxThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    return BlockSettings[OperationIndex].AnalyzedRegionMaxThreshold;
                }
            }
            set
            {
                try
                {
                    BlockSettings[OperationIndex].AnalyzedRegionMaxThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    AnalyzedRegionMaximumThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("AnalyzedRegionMaximumThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find contour")]
        [Description("Minimum selected region size.")]
        [DisplayName(" 5. Min region size")]
        public double MinRegSize
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinRegionSize;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    return BlockSettings[OperationIndex].MinRegionSize;
                }
            }
            set
            {
                try
                {
                    BlockSettings[OperationIndex].MinRegionSize = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    MinRegSize = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinRegionSize"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find contour")]
        [Description("Maximum selected region size.")]
        [DisplayName(" 6. Max region size")]
        public double MaxRegSize
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaxRegionSize;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    return BlockSettings[OperationIndex].MaxRegionSize;
                }
            }
            set
            {
                try
                {
                    BlockSettings[OperationIndex].MaxRegionSize = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    MaxRegSize = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxRegionSize"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find contour")]
        [Description("Region dilation circle.")]
        [DisplayName(" 7. Region dilation circle")]
        public double RegDilationCircle
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].RegionDilationCircle;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    return BlockSettings[OperationIndex].RegionDilationCircle;
                }
            }
            set
            {
                try
                {
                    BlockSettings[OperationIndex].RegionDilationCircle = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    RegDilationCircle = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("RegDilationCircle"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find contour")]
        [Description("Minimum edge threshold value.")]
        [DisplayName(" 9. Low edge threshold")]
        public double LowerEdgeThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].LowEdgeThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    return BlockSettings[OperationIndex].LowEdgeThreshold;
                }
            }
            set
            {
                try
                {
                    BlockSettings[OperationIndex].LowEdgeThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    LowerEdgeThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("LowerEdgeThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find contour")]
        [Description("Maximum edge threshold value.")]
        [DisplayName("10. High edge threshold")]
        public double HigherEdgeThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].HighEdgeThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    return BlockSettings[OperationIndex].HighEdgeThreshold;
                }
            }
            set
            {
                try
                {
                    BlockSettings[OperationIndex].HighEdgeThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    HigherEdgeThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("HigherEdgeThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find contour")]
        [Description("Alpha parameter for edge detection.")]
        [DisplayName("11. Alpha parameter (edge)")]
        public double EdgesAlphaParam
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].EdgesAlphaParameter;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    return BlockSettings[OperationIndex].EdgesAlphaParameter;
                }
            }
            set
            {
                try
                {
                    BlockSettings[OperationIndex].EdgesAlphaParameter = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    EdgesAlphaParam = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("EdgesAlphaParam"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find contour")]
        [Description("The points number for smooth operation.")]
        [DisplayName("15. Smooth points number")]
        public int SmoothPntNmb
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].SmoothPointsNumber;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    return BlockSettings[OperationIndex].SmoothPointsNumber;
                }
            }
            set
            {
                try
                {
                    BlockSettings[OperationIndex].SmoothPointsNumber = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    SmoothPntNmb = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("SmoothPntNmb"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find contour")]
        [Description("Minimal edge contour size.")]
        [DisplayName("12. Min contour size")]
        public double MinContSize
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinContourSize;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    return BlockSettings[OperationIndex].MinContourSize;
                }
            }
            set
            {
                try
                {
                    BlockSettings[OperationIndex].MinContourSize = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    MinContSize = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinContSize"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find contour")]
        [Description("Maximal edge contour size.")]
        [DisplayName("13. Max contour size")]
        public double MaxContSize
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaxContourSize;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    return BlockSettings[OperationIndex].MaxContourSize;
                }
            }
            set
            {
                try
                {
                    BlockSettings[OperationIndex].MaxContourSize = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    MaxContSize = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxContSize"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: contour analysis")]
        [Description("Analysis step.")]
        [DisplayName("2. Analyzed area step")]
        public int AreaStep
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].AnalyzedAreaStep;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    return BlockSettings[OperationIndex].AnalyzedAreaStep;
                }
            }
            set
            {
                try
                {
                    BlockSettings[OperationIndex].AnalyzedAreaStep = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    AreaStep = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("AreaStep"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: contour analysis")]
        [Description("The length of the defect (line length).")]
        [DisplayName("3. Defect length")]
        public double DefLength
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].DefectLength;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    return BlockSettings[OperationIndex].DefectLength;
                }
            }
            set
            {
                try
                {
                    BlockSettings[OperationIndex].DefectLength = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourFragmentationSettings());
                    DefLength = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("DefLength"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find contour")]
        [Description("Show selected region.")]
        [DisplayName(" 1. Show selected region")]
        public bool ShowSelectedRegion
        {
            get
            {
                return showSelectedRegion;
            }
            set
            {
                showSelectedRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowSelectedRegion"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find contour")]
        [Description("Show useful region.")]
        [DisplayName(" 4. Show useful region")]
        public bool ShowUsefulRegion
        {
            get
            {
                return showUsefulRegion;
            }
            set
            {
                showUsefulRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowUsefulRegion"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find contour")]
        [Description("Show edge.")]
        [DisplayName(" 8. Show edge")]
        public bool ShowEdge
        {
            get
            {
                return showEdge;
            }
            set
            {
                showEdge = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowEdge"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find contour")]
        [Description("Show smooth edge.")]
        [DisplayName("14. Show smooth edge")]
        public bool ShowSmoothEdge
        {
            get
            {
                return showSmoothEdge;
            }
            set
            {
                showSmoothEdge = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowSmoothEdge"));
            }
        }

        [CategoryAttribute("Stage II: contour analysis")]
        [Description("Show defects.")]
        [DisplayName("1. Show defects")]
        public bool ShowDefects
        {
            get
            {
                return showDefects;
            }
            set
            {
                showDefects = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowDefects"));
            }
        }


        [CategoryAttribute("Calibration")]
        [Description("Contour fragmentation settings")]
        public List<ContourFragmentationSettings> BlockSettings
        {
            get
            {
                return _contourFragmentationSettings;
            }
            set
            {
                _contourFragmentationSettings = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockSettings"));
            }
        }

        #endregion


        #region Buttons

        [ButtonControl]
        public void NextImage()
        {
            if (BlockOperationMode == OperationMode.SingleImageOperation && BlockOperations != null && BlockOperations.Count > 0)
            {
                OperationIndex = OperationIndex < BlockOperations.Count - 1 ? OperationIndex + 1 : OperationIndex;
                RunBlockOnce = true;
            }
        }

        [ButtonControl]
        public void PreviousImage()
        {
            if (BlockOperationMode == OperationMode.SingleImageOperation && BlockOperations != null && BlockOperations.Count > 0)
            {
                OperationIndex = OperationIndex > 0 ? OperationIndex - 1 : OperationIndex;
                RunBlockOnce = true;
            }
        }       

        #endregion


        #region Events

        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;        

        #endregion
    }
}