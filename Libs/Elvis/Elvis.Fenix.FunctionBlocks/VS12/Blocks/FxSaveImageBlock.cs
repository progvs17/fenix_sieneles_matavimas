﻿using Elvis.Fenix.FunctionBlocks;
using Elvis.Fenix.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using HalconDotNet;
using System.ComponentModel;
using System.Drawing;

namespace Elvis.Fenix.FunctionBlocks
{
    public class FxImageLog : FxBlock
    {
        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;

        private string _prefix;


        [Browsable(false)]
        FxScene scene;

        [Browsable(false)]
        //public HImage InputImage { get; set; }

        private bool _enableLogging;
        private bool _enableNgLogging;

        [Description("Saves image to file")]
        [DisplayName("Logg images")]
        public bool EnableLogging 
        {
            get 
            {
                return _enableLogging;
            }
            set
            {
                _enableLogging = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("EnableLogging"));
            }
        }

        [Description("Saves only negatvie images")]
        [DisplayName("Logg only NG Images")]
        public bool EnableNgLogging
        {
            get
            {
                return _enableNgLogging;
            }

            set
            {
                _enableNgLogging = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("EnableNgLogging"));
            }
        }
        [Description("Save images ordering folders by date")]
        [DisplayName("Order by date")]
        public bool EnableFolderOrderByDate
        {
            get
            {
                return _logger.IsOrderByDateEnabled;
            }

            set
            {
                _logger.IsOrderByDateEnabled = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("EnableFolderOrderByDate"));
            }
        }

        [Description("Saves files to specified derectory")]
        [DisplayName("Saving directory")]
        public string FilePath
        {
            get
            {
                return _logger.FolderPath;
            }

            set
            {
                _logger.FolderPath = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("FilePath"));
            }
        }

        [Description("Save files with specified filename prefix")]
        [DisplayName("File prefix")]
        public string Prefix
        {
            get
            {
                return _prefix;
            }

            set
            {
                _prefix = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Prefix"));
            }
        }

        FxImageLogger _logger;

        public override Bitmap Icon { set; get; }

        [Browsable(false), BlockNameAttribute]
        public override string Name { set; get; }


        public FxImageLog(Object sender, string name, FxImageLogger logger)
        {
            scene = sender as FxScene;
            Name = name;
            Icon = Resource.SaveImage;
            _logger = logger;
            EnableLogging = false;
            Prefix = "IMG";
        }

        public override void Run()
        {
            Judgement = true;

            if(onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            if (EnableLogging)
            {
                if ((EnableLogging && !EnableNgLogging) || (EnableLogging && EnableNgLogging && !scene.Judgement))
                {
                    IntPtr data;
                    string type;
                    int width;
                    int height;

                    try
                    {
                        //data = InputImage.GetImagePointer1(out type, out width, out height);
                        //if(!scene.Judgement)
                        //{
                        //    _logger.SaveImage(InputImage, _prefix + "-OK-");                    
                        //}
                        //else
                        //{
                        //    _logger.SaveImage(InputImage, _prefix + "-NG-");
                        //}                                               
                    }
                    catch (Exception ex)
                    {
                        Judgement = false;
                    }
                    finally
                    {
                        
                    }
                }                              
            }

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }

        public override void Clear()
        {
            
        }








    }
}
