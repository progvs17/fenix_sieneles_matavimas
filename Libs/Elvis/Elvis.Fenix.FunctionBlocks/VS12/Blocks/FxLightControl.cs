﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elvis.Fenix.FunctionBlocks
{
    public class FxLightControl : FxBlock
    {
        public Hardware.GPIO _gpio;
        private bool _disposed;
        private int _output;
        private int defStatus;

        public int Output {
            get
            {
                return _output;
            }
            set
            {
                _output = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Output"));               
            }
        }

        public FxLightControl(string name, Fenix.Hardware.GPIO gpio, int def)
        {
            Name = name;
            _gpio = gpio;
            Icon = Resource.Laser;
            defStatus = def;
        }

        public override void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            _disposed = true;
        }

        public override void Run()
        {
            Judgement = true;

            if(_gpio != null)
            {
              _gpio.SetPin(Output, defStatus);            
            } 
            else
            {
                Judgement = false;
            }
        }

        public override void Clear()
        {
            // Empty
        }       
    }
}
