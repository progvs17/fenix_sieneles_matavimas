﻿using HalconDotNet;
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Threading;

namespace Elvis.Fenix.FunctionBlocks
{
    public class ContourBreakSettings
    {
        #region Constructors

        public ContourBreakSettings()
        {
            MinimumRegionThreshold = 40;
            MaximumRegionThreshold = 180;
            MinimumRegionShape = 3000;
            MaximumRegionShape = 25000;
            DilationCircleRadius = 2;
            ContourLowAccuracy = 10;
            ContourHighAccuracy = 20;
            AlphaParameter = 1;
            AnalyzedAreaStep = 10;
            DefectLength = 3;
        }

        #endregion

        #region Public properties

        public double MinimumRegionThreshold
        { get; set; }

        public double MaximumRegionThreshold
        { get; set; }

        public double MinimumRegionShape
        { get; set; }

        public double MaximumRegionShape
        { get; set; }

        public double DilationCircleRadius
        { get; set; }

        public double ContourLowAccuracy
        { get; set; }

        public double ContourHighAccuracy
        { get; set; }

        public double AlphaParameter
        { get; set; }

        public int AnalyzedAreaStep
        { get; set; }

        public double DefectLength
        { get; set; }

        #endregion
    }

    public class FxHContourBreak : FxBlock
    {
        #region Private Members

        List<HImage> list = new List<HImage>();

        private HImage inputImage;

        private bool showRegion = false;
        private bool showUsefulRegion = false;
        private bool showContour = false;
        private bool _runBlockOnce = false;

        private List<OperationList> _operationsList;
        private int _operationIndex;
        private OperationMode _operationMode;
        private string _dir;
        private string _sceneName;

        List<ContourBreakSettings> _contourBreakSettings;

        #endregion

        #region Constructors

        public FxHContourBreak() { }

        public FxHContourBreak(string name)
        {
            Name = name;
            Icon = Resource.Judgement;
            IsMarkAble = true;
            Marks = new List<MarkObject>();
            Text = new List<TextObject>();

            BlockSettings = new List<ContourBreakSettings>();
        }

        #endregion

        #region Public Methods

        [FilterClass]
        public override void Run()
        {
            Judgement = true;
            Judge = true;
            DateTime startTime = DateTime.Now;

            if (Marks != null)
            {
                Marks.Clear();
                Text.Clear();
            }

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            if (BlockOperationMode == OperationMode.SingleImageOperation)
            {
                if (BlockOperations != null && BlockOperations.Count > 0 && BlockOperations[OperationIndex].Image != null && BlockOperations[OperationIndex].Filter == FilterList.ContourBreak)
                {
                    DrawRectangles();

                    if (!updateRegion)
                    {
                        ROITopRow = BlockOperations[OperationIndex].TopRow;
                        ROIBotRow = BlockOperations[OperationIndex].BottomRow;
                        ROITopCol = BlockOperations[OperationIndex].TopCollumn;
                        ROIBotCol = BlockOperations[OperationIndex].BottomCollumn;
                    }

                    HImage = BlockOperations[OperationIndex].Image.Clone();
                    FilterImage(BlockOperations[OperationIndex].Image, OperationIndex);
                }
                else
                {
                    Text.Add(new TextObject { Text = "Operation: #" + OperationIndex + "image does not exist", Color = "red", Font = "-Courier New-16-*-*-*-*-1-", Position = new Point(20, 20) });
                }
            }
            else
            {
                if (BlockOperations != null && BlockOperations.Count > 0)
                {
                    int oper = 0;

                    foreach (var operation in BlockOperations)
                    {
                        OperationIndex = oper;

                        if (operation.Image != null && operation.Filter == FilterList.ContourBreak)
                        {
                            Judge = true;
                            ROITopRow = operation.TopRow;
                            ROIBotRow = operation.BottomRow;
                            ROITopCol = operation.TopCollumn;
                            ROIBotCol = operation.BottomCollumn;

                            HImage = operation.Image.Clone();
                            FilterImage(operation.Image, oper);
                        }
                        oper++;
                    }
                }
            }

            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }

        public override void Clear()
        {
            if (InputImage != null) InputImage.Dispose();
        }

        #endregion

        #region Private Methods

        void FilterImage(HImage image, int operationIndex)
        {
            try
            {
                HRegion region = new HRegion();
                HRegion rectRegion = new HRegion();
                HImage rectImage;
                HImage imgReduce;
                HObject cont_;
                HXLDCont edges;
                HXLDCont longestContour;

                double length;
                double order;

                HTuple row_, col_;
                HTuple arrayLength;
                HTuple RowYStart;
                HTuple ColXStart;
                HTuple RowYEnd;
                HTuple ColXEnd;
                HTuple RowCenter1;
                HTuple ColCenter1;
                HTuple RowCenter2;
                HTuple ColCenter2;
                HTuple ScaleX;
                HTuple ScaleY;
                HTuple NewXStart;
                HTuple NewYStart;
                HTuple NewXEnd;
                HTuple NewYEnd;
                HTuple X;
                HTuple Y;
                HTuple RowCont;
                HTuple ColCont;
                HTuple RowLine;
                HTuple ColLine;
                HTuple isOverlapping;

                double x1, y1, x2, y2;
                double distance1, distance2;
                double decision;

                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                string color = !Judge ? "red" : "green";

                UpdateThreshold(operationIndex);
                BlockOperations[operationIndex].Marks = new List<MarkObject>();
                BlockOperations[operationIndex].Text = new List<TextObject>();

                region.GenRectangle1(ROITopRow, ROITopCol, ROIBotRow, ROIBotCol);
                rectImage = image.ReduceDomain(region);
                HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow, (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);

                region = rectImage.Threshold(BlockSettings[operationIndex].MinimumRegionThreshold, BlockSettings[operationIndex].MaximumRegionThreshold);

                if (ShowRegion)
                {
                    Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = region });
                    BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = region });
                }

                region = region.Connection();
                region = region.FillUp();
                region = region.SelectShape("area", "and", BlockSettings[operationIndex].MinimumRegionShape, BlockSettings[operationIndex].MaximumRegionShape);
                region = region.DilationCircle(BlockSettings[operationIndex].DilationCircleRadius);

                if (ShowUsefulRegion)
                {
                    Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = region });
                    BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Region", Color = "red", Object = region });
                }

                imgReduce = rectImage.ReduceDomain(region);

                edges = imgReduce.EdgesSubPix("canny", BlockSettings[operationIndex].AlphaParameter, BlockSettings[operationIndex].ContourLowAccuracy, BlockSettings[operationIndex].ContourHighAccuracy);

                length = edges.LengthXld();

                longestContour = edges.SelectObj(1);

                //Kontūro koordinatės
                longestContour.GetContourXld(out row_, out col_);

                if (ShowContour)
                {
                    Marks.Add(new MarkObject { Name = "ShowEdge", Color = "red", Object = longestContour });
                    BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "ShowEdge", Color = "red", Object = longestContour });
                }

                arrayLength = col_.TupleLength();
                order = col_[arrayLength - 1] - col_[0];
                if (order < 0)
                {
                    row_ = row_.TupleInverse();
                    col_ = col_.TupleInverse();
                }

                HOperatorSet.IntersectionLineContourXld(longestContour, row_[20], col_[20] - 15, row_[20], col_[20] + 160, out RowYStart, out ColXStart, out isOverlapping);
                RowCenter1 = RowYStart[0];
                ColCenter1 = (ColXStart[1] - ColXStart[0]) / 2 + ColXStart[0];

                HOperatorSet.IntersectionLineContourXld(longestContour, row_[300], col_[300], row_[300], col_[300] + 150, out RowYEnd, out ColXEnd, out isOverlapping);
                RowCenter2 = RowYEnd[0];
                ColCenter2 = (ColXEnd[1] - ColXEnd[0]) / 2 + ColXEnd[0];

                ScaleX = ColCenter1 - ColCenter2;
                ScaleX = Math.Abs((double)ScaleX);

                ScaleY = RowCenter1 - RowCenter2;
                ScaleY = Math.Abs((double)ScaleY);

                if (ColCenter1 > ColCenter2)
                {
                    NewXStart = ColCenter1 + ScaleX;
                    NewYStart = RowCenter1 - ScaleY;
                    NewXEnd = ColCenter2 - 2 * ScaleX;
                    NewYEnd = RowCenter2 + 2 * ScaleY;
                }
                else
                {
                    NewXStart = ColCenter1 - ScaleX;
                    NewYStart = RowCenter1 - ScaleY;
                    NewXEnd = ColCenter2 + 2 * ScaleX;
                    NewYEnd = RowCenter2 + 2 * ScaleY;
                }

                //Image processing
                for (int i = 0; i < arrayLength / 2 - 5 * BlockSettings[operationIndex].AnalyzedAreaStep; i = i + BlockSettings[operationIndex].AnalyzedAreaStep)
                {
                    if (ColCenter1 > ColCenter2)
                    {
                        X = col_[i] + ScaleY;
                        Y = row_[i] + ScaleX;
                    }
                    else
                    {
                        X = col_[i] + ScaleY;
                        Y = row_[i] - ScaleX;
                    }

                    //LongestContour and test line intersection
                    HOperatorSet.IntersectionLineContourXld(longestContour, row_[i], col_[i] - 5, Y, X + 5, out RowCont, out ColCont, out isOverlapping);

                    order = RowCont.TupleLength();
                    if (order > 1)
                    {
                        //Test line intersection with center line
                        HOperatorSet.IntersectionLines(RowCont[0], ColCont[0], RowCont[1], ColCont[1], NewYStart, NewXStart, NewYEnd, NewXEnd, out RowLine, out ColLine, out isOverlapping);

                        //Distances:
                        x1 = ColCont[1] - ColLine;
                        y1 = RowCont[1] - RowLine;
                        x2 = ColLine - ColCont[0];
                        y2 = RowLine - RowCont[0];

                        distance1 = Math.Sqrt(Math.Pow(x1, 2) + Math.Pow(y1, 2));
                        distance2 = Math.Sqrt(Math.Pow(x2, 2) + Math.Pow(y2, 2));
                        decision = Math.Abs(distance1 - distance2);

                        //Damage recognition
                        if (decision > BlockSettings[operationIndex].DefectLength)
                        {
                            Judgement &= false;
                            Judge = false;
                        }
                    }
                }

                color = !Judge ? "red" : "green";
                Marks.Add(new MarkObject { Name = "Contour", Color = color, Object = cont_ });
                BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Contour", Color = color, Object = cont_ });

            }
            catch (Exception ex)
            {
                Text.Clear();
                Text.Add(new TextObject { Text = "Operation: #" + operationIndex + " error", Color = "red", Font = "-Courier New-16-*-*-*-*-1-", Position = new Point(20, 20) });

                Judgement &= false;
                Judge = false;
            }
            BlockOperations[operationIndex].Judgement = Judge;
        }

        public void UpdateThreshold(int index)
        {
            if (BlockSettings != null && BlockSettings.Count > 0 && BlockSettings.Count > index)
            {
                MinRegionThreshold = BlockSettings[index].MinimumRegionThreshold;
                MaxRegionThreshold = BlockSettings[index].MaximumRegionThreshold;
                MinRegionShape = BlockSettings[index].MinimumRegionShape;
                MaxRegionShape = BlockSettings[index].MaximumRegionShape;
                LowerContourAccuracy = BlockSettings[index].ContourLowAccuracy;
                HigherContourAccuracy = BlockSettings[index].ContourHighAccuracy;
                AreaStep = BlockSettings[index].AnalyzedAreaStep;
                DefLength = BlockSettings[index].DefectLength;
            }
        }

        #endregion

        #region UI mouse clicks interaction

        private double roiTopCol = 200;
        private double roiTopRow = 150;
        private double roiBotCol = 400;
        private double roiBotRow = 315;
        private double roiStartRow = 200;
        private double roiStartCol = 150;
        private double roiEndRow = 315;
        private double roiEndCol = 400;
        private double sx, sy, dx, dy;
        private bool IsDiamBusy;

        private bool updateRegion = false;

        private void DrawRectangles()
        {
            HRegion rect_ = new HRegion();
            HRegion region = new HRegion();
            HObject cont_;

            HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow, (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);

            Marks.Add(new MarkObject { Name = "Contour", Color = "blue", Object = cont_ });
        }


        public void UpdateROI()
        {
            if (updateRegion)
            {
                BlockOperations[OperationIndex].TopRow = ROITopRow;
                BlockOperations[OperationIndex].BottomRow = ROIBotRow;
                BlockOperations[OperationIndex].TopCollumn = ROITopCol;
                BlockOperations[OperationIndex].BottomCollumn = ROIBotCol;
            }
        }

        public void HOnMouseDown(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                sx = (int)e.X;
                sy = (int)e.Y;

                if (sx > (ROITopCol - 15) && sx < (ROITopCol + 15) && sy > (ROITopRow - 15) && sy < (ROITopRow + 15))
                {
                    roiStartRow = ROITopRow;
                    roiStartCol = ROITopCol;
                    IsDiamBusy = false;
                }
                else
                {
                    roiEndRow = ROIBotRow;
                    roiEndCol = ROIBotCol;
                    IsDiamBusy = true;
                }

                updateRegion = true;
            }
        }

        public void HOnMouseUp(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                UpdateROI();
                updateRegion = false;
            }
        }

        public void HOnMouseMove(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {

                dx = (int)e.X - sx;
                dy = (int)e.Y - sy;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    if (!IsDiamBusy)
                    {
                        ROITopCol = roiStartCol + dx;
                        ROITopRow = roiStartRow + dy;
                    }
                    else
                    {
                        ROIBotCol = roiEndCol + dx;
                        ROIBotRow = roiEndRow + dy;
                    }

                }
            }
        }

        public void HOnMouseWheel(object sender, HMouseEventArgs e)
        {

        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top row position.")]
        [DisplayName("ROI top row")]
        public double ROITopRow
        {
            get
            {
                return roiTopRow;
            }
            set
            {
                roiTopRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot row position.")]
        [DisplayName("ROI bot row")]
        public double ROIBotRow
        {
            get
            {
                return roiBotRow;
            }
            set
            {
                roiBotRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top col position.")]
        [DisplayName("ROI top col")]
        public double ROITopCol
        {
            get
            {
                return roiTopCol;
            }
            set
            {
                roiTopCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopCol"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot col position.")]
        [DisplayName("ROI bot col")]
        public double ROIBotCol
        {
            get
            {
                return roiBotCol;
            }
            set
            {
                roiBotCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotCol"));
            }
        }


        #endregion

        #region Public Properties

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage InputImage
        {
            get
            {
                return inputImage;
            }
            set
            {
                inputImage = value != null ? value.CopyImage() : null;
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage _hImage { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage HImage
        {
            get
            {
                return _hImage;
            }
            set
            {
                _hImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HImage"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<HImage> InputImages { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public override Bitmap Icon { set; get; }

        [Browsable(false), BlockNameAttribute]
        public override string Name { set; get; }



        #region Properties from image grab block


        [CategoryAttribute("Block Settings")]
        [Description("Switch between single & multiple image grabbing")]
        [Browsable(false)]
        public OperationMode BlockOperationMode
        {
            get
            {
                return _operationMode;
            }
            set
            {
                _operationMode = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperationMode"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("Currently displayed operation (image)")]
        [XmlIgnoreAttribute, Browsable(false)]
        public int OperationIndex
        {
            get
            {
                return _operationIndex;
            }
            set
            {
                _operationIndex = value;

                OnPropertyChanged(this, new PropertyChangedEventArgs("OperationIndex"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("List of operations")]
        [XmlIgnoreAttribute]
        public List<OperationList> BlockOperations
        {
            get
            {
                return _operationsList;
            }
            set
            {
                _operationsList = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [XmlIgnoreAttribute, Browsable(false)]
        public bool RunBlockOnce
        {
            get
            {
                return _runBlockOnce;
            }
            set
            {
                _runBlockOnce = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("RunBlockOnce"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Show the calibration region")]
        [DisplayName("1. Show the region")]
        public bool ShowRegion
        {
            get
            {
                return showRegion;
            }
            set
            {
                showRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowRegion"));
            }
        }


        #endregion

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("The minimum threshold for the analyzed region.")]
        [DisplayName("2. Min region threshold")]
        public double MinRegionThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumRegionThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    return BlockSettings[OperationIndex].MinimumRegionThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumRegionThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    MinRegionThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinRegionThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("The maximum threshold for the analyzed region.")]
        [DisplayName("3. Max region threshold")]
        public double MaxRegionThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumRegionThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    return BlockSettings[OperationIndex].MaximumRegionThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumRegionThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    MaxRegionThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxRegionThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Show the useful region")]
        [DisplayName("4. Show the useful region")]
        public bool ShowUsefulRegion
        {
            get
            {
                return showUsefulRegion;
            }
            set
            {
                showUsefulRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowUsefulRegion"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Filterable size of the region.")]
        [DisplayName("5. Min region size")]
        public double MinRegionShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumRegionShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    return BlockSettings[OperationIndex].MinimumRegionShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumRegionShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    MinRegionShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinRegionShape"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Filterable size of the region.")]
        [DisplayName("6. Max region size")]
        public double MaxRegionShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumRegionShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    return BlockSettings[OperationIndex].MaximumRegionShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumRegionShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    MaxRegionShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxRegionShape"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: useful region")]
        [Description("Useful region edges dilation circle radius.")]
        [DisplayName("7. Region dilation circle radius")]
        public double RegionDilationCircle
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].DilationCircleRadius;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    return BlockSettings[OperationIndex].DilationCircleRadius;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].DilationCircleRadius = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    RegionDilationCircle = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("RegionDilationCircle"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find contour break")]
        [Description("Contour in the lower limit of detection accuracy.")]
        [DisplayName("1. Contour accuracy (low)")]
        public double LowerContourAccuracy
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].ContourLowAccuracy;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    return BlockSettings[OperationIndex].ContourLowAccuracy;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].ContourLowAccuracy = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    LowerContourAccuracy = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("LowerContourAccuracy"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find contour break")]
        [Description("Contour in the higher limit of detection accuracy.")]
        [DisplayName("2. Contour accuracy (high)")]
        public double HigherContourAccuracy
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].ContourHighAccuracy;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    return BlockSettings[OperationIndex].ContourHighAccuracy;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].ContourHighAccuracy = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    HigherContourAccuracy = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("HigherContourAccuracy"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find contour break")]
        [Description("Alpha parameter for edge detection.")]
        [DisplayName("3. Alpha parameter (edge)")]
        public double AlphaParam
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].AlphaParameter;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    return BlockSettings[OperationIndex].AlphaParameter;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].AlphaParameter = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    AlphaParam = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("AlphaParam"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find contour break")]
        [Description("Show the PET contour")]
        [DisplayName("4. Show the contour")]
        public bool ShowContour
        {
            get
            {
                return showContour;
            }
            set
            {
                showContour = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowContour"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find contour break")]
        [Description("Analysis step.")]
        [DisplayName("5. Analyzed area step")]
        public int AreaStep
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].AnalyzedAreaStep;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    return BlockSettings[OperationIndex].AnalyzedAreaStep;
                }
            }
            set
            {
                try
                {
                    BlockSettings[OperationIndex].AnalyzedAreaStep = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    AreaStep = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("AreaStep"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage II: find contour break")]
        [Description("The length of the defect (line length).")]
        [DisplayName("6. Defect length")]
        public double DefLength
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].DefectLength;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    return BlockSettings[OperationIndex].DefectLength;
                }
            }
            set
            {
                try
                {
                    BlockSettings[OperationIndex].DefectLength = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new ContourBreakSettings());
                    DefLength = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("DefLength"));
            }
        }

        [CategoryAttribute("Calibration")]
        [Description("Contour Break settings")]
        public List<ContourBreakSettings> BlockSettings
        {
            get
            {
                return _contourBreakSettings;
            }
            set
            {
                _contourBreakSettings = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockSettings"));
            }
        }

        #endregion


        #region Buttons

        [ButtonControl]
        public void Refresh()
        {
            RunBlockOnce = true;
        }

        [ButtonControl]
        public void NextImage()
        {
            if (BlockOperationMode == OperationMode.SingleImageOperation && BlockOperations != null && BlockOperations.Count > 0)
            {
                OperationIndex = OperationIndex < BlockOperations.Count - 1 ? OperationIndex + 1 : OperationIndex;
                RunBlockOnce = true;
            }
        }

        [ButtonControl]
        public void PreviousImage()
        {
            if (BlockOperationMode == OperationMode.SingleImageOperation && BlockOperations != null && BlockOperations.Count > 0)
            {
                OperationIndex = OperationIndex > 0 ? OperationIndex - 1 : OperationIndex;
                RunBlockOnce = true;
            }
        }

        #endregion

        #region Events
        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;


        #endregion
    }
}
