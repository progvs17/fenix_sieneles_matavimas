﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

using Elvis.Fenix.Hardware;

namespace Elvis.Fenix.FunctionBlocks
{
    public class FxStartBlock : FxBlock
    {
        private bool _Disposed;
        private FxGpio _gpio;
        private ManualResetEventSlim _exit = new ManualResetEventSlim(false);

        private int bottleFlow;
        private int bottleCatch;   
        private int nozzle1;
        private int bottleIN;
        private int _start;
        private int actuatorOnDO;

        private string nextBlockName;
        private bool _imageLoaderEnabled;
        private bool _position1IsBusy;
        private bool _position2IsBusy;
        private bool bottleCaught = false;
        private bool canCatchNew = false;
        private int bottleFlowDelay = 85;
        private int noBottleDelay = 2000;
        

        public FxStartBlock()
        {
        }

        public FxStartBlock(string name, FxGpio gpio)
        {
            Name = name;
            Icon = Resource.DI_branch;
            _gpio = gpio;
            //_gpio.onInputsChanged += _gpio_onInputsChanged;

            IsStartBlock = true;
        }

        public override void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_Disposed)
                return;

            if (disposing)
            {
                //_gpio.onInputsChanged -= _gpio_onInputsChanged;
                _exit.Set();
                
            }
            _Disposed = true;
        }

        public override void Stop() 
        {
            _exit.Set();
        }

        public override void Run()
        {
            Judgement = true;
            DateTime startTime = DateTime.Now;

            _exit.Reset();

            if (onTaskStarted != null)
                onTaskStarted(this);

            bottleCaught = false;
            NextFxBlock = EndBlock;

            //lock (_gpio)
            //{
            // patikrinti pirmos sklendes busena ir kitas griebtuvas yra pasiruoses pagauti nauja buteli
            // jei ji uzdaryta - atidaryti ~85 ms
            if (_gpio != null && _gpio.GetDOPin(BottleFlowDO) == 0 && _gpio.GetDOPin(BottleCatchDO) == 0 && CanStart)
            {
                DateTime waitTime = DateTime.Now;
                CanStart = false;

                // ijungti putiklius
                _gpio.SetPin(Nozzle1DO, 1);

                
                _gpio.SetPin(BottleFlowDO, 1);
                while (((DateTime.Now - waitTime).TotalMilliseconds < BottleFlowDelay) && !_exit.IsSet) ;

                _gpio.SetPin(BottleFlowDO, 0);

                while (!bottleCaught && ((DateTime.Now - startTime).TotalMilliseconds < NoBottleDelay) && !_exit.IsSet)
                {
                    // daviklis suveike
                    if (_gpio.GetPin(BottleInDI, 0) == 1 && !_exit.IsSet)
                    {
                        bottleCaught = true;
                    }
                }

                // yra uzfiksuotas butelis
                if (bottleCaught)
                {
                    // pagauti buteli
                    _gpio.SetPin(BottleCatchDO, 1);

                    // isjungti putiklius
                    _gpio.SetPin(Nozzle1DO, 0);
                    

                    NextFxBlock = Position1;
                }
                else
                {
                    // kitu atveju

                }
            }
            else
            {
                //if(_gpio.GetPin(0, 0) == 0 && !CanStart)
                //{
                //    // ijungti putiklius
                //    _gpio.SetPin(2, 1);
                //    _gpio.SetPin(3, 1);

                //    CanStart = true;
                //}


                //// kitu atveju ismesti buteli
                //// po 2 sec 
                //DateTime waitTime = DateTime.Now;
                //while (((DateTime.Now - waitTime).TotalMilliseconds < 2000) && !_exit.IsSet) ;

                //// paleisti buteli
                //_gpio.SetPin(1, 0);
            }


            CountResults = bottleCaught;

            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;
            Judge = Judgement;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }

        public override void Clear()
        {
            // base.Clear();
        }


        #region Private Methods

        private void SetScenePath()
        {


        }


        #endregion

        #region Properties

        [XmlIgnoreAttribute, Browsable(false), BlockRellationsAttribute]
        public FxBlock Position1 { set; get; }

        [XmlIgnoreAttribute, Browsable(false), BlockRellationsAttribute]
        public FxBlock Position2 { set; get; }

        [XmlIgnoreAttribute, Browsable(false), BlockRellationsAttribute]
        public FxBlock EndBlock { set; get; }

        [XmlIgnoreAttribute, Browsable(false)]
        public bool CanStart
        {
            get
            {
                return canCatchNew;
            }
            set
            {
                canCatchNew = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("CanStart"));
            }
        }

        [XmlIgnoreAttribute, Browsable(false)]
        public bool CountResults { get; set; }

        public int Start
        {
            get
            {
                return _start;
            }

            set
            {
                _start = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Start"));
            }
        }


        public int BottleFlowDelay
        {
            get
            {
                return bottleFlowDelay;
            }
            set
            {
                bottleFlowDelay = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BottleFlowDelay"));
            }
        }

        public int NoBottleDelay
        {
            get
            {
                return noBottleDelay;
            }
            set
            {
                noBottleDelay = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("NoBottleDelay"));
            }
        }

        public int BottleFlowDO
        {
            get
            {
                return bottleFlow;
            }

            set
            {
                bottleFlow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BottleFlowDO"));
            }
        }

        public int BottleCatchDO
        {
            get
            {
                return bottleCatch;
            }

            set
            {
                bottleCatch = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BottleCatchDO"));
            }
        }

        public int Nozzle1DO
        {
            get
            {
                return nozzle1;
            }

            set
            {
                nozzle1 = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Nozzle1DO"));
            }
        }
        
        public int BottleInDI
        {
            get
            {
                return bottleIN;
            }

            set
            {
                bottleIN = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BottleInDI"));
            }
        }

        public int ActuatorOnDO
        {
            get
            {
                return actuatorOnDO;
            }

            set
            {
                actuatorOnDO = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ActuatorOnDO"));
            }
        }

        [DisplayName("Mount1 Direction")]
        [XmlIgnoreAttribute]
        public string Direction1
        {
            get
            {
                return Position1 != null ? Position1.Name : "null";
            }
        }

        [DisplayName("Mount2 Direction")]
        [XmlIgnoreAttribute]
        public string Direction2
        {
            get
            {
                return Position2 != null ? Position2.Name : "null";
            }
        }


        [DisplayName("Next Block Name")]
        [XmlIgnoreAttribute]
        public string NextBlockName
        {
            get
            {
                return NextFxBlock != null ? NextFxBlock.Name : nextBlockName;
            }
            set
            {
                if (NextFxBlock == null)
                    nextBlockName = value;
                //NextFxBlock != null ? nextBlockName = value : "null";
            }
        }


        [XmlIgnoreAttribute]
        public bool Position1IsBusy
        {
            get
            {
                return _position1IsBusy;
            }
            set
            {
                _position1IsBusy = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Position1IsBusy"));
            }
        }

        [XmlIgnoreAttribute]
        public bool Position2IsBusy
        {
            get
            {
                return _position2IsBusy;
            }
            set
            {
                _position2IsBusy = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Position2IsBusy"));
            }
        }
        
        [XmlIgnoreAttribute, Browsable(false)]
        public bool ImageLoaderEnabled
        {
            get
            {
                return _imageLoaderEnabled;
            }
            set
            {
                _imageLoaderEnabled = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ImageLoaderEnabled"));
            }
        }

        #endregion

        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;

        [ButtonControl]
        public void BottleCatcher()
        {
            if (_gpio != null)
            {
                if (_gpio.GetDOPin(BottleCatchDO) == 0)
                {
                    // pagauti buteli
                    _gpio.SetPin(BottleCatchDO, 1);
                }
                else
                {
                    // paleisti buteli
                    _gpio.SetPin(BottleCatchDO, 0);
                }
            }
        }

    }
}
