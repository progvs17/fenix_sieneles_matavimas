﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Elvis.Fenix.FunctionBlocks
{
    public class FxHBlots : FxBlock
    {
        #region Private Members

        List<HImage> list = new List<HImage>();

        private HImage inputImage;
        private double roiTopCol = 150;
        private double roiTopRow = 180;
        private double roiBotCol = 350;
        private double roiBotRow = 300;
        private double minThreshold1 = 180;
        private double maxThreshold1 = 255;
        private double minThreshold2 = 0;
        private double maxThreshold2 = 110;
        private double sizeBlots = 50;

        private double roiStartRow;
        private double roiStartCol;
        private double roiEndRow;
        private double roiEndCol;
        private double sx, sy, dx, dy;
        private bool IsDiamBusy;

        private bool showBlots = true;

        bool _saveImages = false;
        string _imageDirectory = "C://Fenix//brokas";

        #endregion

        #region Constructors

        public FxHBlots()
        {

        }

        public FxHBlots(string name)
        {
            Name = name;
            Icon = Resource.Default;
            IsMarkAble = true;
            Marks = new List<MarkObject>();
            Text = new List<TextObject>();

            
        }

        #endregion

        #region Public Methods

        [FilterClass]
        public override void Run()
        {
            Judgement = true;
            DateTime startTime = DateTime.Now;

            if (Marks != null)
            {
                Marks.Clear();
                Text.Clear();
            }

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            if (BlockOperations != null)//(InputImages != null)
            {
                try
                {
                    
                    foreach (var operation in BlockOperations)
                    {
                        if (operation.Image != null)  //if (operation.Filter == FilterList.Blots && operation.Image != null)
                        {
                            HImage image = operation.Image;
                            ROITopRow = operation.TopRow;
                            ROITopCol = operation.TopCollumn;
                            ROIBotRow = operation.BottomRow;
                            ROIBotCol = operation.BottomCollumn;

                            HRegion rect_ = new HRegion();
                            HRegion region = new HRegion();
                            HRegion regionBlots = new HRegion();
                            HObject cont_;
                            HImage imgReduce;
                            int Blots;                       

                            rect_.GenRectangle1(ROITopRow, ROITopCol, ROIBotRow, ROIBotCol);
                            imgReduce = image.ReduceDomain(rect_);//image

                            HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow, (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);

                            region = imgReduce.Threshold(minThreshold1, maxThreshold1);

                            region = region.FillUp();
                            region = region.Connection();
                            region = region.SelectShape("area", "and", 500, 100000);
                            region = region.Union1();
                            imgReduce = image.ReduceDomain(region);

                            HSystem.SetSystem("empty_region_result", "exception");

                            region = imgReduce.Threshold(minThreshold2, maxThreshold2);
                            try
                            {
                                region = region.Connection();
                                regionBlots = region.SelectShape("area", "and", 0, sizeBlots);
                                Blots = regionBlots.CountObj();
                                regionBlots = regionBlots.Boundary("outer");
                            }
                            catch
                            {
                                Blots = 0;
                            }

                            Judgement = Blots > 0 || !Judgement ? false : true;
                            string color = !Judgement ? "red" : "green";

                            HImage = image.Clone();

                            Marks.Add(new MarkObject { Name = "Blots", Color = "red", Object = regionBlots });
                            Marks.Add(new MarkObject { Name = "Contour", Color = color, Object = cont_ });

                            if (showBlots) Text.Add(new TextObject { Text = "Blots:" + Blots.ToString(), Color = color, Font = "-Courier New-18-*-*-*-*-1-", Position = new Point((int)ROIBotRow - 15, (int)ROIBotCol + 10) });

                            if (!Judgement)
                            {
                                if (_saveImages)
                                    imgReduce.WriteImage("tiff", 0, _imageDirectory + "//" + "blots_" + DateTime.Now.ToString("yyyy-MM-dd_HHmmssff"));

                                break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Judgement = false;
                }
            }

            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }

        public override void Clear()
        {
            if (OutputImage != null) OutputImage.Dispose();
            if (InputImage != null) InputImage.Dispose();
        }

        public void HOnMouseDown(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                sx = (int)e.X;
                sy = (int)e.Y;

                if (sx > (ROITopCol - 15) && sx < (ROITopCol + 15) && sy > (ROITopRow - 15) && sy < (ROITopRow + 15))
                {
                    roiStartRow = ROITopRow;
                    roiStartCol = ROITopCol;
                    IsDiamBusy = false;
                }
                else
                {
                    roiEndRow = ROIBotRow;
                    roiEndCol = ROIBotCol;
                    IsDiamBusy = true;
                }
            }
        }

        public void HOnMouseUp(object sender, HMouseEventArgs e)
        {

        }

        public void HOnMouseMove(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {

                dx = (int)e.X - sx;
                dy = (int)e.Y - sy;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    if (!IsDiamBusy)
                    {
                        ROITopCol = roiStartCol + dx;
                        ROITopRow = roiStartRow + dy;
                    }
                    else
                    {
                        ROIBotCol = roiEndCol + dx;
                        ROIBotRow = roiEndRow + dy;
                    }

                }
            }
        }

        public void HOnMouseWheel(object sender, HMouseEventArgs e)
        {

        }

        #endregion

        #region Public Properties

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<OperationList> BlockOperations { get; set; }

        [CategoryAttribute("Block Settings")]
        [Description("Save defected images?")]
        [DisplayName("Image logging")]
        public bool SaveImages
        {
            get
            {
                return _saveImages;
            }
            set
            {
                _saveImages = value;
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("Saves files to specified directory")]
        [DisplayName("Saving directory")]
        public string FilePath
        {
            get
            {
                return _imageDirectory;
            }

            set
            {
                _imageDirectory = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("FilePath"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage InputImage
        {
            get
            {
                return inputImage;
            }
            set
            {
                inputImage = value != null ? value.CopyImage() : null;
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage _hImage { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage HImage
        {
            get
            {
                return _hImage;
            }
            set
            {
                _hImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HImage"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<HImage> InputImages { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<HImage> OutputImages { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage OutputImage { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public override Bitmap Icon { set; get; }

        [Browsable(false), BlockNameAttribute]
        public override string Name { set; get; }

        [CategoryAttribute("Filter Settings")]
        [Description("All images with smaller blots will be considered as negative.")]
        [DisplayName("Maximum blot size")]
        public double SizeBlots
        {
            get
            {
                return sizeBlots;
            }
            set
            {
                sizeBlots = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SizeBlots"));
            }
        }

        [CategoryAttribute("Filter Settings")]
        [Description("Minimum threshold of analyzed region.")]
        [DisplayName("Min region threshold")]
        public double MinThreshold1
        {
            get
            {
                return minThreshold1;
            }
            set
            {
                minThreshold1 = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinThreshold1"));
            }
        }

        [CategoryAttribute("Filter Settings")]
        [Description("Maximum threshold of analyzed region.")]
        [DisplayName("Max region threshold")]
        public double MaxThreshold1
        {
            get
            {
                return maxThreshold1;
            }
            set
            {
                maxThreshold1 = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxThreshold1"));
            }
        }

        [CategoryAttribute("Filter Settings")]
        [Description("Minimum threshold of analyzed region defects.")]
        [DisplayName("Min defects threshold")]
        public double MinThreshold2
        {
            get
            {
                return minThreshold2;
            }
            set
            {
                minThreshold2 = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinThreshold2"));
            }
        }

        [CategoryAttribute("Filter Settings")]
        [Description("Maximum threshold of analyzed region defects.")]
        [DisplayName("Max defects threshold")]
        public double MaxThreshold2
        {
            get
            {
                return maxThreshold2;
            }
            set
            {
                maxThreshold2 = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxThreshold2"));
            }
        }

        [CategoryAttribute("Filter Settings")]
        [Description("Region of Interest: top left side row position.")]
        [DisplayName("ROI top row")]
        public double ROITopRow
        {
            get
            {
                return roiTopRow;
            }
            set
            {
                roiTopRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopRow"));
            }
        }

        [CategoryAttribute("Filter Settings")]
        [Description("Region of Interest: bottom right side column position.")]
        [DisplayName("ROI bot col")]
        public double ROIBotCol
        {
            get
            {
                return roiBotCol;
            }
            set
            {
                roiBotCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotCol"));
            }
        }

        [CategoryAttribute("Filter Settings")]
        [Description("Region of Interest: bottom right side row position.")]
        [DisplayName("ROI bot row")]
        public double ROIBotRow
        {
            get
            {
                return roiBotRow;
            }
            set
            {
                roiBotRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotRow"));
            }
        }

        [CategoryAttribute("Filter Settings")]
        [Description("Region of Interest: top left side column position.")]
        [DisplayName("ROI top col")]
        public double ROITopCol
        {
            get
            {
                return roiTopCol;
            }
            set
            {
                roiTopCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopCol"));
            }
        }

        [CategoryAttribute("Filter Settings")]
        [Description("Showing the number of blots in region.")]
        [DisplayName("Showing the number of blots")]
        public bool ShowBlots
        {
            get
            {
                return showBlots;
            }
            set
            {
                showBlots = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowBlots"));
            }
        }

        #endregion

        #region Events

        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;

        #endregion
    }
}
