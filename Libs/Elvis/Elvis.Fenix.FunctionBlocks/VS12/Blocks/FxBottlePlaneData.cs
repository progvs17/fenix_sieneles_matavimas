﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Elvis.Fenix.Hardware;
using System.Drawing;
using System.IO;

namespace Elvis.Fenix.FunctionBlocks
{
    public class BottlePlaneDataSettings : Elvis.Fenix.General.IFxBindable
    {
        
        #region Constructors

        public BottlePlaneDataSettings() { }

        #endregion


        #region Private Members

        private double _actuatorStep;
        private int _chopperPin;

        #endregion


        #region Public Properties

        [CategoryAttribute("Hardware")]
        public double ActuatorStep
        {
            get
            {
                return _actuatorStep;
            }
            set
            {
                _actuatorStep = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ActuatorStep"));
            }
        }

        [CategoryAttribute("Hardware")]
        public int ChopperChannel
        {
            get
            {
                return _chopperPin;
            }
            set
            {
                _chopperPin = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ChopperPin"));
            }
        }

        [CategoryAttribute("Data")]
        public double V0 { get; set; }

        [XmlIgnoreAttribute, Browsable(false)]
        public Bitmap Image { get; set; }

        //public double ActualWidth { get; set; }
        

        //[CategoryAttribute("Positioning")]
        //[Description("Rotary actuator end position (degrees)")]
        //public double RotationAngle
        //{
        //    get { return _rotationAngle; }
        //    set
        //    {
        //        _rotationAngle = value;
        //        OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
        //    }
        //}

        #endregion

    }

    public class PlaneData
    {
        [CategoryAttribute("Calculated results")]
        public double ActualWidth
        {
            get
            {
                try
                {
                    //return a1 * Math.Exp((deltaU - b1) / c1) + a2 * Math.Exp((deltaU - b2) / c2);
                    return a1 * Math.Exp(-Math.Pow(((deltaU - b1) / c1), 2));
                }
                catch
                {
                    return -1;
                }
            }
        }

        [CategoryAttribute("Width Data")]
        public double deltaU { get; set; }
        [CategoryAttribute("Width Data")]
        public double OriginalWidth { get; set; }

        [CategoryAttribute("Coefficients Data")]
        public double a1 { get; set; }
        //[CategoryAttribute("Coefficients Data")]
        //public double a2 { get; set; }
        [CategoryAttribute("Coefficients Data")]
        public double b1 { get; set; }
        //[CategoryAttribute("Coefficients Data")]
        //public double b2 { get; set; }
        [CategoryAttribute("Coefficients Data")]
        public double c1 { get; set; }
        //[CategoryAttribute("Coefficients Data")]
        //public double c2 { get; set; }
    }

    public class CoefficientsData
    {
        public double a1 { get; set; }
        public double a2 { get; set; }
        public double b1 { get; set; }
        public double b2 { get; set; }
        public double c1 { get; set; }
        public double c2 { get; set; }
    }

    public class V0Correction
    {
        public int ChannelNo { get; set; }
        public double V0 { get; set; }
        public string Position { get; set; }
    }

    public class FxBottlePlaneData : FxBlock
    {
        #region Private Members

        private bool _disposed;
        private ManualResetEventSlim _exit = new ManualResetEventSlim(false);
        private List<BottlePlaneDataSettings> _blockSettings;

        private Dictionary<int, double> _bottleData = new Dictionary<int, double>();

        private FxGpio board;
        private FxGpio do_board;

        private OperationMode _operationMode;
        private int _operationIndex;

        private double[] analogData;
        private int dataArraySize = 2000;
        private int scanInterval = 400;
        private bool dataUpdated = false;
        private string _bottleDataString;
        private int actuatorOnDO;
        private bool actuatorON;

        //private System.Threading.Timer stopTimer = null;
        


        #endregion


        #region Constructors

        public FxBottlePlaneData() { }

        public FxBottlePlaneData(string name, FxGpio analogBoard, FxGpio digitalboard)
        {
            Name = name;
            Icon = Resource.Laser;

            board = analogBoard;
            do_board = digitalboard;

            BlockSettings = new List<BottlePlaneDataSettings>();
            PlaneWidthData = new List<PlaneData>();
            V0Corrections = new List<V0Correction>();

            V0 = 4.6;
            //CoeffData = new List<CoefficientsData>();
            // analogData = new double[dataArraySize];
            // board.onDataReady += Board_onDataReady;
        }

        #endregion


        #region Public Methods

        public override void Run()
        {
            Judgement = true;
            DateTime startTime = DateTime.Now;

            _exit.Reset();

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            try
            {
                //ActuatorON = false;
                //Thread.Sleep(200);
                //// paleisti varikli
                //ActuatorON = true;

                // block routine
                if (BottleData.Count > 0) BottleData.Clear();
                if (PlaneWidthData.Count > 0) PlaneWidthData.Clear();


                if (BlockSettings != null && BlockSettings.Count > 0)
                {
                    Random rnd = new Random();
                    int idx = 1;

                    Hardware.Gpio.DAQ_2205 b = null;
                    if (board != null && board.IsOpen)
                    {
                        // gpio connected
                        b = board as Hardware.Gpio.DAQ_2205;
                        //// make 1 dummy read
                        //double[] dummy = b.UpdateData((ushort)BlockSettings[0].ChopperChannel, (uint)DataPoints, (uint)ScanInterval);
                    }

                    lock (b)
                    {
                        switch (BlockOperationMode)
                        {
                            case OperationMode.MultipleImages:

                                foreach (BottlePlaneDataSettings setting in BlockSettings)
                                {
                                    if (b != null)
                                    {
                                        analogData = b.UpdateData((ushort)setting.ChopperChannel, (uint)DataPoints, (uint)ScanInterval);

                                        if (analogData == null)
                                        {
                                            Judgement = false;
                                            AnnounceInfoMessage("Data aquisition error");
                                            setting.Image = null;
                                        }
                                        else
                                        {
                                            // duomenu apdorojimas
                                            V0Correction corr = V0Corrections.Find(x => x.ChannelNo == setting.ChopperChannel);
                                            AddDataToDictionary(idx, corr.V0);
                                            //AddDataToDictionary(idx, setting.V0);

                                            if (SaveImages)
                                            {
                                                PlotDataImage(0, DataPoints);
                                                setting.Image = Image;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        // gpio not connected
                                        BottleData.Add(idx, rnd.Next(600));
                                    }
                                    idx++;
                                }


                                break;

                            case OperationMode.SingleImage:

                                idx = OperationIndex + 1;

                                if (b != null)
                                {

                                    analogData = b.UpdateData((ushort)BlockSettings[OperationIndex].ChopperChannel, (uint)DataPoints, (uint)ScanInterval);

                                    if (analogData == null)
                                    {
                                        Judgement = false;
                                        AnnounceInfoMessage("Data aquisition error");
                                    }
                                    else
                                    {
                                        // duomenu apdorojimas
                                        V0Correction corr = V0Corrections.Find(x => x.ChannelNo == BlockSettings[OperationIndex].ChopperChannel);
                                        AddDataToDictionary(idx, corr.V0);
                                        //AddDataToDictionary(idx, BlockSettings[OperationIndex].V0);
                                    }
                                }
                                else
                                {
                                    // gpio not connected
                                    BottleData.Add(idx, rnd.Next(600));
                                }

                                break;
                        }
                    }
                    
                    
                    BottleDataString = FormatResultString();

                    if (SaveData)
                    {
                        SaveWallThicknessData();
                    }

                    // duomenu atvaizdavimas
                    PlotDataImage(0, DataPoints);
                }

            }
            catch (OutOfMemoryException)
            {
                GC.Collect();
                FxSessionTimeClass.LogErrorMessage("(" + Name + ") ", "Out of memory");
            }
            catch (Exception ex)
            {
                Judgement = false;

                AnnounceInfoMessage("block error: " + ex.Message);

                FxSessionTimeClass.LogErrorMessage("(" + Name + ") ", ex.Message);
            }

            ////stabdyti varikli
            //ActuatorON = false;
            //Thread.Sleep(200);
            //ActuatorON = true;

            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

            Judge = Judgement;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }


        public void SaveWallThicknessData()
        {
            try
            {
                //string path = @"C:\Fenix\Terekas\PET Plane Quality\Logs\" + DateTime.Now.ToShortDateString();
                string path = FilePath + @"\" + DateTime.Now.ToShortDateString();
                //string path = @"C:\Fenix\Terekas\PET Plane Quality\Logs\" + DateTime.Now.ToShortDateString() + "-" + FxSessionTimeClass.SessionStartTime;
                Directory.CreateDirectory(path);
                path += @"\";

                using (StreamWriter sw = new StreamWriter(path + this.Name + ".txt", true))
                {
                    sw.WriteLine(BottleDataString);
                    sw.Dispose();
                }


                if (BlockOperationMode == OperationMode.MultipleImages && SaveImages)
                {
                    int imgIdx = 0;

                    foreach (BottlePlaneDataSettings item in BlockSettings)
                    {
                        if (item.Image != null)
                        {
                            item.Image.Save(path + "img_" + imgIdx + ".tif", System.Drawing.Imaging.ImageFormat.Tiff);
                        }

                        imgIdx++;
                    }
                }
            }
            catch (Exception ex)
            {
                if (onMessage != null)
                {
                    onMessage(this, ex.Message);
                }
            }
        }


        public void UpdateV0Values()
        {
            Hardware.Gpio.DAQ_2205 b = null;
            if (board != null && board.IsOpen)
            {
                // gpio connected
                b = board as Hardware.Gpio.DAQ_2205;              
            }

            if (b != null)
            {
                lock (b)
                {
                    // make 1 dummy read
                    //double[] dummy = b.UpdateData((ushort)BlockSettings[0].ChopperChannel, (uint)DataPoints, (uint)ScanInterval);

                    foreach (V0Correction item in V0Corrections)
                    {
                        DateTime waitTime = DateTime.Now;
                        while (((DateTime.Now - waitTime).TotalMilliseconds < 10) && !_exit.IsSet) ;
                        analogData = b.UpdateData((ushort)item.ChannelNo, (uint)DataPoints, (uint)ScanInterval);

                        if (analogData == null)
                        {
                            Judgement = false;
                            AnnounceInfoMessage("Data aquisition error");
                            //item.V0 = 99;
                        }
                        else
                        {
                            double maxSum = 0, minSum = 0;
                            int size = (int)(DataPoints * 0.5);
                            double[] arrayPart = new double[size];

                            Array.Copy(analogData, 0, arrayPart, 0, size);
                            maxSum += arrayPart.Max();
                            minSum += arrayPart.Min();

                            Array.Copy(analogData, size, arrayPart, 0, size);
                            maxSum += arrayPart.Max();
                            minSum += arrayPart.Min();

                            MaxAverage = maxSum / 2.0;
                            MinAverage = minSum / 2.0;

                            item.V0 = DeltaAverage;
                            item.Position = GetIRName(item.ChannelNo);
                        }
                    }
                }              

              
            }
        }


        public double CalculateActualWidth(double input)
        {

            try
            {
                //return a1 * Math.Exp((deltaU - b1) / c1) + a2 * Math.Exp((deltaU - b2) / c2);
                return a1 * Math.Exp(-Math.Pow(((input - b1) / c1), 2));
                //return a1 * Math.Exp((input - b1) / c1);
            }
            catch
            {
                return -1;
            }
        }


        public override void Clear()
        {
            GC.Collect();
        }

        public override void Exit()
        {
            base.Exit();
        }

        public override void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            board.onDataReady -= Board_onDataReady;

            if (_disposed)
                return;

            if (disposing)
            {
                _exit.Set();
            }
            _disposed = true;
        }

        public override void Stop()
        {
            _exit.Set();
        }

        #endregion


        #region Private Methods

        private void Board_onDataReady(object sender, double[] data)
        {
            analogData = data;
            PlotDataImage(0, DataPoints);
            //throw new NotImplementedException();
        }

        private void AddDataToDictionary(int index, double v0)
        {
            double maxSum = 0, minSum = 0;
            int size = (int)(DataPoints * 0.5);
            double[] arrayPart = new double[size];

            Array.Copy(analogData, 0, arrayPart, 0, size);
            maxSum += arrayPart.Max();
            minSum += arrayPart.Min();

            Array.Copy(analogData, size, arrayPart, 0, size);
            maxSum += arrayPart.Max();
            minSum += arrayPart.Min();

            //Array.Copy(analogData, size * 2, arrayPart, 0, size);
            //maxSum += arrayPart.Max();
            //minSum += arrayPart.Min();

            //Array.Copy(analogData, size * 3, arrayPart, 0, size);
            //maxSum += arrayPart.Max();
            //minSum += arrayPart.Min();

            //Array.Copy(analogData, size * 4, arrayPart, 0, size);
            //maxSum += arrayPart.Max();
            //minSum += arrayPart.Min();

            MaxAverage = maxSum / 2.0;
            MinAverage = minSum / 2.0;

            double V = MaxAverage - MinAverage;

            BottleData.Add(index, (V * 100) / v0);
            
        }

        private string FormatResultString()
        {
            string result = string.Empty;

            if(BottleData != null && BottleData.Count > 0)
            {
                try
                {
                    foreach(KeyValuePair<int,double> item in BottleData)
                    {
                        string itemPair = item.Key.ToString() + ":" + item.Value.ToString("f") + " ";
                        result += itemPair;
                    }
                }
                catch(Exception ex)
                {
                    AnnounceInfoMessage(ex.Message);
                }
            }

            return result;
        }

        private void PlotDataImage(long startidx, long acq_count) //, double[] data)
        {
            //lock (Image)
            //{
            long i, k, n;
            long w, h;
            long X, Y;
            long[] last_x = new long[4];
            long[] last_y = new long[4];
            Pen drawingPen = new Pen(Color.Lime, 2);
            Pen linePen = new Pen(Color.WhiteSmoke, 1);
            Pen mideLinePen = new Pen(Color.White, 2);

            Brush drawingBrush = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(82, 102, 120));
            Brush solidBrush = new SolidBrush(Color.White);

            if (ImageSize == null || ImageSize.X == 0 || ImageSize.Y == 0)
            {
                ImageSize = new Point(300, 200);
            }

            w = ImageSize.X;
            h = ImageSize.Y;

            for (i = 0; i < 4; i++)
            {
                last_x[i] = -1;
                last_y[i] = -1;
            }

            Bitmap flag = new Bitmap((int)w, (int)h);
            using (System.Drawing.Graphics g = Graphics.FromImage(flag)) //Graphics.FromImage(picScreen.Image);
            {
                g.FillRectangle(drawingBrush, new Rectangle(0, 0, (int)w, (int)h));

                for (double m = 1; m > 0; m = m - 0.1) // 0.125
                {
                    if(m == 0.5) g.DrawLine(mideLinePen, 0, (int)(h * m), w, (int)(h * m));
                    else g.DrawLine(linePen, 0, (int)(h * m), w, (int)(h * m));

                    float number = ((float)((-20 * m) + 10)) / 2;

                    if (number > -0.1 && number < 0.1) number = 0;
                    g.DrawString(number.ToString(), SystemFonts.DefaultFont, solidBrush, 0, (int)(h * m) + 1);
                }

                //g.DrawLine(linePen, 0, (int)(h * 0.75), w, (int)(h * 0.75));
                //g.DrawLine(linePen, 0, (int)(h * 0.5), w, (int)(h * 0.5));
                //g.DrawLine(linePen, 0, (int)(h * 0.25), w, (int)(h * 0.25));

                for (double m = 1; m > 0; m = m - 0.2)
                {
                    g.DrawLine(linePen, (int)(w * m), 0, (int)(w * m), h);
                }

                for (i = 0; i < acq_count; i++)
                {
                    k = 0;
                    n = i % acq_count;
                    X = (i * w) / (acq_count - 1);
                    // 20: +-10; 10: +-5
                    Y = (long)(h / 2 - analogData[n] * h / 10); //20
                    if (last_y[k] != -1)
                        g.DrawLine(drawingPen, last_x[k], last_y[k], X, Y);

                    last_x[k] = X;
                    last_y[k] = Y;
                }

                if(BlockOperationMode == OperationMode.SingleImage)
                {
                    string[] str = BottleDataString.Split(':');
                    g.DrawString(str[1] + "%", SystemFonts.DefaultFont, solidBrush, new Point(ImageSize.X - 150, ImageSize.Y - 20));
                    g.DrawString(CalculateActualWidth(BottleData[OperationIndex + 1]).ToString("f") + "mm", SystemFonts.DefaultFont, solidBrush, new Point(ImageSize.X - 235, ImageSize.Y - 20));
    
                }
                g.DrawString("Pos: " + GetIRName(BlockSettings[OperationIndex].ChopperChannel), SystemFonts.DefaultFont, solidBrush, new Point(5, ImageSize.Y - 20));
                g.DrawString(DeltaAverage.ToString("f") + " V", SystemFonts.DefaultFont, solidBrush, new Point(ImageSize.X - 50, ImageSize.Y - 20));

                Image = flag;
                g.Dispose();
            }
            dataUpdated = true;
            //}     
        }

        private void stopActuatorElapsed(object state)
        {
            ActuatorON = false;
        }

        private string GetIRName(int channel)
        {
            string res = "NaN";

            switch (channel)
            {
                case 31:
                    res = "1";
                    break;
                case 29:
                    res = "2";
                    break;
                case 27:
                    res = "3";
                    break;
                case 25:
                    res = "4";
                    break;

                default:
                    break;
            }

            return res;
        }

        #endregion


        #region Private Helpers

        /// <summary>
        /// Announce block's info message
        /// </summary>
        /// <param name="msg"></param>
        private void AnnounceInfoMessage(string msg)
        {
            if (onMessage != null) onMessage(this, msg);
        }

        #endregion


        #region Public Properties

        [CategoryAttribute("Block Settings")]
        public List<BottlePlaneDataSettings> BlockSettings 
        { 
            get
            {
                return _blockSettings;
            }
            set
            {
                _blockSettings = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockSettings"));
            }
        }

        [CategoryAttribute("Block Settings")]
        public int DataPoints
        {
            get
            {
                return dataArraySize;
            }
            set
            {
                dataArraySize = value;
            }
        }

        [CategoryAttribute("Block Settings")]
        public int ScanInterval
        {
            get
            {
                return scanInterval;
            }
            set
            {
                scanInterval = value;
            }
        }

        [Browsable(false)]
        public string FilePath { get; set; }

        [CategoryAttribute("Block Settings")]
        public Point ImageSize { get; set; }

        [XmlIgnoreAttribute]
        [Browsable(false)]
        public Bitmap Image { get; set; }

        [CategoryAttribute("Block Data")]
        [XmlIgnoreAttribute]
        public Dictionary<int, double> BottleData 
        { 
            get
            {
                return _bottleData;
            }
            set
            {
                _bottleData = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BottleData"));
            }
        }


        [CategoryAttribute("Block Data")]
        public List<V0Correction> V0Corrections { get; set; }


        [CategoryAttribute("Block Data")]
        public double V0 { get; set; }

        [CategoryAttribute("Block Data")]
        [XmlIgnoreAttribute]
        public string BottleDataString 
        { 
            get
            {
                return _bottleDataString;
            }
            set
            {
                _bottleDataString = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BottleDataString"));
            }
        }

        [CategoryAttribute("Block Data")]
        //[XmlIgnoreAttribute]
        [DisplayName("Plane Width Data List")]
        public List<PlaneData> PlaneWidthData { get; set; }

        //[CategoryAttribute("Block Data")]
        ////[XmlIgnoreAttribute]
        //[DisplayName("Coefficients Data list")]
        //public List<CoefficientsData> CoeffData { get; set; }

        [CategoryAttribute("Block Data")]
        [XmlIgnoreAttribute]
        public double MaxAverage { get; set; }

        [CategoryAttribute("Block Data")]
        [XmlIgnoreAttribute]
        public double MinAverage { get; set; }

        [CategoryAttribute("Block Data")]
        [XmlIgnoreAttribute]
        public double DeltaAverage
        {
            get
            {
                return MaxAverage - MinAverage;
            }
        }

        [CategoryAttribute("Block Data")]
        //[XmlIgnoreAttribute]
        public bool SaveData { get; set; }

        [CategoryAttribute("Block Data")]
        //[XmlIgnoreAttribute]
        public bool SaveImages { get; set; }

        [CategoryAttribute("Actuator Settings")]
        public int ActuatorOnDO
        {
            get
            {
                return actuatorOnDO;
            }

            set
            {
                actuatorOnDO = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ActuatorOnDO"));
            }
        }

        [CategoryAttribute("Actuator Settings")]
        [XmlIgnoreAttribute, Browsable(false)]
        public bool ActuatorON
        {
            get
            {
                return actuatorON;
            }

            set
            {
                if (value == true)
                {
                    if(do_board != null)
                        do_board.SetPin(actuatorOnDO, 1);
                }
                else
                {
                    if(do_board != null)
                        do_board.SetPin(actuatorOnDO, 0);
                }

                actuatorON = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ActuatorON"));
            }
        }


        [CategoryAttribute("Block Settings")]
        [XmlIgnoreAttribute]
        public OperationMode BlockOperationMode
        {
            get
            {
                return _operationMode;
            }
            set
            {
                _operationMode = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperationMode"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("Currently displayed operation (image)")]
        [XmlIgnoreAttribute]
        public int OperationIndex
        {
            get
            {
                return _operationIndex;
            }
            set
            {
                _operationIndex = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("OperationIndex"));
            }
        }

        [CategoryAttribute("Coefficients Data")]
        public double a1 { get; set; }

        [CategoryAttribute("Coefficients Data")]
        public double b1 { get; set; }

        [CategoryAttribute("Coefficients Data")]
        public double c1 { get; set; }

        //[XmlIgnoreAttribute]
        //[CategoryAttribute("Coefficients Data")]
        //public double MeasuredWidth
        //{
        //    get
        //    {
        //        return CalculateActualWidth(BottleData[OperationIndex + 1]); // CalculateActualWidth(DeltaAverage);
        //    }
        //}

        #endregion


        #region Events

        public override event MessageHandler onMessage;
        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;

        #endregion

        #region Buttons

        [ButtonControl]
        public void NextOperation()
        {
            if (BlockSettings != null && BlockSettings.Count > 0)
            {
                Stop();

                OperationIndex = OperationIndex < BlockSettings.Count - 1 ? OperationIndex + 1 : OperationIndex;
            }
        }

        [ButtonControl]
        public void PreviousOperation()
        {
            if (BlockSettings != null && BlockSettings.Count > 0)
            {
                Stop();

                OperationIndex = OperationIndex > 0 ? OperationIndex - 1 : OperationIndex;
            }
        }


        //[ButtonControl]
        //public void SavePlaneData()
        //{
        //    System.Windows.Forms.SaveFileDialog saveFile = new System.Windows.Forms.SaveFileDialog();
        //    saveFile.Filter = "Text|*.txt";
            
        //    if (saveFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        //    {
        //        string path = saveFile.FileName;

        //        try
        //        {
        //            using (StreamWriter sw = new StreamWriter(path))
        //            {
        //                if (PlaneWidthData != null && PlaneWidthData.Count > 0)
        //                {
        //                    foreach (PlaneData dataPair in PlaneWidthData)
        //                    {
        //                        sw.WriteLine(dataPair.deltaU + "," + dataPair.OriginalWidth);
        //                    }
        //                }

        //                sw.Dispose();
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            if (onMessage != null)
        //            {
        //                onMessage(this, ex.Message);
        //            }
        //        }

        //    }

        //}


        //[ButtonControl]
        //public void LoadCoefficientsData()
        //{
        //    System.Windows.Forms.OpenFileDialog openFile = new System.Windows.Forms.OpenFileDialog();
        //    openFile.Filter = "Text|*.txt";

        //    if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        //    {
        //        string path = openFile.FileName;

        //        try
        //        {
        //            using (StreamReader sr = new StreamReader(path))
        //            {
        //                string line = sr.ReadLine();
        //                int counter = 0;

        //                while (line != null)
        //                {
        //                    string[] list = line.Split(',');
        //                    if (list.Length == 6)
        //                    {
        //                        if (PlaneWidthData == null)
        //                        {
        //                            PlaneWidthData = new List<PlaneData>();
        //                        }

        //                        if (PlaneWidthData.Count <= counter)
        //                        {
        //                            PlaneWidthData.Add(new PlaneData());
        //                        }

        //                        PlaneWidthData[counter].a1 = double.Parse(list[0]);
        //                        PlaneWidthData[counter].b1 = double.Parse(list[1]);
        //                        PlaneWidthData[counter].c1 = double.Parse(list[2]);
        //                        //PlaneWidthData[counter].a2 = double.Parse(list[3]);
        //                        //PlaneWidthData[counter].b2 = double.Parse(list[4]);
        //                        //PlaneWidthData[counter].c2 = double.Parse(list[5]);
        //                    }

        //                    line = sr.ReadLine();
        //                    counter++;
        //                }

        //                sr.Dispose();
        //            }
        //        }
        //        catch (Exception ex)
        //        {

        //            throw;
        //        }
        //    }
        //}

        [ButtonControl]
        public void ActuatorOnOff()
        {
            ActuatorON = !ActuatorON;
        }

        [ButtonControl]
        public void UpdateV0()
        {
            UpdateV0Values();
        }

        #endregion
    }
}
