﻿using HalconDotNet;
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using System.Diagnostics;
using System.Threading;

namespace Elvis.Fenix.FunctionBlocks
{
    public class RubberHolesSettings
    {
        #region Constructors

        public RubberHolesSettings()
        {
            MaximumRegionThreshold = 100;
            MinimumRegionSelectShape = 1000;
            MaximumRegionSelectShape = 100000;
            MinimumDefectsThreshold = 200;
        }

        #region Public properties

        public double MaximumRegionThreshold
        { get; set; }

        public double MinimumRegionSelectShape
        { get; set; }

        public double MaximumRegionSelectShape
        { get; set; }

        public double MinimumDefectsThreshold
        { get; set; }

        #endregion

        #endregion
    }


    public class FxHHoles : FxBlock
    {
        #region Private Members

        List<HImage> list = new List<HImage>();
        
        private HImage inputImage;

        private double dilationCircle = 2;
        private double sizeHoles = 50;
        private bool showRegion = false;
        private bool showHoles = false;

        List<RubberHolesSettings> _rubberHolesSettings;

        private bool _runBlockOnce = false;
        

        private List<OperationList> _operationsList;
        private int _operationIndex;
        private OperationMode _operationMode;

        //FxRotateAndGrabImage _imageGrabBlock;

        #endregion

        #region Constructors

        public FxHHoles() { }

        public FxHHoles(string name) //, FxRotateAndGrabImage imageGrabBlock)
        {
            Name = name;
            Icon = Resource.Default;
            IsMarkAble = true;
            Marks = new List<MarkObject>();
            Text = new List<TextObject>();

            BlockSettings = new List<RubberHolesSettings>();
        }

        #endregion

        #region Public Methods

        [FilterClass]
        public override void Run()
        {
            Judgement = true;
            Judge = true;
            DateTime startTime = DateTime.Now;

            if (Marks != null)
            {
                Marks.Clear();
                Text.Clear();
            }

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }


            if (BlockOperationMode == OperationMode.SingleImageOperation)
            {
                if (BlockOperations != null && BlockOperations.Count > 0 && BlockOperations[OperationIndex].Image != null && BlockOperations[OperationIndex].Filter == FilterList.Holes)
                {
                    DrawRectangles();

                    if (!updateRegion)
                    {

                        ROITopRow = BlockOperations[OperationIndex].TopRow;
                        ROIBotRow = BlockOperations[OperationIndex].BottomRow;
                        ROITopCol = BlockOperations[OperationIndex].TopCollumn;
                        ROIBotCol = BlockOperations[OperationIndex].BottomCollumn;
                    }

                    FilterImage(BlockOperations[OperationIndex].Image, OperationIndex);
                }
                else
                {
                    Text.Add(new TextObject { Text = "Operation: #" + OperationIndex + " image does not exist", Color = "red", Font = "-Courier New-16-*-*-*-*-1-", Position = new Point(20, 20) });
                    //RunBlockOnce = true;
                }
            }
            else
            {
                if (BlockOperations != null && BlockOperations.Count > 0)
                {
                    int oper = 0;

                    foreach (var operation in BlockOperations)
                    {
                        OperationIndex = oper;

                        if (operation.Image != null && operation.Filter == FilterList.Holes) //operation.Filter == FilterList.HolesBlots && operation.Image != null)
                        {
                            ROITopRow = operation.TopRow;
                            ROIBotRow = operation.BottomRow;
                            ROITopCol = operation.TopCollumn;
                            ROIBotCol = operation.BottomCollumn;

                            FilterImage(operation.Image, oper);
                            
                        }
                        oper++;
                    }


                }
            }

            ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

            if (onTaskCompleted != null)
            {
                onTaskCompleted(this);
            }
        }

        public override void Clear()
        {
            if (OutputImage != null) OutputImage.Dispose();
            if (InputImage != null) InputImage.Dispose();
        }

        #endregion

        #region Private methods

        void FilterImage(HImage image, int operationIndex)
        {
            try
            {
                HRegion rect_ = new HRegion();
                HRegion region = new HRegion();
                HRegion regionHoles = new HRegion();
                HRegion regionDilation;
                HObject cont_;
                HImage imgReduce;
                int Holes;
                double defectLength;

                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                BlockOperations[operationIndex].Marks = new List<MarkObject>();
                BlockOperations[operationIndex].Text = new List<TextObject>();



                rect_.GenRectangle1(ROITopRow, ROITopCol, ROIBotRow, ROIBotCol);
                imgReduce = image.ReduceDomain(rect_);

                HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow, (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);

                region = imgReduce.Threshold(0, BlockSettings[operationIndex].MaximumRegionThreshold);

                region = region.Connection();
                region = region.FillUp();

                region = region.SelectShape("area", "and", BlockSettings[operationIndex].MinimumRegionSelectShape, BlockSettings[operationIndex].MaximumRegionSelectShape);
                if (showRegion)
                {
                    Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = region });
                    BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "ShowRegion", Color = "red", Object = region });
                }
                 
                imgReduce = image.ReduceDomain(region);

                HSystem.SetSystem("empty_region_result", "exception");

                region = imgReduce.Threshold(BlockSettings[operationIndex].MinimumDefectsThreshold, 255);
                
                try
                {
                    region = region.Connection();
                  
                    //px -> mm
                    if (MeasurementMode)
                        defectLength = SizeHoles * MeasurementCoefficient;
                    else
                        defectLength = SizeHoles;

                    regionHoles = region.SelectShape("area", "and", 0, defectLength);
                    regionDilation = regionHoles.DilationCircle(DilationCircle);

                    if (showHoles)
                    {
                        Marks.Add(new MarkObject { Name = "ShowRegion", Color = "green", Object = regionDilation });
                        BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "ShowRegion", Color = "green", Object = regionDilation });
                    }

                    Holes = regionHoles.CountObj();
                    regionHoles = regionHoles.Boundary("outer");
                }
                
                    catch(Exception ex)
                {
                    
                    Holes = 0;
                
                }
                
                /*catch
                {
                    Holes = 0;
                }*/

                Judgement &= Holes > 0 ? false : true;
                Judge = Holes > 0 ? false : true;
                string color = !Judge ? "red" : "green";

                HImage = image.Clone();

                Marks.Add(new MarkObject { Name = "Contour", Color = color, Object = cont_ });
                BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Contour", Color = color, Object = cont_ });
                Marks.Add(new MarkObject { Name = "Holes", Color = "red", Object = regionHoles });
                BlockOperations[operationIndex].Marks.Add(new MarkObject { Name = "Holes", Color = "red", Object = regionHoles });

                Text.Add(new TextObject { Text = "", Color = color, Font = "-Courier New-18-*-*-*-*-1-", Position = new Point((int)ROIBotRow - 15, (int)ROIBotCol + 10) });
                BlockOperations[operationIndex].Text.Add(new TextObject { Text = "", Color = color, Font = "-Courier New-18-*-*-*-*-1-", Position = new Point((int)ROIBotRow - 15, (int)ROIBotCol + 10) });
                
                //Temporary
                Text.Add(new TextObject { Text = "Holes:" + Holes.ToString(), Color = color, Font = "-Courier New-18-*-*-*-*-1-", Position = new Point((int)ROITopRow + 25, (int)ROITopCol) });
                BlockOperations[operationIndex].Text.Add(new TextObject { Text = "Holes:" + Holes.ToString(), Color = color, Font = "-Courier New-18-*-*-*-*-1-", Position = new Point((int)ROITopRow + 25, (int)ROITopCol) });
                
                if (operationIndex >= 0)
                {
                    Text.Add(new TextObject { Text = "Operation: #" + operationIndex, Color = color, Font = "-Courier New-14-*-*-*-*-1-", Position = new Point((int)ROITopRow - 20, (int)ROITopCol + 10) });
                    BlockOperations[operationIndex].Text.Add(new TextObject { Text = "Operation: #" + operationIndex, Color = color, Font = "-Courier New-14-*-*-*-*-1-", Position = new Point((int)ROITopRow - 20, (int)ROITopCol + 10) });
                }
                stopwatch.Stop();
                Text.Add(new TextObject { Text = "Elapsed time: " + stopwatch.ElapsedMilliseconds + "ms", Color = color, Font = "-Courier New-14-*-*-*-*-1-", Position = new Point((int)10, 10) });
                BlockOperations[operationIndex].Text.Add(new TextObject { Text = "Elapsed time: " + stopwatch.ElapsedMilliseconds + "ms", Color = color, Font = "-Courier New-14-*-*-*-*-1-", Position = new Point((int)10, 10) });
            }
            
            catch (HOperatorException exception)
            {
                FxSessionTimeClass.LogErrorMessage("(HHoles filter - Stage I) ", exception.GetErrorText());
                Judgement = false;
                Judge = false;
            }

            BlockOperations[operationIndex].Judgement = Judge;
        }

        #endregion

        #region UI mouse clicks interaction

        private double roiTopCol = 200;
        private double roiTopRow = 150;
        private double roiBotCol = 400;
        private double roiBotRow = 315;
        private double roiStartRow = 200;
        private double roiStartCol = 150;
        private double roiEndRow = 315;
        private double roiEndCol = 400;
        private double sx, sy, dx, dy;
        private bool IsDiamBusy;

        private bool updateRegion = false;

        private void DrawRectangles()
        {
            HRegion rect_ = new HRegion();
            HRegion region = new HRegion();
            HObject cont_;

            HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow, (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);

            Marks.Add(new MarkObject { Name = "Contour", Color = "blue", Object = cont_ });
        }


        public void UpdateROI()
        {
            if (updateRegion && BlockOperations != null && BlockOperations.Count > OperationIndex)
            {
                BlockOperations[OperationIndex].TopRow = ROITopRow;
                BlockOperations[OperationIndex].BottomRow = ROIBotRow;
                BlockOperations[OperationIndex].TopCollumn = ROITopCol;
                BlockOperations[OperationIndex].BottomCollumn = ROIBotCol;
            }
        }

        public void HOnMouseDown(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                sx = (int)e.X;
                sy = (int)e.Y;

                if (sx > (ROITopCol - 15) && sx < (ROITopCol + 15) && sy > (ROITopRow - 15) && sy < (ROITopRow + 15))
                {
                    roiStartRow = ROITopRow;
                    roiStartCol = ROITopCol;
                    IsDiamBusy = false;
                }
                else
                {
                    roiEndRow = ROIBotRow;
                    roiEndCol = ROIBotCol;
                    IsDiamBusy = true;
                }

                updateRegion = true;
            }
        }

        public void HOnMouseUp(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                UpdateROI();
                updateRegion = false;
            }
        }

        public void HOnMouseMove(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {

                dx = (int)e.X - sx;
                dy = (int)e.Y - sy;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    if (!IsDiamBusy)
                    {
                        ROITopCol = roiStartCol + dx;
                        ROITopRow = roiStartRow + dy;
                    }
                    else
                    {
                        ROIBotCol = roiEndCol + dx;
                        ROIBotRow = roiEndRow + dy;
                    }

                }
            }
        }

        public void HOnMouseWheel(object sender, HMouseEventArgs e)
        {

        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top row position.")]
        [DisplayName("ROI top row")]
        public double ROITopRow
        {
            get
            {
                return roiTopRow;
            }
            set
            {
                roiTopRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot row position.")]
        [DisplayName("ROI bot row")]
        public double ROIBotRow
        {
            get
            {
                return roiBotRow;
            }
            set
            {
                roiBotRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top col position.")]
        [DisplayName("ROI top col")]
        public double ROITopCol
        {
            get
            {
                return roiTopCol;
            }
            set
            {
                roiTopCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopCol"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot col position.")]
        [DisplayName("ROI bot col")]
        public double ROIBotCol
        {
            get
            {
                return roiBotCol;
            }
            set
            {
                roiBotCol = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotCol"));
            }
        }


        #endregion


        #region Public Properties

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage InputImage
        {
            get
            {
                return inputImage;
            }
            set
            {
                inputImage = value != null ? value.CopyImage() : null;
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage _hImage { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage HImage
        {
            get
            {
                return _hImage;
            }

            set
            {
                _hImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HImage"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<HImage> InputImages { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public List<HImage> OutputImages { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage OutputImage { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public override Bitmap Icon { set; get; }

        [Browsable(false), BlockNameAttribute]
        public override string Name { set; get; }


        [CategoryAttribute("Stage II: analysis")]
        [Description("All images with smaller holes will be considered as negative.")]
        [DisplayName("1. Maximum hole size")]
        public double SizeHoles
        {
            get
            {
                return sizeHoles;
            }
            set
            {
                sizeHoles = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SizeHoles"));
            }
        }

        [CategoryAttribute("Stage II: analysis")]
        [Description("Broadening the region..")]
        [DisplayName("2. Dilation circle")]
        public double DilationCircle
        {
            get
            {
                return dilationCircle;
            }
            set
            {
                dilationCircle = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("DilationCircle"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find region")]
        [Description("Maximum threshold of analyzed region.")]
        [DisplayName("1. Max region threshold")]
        public double MaxRegionThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumRegionThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberHolesSettings());
                    return BlockSettings[OperationIndex].MaximumRegionThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumRegionThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberHolesSettings());
                    MaxRegionThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxRegionThreshold"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find region")]
        [Description("Minimum size of analyzed region.")]
        [DisplayName("2. Min region shape")]
        public double MinRegionSelectShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumRegionSelectShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberHolesSettings());
                    return BlockSettings[OperationIndex].MinimumRegionSelectShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumRegionSelectShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberHolesSettings());
                    MinRegionSelectShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinRegionSelectShape"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find region")]
        [Description("Maximum size of analyzed region.")]
        [DisplayName("3. Max region shape")]
        public double MaxRegionSelectShape
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MaximumRegionSelectShape;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberHolesSettings());
                    return BlockSettings[OperationIndex].MaximumRegionSelectShape;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MaximumRegionSelectShape = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberHolesSettings());
                    MaxRegionSelectShape = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaxRegionSelectShape"));
            }
        }

        [XmlIgnoreAttribute]
        [CategoryAttribute("Stage I: find region")]
        [Description("Minimum threshold of analyzed region defects.")]
        [DisplayName("5. Min defects threshold")]
        public double MinDefectsThreshold
        {
            get
            {
                try
                {
                    return BlockSettings[OperationIndex].MinimumDefectsThreshold;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberHolesSettings());
                    return BlockSettings[OperationIndex].MinimumDefectsThreshold;
                }
            }
            set
            {
                try
                {
                    if (BlockOperationMode == OperationMode.SingleImageOperation)
                        BlockSettings[OperationIndex].MinimumDefectsThreshold = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    BlockSettings.Add(new RubberHolesSettings());
                    MinDefectsThreshold = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("MinDefectsThreshold"));
            }
        }


        [CategoryAttribute("Stage II: analysis")]
        [Description("Show the holes in the test region.")]
        [DisplayName("3. Show the holes")]
        public bool ShowHoles
        {
            get
            {
                return showHoles;
            }
            set
            {
                showHoles = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowHoles"));
            }
        }

        [CategoryAttribute("Stage I: find region")]
        [Description("Show the analyzed region.")]
        [DisplayName("4. Show rubber region")]
        public bool ShowRegion
        {
            get
            {
                return showRegion;
            }
            set
            {
                showRegion = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ShowRegion"));
            }
        }

        [CategoryAttribute("Calibration")]
        [Description("Holes settings")]
        public List<RubberHolesSettings> BlockSettings
        {
            get
            {
                return _rubberHolesSettings;
            }
            set
            {
                _rubberHolesSettings = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockSettings"));
            }
        }

        #region Properties from image grab block

        [CategoryAttribute("Block Settings")]
        [Description("Switch between single & multiple image grabbing")]
        [Browsable(false)]
        public OperationMode BlockOperationMode
        {
            get
            {
                return _operationMode;
            }
            set
            {
                _operationMode = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperationMode"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("Currently displayed operation (image)")]
        [XmlIgnoreAttribute, Browsable(false)]
        public int OperationIndex
        {
            get
            {
                return _operationIndex;
            }
            set
            {
                _operationIndex = value;

                OnPropertyChanged(this, new PropertyChangedEventArgs("OperationIndex"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("List of operations")]
        [XmlIgnoreAttribute]
        public List<OperationList> BlockOperations
        {
            get
            {
                return _operationsList;
            }
            set
            {
                _operationsList = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [XmlIgnoreAttribute, Browsable(false)]
        public bool RunBlockOnce
        {
            get { return _runBlockOnce; }
            set
            {
                _runBlockOnce = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("RunBlockOnce"));
            }
        }

        

        #endregion

        #endregion


        #region Buttons


        [ButtonControl]
        public void NextImage()
        {
            if (BlockOperationMode == OperationMode.SingleImageOperation && BlockOperations != null && BlockOperations.Count > 0)
            {
                OperationIndex = OperationIndex < BlockOperations.Count - 1 ? OperationIndex + 1 : OperationIndex;
                RunBlockOnce = true;
            }
        }

        [ButtonControl]
        public void PreviousImage()
        {
            if (BlockOperationMode == OperationMode.SingleImageOperation && BlockOperations != null && BlockOperations.Count > 0)
            {
                OperationIndex = OperationIndex > 0 ? OperationIndex - 1 : OperationIndex;
                RunBlockOnce = true;
            }
        }

        #endregion


        #region Events

        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;

        #endregion
    }
}
