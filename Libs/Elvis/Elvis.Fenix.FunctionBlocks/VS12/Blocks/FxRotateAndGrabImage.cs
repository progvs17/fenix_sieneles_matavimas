﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

using Elvis.Fenix.Hardware;
using Elvis.Fenix.IO;
using HalconDotNet;

namespace Elvis.Fenix.FunctionBlocks
{

    public delegate void ImageGrabbedHandler(Object sender);

    public class OperationList : Elvis.Fenix.General.IFxBindable
    {

        #region Constructors

        public OperationList() { }

        public OperationList(double rotationAngle, double reductionAngle, double maskLinearPosition, double cameraLinearPosition)
        {

            this.RotationAngle = rotationAngle;           
            this.ReductionAngle = reductionAngle;

            this.MaskPosition = maskLinearPosition;
            this.CameraPosition = cameraLinearPosition;
        }

        #endregion


        #region Private Fields

        private double _rotationAngle;
        private double _reductionAngle;
        private double _maskPosition;
        private double _cameraPosition;
        private double _topCollumn = 100;
        private double _bottomCollumn = 200;
        private double _topRow = 100;
        private double _bottomRow = 200;
        private int _exposure = 100;
        private HImage _image;
        //private FilterList _filter;
        private ControllerMode _mode = ControllerMode.Pulse;
        private double _pulseWidth = 10;
        private double _pulseDelay = 0.02;
        private double _retriggerDelay = 160;
        private int _brightness = 50;
        private int _outputChannel = 1;
        private int _triggerInput = 1;
        private int _frames2Delay = 0;
        private FilterList _filter;
        private bool _judgement;
        private  bool _markable;
        private  List<MarkObject> _marks = new List<MarkObject>();
        private  List<TextObject> _text = new List<TextObject>();

        #endregion


        #region Public Properties

        [CategoryAttribute("Positioning")]
        [Description("Rotary actuator end position (degrees)")]
        public double RotationAngle 
        {
            get { return _rotationAngle; }
            set
            {
                _rotationAngle = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            } 
        }

        [CategoryAttribute("Positioning")]
        [Description("Reduction actuator position (degrees)")]
        public double ReductionAngle 
        {
            get { return _reductionAngle; } 
            set
            {
                _reductionAngle = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [CategoryAttribute("Positioning")]
        [Description("Mask linear actuator position (mm)")]
        public double MaskPosition
        {
            get { return _maskPosition; }
            set
            {
                _maskPosition = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [CategoryAttribute("Positioning")]
        [Description("Camera linear actuator position (mm)")]
        public double CameraPosition
        {
            get { return _cameraPosition; }
            set
            {
                _cameraPosition = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [CategoryAttribute("ROI")]
        public double TopCollumn
        {
            get { return _topCollumn; }
            set
            {
                _topCollumn = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [CategoryAttribute("ROI")]
        public double BottomCollumn
        {
            get { return _bottomCollumn; }
            set
            {
                _bottomCollumn = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [CategoryAttribute("ROI")]
        public double TopRow
        {
            get { return _topRow; }
            set
            {
                _topRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [CategoryAttribute("ROI")]
        public double BottomRow
        {
            get { return _bottomRow; }
            set
            {
                _bottomRow = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [CategoryAttribute("Led Controller")]
        public int TriggerInput
        {
            get { return _triggerInput; }
            set
            {
                _triggerInput = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [CategoryAttribute("Led Controller")]
        public ControllerMode Mode
        {
            get { return _mode; }
            set
            {
                _mode = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [CategoryAttribute("Led Controller")]
        public double PulseWidth
        {
            get { return _pulseWidth; }
            set
            {
                _pulseWidth = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [CategoryAttribute("Led Controller")]
        public double PulseDelay
        {
            get { return _pulseDelay; }
            set
            {
                _pulseDelay = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [CategoryAttribute("Led Controller")]
        public double RetriggerDelay
        {
            get { return _retriggerDelay; }
            set
            {
                _retriggerDelay = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [CategoryAttribute("Led Controller")]
        public int Brightness
        {
            get { return _brightness; }
            set
            {
                _brightness = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [CategoryAttribute("Led Controller")]
        public int OutputChannel
        {
            get { return _outputChannel; }
            set
            {
                _outputChannel = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [CategoryAttribute("Camera")]
        public int FramesToDelay
        {
            get { return _frames2Delay; }
            set
            {
                _frames2Delay = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [CategoryAttribute("Camera")]
        public int Exposure
        {
            get { return _exposure; }
            set
            {
                _exposure = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage Image
        {
            get { return _image; }
            set
            {
                _image = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        //[XmlIgnoreAttribute]
        public FilterList Filter
        {
            get { return _filter; }
            set
            {
                _filter = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [XmlIgnoreAttribute, Browsable(false)]
        public bool Judgement
        {
            get { return _judgement; }
            set
            {
                _judgement = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [XmlIgnoreAttribute, Browsable(false)]
        public bool IsMarkAble
        {
            get { return _markable; }
            set
            {
                _markable = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [XmlIgnoreAttribute, Browsable(false)]
        public List<MarkObject> Marks
        {
            get { return _marks; }
            set
            {
                _marks = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [XmlIgnoreAttribute, Browsable(false)]
        public List<TextObject> Text 
        {
            get { return _text; }
            set
            {
                _text = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        #endregion
    }

    public enum OperationMode
    {
        SingleImageOperation,
        MultipleImagesOperations
    };

    public enum FilterList
    {
        None,
        HolesBlobs,
        Holes,
        ContourFragmentation,
        RegionUneven,
        ContourBreak,
        NeckDeformation
    }


    public class ButtonControlAttribute : System.Attribute
    {
        public ButtonControlAttribute()
        {
        }
    }

    public class FxRotateAndGrabImage : FxBlock
    {
        #region Private Fields

        //private Mutex operationSync = new Mutex();

        OperationMode _operationMode;

        List<OperationList> _operationsList;

        int _operationIndex = 0;
        bool _measurementMode;
        int _measurementCoefficient;
        // actuators
        FxActuator _rotationActuator = null;
        FxActuator _reductionActuator = null;
        FxActuator _maskActuator = null;
        FxActuator _cameraActuator = null;

        ManualResetEventSlim _stop = new ManualResetEventSlim(false);

        string _rotaryActuatorId = "NaN";
        string _reductionActuatorId = "NaN";
        string _maskActuatorId = "NaN";
        string _cameraActuatorId = "NaN";
        string _cameraId = "NaN";
        string _ledControllerId = "NaN";
        double _currentRotation = 1;
        double _currentReduction = 1;
        double _currentMaskPos = 1;
        double _currentCameraPos = 1;

        // actuators movement indicators
        private bool _rotationActuatorStopped = false;
        private bool _reductionActuatorStopped = false;
        private bool _maskActuatorStopped = false;
        private bool _cameraActuatorStopped = false;

        private bool _imageLoaderEnabled;
        private bool _useSingleImage;
        private bool _saveGrabbedImages = false;

        private string _imageDirectory;
        private string _sceneName;
        private string _sessionImagesDir;
        private string _prefix;
        private string _fileName;

        private bool _runBlockOnce = false;

        private FxLEDController _ledController;
        private FxCamera _camera;
        private FxSaveGrabbedImagesBlock _imageSaveBlock;
        private FxFrame _frame;

        #endregion


        #region Constructors

        public FxRotateAndGrabImage()
        {

        }

        public FxRotateAndGrabImage(string name)
        {
            Name = name;
            Icon = Resource.Camera;

            IsMarkAble = true;

            Marks = new List<MarkObject>();
            Text = new List<TextObject>();
        }


        public FxRotateAndGrabImage(string name, string rotaryActuatorId, string reductionActuatorId, string maskActuatorId, string cameraActuatorId, string cameraId, string ledControllerId)
        {
            Name = name;
            Icon = Resource.Camera;

            BlockOperations = new List<OperationList>();
            

            RotaryActuatorId = rotaryActuatorId;
            ReductionActuatorId = reductionActuatorId;
            MaskActuatorId = maskActuatorId;
            CameraActuatorId = cameraActuatorId;
            CameraId = cameraId;
            LedControllerId = ledControllerId;


            IsMarkAble = true;
            Marks = new List<MarkObject>();
            Text = new List<TextObject>();
        }

        /// <summary>
        /// Multiple operation constructor. Add operations later with "AddOperation" method
        /// </summary>
        /// <param name="name">Block name</param>
        /// <param name="camera">Camera object</param>
        /// <param name="loader">Loader object</param>
        public FxRotateAndGrabImage(string name, FxCamera camera, FxSaveGrabbedImagesBlock imageSaveBlock, FxActuator rotaryActuator, FxActuator reductionActuator, FxActuator maskLinearActuator, FxActuator cameraLinearActuator)
        {
            Name = name;
            _camera = camera;
            Icon = Resource.Camera;

            BlockOperationMode = OperationMode.MultipleImagesOperations;
            BlockOperations = new List<OperationList>();

            _rotationActuator = rotaryActuator;
            _reductionActuator = reductionActuator;
            _maskActuator = maskLinearActuator;
            _cameraActuator = cameraLinearActuator;

            // subscribe to events
            _rotationActuator.onActuatorStop += _rotationActuator_onActuatorStop;
            _rotationActuator.onActuatorStart += _rotationActuator_onActuatorStart;

            _reductionActuator.onActuatorStop += _reductionActuator_onActuatorStop;
            _reductionActuator.onActuatorStart += _reductionActuator_onActuatorStart;

            _maskActuator.onActuatorStop += _maskActuator_onActuatorStop;
            _maskActuator.onActuatorStart += _maskActuator_onActuatorStart;

            _cameraActuator.onActuatorStop += _cameraActuator_onActuatorStop;
            _cameraActuator.onActuatorStart += _cameraActuator_onActuatorStart;

            IsMarkAble = true;

            Marks = new List<MarkObject>();
            Text = new List<TextObject>();

            _imageSaveBlock = imageSaveBlock;
        }

        #endregion


        #region Private Methods


        private void GrabEmptyFrames(int numberOfFrames)
        {
            while (numberOfFrames > 0)
            {
                if (!_camera.IsConnected)
                    _camera.Connect();

                //if (!_camera.GlobalResetReleaseMode)
                //    _camera.GlobalResetReleaseMode = true;

                if (!_camera.IsGrabbing)
                    _camera.StartGrab();

                _frame = _camera.GrabFrame() as FxFrame;
                _camera.ReleaseFrame(_frame);
                Thread.Sleep(70);
                _frame = null;
                numberOfFrames--;
            }
        }

        /// <summary>
        /// function to save grabbed image(s)
        /// </summary>
        void SaveImage()
        {
            string path = FilePath + @"\" + DateTime.Now.ToString("yyyy-MM-dd") + @"\" + SceneName + @"\Session-" + FxSessionTimeClass.SessionStartTime + @"\" + Name;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string imageNumber;

            switch (BlockOperationMode)
            {
                case OperationMode.MultipleImagesOperations:

                    int i = 0;

                    if (BlockOperations != null && BlockOperations.Count > 0)
                    {
                        foreach (var operation in BlockOperations)
                        {
                            if (i < 10)
                            {
                                imageNumber = "0" + i.ToString();
                            }
                            else
                            {
                                imageNumber = i.ToString();
                            }

                            operation.Image.WriteImage("tiff", 0, path + @"\" + Prefix + " (" + imageNumber + ")");
                            i++;
                        }
                    }

                    break;

                case OperationMode.SingleImageOperation:

                    if (BlockOperations != null && BlockOperations[OperationIndex].Image != null)
                    {
                        
                        if(OperationIndex < 10)
                        {
                            imageNumber = "0" + OperationIndex.ToString();
                        }
                        else
                        {
                            imageNumber = OperationIndex.ToString();
                        }

                        BlockOperations[OperationIndex].Image.WriteImage("tiff", 0, path + @"\" + Prefix + " (" + imageNumber + ")");
                    }

                    break;
            }
        }

        #endregion


        #region Public Methods

        public override void Dispose()
        {
            Stop();

            // unsubscribe to events
            _rotationActuator.onActuatorStop -= _rotationActuator_onActuatorStop;
            _rotationActuator.onActuatorStart -= _rotationActuator_onActuatorStart;

            _reductionActuator.onActuatorStop -= _reductionActuator_onActuatorStop;
            _reductionActuator.onActuatorStart -= _reductionActuator_onActuatorStart;

            _maskActuator.onActuatorStop -= _maskActuator_onActuatorStop;
            _maskActuator.onActuatorStart -= _maskActuator_onActuatorStart;

            _cameraActuator.onActuatorStop -= _cameraActuator_onActuatorStop;
            _cameraActuator.onActuatorStart -= _cameraActuator_onActuatorStart;

            GC.SuppressFinalize(this);
        }

        public override void Stop() 
        {
            _stop.Set();

            //Thread.Sleep(50);
            if (_rotationActuator != null && !_rotationActuatorStopped)
                _rotationActuator.Stop();
            _rotationActuatorStopped = true;

            //Thread.Sleep(50);
            if (_rotationActuator != null && !_rotationActuatorStopped)
                _reductionActuator.Stop();
            _reductionActuatorStopped = true;

            //Thread.Sleep(50);
            if (_rotationActuator != null &&  !_rotationActuatorStopped)
                _maskActuator.Stop();
            _maskActuatorStopped = true;

            //Thread.Sleep(50);
            if (_rotationActuator != null && !_rotationActuatorStopped)
                _cameraActuator.Stop();
            _cameraActuatorStopped = true;

            //_camera.GlobalResetReleaseMode = false;

            if (_camera != null && _camera.IsGrabbing)
                _camera.StopGrab();

            if(_camera != null)
                _camera.ReleaseFrame(_frame);

            //Thread.Sleep(250);
        }

        /// <summary>
        /// Main run method
        /// </summary>
        public override void Run()
        {
            Judgement = true;

            DateTime startTime = DateTime.Now;

            if (Marks != null)
            {
                Marks.Clear();
                Text.Clear();
            }

            if (onTaskStarted != null)
            {
                onTaskStarted(this);
            }

            try
            {        
                // clear stopped flags
                _rotationActuatorStopped = false;
                _reductionActuatorStopped = false;
                _maskActuatorStopped = false;
                _cameraActuatorStopped = false;

                _stop.Reset();
                // update variable for filter blocks
                UseSingleImage = BlockOperationMode == OperationMode.SingleImageOperation ? true : false;

                if (ImageLoaderEnabled)
                {
                    if (SelectedSessionImagesDir != null)
                        LoadImagesFromFile();
                }
                else
                {
                    if (!_rotationActuator.IsConnected || !_reductionActuator.IsConnected || !_cameraActuator.IsConnected || !_maskActuator.IsConnected || _camera == null || _ledController == null)
                    //if(_ledController == null && _camera != null)
                    {
                        if (!_rotationActuator.IsConnected) FxSessionTimeClass.LogErrorMessage("(" + _rotationActuator.Name + ") ", "Not connected!");
                        if (!_reductionActuator.IsConnected) FxSessionTimeClass.LogErrorMessage("(" + _reductionActuator.Name + ") ", "Not connected!");
                        if (!_cameraActuator.IsConnected) FxSessionTimeClass.LogErrorMessage("(" + _cameraActuator.Name + ") ", "Not connected!");
                        if (!_maskActuator.IsConnected) FxSessionTimeClass.LogErrorMessage("(" + _maskActuator.Name + ") ", "Not connected!");
                        if (_camera == null) FxSessionTimeClass.LogErrorMessage("( Camera ) ", "Null");
                        if (_ledController == null) FxSessionTimeClass.LogErrorMessage("( LED controller ) ", "Null");

                        Judgement = false;
                    }
                    else
                    {

                        switch (BlockOperationMode)
                        {
                            case OperationMode.SingleImageOperation:

                                //operationSync.WaitOne();

                                if (_ledController != null)
                                {
                                    bool skipFrames = false;

                                    if (_camera.IsGrabbing)
                                        _camera.StopGrab();

                                    _ledController.SetTriggerInput(BlockOperations[OperationIndex].OutputChannel, BlockOperations[OperationIndex].TriggerInput);

                                    skipFrames = _ledController.SetChannelBrightness(BlockOperations[OperationIndex].Mode, BlockOperations[OperationIndex].OutputChannel, BlockOperations[OperationIndex].PulseWidth,
                                        BlockOperations[OperationIndex].PulseDelay, BlockOperations[OperationIndex].Brightness, BlockOperations[OperationIndex].RetriggerDelay);

                                    // delay some frames so LED controller would adjust brightness
                                    if (BlockOperations[OperationIndex].FramesToDelay > 0 && skipFrames) GrabEmptyFrames(BlockOperations[OperationIndex].FramesToDelay);
                                }


                                if (_rotationActuator.CurrentPosition != BlockOperations[OperationIndex].RotationAngle)
                                {
                                    //// rotate to start position
                                    _rotationActuator.MoveTo(BlockOperations[OperationIndex].RotationAngle);
                                    while (!_rotationActuatorStopped) ;
                                    _currentRotation = BlockOperations[OperationIndex].RotationAngle;
                                }

                                if (_maskActuator.CurrentPosition != BlockOperations[OperationIndex].MaskPosition)
                                {
                                    //// slide mask to position
                                    _maskActuator.MoveTo(BlockOperations[OperationIndex].MaskPosition);
                                    while (!_maskActuatorStopped) ;
                                    _currentMaskPos = BlockOperations[OperationIndex].MaskPosition;
                                }

                                if (_cameraActuator.CurrentPosition != BlockOperations[OperationIndex].CameraPosition)
                                {
                                    //// slide camera to position
                                    _cameraActuator.MoveTo(BlockOperations[OperationIndex].CameraPosition);
                                    while (!_cameraActuatorStopped) ;
                                    _currentCameraPos = BlockOperations[OperationIndex].CameraPosition;
                                }

                                if (_reductionActuator.CurrentPosition != BlockOperations[OperationIndex].ReductionAngle)
                                {
                                    //// rotate to certain angle
                                    _reductionActuator.MoveTo(BlockOperations[OperationIndex].ReductionAngle);
                                    while (!_reductionActuatorStopped) ;
                                    _currentReduction = BlockOperations[OperationIndex].ReductionAngle;
                                }

                                _frame = null;

                                if (!_camera.IsConnected)
                                    _camera.Connect();

                                Exposure = BlockOperations[OperationIndex].Exposure;

                                if (!_camera.IsGrabbing)
                                    _camera.StartGrab();


                                _frame = _camera.GrabFrame() as FxFrame;

                                if (_frame != null && _frame.Data != null)
                                {

                                    HImage img = new HImage();
                                    img.GenImage1Extern("byte", _frame.Width, _frame.Height, _frame.Data, IntPtr.Zero);

                                    BlockOperations[OperationIndex].Image = HImage = img.Clone();

                                    DrawRectangles();

                                    Text.Add(new TextObject { Text = "Operation: #" + (OperationIndex), Color = "red", Font = "-Courier New-14-*-*-*-*-1-", Position = new Point(_frame.Height - 100, 10) });

                                    _camera.ReleaseFrame(_frame);
                                    _frame = null;
                                    _camera.StopGrab();
                                }
                                else
                                {
                                    Judgement = false;
                                }

                                break;

                            case OperationMode.MultipleImagesOperations:

                                //operationSync.WaitOne();
                                int idx = 0;

                                if (BlockOperations.Count >= 1)
                                {
                                    _frame = null;
                                    if(!_camera.IsConnected)
                                        _camera.Connect();

                                    foreach (var operation in BlockOperations)
                                    {
                                        //Thread.Sleep(10);
                                        OperationIndex = idx++;

                                        if (_stop.IsSet)
                                            break;

                                        if (_ledController != null)
                                        {
                                            bool skipFrames = false;

                                            _ledController.SetTriggerInput(operation.OutputChannel, operation.TriggerInput);

                                            _ledController.SetChannelBrightness(operation.Mode, operation.OutputChannel, operation.PulseWidth, operation.PulseDelay,
                                                operation.Brightness, operation.RetriggerDelay);

                                            // delay some frames so LED controller would adjust brightness
                                            if (operation.FramesToDelay > 0 && skipFrames) GrabEmptyFrames(operation.FramesToDelay);
                                            else GrabEmptyFrames(1);
                                        }

                                        // rotate to start position
                                        _rotationActuator.MoveTo(operation.RotationAngle);
                                        while (!_rotationActuatorStopped) ;

                                        // slide mask to position
                                        _maskActuator.MoveTo(operation.MaskPosition);
                                        while (!_maskActuatorStopped) ;

                                        // slide camera to position
                                        _cameraActuator.MoveTo(operation.CameraPosition);
                                        while (!_cameraActuatorStopped) ;

                                        // rotate to certain anlge
                                        _reductionActuator.MoveTo(operation.ReductionAngle);
                                        while (!_reductionActuatorStopped) ;

                                        Exposure = operation.Exposure;

                                        if (!_camera.IsGrabbing)
                                            _camera.StartGrab();


                                        _frame = _camera.GrabFrame() as FxFrame;

                                        if (_frame != null && _frame.Data != null)
                                        {
                                            HImage img = new HImage();
                                            img.GenImage1Extern("byte", _frame.Width, _frame.Height, _frame.Data, IntPtr.Zero);

                                            HImage = operation.Image = img.Clone();

                                            if (onImageGrabbed != null) onImageGrabbed(this);


                                            _camera.ReleaseFrame(_frame);
                                            _frame = null;
                                        }
                                        else
                                        {
                                            Judgement = false;
                                        }

                                    }
                                }

                                break;

                            default:

                                FxSessionTimeClass.LogErrorMessage("(" + Name + ") ", "Invalid operation mode");
                                Judgement = false;

                                break;
                        }
                    }

                    if (SaveGrabbedImages && !_stop.IsSet)
                    {
                        SaveImage();
                    }

                }

                // leave last (dummy) image empty (null)
                BlockOperations[BlockOperations.Count - 1].Image = null;

                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations")); // make sure operations are updated
                GC.Collect(0, GCCollectionMode.Optimized);

                ElapsedTime = (DateTime.Now - startTime).TotalMilliseconds;

                Judge = Judgement;

                if (onTaskCompleted != null)
                {
                    onTaskCompleted(this);
                }

            }
            catch(OutOfMemoryException)
            {
                GC.Collect();
                FxSessionTimeClass.LogErrorMessage("(" + Name + ") ", "Out of memory");
            }
            catch (Exception ex)
            {
                Judgement = false;

                if (onMessage != null)
                {
                    onMessage(this, "FxRotateAndGrabImage block error: " + ex.Message);
                }
                FxSessionTimeClass.LogErrorMessage("(" + Name + ") ", ex.Message);
            }

            //operationSync.ReleaseMutex();
        }

 
        /// <summary>
        /// Memory clean method
        /// </summary>
        public override void Clear()
        {
            Stop();

            if(_camera != null)
                _camera.ReleaseFrame(_frame);

            if (HImage != null) HImage.Dispose();
            HImage = null;

            GC.Collect();
        }


        public void LoadImagesFromFile()
        {
            try
            {
                string[] files = Directory.GetFiles(SelectedSessionImagesDir + @"\" + Name, "*.*");
                HImage img = new HalconDotNet.HImage();

                switch (BlockOperationMode)
                {
                    case OperationMode.MultipleImagesOperations:

                        int i = 0;

                        foreach (var operation in BlockOperations)
                        {
                            OperationIndex = i;
                            img.ReadImage(files[i++]);
                            operation.Image = HImage = img.Clone();

                            if (onImageGrabbed != null)
                            {
                                onImageGrabbed(this);
                                Thread.Sleep(100);
                            }
                        }

                        break;

                    case OperationMode.SingleImageOperation:

                        img.ReadImage(files[OperationIndex]);
                        BlockOperations[OperationIndex].Image = HImage = img.Clone();

                        DrawRectangles();

                        Text.Add(new TextObject { Text = "Operation: #" + (OperationIndex), Color = "red", Font = "-Courier New-14-*-*-*-*-1-", Position = new Point(20, 10) });

                        break;
                }
            }
            catch (Exception ex)
            {
                if (onMessage != null)
                    onMessage(this, "Wrong path selected");

                FxSessionTimeClass.LogErrorMessage("(" + Name + ") ", ex.Message);


                Judgement = false;
                //throw new Exception(ex.Message);
            }
        }

        [XmlIgnore]
        public HardwareObject hw = new HardwareObject();

        [HardwareAtrribute]
        public void SetHw(HardwareObject hwObj)
        {
            hw = new HardwareObject();

            hw = hwObj;

            if (hw.Obj != null)
            {
                if (hw.Name == RotaryActuatorId)
                {
                    _rotationActuator = (FxActuator)hw.Obj;
                    _rotationActuator.onActuatorStop += _rotationActuator_onActuatorStop;
                    _rotationActuator.onActuatorStart += _rotationActuator_onActuatorStart;
                }
                if (hw.Name == ReductionActuatorId)
                {
                    _reductionActuator = (FxActuator)hw.Obj;
                    _reductionActuator.onActuatorStop += _reductionActuator_onActuatorStop;
                    _reductionActuator.onActuatorStart += _reductionActuator_onActuatorStart;
                }
                if (hw.Name == MaskActuatorId)
                {
                    _maskActuator = (FxActuator)hw.Obj;
                    _maskActuator.onActuatorStop += _maskActuator_onActuatorStop;
                    _maskActuator.onActuatorStart += _maskActuator_onActuatorStart;
                }
                if (hw.Name == CameraActuatorId)
                {
                    _cameraActuator = (FxActuator)hw.Obj;
                    _cameraActuator.onActuatorStop += _cameraActuator_onActuatorStop;
                    _cameraActuator.onActuatorStart += _cameraActuator_onActuatorStart;
                }
                if (hw.Name == CameraId)
                {
                    _camera = (FxCamera)hw.Obj;
                }
                if(hw.Name == LedControllerId)
                {
                    _ledController = (FxLEDController)hw.Obj;
                }
            }
        }

        #region UI mouse clicks interaction

        private double roiStartRow = 200;
        private double roiStartCol = 150;
        private double roiEndRow = 315;
        private double roiEndCol = 400;
        private double sx, sy, dx, dy;
        private bool IsDiamBusy;

        private bool updateRegion = true;

        private void DrawRectangles()
        {
            HRegion rect_ = new HRegion();
            HRegion region = new HRegion();
            HObject cont_;

            HalconDotNet.HOperatorSet.GenRectangle2ContourXld(out cont_, (ROIBotRow - ROITopRow) / 2 + ROITopRow, (ROIBotCol - ROITopCol) / 2 + ROITopCol, 0, (ROIBotCol - ROITopCol) / 2, (ROIBotRow - ROITopRow) / 2);
            Marks.Add(new MarkObject { Name = "Contour", Color = "blue", Object = cont_ });
        }

        public void HOnMouseDown(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                sx = (int)e.X;
                sy = (int)e.Y;

                if (sx > (ROITopCol - 15) && sx < (ROITopCol + 15) && sy > (ROITopRow - 15) && sy < (ROITopRow + 15))
                {
                    roiStartRow = ROITopRow;
                    roiStartCol = ROITopCol;
                    IsDiamBusy = false;
                }
                else
                {
                    roiEndRow = ROIBotRow;
                    roiEndCol = ROIBotCol;
                    IsDiamBusy = true;
                }
                updateRegion = true;
            }
        }

        public void HOnMouseUp(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                updateRegion = false;
            }
        }

        public void HOnMouseMove(object sender, HMouseEventArgs e)
        {
            if (IsFocused)
            {
                dx = (int)e.X - sx;
                dy = (int)e.Y - sy;

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    if (!IsDiamBusy)
                    {
                        ROITopCol = roiStartCol + dx;
                        ROITopRow = roiStartRow + dy;
                    }
                    else
                    {
                        ROIBotCol = roiEndCol + dx;
                        ROIBotRow = roiEndRow + dy;
                    }
                }
            }
        }

        public void HOnMouseWheel(object sender, HMouseEventArgs e)
        {

        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top row position.")]
        [DisplayName("ROI top row")]
        public double ROITopRow
        {
            get
            {
                try
                {
                    return BlockOperations[OperationIndex].TopRow;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Operation does not exist");
                    return 0;
                }
            }
            set
            {
                try
                {
                    if (updateRegion)
                        BlockOperations[OperationIndex].TopRow = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Operation does not exist");
                    BlockOperations.Add(new OperationList());
                    ROITopRow = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopRow"));
            }

        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot row position.")]
        [DisplayName("ROI bot row")]
        public double ROIBotRow
        {
            get
            {
                try
                {
                    return BlockOperations[OperationIndex].BottomRow;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Operation does not exist");
                    return 0;
                }
            }
            set
            {
                try
                {
                    if (updateRegion)
                        BlockOperations[OperationIndex].BottomRow = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Operation does not exist");
                    BlockOperations.Add(new OperationList());
                    ROIBotRow = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotRow"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest top col position.")]
        [DisplayName("ROI top col")]
        public double ROITopCol
        {
            get
            {
                try
                {
                    return BlockOperations[OperationIndex].TopCollumn;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Operation does not exist");
                    return 0;
                }
            }
            set
            {
                try
                {
                    if (updateRegion)
                        BlockOperations[OperationIndex].TopCollumn = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Operation does not exist");
                    BlockOperations.Add(new OperationList());
                    ROITopCol = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROITopCol"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Filter settings")]
        [Description("Region of Interest bot col position.")]
        [DisplayName("ROI bot col")]
        public double ROIBotCol
        {
            get
            {
                try
                {
                    return BlockOperations[OperationIndex].BottomCollumn;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Operation does not exist");
                    return 0;
                }
            }
            set
            {
                try
                {
                    if (updateRegion)
                        BlockOperations[OperationIndex].BottomCollumn = value;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Operation does not exist");
                    BlockOperations.Add(new OperationList());
                    ROIBotCol = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("ROIBotCol"));
            }
        }
 

        #endregion


        #endregion


        #region Public Properties

        /// <summary>
        /// Block name
        /// </summary>
        [Browsable(false), BlockNameAttribute]
        public override string Name { set; get; }

        /// <summary>
        /// Block icon
        /// </summary>
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public override System.Drawing.Bitmap Icon { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage _hImage { get; set; }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public HImage HImage
        {
            get
            {
                return _hImage;
            }

            set
            {
                _hImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HImage"));
            }
        }

        [Browsable(false)]
        [HardwareIdAtrribute]
        public string RotaryActuatorId
        {
            get { return _rotaryActuatorId; }
            set 
            { 
                _rotaryActuatorId = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("RotaryActuatorId"));
            }
            
        }

        [Browsable(false)]
        [HardwareIdAtrribute]
        public string ReductionActuatorId
        {
            get { return _reductionActuatorId; }
            set 
            { 
                _reductionActuatorId = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ReductionActuatorId"));
            }
        }

        [Browsable(false)]
        [HardwareIdAtrribute]
        public string MaskActuatorId 
        {
            get { return _maskActuatorId; }
            set 
            { 
                _maskActuatorId = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("MaskActuatorId"));
            }
        }

        [Browsable(false)]
        [HardwareIdAtrribute]
        public string CameraActuatorId
        {
            get { return _cameraActuatorId; }
            set 
            { 
                _cameraActuatorId = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("CameraActuatorId"));
            }
        }

        [Browsable(false)]
        [HardwareIdAtrribute]
        public string CameraId
        {
            get { return _cameraId; }
            set 
            { 
                _cameraId = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("CameraId"));
            }
        }

        [Browsable(false)]
        [HardwareIdAtrribute]
        public string LedControllerId
        {
            get { return _ledControllerId; }
            set
            {
                _ledControllerId = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("LedControllerId"));
            }
        }

        #region Props for image save/load block

        [Browsable(false)]
        [XmlIgnoreAttribute]
        public bool ImageLoaderEnabled
        {
            get
            {
                return _imageLoaderEnabled;
            }
            set
            {
                _imageLoaderEnabled = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ImageLoaderEnabled"));
            }
        }

        [Description("Use only 1 image")]
        [Browsable(false)]
        [XmlIgnoreAttribute]
        public bool UseSingleImage
        {
            get
            {
                return _useSingleImage;
            }
            set
            {
                _useSingleImage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("UseSingleImage"));
            }
        }

        [Description("Session location to load images from")]
        [Browsable(false)]
        public string SelectedSessionImagesDir
        {
            get
            {
                return _sessionImagesDir;
            }

            set
            {
                _sessionImagesDir = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SelectedSessionImagesDir"));
            }
        }

        [Description("Location where grabbed images are stored")]
        [Browsable(false)]
        public string FilePath
        {
            get
            {
                return _imageDirectory;
            }

            set
            {
                _imageDirectory = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("FilePath"));
            }
        }

        [Description("Current scene name")]
        [Browsable(false)]
        public string SceneName
        {
            get
            {
                return _sceneName;
            }
            set
            {
                _sceneName = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SceneName"));
            }
        }

        [Description("Saved image prefix (name)")]
        [Browsable(false)]
        public string Prefix
        {
            get
            {
                return _prefix;
            }
            set
            {
                _prefix = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Prefix"));
            }
        }

        [Description("Saved image prefix (name)")]
        [Browsable(false)]
        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("FileName"));
            }
        }

        [Description("Save grabbed images?")]
        public bool SaveGrabbedImages
        {
            get
            {
                return _saveGrabbedImages;
            }
            set
            {
                _saveGrabbedImages = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SaveGrabbedImages"));
            }
        }

        #endregion

        [CategoryAttribute("Block Settings")]
        [Description("Switch between single & multiple image grabbing")]
        public OperationMode BlockOperationMode 
        { 
            get
            {
                return _operationMode;
            }
            set
            {
                _operationMode = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperationMode"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("Currently displayed operation (image)")]
        [XmlIgnoreAttribute]
        public int OperationIndex
        {
            get
            {
                return _operationIndex;
            }
            set
            {
                _operationIndex = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("OperationIndex"));
            }
        }
//****************************
        [CategoryAttribute("Block Settings")]
        [Description("Measurement mode")]
        //[XmlIgnoreAttribute]
        public bool MeasurementMode
        {
            get
            {
                return _measurementMode;
            }
            set
            {
                _measurementMode = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("MeasurementMode"));
            }
        }

        [CategoryAttribute("Block Settings")]
        [Description("Measurement coefficient")]
        //[XmlIgnoreAttribute]
        public int MeasurementCoefficient
        {
            get
            {
                return _measurementCoefficient;
            }
            set
            {
                _measurementCoefficient = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("MeasurementCoefficient"));
            }
        }
//*******************************
        [CategoryAttribute("Block Settings")]
        [Description("List of operations")]
        public List<OperationList> BlockOperations
        {
            get
            {
                return _operationsList;
            }
            set
            {
                _operationsList = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BlockOperations"));
            }
        }

        [XmlIgnoreAttribute, Browsable(false)]
        public bool RunBlockOnce 
        {
            get { return _runBlockOnce; }
            set
            {
                if(value)
                {
                    Run();
                }
                _runBlockOnce = false;
                OnPropertyChanged(this, new PropertyChangedEventArgs("RunBlockOnce"));
            }
        }


        #region CAMERA OPTIONS - HIDDEN

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Camera Settings")]
        public int Width
        {
            get
            {
                return _camera != null && _camera.IsConnected ? _camera.Width : 0;
            }
            set
            {
                if (_camera != null && _camera.IsConnected)
                {
                    bool restart = _camera.IsGrabbing;
                    if (restart)
                    {
                        _camera.StopGrab();
                        _camera.Width = value;
                        _camera.StartGrab();
                    }
                    else
                    {
                        _camera.Width = value;
                    }
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("Width"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Camera Settings")]
        public int Height
        {
            get
            {
                return _camera != null && _camera.IsConnected ? _camera.Height : 0;
            }
            set
            {
                if (_camera != null && _camera.IsConnected)
                {
                    bool restart = _camera.IsGrabbing;
                    if (restart)
                    {
                        _camera.StopGrab();
                        _camera.Height = value;
                        _camera.StartGrab();
                    }
                    else
                    {
                        _camera.Height = value;
                    }
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("Height"));
            }
        }

        [Browsable(false)]
        [XmlIgnoreAttribute]
        [CategoryAttribute("Camera Settings")]
        public int Exposure
        {
            get
            {
                return _camera != null && _camera.IsConnected ? _camera.Exposure : 0;
            }
            set
            {
                if (_camera != null && _camera.IsConnected)
                {
                    _camera.Exposure = value;
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs("Exposure"));
            }
        }

        

        #endregion


        #endregion


        #region Buttons

        [ButtonControl]
        public void DuplicateLastOperation()
        {
            if (BlockOperations != null && BlockOperations.Count > 0)
            {
                Stop();

                int lastIdx = BlockOperations.Count - 1;
                OperationList newItem = new OperationList();

                foreach (var property in newItem.GetType().GetProperties())
                {
                    newItem.GetType().GetProperty(property.Name).SetValue(newItem, BlockOperations[lastIdx].GetType().GetProperty(property.Name).GetValue(BlockOperations[lastIdx]));
                }

                BlockOperations.Add(newItem);
                NextOperation();
            }
        }

        [ButtonControl]
        public void NextOperation()
        {
            if (BlockOperations != null && BlockOperations.Count > 0)
            {
                Stop();

                OperationIndex = OperationIndex < BlockOperations.Count - 1 ? OperationIndex + 1 : OperationIndex;
            }
        }

        [ButtonControl]
        public void PreviousOperation()
        {
            if (BlockOperations != null && BlockOperations.Count > 0)
            {
                Stop();

                OperationIndex = OperationIndex > 0 ? OperationIndex - 1 : OperationIndex;
            }
        }

        #endregion


        #region Events


        void _rotationActuator_onActuatorStop(object sender)
        {
            _rotationActuatorStopped = true;
        }

        void _rotationActuator_onActuatorStart(object sender)
        {
            _rotationActuatorStopped = false;
        }

        void _reductionActuator_onActuatorStop(object sender)
        {
            _reductionActuatorStopped = true;
        }

        void _reductionActuator_onActuatorStart(object sender)
        {
            _reductionActuatorStopped = false;
        }

        void _cameraActuator_onActuatorStart(object sender)
        {
            _cameraActuatorStopped = false;
        }

        void _cameraActuator_onActuatorStop(object sender)
        {
            _cameraActuatorStopped = true;
        }

        void _maskActuator_onActuatorStart(object sender)
        {
            _maskActuatorStopped = false;
        }

        void _maskActuator_onActuatorStop(object sender)
        {
            _maskActuatorStopped = true;
        }

        public override event TaskStartHandler onTaskStarted;
        public override event TaskCompleteHandler onTaskCompleted;
        public override event MessageHandler onMessage;

        public event ImageGrabbedHandler onImageGrabbed;

        #endregion
    }

}
