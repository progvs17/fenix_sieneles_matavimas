﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml.Serialization;
using System.ComponentModel;
using System.Threading;
using System.Drawing;

using Elvis.Fenix.FunctionBlocks;
using Elvis.Fenix.General;


namespace Elvis.Fenix.FunctionBlocks
{
    public class FxSceneImage
    {
        #region Private Members

        private HImage _hImage;

        #endregion


        #region Constructors

        public FxSceneImage()
        {
            Marks = new List<MarkObject>();
            Text = new List<TextObject>();
            HImage = new HImage();
        }

        #endregion


        #region Public Properties

        /// <summary>
        /// Image variable
        /// </summary>
        [Browsable(false), XmlIgnoreAttribute]
        public HImage HImage
        {
            get { return _hImage; }
            set { _hImage = value; }
        }

        /// <summary>
        /// Flag is true if class supports Halcon drawing (Regions, Countours, Strings)
        /// </summary>
        [Browsable(false), XmlIgnoreAttribute]
        public bool IsMarkAble { get; set; }

        /// <summary>
        /// List for Halcon drawing objects (Regions, Countours)
        /// </summary>
        [Browsable(false), XmlIgnoreAttribute]
        public List<MarkObject> Marks { set; get; }

        /// <summary>
        /// List for Halcon string objects
        /// </summary>
        [Browsable(false), XmlIgnoreAttribute]
        public List<TextObject> Text { set; get; }

        /// <summary>
        /// Image is good or bad
        /// </summary>
        public bool Judgement { get; set; }

        /// <summary>
        /// Filter block's name
        /// </summary>
        public string BlockName { get; set; }

        #endregion


    }
}
