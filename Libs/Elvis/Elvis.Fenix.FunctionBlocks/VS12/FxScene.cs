﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using System.ComponentModel;
using System.Threading;
using System.Drawing;

using Elvis.Fenix.FunctionBlocks;
using Elvis.Fenix.General;


namespace Elvis.Fenix.FunctionBlocks
{
    public delegate void StartBlockCompleted(Object sender);
    public delegate void SceneCompleteHandler(Object sender);
    public delegate void SceneStartHandler(Object sender);


    public static class FxSessionTimeClass
    {
        private static string time = "NaN";
        public static string SessionStartTime
        {
            get { return time; }
            set { time = value; }
        }

        public static void UpdateTime()
        {
            time = DateTime.Now.ToString("HH_mm_ss_ff");
        }

        public static void LogErrorMessage(string filter, string message)
        {
            string timeStamp = DateTime.Now.ToString();

            using (StreamWriter writer = new StreamWriter(@"AppCrash_log.txt", true))
            {
                writer.WriteLine("[" + timeStamp + "] " + filter);
                writer.WriteLine(message);
                writer.WriteLine("-------------------------");
            }
        }
    }

    public class FxScene : IFxBindable
    {
        [Browsable(false)]
        public int BlockIndex
        {
            get
            {
                return _blockIndex;            
            }
            set
            {
                LastIndex = _blockIndex;
                _blockIndex = value;
            } 
        }

        public bool IsRunMode { get; set; }

        [Browsable(false)]
        public int LastIndex;// { get; set; }
        
        public ManualResetEventSlim _exit = new ManualResetEventSlim(false);
        bool _sessionFinished = false;

        //public List<FxSceneImage> SceneImages = new List<FxSceneImage>();
        //public List<OperationList> OperationsList = new List<OperationList>();

        #region Public region

        [BlockNameAttribute]
        [Browsable(false)]
        public string Name
        {
            set
            {
                _name = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Name"));
            }
            get { return _name; }

        }

        [Browsable(false)]
        public List<FxBlock> blocks = new List<FxBlock>();

        [Browsable(false)]
        private List<Task> tasks = new List<Task>();

        //public HImage SourceImage;

        [Browsable(false)]
        public FxBlock StartBlock { set; get; }

        public bool CountScene { get; set; }

        #endregion

        #region Constructor

        public FxScene(string name, FxBlock startBlock)
        {
            Name = name;
            AddBlock(startBlock);
            StartBlock = startBlock;
            BlockIndex = 0;
        }

        public FxScene(string name)
         {
             Name = name;
        }

        #endregion

        #region Public Members

        public void SetStartBlock(FxBlock startBlock)
        {
             AddBlock(startBlock);
             StartBlock = startBlock;
             BlockIndex = 0;
         }

        public void AddBlock(FxBlock block)
        {
            blocks.Add(block);
        }

        private void ExitLastBlock()
        {
            ((FxBlock)blocks[LastIndex]).Stop();          
        }


        public void RunBlock()
        {
            bool ran = false;
            int previousBlockIdx = 0;

            IsRunMode = false;
            
            ExitLastBlock();

            _exit.Reset();


            while (!_exit.IsSet)
            {
                try
                {

                    if (blocks[BlockIndex].SingleRun)
                    {
                        Thread.Sleep(200);
                    }

                    if (!ran)
                    {
                        Task task = new Task(blocks[BlockIndex].Run);
                        task.Start();
                        task.Wait();

                        previousBlockIdx = BlockIndex;
                    }

                    ran = (blocks[BlockIndex].SingleRun && BlockIndex == previousBlockIdx) ? true : false;
                }
                catch (Exception ex)
                {
                    FxSessionTimeClass.LogErrorMessage("(" + Name + ") ", ex.Message); 
                }
            }
  

            //while (!_exit.IsSet)
            //{
            //    Task task = new Task(blocks[BlockIndex].Run);
            //    task.Start();
            //    task.Wait();

            //    blocks[BlockIndex].Judgement = blocks[BlockIndex].Judgement;

            //}
        }


        public void HandleSceneStartEvent()
        {
            var handler = onStarted;            
            if (handler != null)
            {
                handler(this);
            }
        }

        public void HandleSceneCompleteEvent()
        {

            var handler = onCompleted;
            if (handler != null)
            {
                handler(this);
            }
        }

        public void HandleBlockCompleteEvent()
        {
            var handler = onStartBlockCompleted;
            if (handler != null)
            {
                handler(this);
            }
        }


        public void Run()
        {
            IsRunMode = true;

            _exit.Reset();
            _sessionFinished = false;

            while (!_exit.IsSet)
            {
                HandleSceneStartEvent();
                CountScene = true;

                FxBlock block = StartBlock;

               /* while (block != null)
                {
                    block.Clear();
                    block = block.NextFxBlock;
                }*/
                foreach(FxBlock b in blocks)
                {
                    b.Clear();
                }
 
                GC.Collect();


                Judgement = true;
                ElapsedTime = 0;

                block = StartBlock;

                _sessionFinished = _exit.IsSet;

                while (block != null && !_sessionFinished)
                {
                    try
                    {

                        if(block.UseBlock)
                        {
                            Task task = new Task(block.Run);
                            task.Start();
                            task.Wait();
                        }
                        else
                        {
                            block.Judgement = true;
                        }
                        

                        //Judgement &= block.Judgement;
                        //ElapsedTime += block.ElapsedTime;                       
                        
                    }
                    catch(Exception ex)
                    {
                        Judgement = false;
                        FxSessionTimeClass.LogErrorMessage("(" + Name + ") ", ex.Message);               
                    }
                    finally
                    {
                       

                        if (block == StartBlock && !_sessionFinished)
                        {
                            HandleBlockCompleteEvent();    
                        }

                        Judgement &= block.Judgement;
                        ElapsedTime += block.ElapsedTime;

                        block = _exit.IsSet == true ? null : block.NextFxBlock;
                    }
                }

                HandleSceneCompleteEvent();

                //StartBlock.NextFxBlock = block;
            }

            IsRunMode = false;
        }

        bool canStop = true;

        public void Stop()
        {
            while (!canStop) ;

            _exit.Set();

            foreach (FxBlock block in blocks)
            {
                block.Stop();
            }
        }


        public FxBlock GetBlockByOrder(int ind)
        {
            if(ind < blocks.Count)
            {
                return blocks[ind];
            }
            return null;            
        }

        #endregion

        #region Private members

        private Object sync = new Object();
        private bool _judgement;
        private double _elapsedTime;
        private int _blockIndex;
        private string _name;

        private int _horizontalImagesToTile;
        private bool _cropTiledImages;
        private int _tiledImagesBorderWidth;
        private int _croppedImageSize;

        #endregion

        #region Properties


        public bool Judgement
        {
            get
            {
                return _judgement;
            }
            set
            {
                _judgement = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Judgement"));
            }
        }

        public double ElapsedTime
        {
            get
            {
                return _elapsedTime;
            }
            set
            {
                _elapsedTime = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ElapsedTime"));
            }
        }

        public int BlocksCount
        {
            get
            {
                if(blocks != null)
                {
                    return blocks.Count;
                }
                return 0;
            }
        }

        // image tile'ing properties
        public int HorizontalImagesToTile
        {
            get
            {
                return _horizontalImagesToTile;
            }
            set
            {
                _horizontalImagesToTile = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HorizontalImagesToTile"));
            }
        }

        public bool CropTiledImages
        {
            get
            {
                return _cropTiledImages;
            }
            set
            {
                _cropTiledImages = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("CropTiledImages"));
            }
        }

        public int TiledImagesBorderWidth
        {
            get
            {
                return _tiledImagesBorderWidth;
            }
            set
            {
                _tiledImagesBorderWidth = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("TiledImagesBorderWidth"));
            }
        }

        public int CroppedImageSize
        {
            get
            {
                return _croppedImageSize;
            }
            set
            {
                _croppedImageSize = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("CroppedImageSize"));
            }
        }


        #endregion


        public event SceneCompleteHandler onCompleted;
        public event SceneStartHandler onStarted;
        public event StartBlockCompleted onStartBlockCompleted;

    }
}
