﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.Xml.Serialization;
using System.ComponentModel;
using System.Threading;
using System.Drawing;
using System.Drawing.Imaging;

using Elvis.Fenix.FunctionBlocks;
using Elvis.Fenix.General;
using HalconDotNet;

namespace Elvis.Fenix.FunctionBlocks
{
    public class FxTileImages
    {
        public FxTileImages() { }


        //public static HImage TileImages(List<FxSceneImage> sceneImageList, int horizontalImages, bool cropImages, int croppedImageSize, float borderSize = 30)
        //{
        //    HImage tiledImage = new HImage();

        //    if(sceneImageList != null && sceneImageList.Count > 0)
        //    {
        //        HImage allImages = new HImage();
        //        HImage croppedImage = new HImage();
        //        bool firstImage = true;

        //        //borderSize = 30;

        //        //HWindow hWindow = new HWindow();

        //        foreach(FxSceneImage imageItem in sceneImageList)
        //        {
        //            if(firstImage)
        //            {
        //                if(cropImages)
        //                {
        //                    croppedImage = imageItem.HImage.ZoomImageSize(croppedImageSize, croppedImageSize, "bilinear");
        //                    allImages = AddFrame(croppedImage, borderSize, imageItem.Judgement == true ? Color.Green : Color.Red);
        //                    //allImages = croppedImage;
        //                }
        //                else
        //                {
        //                    allImages = AddFrame(imageItem.HImage, borderSize, imageItem.Judgement == true ? Color.Green : Color.Red);
        //                   // allImages = imageItem.HImage;
        //                }
        //                firstImage = false;
        //            }
        //            else
        //            { 
        //                if(cropImages)
        //                {
        //                    croppedImage = imageItem.HImage.ZoomImageSize(croppedImageSize, croppedImageSize, "bilinear");
        //                    allImages = allImages.ConcatObj(AddFrame(croppedImage, borderSize, imageItem.Judgement == true ? Color.Green : Color.Red));
        //                    //allImages = allImages.ConcatObj(croppedImage);
        //                }
        //                else
        //                {
        //                    allImages = allImages.ConcatObj(AddFrame(imageItem.HImage, borderSize, imageItem.Judgement == true ? Color.Green : Color.Red));
        //                    //allImages = allImages.ConcatObj(imageItem.HImage);
        //                }
        //            }
        //        }

        //        if (allImages != null) tiledImage = allImages.TileImages(horizontalImages, "horizontal");
        //    }

        //    return tiledImage;
        //}


        public static HImage TileImages(List<OperationList> blockOperations, int horizontalImages, bool cropImages, int croppedImageSize, float borderSize = 30)
        {
            HImage tiledImage = new HImage();

            if (blockOperations != null && blockOperations.Count > 0)
            {
                HImage allImages = new HImage();
                HImage croppedImage = new HImage();
                bool firstImage = true;
                int imgNumber = 0;

                //borderSize = 30;

                //HWindow hWindow = new HWindow();

                foreach (OperationList item in blockOperations)
                {
                    if (firstImage)
                    {
                        if (cropImages)
                        {
                            if (item.Image != null)
                            {
                                croppedImage = item.Image.ZoomImageSize(croppedImageSize, croppedImageSize, "bilinear");
                                allImages = AddFrame(croppedImage, borderSize, item.Judgement == true ? Color.Green : Color.Red, imgNumber);
                                //allImages = croppedImage;
                            }
                        }
                        else
                        {
                            if (item.Image != null) allImages = AddFrame(item.Image, borderSize, item.Judgement == true ? Color.Green : Color.Red, imgNumber);
                            // allImages = imageItem.HImage;
                        }
                        firstImage = false;
                    }
                    else
                    {
                        if (cropImages)
                        {

                            try
                            {

                                item.Image = item.Image != null ? item.Image.Clone() : null;
                            }
                            catch (Exception ex)
                            {
                                //AppendLogMessage(this, new FxMessage(FxMessageType.WARNING_MSG, "GUI", "Failed to update window"));
                                item.Image = null;
                            }

                            if (item.Image != null)
                            {
                                croppedImage = item.Image.ZoomImageSize(croppedImageSize, croppedImageSize, "bilinear");
                                allImages = allImages.ConcatObj(AddFrame(croppedImage, borderSize, item.Judgement == true ? Color.Green : Color.Red, imgNumber));
                                //allImages = allImages.ConcatObj(croppedImage);
                            }
                        }
                        else
                        {
                            if (item.Image != null) allImages = allImages.ConcatObj(AddFrame(item.Image, borderSize, item.Judgement == true ? Color.Green : Color.Red, imgNumber));
                            //allImages = allImages.ConcatObj(imageItem.HImage);
                        }
                    }

                    imgNumber++;
                }

                try
                {
                    if (allImages != null) tiledImage = allImages.TileImages(horizontalImages, "horizontal");
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return tiledImage;
        }

        private static HImage AddFrame(HImage inputImage, float borderWidth, Color color, int imgNo)
        {
            HImage resultImage = new HImage();

            if (inputImage != null)
            {
                try
                {
                    IntPtr imgPtr = new IntPtr();
                    string type;
                    int width, height;

                    imgPtr = inputImage.GetImagePointer1(out type, out width, out height);

                    Bitmap bmp = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                    System.Drawing.Imaging.BitmapData data = bmp.LockBits(new Rectangle { X = 0, Y = 0, Width = width, Height = height }, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                    IntPtr bmpPtr = data.Scan0;
                    byte[] bmpBytes = new byte[data.Stride * height * 3];
                    byte[] greyBytes = new byte[data.Stride * height];

                    System.Runtime.InteropServices.Marshal.Copy(bmpPtr, bmpBytes, 0, width * height * 3);
                    System.Runtime.InteropServices.Marshal.Copy(imgPtr, greyBytes, 0, width * height);

                    int remain = data.Stride - data.Width * 3;

                    for (int i = 0; i < height; i++)
                    {
                        for (int j = 0; j < data.Width; j++)
                        {
                            int index = i * data.Width * 3 + i * remain + j * 3;

                            bmpBytes[index] = greyBytes[i * data.Width + j];
                            bmpBytes[index + 1] = greyBytes[i * data.Width + j];
                            bmpBytes[index + 2] = greyBytes[i * data.Width + j];
                        }
                    }
                    System.Runtime.InteropServices.Marshal.Copy(bmpBytes, 0, bmpPtr, width * height * 3);

                    bmp.UnlockBits(data);

                    Graphics gr = Graphics.FromImage(bmp);
                    Brush solidBrush = new SolidBrush(color);
                    Pen pen = new Pen(solidBrush);
                    pen.Width = borderWidth;

                    Brush blackBrush = new SolidBrush(Color.Black);
                    Pen blackPen = new Pen(blackBrush);
                    blackPen.Width = 3F;

                    gr.DrawRectangle(pen, new Rectangle { X = 3, Y = 3, Width = width - 3, Height = height - 3 });
                    gr.DrawRectangle(blackPen, new Rectangle { X = 0, Y = 0, Width = width, Height = height });

                    gr.DrawString(imgNo.ToString(), SystemFonts.DefaultFont, solidBrush, 10, 10);


                    Bitmap newBmp = new Bitmap(bmp.Width, bmp.Height, PixelFormat.Format32bppArgb);
                    newBmp = bmp.Clone(new Rectangle { X = 0, Y = 0, Width = bmp.Width, Height = bmp.Height }, PixelFormat.Format32bppArgb);
                    BitmapData dataRgb = newBmp.LockBits(new Rectangle { X = 0, Y = 0, Width = bmp.Width, Height = bmp.Height }, ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
                    IntPtr ptr = dataRgb.Scan0;
                    newBmp.UnlockBits(dataRgb);

                    try
                    {
                        resultImage.GenImageInterleaved(ptr, "bgrx", bmp.Width, bmp.Height, 1, "byte", newBmp.Width, newBmp.Height, 0, 0, 8, 0);
                    }
                    catch (AccessViolationException)
                    {
                        resultImage = ReturnErrorImage();
                    }
                }

                catch (Exception)
                {
                    resultImage = ReturnErrorImage();
                }
            }
            return resultImage;
        }

        private static HImage ReturnErrorImage()
        {
            HImage resultImage = new HImage();

            Bitmap img = new Bitmap(50, 100);

            using (Graphics g = Graphics.FromImage(img))
            {
                Color customColor = Color.FromArgb(50, Color.Red);
                SolidBrush shadowBrush = new SolidBrush(customColor);
                g.FillRectangle(shadowBrush, new Rectangle(0, 0, 50, 50));

            }

            BitmapData dataRgb = img.LockBits(new Rectangle { X = 0, Y = 0, Width = 50, Height = 50 }, ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            IntPtr ptr = dataRgb.Scan0;
            img.UnlockBits(dataRgb);

            resultImage.GenImageInterleaved(ptr, "bgrx", 50, 50, 1, "byte", 50, 50, 0, 0, 8, 0);

            return resultImage;
        }

    }
}
