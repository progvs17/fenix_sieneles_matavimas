﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elvis.Fenix.General
{
    public enum FxMessageType
    {
        INFO_MSG,
        WARNING_MSG,
        ERROR_MSG,
        MASK_OK_MSG
    }

    public class FxMessage
    {
        public FxMessageType Type { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }

        public FxMessage(FxMessageType type, string title, string message)
        {
            Type = type;
            Title = title;
            Message = message;
        }
    }
}
