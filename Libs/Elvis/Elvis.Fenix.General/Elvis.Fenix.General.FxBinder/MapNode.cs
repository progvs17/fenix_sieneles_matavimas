﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elvis.Fenix.General
{
    public class MapNode
    {
        public Object Object;
        public string PropName { get; set; }

        public MapNode(Object obj, string propName)
        {
            Object = obj;
            PropName = propName;
        }
    }
}
