﻿using System;
using System.ComponentModel;
using System.Threading;


namespace Elvis.Fenix.General
{
    public abstract class IFxBindable : INotifyPropertyChanged
    {
        private Object sync = new Object();
        private readonly Mutex _mutex = new Mutex();

        public IFxBindable()
        {
        }

        public virtual event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            //lock(sync)
            //{
                if (PropertyChanged != null) PropertyChanged(this, e);
            //}  
        }

        protected void OnPropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            //lock (sync)
            //{
            //    if (PropertyChanged != null) PropertyChanged(sender, e);
            //} 
            _mutex.WaitOne(100);
            try
            {
                if (PropertyChanged != null) PropertyChanged(sender, e);
            }
            catch { }
            finally
            {
                _mutex.ReleaseMutex();
            }      
        }
    }
}
