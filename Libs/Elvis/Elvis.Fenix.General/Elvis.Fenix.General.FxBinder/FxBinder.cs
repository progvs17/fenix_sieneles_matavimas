﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Elvis.Fenix.General
{
    public enum BindSynchMode
    {
        ONE_WAY = 0,
        TWO_WAY = 1
    }

    public class FxBinder
    {
        public List<Object> Objects { get; set; }
        public List<MapPair> Map { set; get; }

        private Object sync = new Object();

        public FxBinder()
        {
            Map = new List<MapPair>();
            Objects = new List<Object>();
        }

        public void AddBind(MapPair pair, BindSynchMode mode = BindSynchMode.TWO_WAY)
        {
            Object src = Objects.FirstOrDefault(o => o == pair.Source);
            if(src == null)
            {
                AddObject(pair.Source.Object);
            }

            Object targ = Objects.FirstOrDefault(o => o == pair.Target);
            if (targ == null)
            {
                AddObject(pair.Target.Object);
            }

            Map.Add(pair);

            if(mode == BindSynchMode.TWO_WAY)
            {
                MapPair backpair = new MapPair(  pair.Target, pair.Source );
                Map.Add(backpair);
            }            
        }

        private void AddObject(Object obj)
        {
            Objects.Add(obj);
            var o = (IFxBindable)obj;
            o.PropertyChanged += new PropertyChangedEventHandler(OnPropChanged);
        }

        protected void OnPropChanged(Object sender, PropertyChangedEventArgs e)
        {
                Update(sender, e);           
        }


        public void Update(object sender, PropertyChangedEventArgs e)
        {
            if (Map != null && Map.Count > 0)
            {
                var list = Map.Where(m => m.Source.Object == sender && m.Source.PropName == e.PropertyName);

                if (list != null)
                {
                    foreach (var pair in list)
                    {
                        var value = sender.GetType().GetProperty(e.PropertyName).GetValue(sender);
                        var currValue = pair.Target.Object.GetType().GetProperty(pair.Target.PropName).GetValue(pair.Target.Object);

                        try
                        {
                            if (value != null && currValue != null && value.GetHashCode() != currValue.GetHashCode() || currValue == null)
                            {
                                pair.Target.Object.GetType().GetProperty(pair.Target.PropName).SetValue(pair.Target.Object, value);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
            }
        }
    }

}
