﻿using System;

namespace Elvis.Fenix.General
{
    public class MapPair
    {
        public MapNode Source { set; get; }
        public MapNode Target { set; get; }

        public MapPair(MapNode src, MapNode dest)
        {
            Source = src;
            Target = dest;
        }
    }
}
