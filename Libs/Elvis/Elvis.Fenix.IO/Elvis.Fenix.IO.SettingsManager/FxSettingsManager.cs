﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.Schema;

namespace Elvis.Fenix.IO
{
    [Serializable]
    public class ObjectItems
    {
        public ObjectItems()
        {

        }

        public ObjectItems (string name, Object obj, Type objType)
        {
            Name = name;
            ClassObj = obj;
            ClassType = objType;
        }

        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlElement("Class Object")]
        public Object ClassObj { get; set; }

        [Browsable(false), XmlIgnore]
        public Type ClassType { get; set; }

    }


    [Serializable]
    public class FxSettingsManager : Elvis.Fenix.General.IFxBindable
    {
        #region Private Fields

        List<ObjectItems> _settingsList;
        List<ObjectItems> _settingsFromFileList;
        FxXmlFileManager xmlManager;

        List<string> _folderNames;

        private string _dir;

        #endregion


        #region Public Properties
   
        /// <summary>
        /// Classes / Objects to read / write list
        /// </summary>
        public List<ObjectItems> SettingsList
        {
            get
            {
                return _settingsList;
            }
            set
            {
                _settingsList = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SettingsList"));
            }
        }

        public int NumberOfSettings
        {
            get { return _folderNames.Count; }
        }

        public List<string> FolderNames
        {
            get
            {
                return _folderNames;
            }
        }

        public string SettingsPath 
        {
            get { return _dir; } 
            set
            {
                _dir = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SettingsPath"));
            }
        }

        #endregion


        #region Constructors

        /// <summary>
        /// Creates new FxSettingsManager instance
        /// </summary>
        public FxSettingsManager ()
        {
            xmlManager = new FxXmlFileManager();
            _settingsList = new List<ObjectItems>();
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Add class object to settings list
        /// </summary>
        /// <typeparam name="T">The type of object</typeparam>
        /// <param name="name">Item name (will be used as filename on saving)</param>
        /// <param name="objectToWrite">Object</param>
        public void Add<T>(string name, T objectToWrite) 
        {
            try
            {
                _settingsList.Add(new ObjectItems(name, objectToWrite, typeof(T)));
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        /// <summary>
        /// Remove an existing item from list
        /// </summary>
        /// <param name="name">Item name</param>
        public void Remove(string name)
        {
            try
            {
                ObjectItems item = _settingsList.Find(x => x.Name.Contains(name));

                if (item != null)
                    _settingsList.Remove(item);
            }
            catch { }
        }


        /// <summary>
        /// Save all settings in the list. Files will be saved as path:/className_prefix.xml
        /// </summary>
        /// <param name="path">Folder path where all .xml files will be saved</param>
        /// <param name="prefix">Prefix name</param>
        public void WriteToXmlFile(string path, string prefix)
        {
            try
            {
                foreach (var item in _settingsList)
                {
                    xmlManager.WriteToXmlFile(path + item.Name + "_" + prefix + ".xml", item.ClassObj, item.ClassType, false);
                }
                //Console.WriteLine("Settings were written to files succesfully.");
            }
            catch
            {
                Console.WriteLine("Writing settings to files failed!");
            }       
        }

        /// <summary>
        /// Writes single file to XML file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="obj"></param>
        public void WriteSingleFileToXml<T>(string name, T obj)
        {
            try
            {
                xmlManager.WriteToXmlFile<T>(name + ".xml", obj, false);
            }
            catch
            {
                Console.WriteLine("Writing settings to files failed!");
            }
        }

        /// <summary>
        /// Read single XML file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public T ReadSingleFileFromXml<T>(string name)
        {
            return (T)xmlManager.ReadFromXmlFile<T>(name + ".xml");
        }

      
        /// <summary>
        /// Reads & applies settings from files in particular directory
        /// </summary>
        /// <param name="path">*.xml files directory</param>
        public void ReadFromXmlFile(string path)
        {
            _settingsFromFileList = new List<ObjectItems>();

            try
            {
                int i = 0;
                string[] files = Directory.GetFiles(path, "*.xml");
                foreach (var filename in files)
                {
                    foreach (var item in _settingsList)
                    {
                        string str = " ";

                        try
                        {
                            str = filename.Substring((path.Length + 1), item.Name.Length);
                        }
                        catch(ArgumentOutOfRangeException)
                        {
                            str = "not good";
                        }

                        if(str == item.Name)
                        {
                            int index = _settingsList.FindIndex(x => x.Name.Contains(str));
                            _settingsFromFileList.Add(_settingsList[index]);
                            xmlManager.ReadFromXmlFile(filename, _settingsFromFileList[i].ClassObj);
                            i++;
                        }
                    }

                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Reading from xml file failed!");
                Console.WriteLine(ex.Message);
            }
        }




        public void ApplyNewSettings(int numberFromList)
        {
            string path = SettingsPath + @"\" + _folderNames[numberFromList];

            ReadFromXmlFile(path);
        }

        public void SaveCurrentSettings(int numberFromList, string prefix)
        {
            string path = SettingsPath + @"\" + _folderNames[numberFromList] + @"\";

            WriteToXmlFile(path, prefix);
        }


        public void GetSettingsFolders(string path)
        {           
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);

                if (!Directory.Exists(path + @"\config 1"))
                {
                    Directory.CreateDirectory(path + @"\config 1");
                }

#if DEBUG
                Console.WriteLine("New directory created.");
#endif
            }

            SettingsPath = path;

            var directoryInfo = new System.IO.DirectoryInfo(path);

            try
            {
                int directoryCount = directoryInfo.GetDirectories().Length;
  
                if(directoryCount > 0)
                {
                    var dirList = directoryInfo.EnumerateDirectories("*");
                    _folderNames = new List<string>();

                    foreach (var item in dirList)
                    {
                        _folderNames.Add(item.Name);
                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

    }
}
