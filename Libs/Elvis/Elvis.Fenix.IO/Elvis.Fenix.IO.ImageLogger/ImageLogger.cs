﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

using System.Windows.Forms;
using Elvis.Fenix.General;
//using HalconDotNet;



namespace Elvis.Fenix.IO
{
    public delegate void LogMessageHandler(object sender, FxMessage message);

    public class FxImageLogger
    {

        #region Private members

        const string _defaultPath = "C:\\Fenix\\Log Images";
        private string _folderPath;

        //private bool _enAutoClean; 
        // private int _maxNoF;  // Max number of files in one folder
        //private int _maxNoD;  // Max number of days folders to save
        #endregion   


        #region Constructor

        public FxImageLogger(string path = _defaultPath)
        {
            FolderPath = path;
        }
        #endregion      
        
        

        #region Public members

        /// <summary>
        /// Save Halcon Image to file
        /// </summary>
        /// <param name="image"> Source image </param>
        /// <param name="prefix"> Filenaem prefix </param>
        //public void SaveImage(HImage image, string prefix)
        //{
        //    string path = IsOrderByDateEnabled == true ? _folderPath + DateTime.Now.ToString("yyyy-MM-dd") + "\\" : _folderPath;

        //    if (!Directory.Exists(path))
        //    {
        //        Directory.CreateDirectory(path);
        //    }

        //    var fileName = prefix + DateTime.Now.ToString("HHmmssff");
        //    image.WriteImage("bmp", 0, path + fileName);
        //}

        /*
        /// <summary>
        /// Saves IntPtr data to image file with specified filename prefix
        /// </summary>
        /// <param name="width">Image width</param>
        /// <param name="height">Image height</param>
        /// <param name="pixelFormat">Pixel format</param>
        /// <param name="dataPtr">Data pointer</param>
        /// <param name="prefix">Filename prefix</param>
        public void SaveImage(int width, int height, PixelFormat pixelFormat, IntPtr dataPtr, string prefix)
        {
            Bitmap img = new Bitmap(width, height, pixelFormat);
            Rectangle rect = new Rectangle(0, 0, width, height);
            BitmapData data = img.LockBits(rect, ImageLockMode.WriteOnly, pixelFormat);

            int step = data.Stride / data.Width;

            int size = width * height;
            byte[] values = new byte[size];
            System.Runtime.InteropServices.Marshal.Copy(dataPtr, values, 0, size);

            byte[] rgbValues = new byte[size * step];
            for (int ind = 0; ind < size; ind++)
            {
                rgbValues[ind * step] = values[ind];
                rgbValues[ind * step + 1] = values[ind];
                rgbValues[ind * step + 2] = values[ind];
            }
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, data.Scan0, size * step);

            img.UnlockBits(data);

            SaveImage(img, prefix);
        }

        /// <summary>
        /// Saves bitmap to image file
        /// </summary>
        /// <param name="image">Bitmap input</param>
        /// <param name="prefix">Filename prefix</param>
        public void SaveImage(Bitmap image, string prefix)
        {
            CheckPathValidity();
            var fileName = prefix + DateTime.Now.ToString("HHmmssff");
            FolderPath = _folderPath;
            image.Save(_folderPath + fileName);
        }

        /// <summary>
        /// Saves bitmap array to separate image files with same id number
        /// </summary>
        /// <param name="images">Bitmao array input</param>
        public void SaveImages(Bitmap[] images)
        {
            if (images != null)
            {
                for (int i = 0; i < images.Length; i++)
                {
                    var prefix = "C" + (i + 1).ToString() + "_";
                    SaveImage(images[i], prefix);
                }
            }
        }
         * 
         */

        /// <summary>
        /// Set default path if current path doesn't exist
        /// </summary>
        public void CheckPathValidity()
        {
            if (!Directory.Exists(_folderPath))
            {
                try
                {
                   _folderPath = _defaultPath;               
                    Directory.CreateDirectory(_folderPath);
                }
                catch (Exception ex)
                {
                    throw; // error
                }
            }
        }    

        #endregion


        #region Public properties

        public string FolderPath
        {
            set
            {
                _folderPath = value + "\\";
                if (Directory.Exists(_folderPath) == false)
                {
                    try
                    {
                        //onMessage(this, new TMessage(TMessage.TMessageType.INFO_MSG, "Created new Image Log directory"));
                        Directory.CreateDirectory(_folderPath);
                    }
                    catch (Exception ex)
                    {
                        //throw;
                        // Insert message handler
                    }
                }
            }
            get
            {
                return _folderPath;
            }
        }

        public string DefaultFolderPath
        {
            get
            {
                return _defaultPath;
            }
        }

        public bool IsOrderByDateEnabled { get; set; }

        #endregion



        public event LogMessageHandler onMessage;

    }
}
