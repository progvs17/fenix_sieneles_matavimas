﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Ports;
using System.Runtime.InteropServices;
//using CRC;

namespace Elvis.Fenix.IO
{
    public class FxModbusPortManager
    {

        #region Private Members

        private static readonly Mutex m = new Mutex();

        private static bool _portIsBusy = false;

        private SerialPort _port;

        #endregion


        #region Public Properties

        public bool ResponseReceived { get; set; }

        public byte[] ReceivedData { get; set; }

        #endregion


        #region Constructors

        //public FxModbusPortManager()
        //{
        //    //managerThread = new Thread(new ThreadStart(MainProcess));

        //    //managerThread.Start();

        //   // m = new Mutex();
        //}

        #endregion


        #region Public Methods

        public void SendMessage(SerialPort port, byte[] message)
        {
            _port = port;
            _port.DataReceived += _port_DataReceived;

            System.Diagnostics.Stopwatch watchDog = new System.Diagnostics.Stopwatch();

            ResponseReceived = false;

            if(_port.IsOpen && !_portIsBusy)
            {
                _portIsBusy = true;

                m.WaitOne();

                _port.DiscardInBuffer();
                _port.DiscardOutBuffer();

                try
                {
                    _port.Write(message, 0, message.Length);
                }
                catch (TimeoutException)
                {
                    Console.WriteLine("Write failed.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    //throw new Exception(ex.Message);
                }
                finally
                {
                    _portIsBusy = false;

                    if (m != null)
                        m.ReleaseMutex();
                }

                watchDog.Start();

                while (!ResponseReceived && watchDog.ElapsedMilliseconds < 50) ;

                watchDog.Stop();
            }
        }

        void _port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;

            try
            {
                System.Threading.Thread.Sleep(5);
                // read data
                int messageLen = sp.BytesToRead;
                byte[] result = new byte[messageLen];
                ReceivedData = new byte[messageLen];

                if (sp.BytesToRead.Equals(messageLen))
                    sp.Read(result, 0, messageLen);

                foreach (byte bit in result)
                {
                    Console.Write(bit.ToString("X") + " ");
                }

                Console.WriteLine(result.ToString());
                // clear input buffer
                sp.DiscardInBuffer();

                ReceivedData = result;
                ResponseReceived = true;

            }
            catch (TimeoutException)
            {
                Console.WriteLine("COM port connection timeout!", "ERROR!");
            }

            _port.DataReceived -= _port_DataReceived;
        }

        #endregion

    }
}
