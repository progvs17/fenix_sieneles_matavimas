﻿using Elvis.Fenix.General;
using Elvis.Fenix.Hardware;
//using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Elvis.Fenix.IO
{
    public delegate void ImageLoadHandler(Object sender);

    public class FxImageLoader : IFxBindable
    {
        private bool isEnabled;
        //private HImage image;
        private int initIndex;
        private int currIndex;
        private string[] files;
        private string path;
        private string filename;

       // public string FileName { get; set; }      

        public event ImageLoadHandler onImageLoaded;        

        public FxImageLoader()
        {
            currIndex = 0;
        }

        public void LoadImages(string path, string fileName)
        {
            if(System.IO.Directory.Exists(path))
            {
                files = System.IO.Directory.GetFiles(path);

                // Get selected file index
                for(int i = 0; i < files.Length; i++)
                {
                    if(files[i].Contains(fileName))
                    {
                        initIndex = i;
                        break;
                    }                                       
                }
                currIndex = initIndex;
                LoadFileByIndex(currIndex);               
            }
        }

        public void LoadFileByIndex(int index)
        { 
            if(files != null && files.Length > index && index >= 0)
            {
                //try
                //{
                //    currIndex = index;
                //    image = new HImage(files[currIndex]);
                //    FileName = files[currIndex];
                //}
                //catch (Exception ex)
                //{
                //    image = null;
                //}                
            }        
        }

        public void LoadNext()
        {
            if(files != null)
            {
                currIndex = currIndex < files.Length - 1 ? currIndex + 1 : currIndex;
                LoadFileByIndex(currIndex);
            } 
        }

        public void LoadPrev()
        {
            if (files != null)
            {
                currIndex = currIndex > 0 ? currIndex - 1 : currIndex;
                LoadFileByIndex(currIndex);
            }
        }

        void LoadFirst()
        {
            if(files != null)
            {
                currIndex = 0;
                LoadFileByIndex(currIndex);
            }            
        }

        void LoadLast()
        {
            if (files != null)
            {
                currIndex = files.Length - 1;
                LoadFileByIndex(currIndex);
            }
        }
        
        private void LoadCurrentImage()
        {
            if(files != null)
            {
                //try
                //{
                //    image = new HImage(files[currIndex]);
                //}
                //catch (Exception ex)
                //{
                //    image = null;
                //}
            }                        
        }

        // props



        public string FileName
        {
            get { return filename; }
            set
            {
                filename = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("FileName"));
            }
        }

        //public HImage CurrentImage
        //{
        //    get { return image; }
        //    set
        //    {
        //        image = value;
        //        OnPropertyChanged(this, new PropertyChangedEventArgs("CurrentImage"));
        //        if (onImageLoaded != null)
        //        {
        //            onImageLoaded(this);
        //        }
        //    }
        //}

        public bool IsEnabled
        {
            get
            {
                return isEnabled;
            }
            set
            {
                isEnabled = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("IsEnabled"));
            }
        }



    }
}
