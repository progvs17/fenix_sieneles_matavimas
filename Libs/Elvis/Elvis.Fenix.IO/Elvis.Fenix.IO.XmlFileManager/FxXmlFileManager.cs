﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.IO;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Reflection;

namespace Elvis.Fenix.IO
{
    public class FxXmlFileManager
    {
        /// <summary>
        /// Writes the given object instance properties to a XML file (with 'T').
        /// <para>Only Public properties and variables will be written to the file. These can be any type though, even other classes.</para>
        /// <para>If there are public properties/variables that you do not want written to the file, decorate them with the [XmlIgnore] attribute.</para>
        /// <para>Object type must have a parameterless constructor.</para>
        /// </summary>
        /// <typeparam name="T">The type of object being written to the file.</typeparam>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <param name="objectToWrite">The object instance to write to the file.</param>
        /// <param name="append">If false the file will be overwritten if it already exists. If true the contents will be appended to the file.</param>
        public void WriteToXmlFile<T>(string filePath, T objectToWrite, bool append = false) // where T : new()
        {
            TextWriter writer = null;
            try
            {
                var serializer = new XmlSerializer(typeof(T));
                writer = new StreamWriter(filePath, append);
                serializer.Serialize(writer, objectToWrite);
            }
            catch (Exception ex) { }
            finally
            {
                if (writer != null)
                    writer.Close();
            }

        }

        /// <summary>
        /// Writes the given object instance properties to a XML file (without 'T'). 
        /// </summary>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <param name="objectToWrite">The object instance to write to the file.</param>
        /// <param name="objType">The type of object being written to the file.</param>
        /// <param name="append">If false the file will be overwritten if it already exists. If true the contents will be appended to the file.</param>
        public void WriteToXmlFile(string filePath, Object objectToWrite, Type objType, bool append = false) // where T : new()
        {
            TextWriter writer = null;
            try
            {
                var serializer = new XmlSerializer(objType);
                writer = new StreamWriter(filePath, append);
                serializer.Serialize(writer, objectToWrite);
            }
            catch (Exception ex) { }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }


        public void WriteToXmlFile3<T>(string filePath, List<T> list, bool append = false)  where T : new()
        {
            TextWriter writer = null;
            try
            {
                var serializer = new XmlSerializer(typeof(T));
                writer = new StreamWriter(filePath, append);
                serializer.Serialize(writer, list);
            }
            catch(Exception ex) { }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }


        /// <summary>
        /// Writes properties to object from a XML file.
        /// <param> How to use: object = FxXmlFileManager.ReadFromXmlFile[object base class](filePath); </param>
        /// </summary>
        /// <typeparam name="T">The type of object being written to the file.</typeparam>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <returns>New object with property values from provided xml file.</returns>
        public T ReadFromXmlFile<T>(string filePath)
        {
            T result;
            StreamReader reader = new StreamReader(filePath);

            try
            {
                XmlSerializer xmlserializer = new XmlSerializer(typeof(T));
                result = (T)xmlserializer.Deserialize(reader);
            }
            finally
            {
                if(reader != null)
                {
                    reader.Close();
                }
            }
    
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="obj"></param>
        public void ReadFromXmlFile(string filePath, Object obj)
        {
            StreamReader reader = new StreamReader(filePath);

            try
            {
                XmlSerializer xmlserializer = new XmlSerializer(obj.GetType());
                var result = (Object)xmlserializer.Deserialize(reader);

                var props = result.GetType().GetProperties();

                foreach (var prop in props)
                {
                    try
                    {
                        bool isIgnored = false;
                        var property = obj.GetType().GetProperty(prop.Name);

                        if(property != null)
                        {
                            isIgnored = property.GetCustomAttributes(false).Any(a => a is XmlIgnoreAttribute);                        
                        }

                        if (!isIgnored)
                        {
                            obj.GetType().GetProperty(prop.Name).SetValue(obj, prop.GetValue(result));
                        }                        
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            catch (Exception ex)
            { 
            }

            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
        }



    }
}
