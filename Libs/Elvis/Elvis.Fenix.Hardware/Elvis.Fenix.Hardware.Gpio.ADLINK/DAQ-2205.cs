﻿using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;
using System.ComponentModel;

using Elvis.Fenix.Hardware;
using System.Runtime.InteropServices;

namespace Elvis.Fenix.Hardware.Gpio
{
    public class DAQ_2205 : FxGpio
    {
        #region Enums

        public enum DEVICE_MODEL
        {
            DAQ_2010 = D2KDASK.DAQ_2010,
            DAQ_2205 = D2KDASK.DAQ_2205,
            DAQ_2206 = D2KDASK.DAQ_2206,
            DAQ_2005 = D2KDASK.DAQ_2005,
            DAQ_2204 = D2KDASK.DAQ_2204,
            DAQ_2006 = D2KDASK.DAQ_2006,
            DAQ_2501 = D2KDASK.DAQ_2501,
            DAQ_2502 = D2KDASK.DAQ_2502,
            DAQ_2208 = D2KDASK.DAQ_2208,
            DAQ_2213 = D2KDASK.DAQ_2213,
            DAQ_2214 = D2KDASK.DAQ_2214,
            DAQ_2016 = D2KDASK.DAQ_2016,
            DAQ_2020 = D2KDASK.DAQ_2020,
            DAQ_2022 = D2KDASK.DAQ_2022
        }

        public enum OPERATION_MODE
        {
            SYNCH = D2KDASK.SYNCH_OP,
            ASYNC = D2KDASK.ASYNCH_OP
        }

        public enum AD_RANGE
        {
            AD_B_10_V       = D2KDASK.AD_B_10_V,
            AD_B_5_V        = D2KDASK.AD_B_5_V,
            AD_B_2_5_V      = D2KDASK.AD_B_2_5_V,
            AD_B_2_V        = D2KDASK.AD_B_2_V,
            AD_B_1_25_V     = D2KDASK.AD_B_1_25_V,
            AD_B_1_V        = D2KDASK.AD_B_1_V,
            AD_B_0_625_V    = D2KDASK.AD_B_0_625_V,
            AD_B_0_3125_V   = D2KDASK.AD_B_0_3125_V,
            AD_B_0_5_V      = D2KDASK.AD_B_0_5_V,
            AD_B_0_1_V      = D2KDASK.AD_B_0_1_V,
            AD_B_0_05_V     = D2KDASK.AD_B_0_05_V,
            AD_B_0_25_V     = D2KDASK.AD_B_0_25_V,
            AD_B_0_2_V      = D2KDASK.AD_B_0_2_V,
            AD_B_0_01_V     = D2KDASK.AD_B_0_01_V,
            AD_B_0_005_V    = D2KDASK.AD_B_0_005_V,
            AD_B_0_001_V    = D2KDASK.AD_B_0_001_V,
            AD_U_20_V       = D2KDASK.AD_U_20_V,
            AD_U_10_V       = D2KDASK.AD_U_10_V,
            AD_U_5_V        = D2KDASK.AD_U_5_V,
            AD_U_2_5_V      = D2KDASK.AD_U_2_5_V,
            AD_U_1_25_V     = D2KDASK.AD_U_1_25_V,
            AD_U_1_V        = D2KDASK.AD_U_1_V,
            AD_U_0_1_V      = D2KDASK.AD_U_0_1_V,
            AD_U_0_01_V     = D2KDASK.AD_U_0_01_V,
            AD_U_0_001_V    = D2KDASK.AD_U_0_001_V,
            AD_U_4_V        = D2KDASK.AD_U_4_V,
            AD_U_2_V        = D2KDASK.AD_U_2_V,
            AD_U_0_5_V      = D2KDASK.AD_U_0_5_V,
            AD_U_0_4_V      = D2KDASK.AD_U_0_4_V,
        }

        #endregion

        #region Private Members

        private const int DATA_SIZE = 10000;
        private int arraySize = 5;

        DEVICE_MODEL device;
        short cardNumber = -1;
        //short m_dev = -1;
        AD_RANGE deviceVoltage;

        IntPtr data_buffer = Marshal.AllocHGlobal(sizeof(short) * DATA_SIZE);
        IntPtr m_data_buffer = Marshal.AllocHGlobal(sizeof(short) * DATA_SIZE);
        //IntPtr data_buffer_0 = Marshal.AllocHGlobal(sizeof(short) * DATA_SIZE);
        //IntPtr data_buffer_1 = Marshal.AllocHGlobal(sizeof(short) * DATA_SIZE);
        double[] m_voltage_array = new double[DATA_SIZE];
        double[] voltage_array = new double[DATA_SIZE];
        CallbackDelegate data_del;

        #endregion

        #region Constructors

        public DAQ_2205()
        {

        }

        public DAQ_2205(DEVICE_MODEL card_model, ushort number)
        {
            device = card_model;
            cardNumber = (short)number;
            IsOpen = false;

            data_del = new CallbackDelegate(DataReady);
        }

        #endregion


        #region Public Prototypes

        /// <summary>
        /// Maximum available port's pins amount
        /// </summary>
        [Description("Maximum amount of pins available"), ReadOnly(true)]
        public override int MaxPinNumber { get; set; }

        /// <summary>
        /// Device ID depending on code
        /// </summary>
        [Description("Device ID"), ReadOnly(true)]
        public override string DeviceId { get; set; }

        /// <summary>
        /// Device serial number
        /// </summary>
        [Description("Device serial number"), ReadOnly(true)]
        public override string DeviceSerial { get; set; }

        /// <summary>
        /// Device's vendor id number
        /// </summary>
        [Description("Device's vendor ID number"), ReadOnly(true)]
        public override string Vendor { get; set; }

        [Description("Device's status - open/closed"), ReadOnly(true)]
        public override bool IsOpen { get; set; }

        #endregion

        //public virtual bool EnableDI { set; get; }
        //public virtual bool EnableDO { set; get; }

        #region Public Methods

        public override void Open()
        {
            short err = 0;

            if (cardNumber >= 0)
            {
                err = D2KDASK.D2K_Release_Card((ushort)cardNumber);
            }

            cardNumber = D2KDASK.D2K_Register_Card((ushort)device, (ushort)cardNumber);
            if (cardNumber < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_Register_Card error!");
#endif
                IsOpen = false;
            }
            else
            {
                err = D2KDASK.D2K_DIO_PortConfig((ushort)cardNumber, D2KDASK.Channel_P1A, D2KDASK.OUTPUT_PORT);
                if (err < 0)
                {
#if DEBUG
                    Console.WriteLine("D2K_DIO_PortConfig error!");
#endif
                }
                err = D2KDASK.D2K_DIO_PortConfig((ushort)cardNumber, D2KDASK.Channel_P1B, D2KDASK.INPUT_PORT);
                if (err < 0)
                {
#if DEBUG
                    Console.WriteLine("D2K_DIO_PortConfig error!");
#endif
                }

                if(cardNumber >= 0)
                {
                    IsOpen = true;
                }

                
            }
        }

        public override void Close()
        {
            short err;
            if (cardNumber >= 0)
            {
                err = D2KDASK.D2K_Release_Card((ushort)cardNumber);
            }
            Marshal.FreeHGlobal(m_data_buffer);

            IsOpen = false;
        }

        public override void TogglePin(int pin) { throw new NotImplementedException(); }

        public override void SetPin(int pin, int value) { throw new NotImplementedException(); }

        public override void SetPin(int port, int pin, int value) { throw new NotImplementedException(); }

        public override void SetPort(int port, int value)
        {
            short err;

            err = D2KDASK.D2K_DO_WritePort((ushort)cardNumber, D2KDASK.Channel_P1A, (uint)value);
            if (err < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_DO_WritePort error!");
#endif
            }
        }

        public override int GetPin(int port, int pin) { throw new NotImplementedException(); }


        public override int GetPort(int port)
        {
            short err;
            uint int_value;

            err = D2KDASK.D2K_DI_ReadPort((ushort)cardNumber, D2KDASK.Channel_P1B, out int_value);
            if (err < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_DI_ReadPort error!");
#endif

                return - 1;
            }

            return (int)int_value;
        }

        public override int GetDOPin(int port) { throw new NotImplementedException(); }


        public override void SetReferenceVoltage(short channel, ushort voltageRange = (ushort)AD_RANGE.AD_B_10_V)
        {
            try
            {
                if (IsOpen)
                {
                    short err;
                    // channel = -1 - all channels
                    deviceVoltage = (AD_RANGE)voltageRange;
                    err = D2KDASK.D2K_AI_CH_Config((ushort)cardNumber, channel, (ushort)(voltageRange)); // | D2KDASK.AI_NRSE));

                    if (err < 0)
                    {
#if DEBUG
                        Console.WriteLine("D2K_AI_CH_Config error!");
#endif
                    }
                }         
                
            }
            catch (Exception)
            {
                
                //throw;
            }
            
        }

        public override double ReadAnalogVoltage(ushort channel)
        {
            short err;
            double voltage;
            

            err = D2KDASK.D2K_AI_VReadChannel((ushort)cardNumber, channel, out voltage);

            if (err < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_AI_VReadChannel error!");
#endif
                return -99;
            }

            return voltage;
        }

        public override int ReadAnalogValue(ushort channel)
        {

            short err;
            ushort value;

            err = D2KDASK.D2K_AI_ReadChannel((ushort)cardNumber, channel, out value);

            if (err < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_AI_VReadChannel error!");
#endif
                return -1;
            }

            return value;
        }

        public void UpdateDataCallback(uint numberOfDatapoints, uint scanInterval = 400)
        {
            short ret;
            ushort buf_id;

            if (numberOfDatapoints > DATA_SIZE)
                arraySize = DATA_SIZE;
            else
                arraySize = (int)numberOfDatapoints;

            ret = D2KDASK.D2K_AI_EventCallBack((ushort)cardNumber, 1, D2KDASK.DAQEnd, data_del);
            if (ret < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_AI_EventCallBack error!");
#endif
                return;
            }
            ret = D2KDASK.D2K_AI_CH_Config((ushort)cardNumber, -1, (ushort)deviceVoltage); // (ushort)((ushort)deviceVoltage | D2KDASK.AI_NRSE));
            if (ret < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_AI_CH_Config error");
#endif
                return;
            }
            ret = D2KDASK.D2K_AI_Config((ushort)cardNumber, D2KDASK.DAQ2K_AI_ADCONVSRC_Int, D2KDASK.DAQ2K_AI_TRGSRC_SOFT, 0, 0, 1, true);
            if (ret < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_AI_Config error!");
#endif
                return;
            }
            ret = D2KDASK.D2K_AI_ContBufferSetup((ushort)cardNumber, m_data_buffer, (uint)arraySize, out buf_id);
            if (ret < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_AI_ContBufferSetup error!");
#endif
                return;
            }
            ret = D2KDASK.D2K_AI_ContReadChannel((ushort)cardNumber, 0, buf_id, (uint)arraySize, scanInterval, 400, D2KDASK.ASYNCH_OP);
            if (ret < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_AI_ContReadChannel error!");
#endif
                return;
            }
        }

        public double[] UpdateData(ushort channel, uint numberOfDatapoints, uint scanInterval = 400)
        {
            short ret;
            ushort buf_id;
            byte stopped;
            uint access_cnt;
            uint start_pos;

            if (numberOfDatapoints > DATA_SIZE)
                arraySize = DATA_SIZE;
            else
                arraySize = (int)numberOfDatapoints;

            IntPtr bufferD = Marshal.AllocHGlobal(sizeof(short) * arraySize);
            double[] bufferV = new double[arraySize];


            ret = D2KDASK.D2K_AI_CH_Config((ushort)cardNumber, (short)channel, (ushort)deviceVoltage); // (ushort)((ushort)deviceVoltage | D2KDASK.AI_NRSE));
            if (ret < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_AI_CH_Config error!");
#endif
                Marshal.FreeHGlobal(bufferD);
                return null;
            }
            ret = D2KDASK.D2K_AI_Config((ushort)cardNumber, D2KDASK.DAQ2K_AI_ADCONVSRC_Int, D2KDASK.DAQ2K_AI_TRGSRC_SOFT, 0, 0, 1, true);
            if (ret < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_AI_Config error!");
#endif
                Marshal.FreeHGlobal(bufferD);
                return null;
            }
            ret = D2KDASK.D2K_AI_ContBufferSetup((ushort)cardNumber, bufferD, (uint)arraySize, out buf_id);
            if (ret < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_AI_ContBufferSetup error!");
#endif
                Marshal.FreeHGlobal(bufferD);
                return null;
            }
            ret = D2KDASK.D2K_AI_ContReadChannel((ushort)cardNumber, channel, buf_id, (uint)arraySize, scanInterval, 400, D2KDASK.ASYNCH_OP);
            if (ret < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_AI_ContReadChannel error!");
#endif
                Marshal.FreeHGlobal(bufferD);
                return null;
            }
            do
            {
                ret = D2KDASK.D2K_AI_AsyncCheck((ushort)cardNumber, out stopped, out access_cnt);
                if (ret < 0)
                {
#if DEBUG
                    Console.WriteLine("D2K_AI_AsyncCheck error!");
#endif
                    Marshal.FreeHGlobal(bufferD);
                    return null;
                }
            } while (stopped == 0);


            ret = D2KDASK.D2K_AI_AsyncClear((ushort)cardNumber, out start_pos, out access_cnt);
            if (ret < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_AI_AsyncClear error!");
#endif
                Marshal.FreeHGlobal(bufferD);
                return null;
            }
            ret = D2KDASK.D2K_AI_ContVScale((ushort)cardNumber, (ushort)deviceVoltage, bufferD, bufferV, arraySize);
            if (ret < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_AI_ContVScale error!");
#endif
                return null;
            }
            //ShowData(0, 1000);
            Marshal.FreeHGlobal(bufferD);
            return bufferV;
        }

        #endregion


        #region Private Methods      

        private void DataReady()
        {
            short ret;
            uint start_pos;
            uint access_cnt;

            ret = D2KDASK.D2K_AI_AsyncClear((ushort)cardNumber, out start_pos, out access_cnt);
            if (ret < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_AI_AsyncClear error!");
#endif
                return;
            }
            ret = D2KDASK.D2K_AI_ContVScale((ushort)cardNumber, (ushort)deviceVoltage, m_data_buffer, m_voltage_array, arraySize);
            if (ret < 0)
            {
#if DEBUG
                Console.WriteLine("D2K_AI_ContVScale error!");
#endif
                return;
            }

            if (onDataReady != null)
                onDataReady(this, m_voltage_array);
        }







        private void OutputsChanged(int pins)
        {
            if (onOutputsChanged != null)
            {
                onOutputsChanged(this, pins);
            }
        }

        private void InputsChanged(int pins)
        {
            if (onInputsChanged != null)
            {
                onInputsChanged(this, pins);
            }
        }


        #endregion


        #region Events

        /// <summary>
        /// Occurs on output port/pin value change. 
        /// Returns output port pins[bytes] combination</param>
        /// </summary>
        public override event GpioOutputsEventHandler onOutputsChanged;

        /// <summary>
        /// Occurs on input port/pin value change. 
        /// Returns input port pins[bytes] combination
        /// </summary>     
        public override event GpioInputsEventHandler onInputsChanged;

        public override event DataReadyEventHandler onDataReady;

        #endregion
    }
}
