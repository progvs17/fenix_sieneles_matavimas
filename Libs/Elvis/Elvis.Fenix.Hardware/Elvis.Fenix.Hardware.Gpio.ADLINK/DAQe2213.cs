﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;
//using System.Runtime.InteropServices;
using System.ComponentModel;

using Elvis.Fenix.Hardware;

namespace Elvis.Fenix.Hardware.Gpio
{
    public class DAQe2213 : FxGpio
    {

        #region Constructors

        public DAQe2213()
        {

        }

        #endregion


        #region Public Prototypes

        /// <summary>
        /// Maximum available port's pins amount
        /// </summary>
        [Description("Maximum amount of pins available"), ReadOnly(true)]
        public override int MaxPinNumber { get; set; }

        /// <summary>
        /// Device ID depending on code
        /// </summary>
        [Description("Device ID"), ReadOnly(true)]
        public override string DeviceId { get; set; }

        /// <summary>
        /// Device serial number
        /// </summary>
        [Description("Device serial number"), ReadOnly(true)]
        public override string DeviceSerial { get; set; }

        /// <summary>
        /// Device's vendor id number
        /// </summary>
        [Description("Device's vendor ID number"), ReadOnly(true)]
        public override string Vendor { get; set; }

        [Description("Device's status - open/closed"), ReadOnly(true)]
        public override bool IsOpen { get; set; }

        #endregion

        //public virtual bool EnableDI { set; get; }
        //public virtual bool EnableDO { set; get; }

        #region Public Methods

        public override void Open() { throw new NotImplementedException(); }

        public override void Close() { throw new NotImplementedException(); }

        public override void TogglePin(int pin) { throw new NotImplementedException(); }

        public override void SetPin(int pin, int value) { throw new NotImplementedException(); }

        public override void SetPin(int port, int pin, int value) { throw new NotImplementedException(); }

        public override void SetPort(int port, int value) { throw new NotImplementedException(); }

        public override int GetPin(int port, int pin) { throw new NotImplementedException(); }

        public override int GetPort(int port) { throw new NotImplementedException(); }

        public override int GetDOPin(int port) { throw new NotImplementedException(); }

        #endregion


        #region Private Methods

        private void OutputsChanged(int pins)
        {
            if(onOutputsChanged != null)
            {
                onOutputsChanged(this, pins);
            }
        }

        private void InputsChanged(int pins)
        {
            if (onInputsChanged != null)
            {
                onInputsChanged(this, pins);
            }
        }

        #endregion


        #region Events

        /// <summary>
        /// Occurs on output port/pin value change. 
        /// Returns output port pins[bytes] combination</param>
        /// </summary>
        public override event GpioOutputsEventHandler onOutputsChanged;

        /// <summary>
        /// Occurs on input port/pin value change. 
        /// Returns input port pins[bytes] combination
        /// </summary>     
        public override event GpioInputsEventHandler onInputsChanged;

        public override event DataReadyEventHandler onDataReady;


        #endregion
    }
}
