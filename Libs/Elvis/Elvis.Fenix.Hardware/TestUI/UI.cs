﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Elvis.Fenix.Hardware;
using Elvis.Fenix.Hardware.Gpio.ICPDAS;

namespace WindowsFormsApplication1
{
    public partial class UI : Form
    {
        ICPDASUniDAQ _boardControl = new ICPDASUniDAQ();

        public UI()
        {
            InitializeComponent();
            _boardControl.onInputsChanged += InputChanged;
            _boardControl.onOutputsChanged += _boardControl_onOutputsChanged;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            try
            {
                _boardControl.Close();
                //Application.Exit();

                //ConfirmClose = true;
                base.OnFormClosing(e);
                if (e.CloseReason == CloseReason.WindowsShutDown)
                {
                    //Do some action
                    _boardControl.Close();
                }
            }

            catch (Exception ex)
            {

            }
        }

        void _boardControl_onOutputsChanged(object sender, int pins)
        {
            Console.WriteLine("Output pins' value: " + pins.ToString());
        }

        private void InputChanged(object sender, int pins)
        {
            //Convert.to
            Console.WriteLine("Input pins' value: " + pins.ToString());
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                //_boardControl.SetPort(0, int.Parse( (numPin.Value+1).ToString(), System.Globalization.NumberStyles.HexNumber));
                _boardControl.SetPin(0, (int)numPin.Value, 1);
            }
            else
            {
                _boardControl.SetPin(0, (int)numPin.Value, 0);
                //_boardControl.SetPort(0, 0x00);
            }


            string kazkas = _boardControl.DeviceSerial;

        }

        // ON
        private void button1_Click(object sender, EventArgs e)
        {
            _boardControl.SetPin(0, (int)numPin.Value, 1);

           // read();
        }

        // OFF
        private void button2_Click(object sender, EventArgs e)
        {
            _boardControl.SetPin(0, (int)numPin.Value, 0);

           // read();
        }

      /*  void read()
        {
            int temp = _boardControl.ReadBackDOValues(0);
            Console.WriteLine(temp.ToString());
        }*/


    }
}
