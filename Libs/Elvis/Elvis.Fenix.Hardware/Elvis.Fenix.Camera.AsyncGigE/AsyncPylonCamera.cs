﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PylonC.NET;
using System.Threading;

using Elvis.Fenix.Hardware;
using System.Xml.Serialization;

namespace Elvis.Fenix.Hardware.Camera.AsyncGigE
{
    public struct SynchContext
    {
        public int ContextId { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }


    public class AsyncPylonCamera : FxCamera, IDisposable
    {
        #region Camera variables

        ManualResetEventSlim stopGrab = new ManualResetEventSlim();
        private Thread thread;

        const uint NUM_GRABS = 20;          /* Number of images to grab. */
        const uint NUM_IMAGE_BUFFERS = 20;         /* Number of buffers used for grabbing. */
        const uint NUM_EVENT_BUFFERS = 20;   /* Number of buffers used for grabbing. */

        const uint NUM_DEVICES = 1;
        const uint NUM_BUFFERS = 20;         /* Number of buffers used for grabbing. */

        const uint GIGE_PACKET_SIZE = 1500; /* Size of one Ethernet packet. */
        const uint GIGE_PROTOCOL_OVERHEAD = 36;   /* Total number of bytes of protocol overhead. */

        string[] cameraId = new string[NUM_DEVICES];
        string[] id = new string[NUM_DEVICES];
        uint camIdx;
        uint frameNumber;

        public bool Judgement;

        uint numDevicesAvail;         /* Number of the available devices. */
        bool isAvail;                 /* Used for checking feature availability. */
        bool isReady;                 /* Used as an output parameter. */
        bool grabOnce = false;
        bool triggerStatus = false;
        bool shot1 = false;
        bool shot2 = false;
        public bool settingsChange;
        public string exposureTime1 = Convert.ToString(400);
        public string exposureTime2 = Convert.ToString(400);
        public int DOPort;
        public int BadPETPin1;
        public int BadPETPin2;
        public int ReadyPin;
        int i;                        /* Counter. */
        int deviceIndex;              /* Index of device used in the following variables. */
        PYLON_WAITOBJECTS_HANDLE wos; /* Wait objects. */
        int nGrabs;                   /* Counts the number of grab iterations. */
        PYLON_WAITOBJECT_HANDLE woTimer;/* Timer wait object. */

        //PYLON_DEVICE_HANDLE[] hDev = new PYLON_DEVICE_HANDLE[NUM_DEVICES];        /* Handles for the pylon devices. */

        ///* These are camera specific variables: */
        PYLON_EVENTGRABBER_HANDLE[] hEventGrabber = new PYLON_EVENTGRABBER_HANDLE[NUM_DEVICES]; /* Handle for the event grabber used for receiving events. */
        PYLON_WAITOBJECT_HANDLE[] hWaitEvent = new PYLON_WAITOBJECT_HANDLE[NUM_DEVICES]; /* Handle used for waiting for an event message. */
        PYLON_EVENTADAPTER_HANDLE[] hEventAdapter = new PYLON_EVENTADAPTER_HANDLE[NUM_DEVICES]; /* Handle for the event adapter used for dispatching events. */
        PYLON_WAITOBJECT_HANDLE[] hWaitStream = new PYLON_WAITOBJECT_HANDLE[NUM_DEVICES]; /* Handle used for waiting for a grab to be finished. */
        NODEMAP_HANDLE[] hNodeMap = new NODEMAP_HANDLE[NUM_DEVICES]; /* Handle for the node map containing the camera parameters. */
        NODE_HANDLE[] hNode = new NODE_HANDLE[NUM_DEVICES]; /* Handle for a camera parameter. */
        NodeCallbackHandler callbackHandler = new NodeCallbackHandler(); /* Handles incoming callbacks. */
        NODE_CALLBACK_HANDLE[] hCallback = new NODE_CALLBACK_HANDLE[NUM_DEVICES]; /* Used for deregistering a callback function. */
        PYLON_STREAMGRABBER_HANDLE[] hGrabber = new PYLON_STREAMGRABBER_HANDLE[NUM_DEVICES]; /* Handle for the pylon stream grabber. */
        PYLON_WAITOBJECT_HANDLE[] hWait = new PYLON_WAITOBJECT_HANDLE[NUM_DEVICES];       /* Handle used for waiting for a grab to be finished. */
        uint[] payloadSize = new uint[NUM_DEVICES];                    /* Size of an image frame in bytes. */
        PylonGrabResult_t[] grabResult = new PylonGrabResult_t[NUM_DEVICES];        /* Stores the result of a grab operation. */
        uint[] nStreams = new uint[NUM_DEVICES];                       /* The number of streams provided by the device. */
        Dictionary<PYLON_STREAMBUFFER_HANDLE, PylonBuffer<Byte>>[] buffers = new Dictionary<PYLON_STREAMBUFFER_HANDLE, PylonBuffer<Byte>>[NUM_DEVICES]; /* Holds handles and buffers used for grabbing. */


        ///* These are camera specific variables: */
        //PYLON_EVENTGRABBER_HANDLE hEventGrabber = new PYLON_EVENTGRABBER_HANDLE(); /* Handle for the event grabber used for receiving events. */
        //PYLON_WAITOBJECT_HANDLE hWaitEvent = new PYLON_WAITOBJECT_HANDLE(); /* Handle used for waiting for an event message. */
        //PYLON_EVENTADAPTER_HANDLE hEventAdapter = new PYLON_EVENTADAPTER_HANDLE(); /* Handle for the event adapter used for dispatching events. */
        //PYLON_WAITOBJECT_HANDLE hWaitStream = new PYLON_WAITOBJECT_HANDLE(); /* Handle used for waiting for a grab to be finished. */
        //NODEMAP_HANDLE hNodeMap = new NODEMAP_HANDLE(); /* Handle for the node map containing the camera parameters. */
        //NODE_HANDLE hNode = new NODE_HANDLE(); /* Handle for a camera parameter. */
        //NodeCallbackHandler callbackHandler = new NodeCallbackHandler(); /* Handles incoming callbacks. */
        //NODE_CALLBACK_HANDLE hCallback = new NODE_CALLBACK_HANDLE(); /* Used for deregistering a callback function. */
        //PYLON_STREAMGRABBER_HANDLE hGrabber = new PYLON_STREAMGRABBER_HANDLE(); /* Handle for the pylon stream grabber. */
        //PYLON_WAITOBJECT_HANDLE hWait = new PYLON_WAITOBJECT_HANDLE();       /* Handle used for waiting for a grab to be finished. */
        //uint payloadSize = new uint();                    /* Size of an image frame in bytes. */
        //PylonGrabResult_t grabResult = new PylonGrabResult_t();        /* Stores the result of a grab operation. */
        //uint nStreams = new uint();                       /* The number of streams provided by the device. */
        //Dictionary<PYLON_STREAMBUFFER_HANDLE, PylonBuffer<Byte>> buffers = new Dictionary<PYLON_STREAMBUFFER_HANDLE, PylonBuffer<Byte>>(); /* Holds handles and buffers used for grabbing. */


        uint woIndex;


        string serialNumber = "Unknown";
        string friendlyName = "Unknown";
        bool frameReceived = false;
        /// <summary>
		/// Camera object handle.
		/// </summary>
		private PYLON_DEVICE_HANDLE _camera;

        /// <summary>
        /// Mutex used to synchronize buffer accesses between threads.
        /// </summary>
        private Mutex _mutex = new Mutex();

        private const int TotalBuffers = 5;

        #endregion



        /// <summary>
        /// Buffers to hold image data.
        /// </summary>
        private PylonBuffer<byte>[] _buffers;
        private Queue<SynchContext> _resultsContexts = new Queue<SynchContext>();
        private Queue<FxFrame> _frameQueue = new Queue<FxFrame>();

        /// <summary>
        /// Index queue of buffers ready to be enqueued.
        /// </summary>
        private Queue<int> _ready;
        bool grabbing;
        bool connected;
        private FxFrame lastFrame = new FxFrame();


        #region Constructors

        public AsyncPylonCamera() { }

        public AsyncPylonCamera(string name, bool triggerOn)
        {
            Name = name;
            triggerStatus = triggerOn;

            ClearError(ErrorFlags.ALL);
        }

        #endregion


        #region Destructors

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                try
                {
                    StopGrab();
                    Disconnect();
                }
                catch (Exception)
                {
                    System.Diagnostics.Debug.WriteLine(GenApi.GetLastErrorMessage());
                }

            }
        }

        #endregion
    

        #region Public Types

        public enum SelectBy
        {
            SerialNumber,
            UserDefinedId,
        }

        public enum AcquisitionModeType
        {
            SingleFrame,
            Continuous,
        }

        public enum TriggerSelector
        {
            AcquisitionStart,
            FrameStart,
            LineStart,
        }

        public enum TriggerSourceType
        {
            Unknown = -1,
            Software,
            Line1,
            Line2,
            Line3,
        }

        public enum TriggerModeType
        {
            Off,
            On,
        }

        public enum ConfigurationSet
        {
            Default,
            UserSet1,
            UserSet2,
            UserSet3,
        }

        public enum LineSource
        {
            ExposureActive,
            FrameTriggerWait,
            TimerActive,
            UserOutput,
            TriggerReady,
            AcquisitionTriggerWait,
            AcquisitionTriggerReady,
            FlashWindow
        }

        #endregion


        #region Public Properties

        public bool TriggerOn 
        {
            get
            {
                return triggerStatus;
            }
            set
            {

                /* Disable frame start trigger if available */
                isAvail = Pylon.DeviceFeatureIsAvailable(_camera, "EnumEntry_TriggerSelector_FrameStart");

                if (isAvail)
                {
                    Pylon.DeviceFeatureFromString(_camera, "TriggerSelector", "FrameStart");

                    if (value == true)
                    {
                        Pylon.DeviceFeatureFromString(_camera, "TriggerMode", "On");
                    }
                    else
                    {
                        Pylon.DeviceFeatureFromString(_camera, "TriggerMode", "Off");
                    }

                    triggerStatus = value;
                }

            }
        }

        public override string Name { get; set; }

        public override int Width
        {
            get { return GetIntegerFeature("Width"); }
            set { SetIntegerFeature("Width", value); }
        }

        public override int Height 
        {
            get { return GetIntegerFeature("Height"); }
            set { SetIntegerFeature("Height", value); }
        }

        public override int Left { get; set; }
        public override int Top { get; set; }

        int exp = 400;
        public override int Exposure 
        {
            get { return GetIntegerFeature("ExposureTimeRaw"); }
            set { SetIntegerFeatureInc("ExposureTimeRaw", value); }
            //get
            //{
            //    return (int)Pylon.DeviceGetIntegerFeature(_camera, "ExposureTimeRaw");
            //}

            //set 
            //{
            //    Pylon.DeviceSetIntegerFeature(_camera, "ExposureTimeRaw", value);
            //}  
        }

        public override bool GlobalResetReleaseMode { set; get; }

        public override bool IsConnected 
        { 
            get
            {
                return connected;
            }
       
        }
        public override bool IsGrabbing
        {
            get
            {
                if (stopGrab.IsSet) grabbing = false;
                else grabbing = true;

                return grabbing;
            }
        }

        #endregion



        #region Public Methods

        public override void Connect()
        {

            try
            {

                System.Diagnostics.Debug.WriteLine("Looking for cameras...");

                /* Before using any pylon methods, the pylon runtime must be initialized. */
                //Pylon.Initialize();

                /* Get all attached devices. */
                uint devices = Pylon.EnumerateDevices();
                if (devices == 0)
                {
                    if (string.IsNullOrEmpty(Name))
                        throw new Exception("No camera was found.");
                }


                /* Look for the requested camera. */
                uint device = devices;
                if (Name != null && Name.Length > 0)
                {
                    for (uint i = 0; i < devices; ++i)
                    {
                        PYLON_DEVICE_INFO_HANDLE info =
                            Pylon.GetDeviceInfoHandle(i);

                        string id = "";

                        id = Pylon.DeviceInfoGetPropertyValueByName(
                                    info, Pylon.cPylonDeviceInfoUserDefinedNameKey);

                        

                        if (Name.Equals(id))
                        {
                            device = i;
                            serialNumber = Pylon.DeviceInfoGetPropertyValueByName(
                                info, Pylon.cPylonDeviceInfoSerialNumberKey);
                            friendlyName = Pylon.DeviceInfoGetPropertyValueByName(
                                info, Pylon.cPylonDeviceInfoFriendlyNameKey);
                            System.Diagnostics.Debug.WriteLine(friendlyName +
                                                               " found.");
                            break;
                        }
                    }
                }
                else
                {
                    /* Select the first available device. */
                    device = 0;

                    PYLON_DEVICE_INFO_HANDLE info =
                            Pylon.GetDeviceInfoHandle(device);
                    serialNumber = Pylon.DeviceInfoGetPropertyValueByName(
                        info, Pylon.cPylonDeviceInfoSerialNumberKey);
                    friendlyName = Pylon.DeviceInfoGetPropertyValueByName(
                        info, Pylon.cPylonDeviceInfoFriendlyNameKey);
                    System.Diagnostics.Debug.WriteLine(friendlyName + " found.");
                }

                if (device == devices)
                {
                    /* No suitable camera was found. */
                    throw new Exception("No suitable camera was found.");
                }

                _camera = Pylon.CreateDeviceByIndex(device);
                try
                {
                    Pylon.DeviceOpen(_camera, Pylon.cPylonAccessModeControl |
                                     Pylon.cPylonAccessModeStream);
                }
                catch (Exception)
                {
                    Pylon.DestroyDevice(_camera);
                    throw;
                }



                /* Create wait objects. This must be done outside of the loop. */
                wos = Pylon.WaitObjectsCreate();

                /* In this sample, we want to grab for a given amount of time, then stop.
                Create a timer that tiggers an AutoResetEvent, wrap the AutoResetEvent in a pylon C.NET wait object, and add it to
                the wait object set. */
                AutoResetEvent timoutEvent = new AutoResetEvent(false); /* The timeout event to wait for. */
                TimerCallbackWrapper timerCallbackWrapper = new TimerCallbackWrapper(timoutEvent); /* Receives the timer callback and sets the timeout event. */
                System.Threading.Timer timer = new System.Threading.Timer(timerCallbackWrapper.TimerCallback); /* The timeout timer. */

                woTimer = Pylon.WaitObjectFromW32(timoutEvent.SafeWaitHandle, true);

                Pylon.WaitObjectsAdd(wos, woTimer);


                /* Open cameras and set parameters. */
                //for (deviceIndex = 0; deviceIndex < numDevicesAvail; ++deviceIndex)
                //{

                    /* Set the pixel format to Mono8, where gray values will be output as 8 bit values for each pixel. */
                    /* ... Check first to see if the device supports the Mono8 format. */
                    isAvail = Pylon.DeviceFeatureIsAvailable(_camera, "EnumEntry_PixelFormat_Mono8");
                    if (!isAvail)
                    {
                        /* Feature is not available. */
                        throw new Exception("Device doesn't support the Mono8 pixel format.");
                    }

                    /* ... Set the pixel format to Mono8. */
                    Pylon.DeviceFeatureFromString(_camera, "PixelFormat", "Mono8");


                    /* Disable acquisition start trigger if available */
                    isAvail = Pylon.DeviceFeatureIsAvailable(_camera, "EnumEntry_TriggerSelector_AcquisitionStart");
                    if (isAvail)
                    {
                        Pylon.DeviceFeatureFromString(_camera, "TriggerSelector", "AcquisitionStart");
                        Pylon.DeviceFeatureFromString(_camera, "TriggerMode", "Off");
                    }

                    /* Disable frame burst start trigger if available */
                    isAvail = Pylon.DeviceFeatureIsAvailable(_camera, "EnumEntry_TriggerSelector_FrameBurstStart");
                    if (isAvail)
                    {
                        Pylon.DeviceFeatureFromString(_camera, "TriggerSelector", "FrameBurstStart");
                        Pylon.DeviceFeatureFromString(_camera, "TriggerMode", "Off");
                    }

                    /* Disable frame start trigger if available */
                    isAvail = Pylon.DeviceFeatureIsAvailable(_camera, "EnumEntry_TriggerSelector_FrameStart");
                    if (isAvail)
                    {
                        Pylon.DeviceFeatureFromString(_camera, "TriggerSelector", "FrameStart");
                        if (triggerStatus == true)
                        {
                            Pylon.DeviceFeatureFromString(_camera, "TriggerMode", "On");
                        }
                        else
                        {
                            Pylon.DeviceFeatureFromString(_camera, "TriggerMode", "Off");

                        }
                    }

                    isAvail = Pylon.DeviceFeatureIsAvailable(_camera, "EnumEntry_TriggerSource_Line1");

                    if (isAvail)
                    {
                        Pylon.DeviceFeatureFromString(_camera, "TriggerSource", "Line1");
                    }

                    isAvail = Pylon.DeviceFeatureIsAvailable(_camera, "EnumEntry_TriggerActivation_RisingEdge");

                    if (isAvail)
                    {
                        Pylon.DeviceFeatureFromString(_camera, "TriggerActivation", "RisingEdge");
                    }

                    isAvail = Pylon.DeviceFeatureIsAvailable(_camera, "EnumEntry_ExposureAuto_Off");

                    if (isAvail)
                    {
                        Pylon.DeviceFeatureFromString(_camera, "ExposureAuto", "Off");
                    }

                    isAvail = Pylon.DeviceFeatureIsAvailable(_camera, "EnumEntry_ExposureMode_Timed");

                    if (isAvail)
                    {
                        Pylon.DeviceFeatureFromString(_camera, "ExposureMode", "Timed");
                    }

                    isAvail = Pylon.DeviceFeatureIsAvailable(_camera, "EnumEntry_TriggerDelayAbs");

                    if (isAvail)
                    {
                        Pylon.DeviceFeatureFromString(_camera, "TriggerDelayAbs", "400");
                    }

                    /* We will use the Continuous frame mode, i.e., the camera delivers
                    images continuously. */
                    if (grabOnce == false)
                    {
                        Pylon.DeviceFeatureFromString(_camera, "AcquisitionMode", "Continuous");//SingleFrame
                    }
                    else
                    {
                        Pylon.DeviceFeatureFromString(_camera, "AcquisitionMode", "SingleFrame");
                    }

                    PYLON_DEVICE_INFO_HANDLE hDi = Pylon.GetDeviceInfoHandle((uint)deviceIndex);
                    string deviceClass = Pylon.DeviceInfoGetPropertyValueByName(hDi, Pylon.cPylonDeviceInfoDeviceClassKey);
                    if (deviceClass == "BaslerGigE")
                    {
                        /* For GigE cameras, we recommend increasing the packet size for better 
                           performance. When the network adapter supports jumbo frames, set the packet 
                           size to a value > 1500, e.g., to 8192. In this sample, we only set the packet size
                           to 1500.
            
                           We also set the Inter-Packet and the Frame Transmission delay
                           so the switch can line up packets better.
                        */

                        Pylon.DeviceSetIntegerFeature(_camera, "GevSCPSPacketSize", GIGE_PACKET_SIZE);
                        Pylon.DeviceSetIntegerFeature(_camera, "GevSCPD", (GIGE_PACKET_SIZE + GIGE_PROTOCOL_OVERHEAD) * (NUM_DEVICES - 1));
                        Pylon.DeviceSetIntegerFeature(_camera, "GevSCFTD", (GIGE_PACKET_SIZE + GIGE_PROTOCOL_OVERHEAD) * deviceIndex);
                    }
                    else if (deviceClass == "Basler1394")
                    {
                        /* For FireWire we just set the PacketSize node to limit the bandwidth we're using. */

                        /* We first divide the available bandwidth (4915 for FW400, 9830 for FW800)
                           by the number of devices we are using. */
                        long newPacketSize = 4915 / NUM_DEVICES;
                        long recommendedPacketSize = 0;

                        /* Get the recommended packet size from the camera. */
                        recommendedPacketSize = Pylon.DeviceGetIntegerFeature(_camera, "RecommendedPacketSize");

                        if (newPacketSize < recommendedPacketSize)
                        {
                            /* Get the increment value for the packet size.
                               We must make sure that the new value we're setting is divisible by the increment of that feature. */
                            long packetSizeInc = 0;
                            packetSizeInc = Pylon.DeviceGetIntegerFeatureInc(_camera, "PacketSize");

                            /* Adjust the new packet size so is divisible by its increment. */
                            newPacketSize -= newPacketSize % packetSizeInc;
                        }
                        else
                        {
                            /* The recommended packet size should always be valid. No need to check against the increment. */
                            newPacketSize = recommendedPacketSize;
                        }

                        /* Set the new packet size. */
                        Pylon.DeviceSetIntegerFeature(_camera, "PacketSize", newPacketSize);
                        Console.WriteLine("Using packetsize: {0}", newPacketSize);
                    }
                //}
                /* Allocate and register buffers for grab. */
                //for (deviceIndex = 0; deviceIndex < numDevicesAvail; ++deviceIndex)
                //{


                    /* Determine the required size for the grab buffer. */
                    payloadSize[0] = checked((uint)Pylon.DeviceGetIntegerFeature(_camera, "PayloadSize"));

                    //*************************************************
                    /* Enable camera events. */
                    /* ... Select the end-of-exposure event. */
                    Pylon.DeviceFeatureFromString(_camera, "EventSelector", "ExposureEnd");

                    /* ... Enable the event. */
                    Pylon.DeviceFeatureFromString(_camera, "EventNotification", "GenICamEvent");
                    //************************************************************

                    /* Image grabbing is done using a stream grabber.  
                      A device may be able to provide different streams. A separate stream grabber must 
                      be used for each stream. In this sample, we create a stream grabber for the default 
                      stream, i.e., the first stream ( index == 0 ).
                      */

                    /* Get the number of streams supported by the device and the transport layer. */
                    nStreams[0] = Pylon.DeviceGetNumStreamGrabberChannels(_camera);

                    if (nStreams[0] < 1)
                    {
                        throw new Exception("The transport layer doesn't support image streams.");
                    }

                    /* Create and open a stream grabber for the first channel. */
                    hGrabber[0] = Pylon.DeviceGetStreamGrabber(_camera, 0);

                    Pylon.StreamGrabberOpen(hGrabber[0]);


                    /* Get a handle for the stream grabber's wait object. The wait object
                       allows waiting for buffers to be filled with grabbed data. */
                    hWait[0] = Pylon.StreamGrabberGetWaitObject(hGrabber[0]);

                    /* Add the stream grabber's wait object to our wait objects.
                       This is needed to be able to wait until all cameras have 
                       grabbed an image in our grab loop below. */
                    Pylon.WaitObjectsAdd(wos, hWait[0]);

                    /* We must tell the stream grabber the number and size of the buffers 
                        we are using. */
                    /* .. We will not use more than NUM_BUFFERS for grabbing. */
                    Pylon.StreamGrabberSetMaxNumBuffer(hGrabber[0], NUM_BUFFERS);

                    /* .. We will not use buffers bigger than payloadSize bytes. */
                    Pylon.StreamGrabberSetMaxBufferSize(hGrabber[0], payloadSize[0]);

                    /*  Allocate the resources required for grabbing. After this, critical parameters 
                        that impact the payload size must not be changed until FinishGrab() is called. */
                    Pylon.StreamGrabberPrepareGrab(hGrabber[0]);

                    /* Before using the buffers for grabbing, they must be registered at
                       the stream grabber. For each registered buffer, a buffer handle
                       is returned. After registering, these handles are used instead of the
                       buffer objects pointers. The buffer objects are held in a dictionary,
                       that provides access to the buffer using a handle as key.
                     */

                    buffers[0] = new Dictionary<PYLON_STREAMBUFFER_HANDLE, PylonBuffer<Byte>>();
                    for (i = 0; i < NUM_BUFFERS; ++i)
                    {
                        PylonBuffer<Byte> buffer = new PylonBuffer<byte>(payloadSize[0], true);
                        PYLON_STREAMBUFFER_HANDLE handle = Pylon.StreamGrabberRegisterBuffer(hGrabber[0], ref buffer);
                        buffers[0].Add(handle, buffer);
                    }

                    /* Feed the buffers into the stream grabber's input queue. For each buffer, the API 
                       allows passing in an integer as additional context information. This integer
                       will be returned unchanged when the grab is finished. In our example, we use the index of the 
                       buffer as context information. */
                    i = 0;
                    foreach (KeyValuePair<PYLON_STREAMBUFFER_HANDLE, PylonBuffer<Byte>> pair in buffers[0])
                    {
                        Pylon.StreamGrabberQueueBuffer(hGrabber[0], pair.Key, i++);
                    }

                //}

                /* The stream grabber is now prepared. As soon the camera starts acquiring images,
               the image data will be grabbed into the provided buffers.  */

                //**************************************************
                //for (deviceIndex = 0; deviceIndex < numDevicesAvail; ++deviceIndex)
                //{


                    /* Create and prepare an event grabber. */
                    /* ... Get a handle for the event grabber. */
                    hEventGrabber[0] = Pylon.DeviceGetEventGrabber(_camera);

                    if (!hEventGrabber[0].IsValid)
                    {
                        /* The transport layer doesn't support event grabbers. */
                        throw new Exception("No event grabber supported.");
                    }

                    /* ... Tell the grabber how many buffers to use. */
                    Pylon.EventGrabberSetNumBuffers(hEventGrabber[0], NUM_EVENT_BUFFERS);

                    /* ... Open the event grabber. */
                    Pylon.EventGrabberOpen(hEventGrabber[0]);  /* The event grabber is now ready
                                                   for receiving events. */

                    /* Retrieve the wait object that is associated with the event grabber. The event 
                        will be signalled when an event message has been received. */
                    hWaitEvent[0] = Pylon.EventGrabberGetWaitObject(hEventGrabber[0]);

                    /* For extracting the event data from an event message, an event adapter is used. */
                    hEventAdapter[0] = Pylon.DeviceCreateEventAdapter(_camera);

                    if (!hEventAdapter[0].IsValid)
                    {
                        /* The transport layer doesn't support event grabbers. */
                        throw new Exception("No event adapter supported.");
                    }

                    /* Register the callback function for the ExposureEndEventFrameID parameter. */
                    /*... Get the node map containing all parameters. */
                    hNodeMap[0] = Pylon.DeviceGetNodeMap(_camera);

                    /* ... Get the ExposureEndEventFrameID parameter. */
                    hNode[0] = GenApi.NodeMapGetNode(hNodeMap[0], "ExposureEndEventFrameID");

                    if (!hNode[0].IsValid)
                    {
                        /* There is no ExposureEndEventFrameID parameter. */
                        throw new Exception("There is no ExposureEndEventFrameID parameter!");
                    }
                    /* ... Register the callback function. */
                    callbackHandler.CallbackEvent += new NodeCallbackHandler.NodeCallback(endOfExposureCallback);
                    hCallback[0] = GenApi.NodeRegisterCallback(hNode[0], callbackHandler);

                    /* Put the wait objects into a container. */
                    /* ... Create the container. */
                    //wos = Pylon.WaitObjectsCreate();

                    /* ... Add the wait objects' handles. */
                    Pylon.WaitObjectsAdd(wos, hWaitEvent[0]);
                    Pylon.WaitObjectsAdd(wos, hWait[0]);

                    connected = true;
                //}
            }
            catch (Exception ex)
            {
                /* Retrieve the error message. */
                string msg = GenApi.GetLastErrorMessage() + "\n" + GenApi.GetLastErrorDetail();

                SetError(ErrorFlags.INIT_ERROR);

                Console.WriteLine("Exception caught: " + ex.Message.ToString());
                if (msg != "\n")
                    Console.WriteLine("Last error message:" + msg.ToString());


                //for (uint deviceIndex = 0; deviceIndex < NUM_DEVICES; ++deviceIndex)
                //{
                    try
                    {
                        if (_camera.IsValid)
                        {
                            /* ... Close and release the pylon device. */
                            if (Pylon.DeviceIsOpen(_camera))
                            {
                                Pylon.DeviceClose(_camera);
                            }
                            Pylon.DestroyDevice(_camera);
                        }
                    }
                    catch (Exception)
                    {
                        /*No further handling here.*/
                    }
                //}
                Pylon.Terminate();  /* Releases all pylon resources. */

                connected = false;
            }
 
        }

        public override void Disconnect()
        {
            /* Clean up. */
            /* Stop the image aquisition on the cameras. */
            try
            {

                /*  ... Stop the camera. */
                Pylon.DeviceExecuteCommandFeature(_camera, "AcquisitionStop");

                /* ... Switch off the events. */
                Pylon.DeviceFeatureFromString(_camera, "EventSelector", "ExposureEnd");
                Pylon.DeviceFeatureFromString(_camera, "EventNotification", "Off");


                // Remove all wait objects from WaitObjects.
                Pylon.WaitObjectsRemoveAll(wos);
                Pylon.WaitObjectDestroy(woTimer);

                Pylon.WaitObjectsDestroy(wos);

                /* ... We must issue a cancel call to ensure that all pending buffers are put into the
                   stream grabber's output queue. */
                Pylon.StreamGrabberCancelGrab(hGrabber[0]);

                /* ... The buffers can now be retrieved from the stream grabber. */
                do
                {
                    isReady = Pylon.StreamGrabberRetrieveResult(hGrabber[0], out grabResult[0]);

                } while (isReady);

                /* ... When all buffers are retrieved from the stream grabber, they can be deregistered.
                       After deregistering the buffers, it is safe to free the memory. */

                foreach (KeyValuePair<PYLON_STREAMBUFFER_HANDLE, PylonBuffer<Byte>> pair in buffers[0])
                {
                    Pylon.StreamGrabberDeregisterBuffer(hGrabber[0], pair.Key);
                    pair.Value.Dispose();
                }
                //buffers = null;

                /* ... Release grabbing related resources. */
                Pylon.StreamGrabberFinishGrab(hGrabber[0]);

                /* After calling PylonStreamGrabberFinishGrab(), parameters that impact the payload size (e.g., 
                the AOI width and height parameters) are unlocked and can be modified again. */

                /* ... Close the stream grabber. */
                Pylon.StreamGrabberClose(hGrabber[0]);

                /* ... Deregister the callback. */
                GenApi.NodeDeregisterCallback(hNode[0], hCallback[0]);

                /* ... Close the event grabber.*/
                Pylon.EventGrabberClose(hEventGrabber[0]);

                /* ... Release the event adapter. */
                Pylon.DeviceDestroyEventAdapter(_camera, hEventAdapter[0]);



                /* ... Close and release the pylon device. The stream grabber becomes invalid
                   after closing the pylon device. Don't call stream grabber related methods after 
                   closing or releasing the device. */
                Pylon.DeviceClose(_camera);
                Pylon.DestroyDevice(_camera);

            }
            catch
            {
                SetError(ErrorFlags.CONNECT_ERROR);
            }

            connected = false;
        }

        public override void StartGrab()
        {
            stopGrab.Reset();

            if (thread == null && IsConnected)
            {
                _ready = new Queue<int>();
                AllocateBuffers();
                thread = new Thread(new ThreadStart(Grab));
                thread.Start();

                grabbing = true;
#if DEBUG
                Console.WriteLine("Grab thread started");
#endif
            }
        }

        public override void StopGrab()
        {
            stopGrab.Set();

            CleanupGrab();   

            if (thread != null)
            {
                if (thread.IsAlive)
                {
                    thread.Abort();
                }
                thread = null;
                grabbing = false;

                ReleaseBuffers();
            }
        }

        public override FxFrame GrabFrame()
        {
            throw new NotImplementedException();
        }

        public override FxFrame RetrieveFrame()
        {
            if (_frameQueue.Count > 0)
            {
                FxFrame frame = new FxFrame();
                frame = _frameQueue.Dequeue();

                return frame;
                //PylonBuffer<Byte> buffer = _buffers[context.ContextId];
                //return new FxFrame(context, buffer.Pointer, context.Width, context.Height, 1);
            }
            return null;

            //if (_resultsContexts.Count > 0)
            //{
            //    SynchContext context = _resultsContexts.Dequeue();
            //    PylonBuffer<Byte> buffer = _buffers[context.ContextId];
            //    return new FxFrame(context, buffer.Pointer, context.Width, context.Height, 1);
            //}
            //return null; 
            
        }

        public override void ReleaseFrame(FxFrame frame)
        {
            if(_frameQueue.Count > 0)
            {
                _frameQueue.Clear();
            }
            //ReleaseBuffers();
            //throw new NotImplementedException();
        }

        #endregion


        #region Events

        public override event FrameReadyHandler onFrameReady;
        public event CameraConnectedHandler onCameraConnected;
        public event CameraDisconectedHandler onCameraDisconected;
        public override event GrabStartedHandler onGrabStarted;
        public event GrabStoppedHandler onGrabStopped;
        public event ErrorChangedHandler onErrorChanged;

        #endregion


        #region Private Methods

        void AllocateBuffers()
        {
            /* Determine the required data buffer size. */
            uint payloadSize = (uint)Pylon.DeviceGetIntegerFeature(
                _camera, "PayloadSize");

            /* Check the state of previously allocated buffers. */
            _mutex.WaitOne();
            try
            {
                if (_buffers != null)
                {
                    bool correct = true;
                    for (int i = 0; i < _buffers.Length; ++i)
                    {
                        if (_buffers[i] == null ||
                            _buffers[i].Array.Length != payloadSize)
                        {
                            correct = false;
                            break;
                        }
                    }

                    if (correct)
                    {
                        /*
                         * All of the previously allocated buffers are
                         * of correct size, just use them.
                         */
                        return;
                    }
                }
            }
            finally
            {
                _mutex.ReleaseMutex();
            }

            /* Release any previously allocated buffers. */
            ReleaseBuffers();

            /* Allocate image data buffers. */
            _mutex.WaitOne();

            _buffers = new PylonBuffer<Byte>[TotalBuffers];
            for (int i = 0; i < TotalBuffers; ++i)
            {
                _buffers[i] = new PylonBuffer<Byte>(payloadSize, true);
                _ready.Enqueue(i);
            }

            _mutex.ReleaseMutex();
        }

        void ReleaseBuffers()
        {
            if (_buffers == null)
                return;

            _mutex.WaitOne();

            /* Release all of the idle image data buffers. */
            while (_ready.Count > 0)
            {
                int index = _ready.Dequeue();
                PylonBuffer<Byte> buffer = _buffers[index];
                buffer.Dispose();
            }
            _buffers = null;

            while (_frameQueue.Count > 0)
            {
                FxFrame f = _frameQueue.Dequeue();
            }

            _mutex.ReleaseMutex();
        }

        private void Grab()
        {
            try
            {               

                /* Let the camera acquire images. */
                Pylon.DeviceExecuteCommandFeature(_camera, "AcquisitionStart");

                // set delay to 0
                Pylon.DeviceFeatureFromString(_camera, "TriggerSelector", "FrameStart");
                Pylon.DeviceFeatureFromString(_camera, "TriggerDelayAbs", "0.0");

                nGrabs = 0;

                while (!stopGrab.IsSet)
                {
                    //_mutex.WaitOne();

                    int bufferIndex;  /* Index of the buffer. */
                    //uint woIndex;

                    /* Wait for either an image buffer grabbed or an event received. Wait up to 20 s (1000 ms). */
                    //isReady = Pylon.WaitObjectsWaitForAny(wos, 20000, out woIndex);
                    isReady = Pylon.WaitObjectsWaitForAny(wos, System.UInt32.MaxValue, out woIndex);


                    if (!isReady)
                    {
                        /* Timeout occurred. */
                        SetError(ErrorFlags.CONNECT_ERROR);
                        throw new Exception("Timeout. Neither grabbed an image nor received an event.");
                    }

                    if (0 == woIndex)
                    {
                        SetError(ErrorFlags.CONNECT_ERROR);

                        throw new Exception("Game over.");

                    }
                    else if (woIndex > 0)
                    {
                        --woIndex;

                        /* Since the wait operation was successful, the result of at least one grab 
                           operation is available. Retrieve it. */
                        isReady = Pylon.StreamGrabberRetrieveResult(hGrabber[0], out grabResult[0]);

                        //if (!isReady)
                        //{
                        //    /* Oops. No grab result available? We should never have reached this point. 
                        //       Since the wait operation above returned without a timeout, a grab result 
                        //       should be available. */

                        //    // return;

                        //    throw new Exception("Failed to retrieve a grab result.");
                        //}

                        /* Get the buffer index from the context information. */
                        bufferIndex = grabResult[0].Context;

                        /* Check to see if the image was grabbed successfully. */
                        if (grabResult[0].Status == EPylonGrabStatus.Grabbed)
                        {
                            /*  Success. Perform image processing. Since we passed more than one buffer
                            to the stream grabber, the remaining buffers are filled in the background while
                            we do the image processing. The processed buffer won't be touched by
                            the stream grabber until we pass it back to the stream grabber. */

                            PylonBuffer<Byte> buffer;        /* Reference to the buffer attached to the grab result. */

                            /* Get the buffer from the dictionary. Since we also got the buffer index, 
                               we could alternatively use an array, e.g. buffers[bufferIndex]. */
                            if (!buffers[0].TryGetValue(grabResult[0].hBuffer, out buffer))
                            {
                                /* Oops. No buffer available? We should never have reached this point. Since all buffers are
                                   in the dictionary. */
                                throw new Exception("Failed to find the buffer associated with the handle returned in grab result.");
                            }

                            /* Perform processing. */
                            if (buffer != null)
                            {
                                FxFrame frame = new FxFrame(grabResult[0].Context, buffer.Pointer, grabResult[0].SizeX, grabResult[0].SizeY, 1);
                                _frameQueue.Enqueue(frame);

                                //SynchContext con = new SynchContext();
                                //con.ContextId = grabResult[0].Context;
                                //con.Width = grabResult[0].SizeX;
                                //con.Height = grabResult[0].SizeY;
                                
                                //_resultsContexts.Enqueue(con);


                                if (onFrameReady != null)
                                    onFrameReady(this);

                                /* Once finished with the processing, requeue the buffer to be filled again. */
                                Pylon.StreamGrabberQueueBuffer(hGrabber[0], grabResult[0].hBuffer, bufferIndex);
                            }
                        }
                        else if (grabResult[0].Status == EPylonGrabStatus.Failed)
                        {
                            SetError(ErrorFlags.GRAB_ERROR);

                            Console.WriteLine("Frame " + nGrabs.ToString() + " wasn't grabbed successfully.  Error code = " + grabResult[0].ErrorCode.ToString());
                        }

                        nGrabs++;
                    }
                
                    //_mutex.ReleaseMutex();
                }

            }

            catch (Exception ex)
            {
                /* Retrieve the error message. */
                string msg = GenApi.GetLastErrorMessage() + "\n" + GenApi.GetLastErrorDetail();

                // SICIA KAZKA DARYTI - ERROR PASTOVIAI SUSTABDZIUS
                SetError(ErrorFlags.GRAB_ERROR);

                Console.WriteLine("Exception caught: " + ex.Message.ToString());

                if (msg != "\n")
                    Console.WriteLine("Last error message:" + msg.ToString());

                try
                {
                    if (_camera.IsValid)
                    {
                        /* ... Close and release the pylon device. */
                        if (Pylon.DeviceIsOpen(_camera))
                        {
                            Pylon.DeviceClose(_camera);
                        }
                        Pylon.DestroyDevice(_camera);
                    }
                }
                catch (Exception)
                {
                    /*No further handling here.*/
                }

                //Pylon.Terminate();  /* Releases all pylon resources. */
            }
        }


        void CleanupGrab()
        {
            /* Stop the camera acquisition. */
            try
            {
                Pylon.DeviceExecuteCommandFeature(_camera, "AcquisitionStop");

                ///*
                // * Cancel grab operation ensuring that all pending buffers are put
                // * into the stream grabber's output queue.
                // */
                //Pylon.StreamGrabberCancelGrab(hGrabber[0]);

                ///* Shut down a stream grabber. */
                //Pylon.StreamGrabberFinishGrab(hGrabber[0]);
            }
            catch (Exception)
            {
                /*
                 * Command execution will fail if camera has been disconnected
                 * while grabbing images, thus this guard is needed.
                 */
                System.Diagnostics.Debug.WriteLine(
                    "CleanupGrab(): " + GenApi.GetLastErrorMessage());
            }

            

            /* Retrieve buffers from the stream grabber's output queue. */
            //_mutex.WaitOne();

            //PylonGrabResult_t result;
            //while (Pylon.StreamGrabberRetrieveResult(hGrabber[0], out result))
            //    _ready.Enqueue(result.Context);

            //_mutex.ReleaseMutex();

            ///* Detach image data buffers. */
            //for (int i = 0; i < TotalBuffers; ++i)
            //    Pylon.StreamGrabberDeregisterBuffer(hGrabber[0], _handles[i]);
            //_handles = null;


            
        }


        /* Adapts the timer callback to setting the timeout event that stops the grabbing.*/
        public class TimerCallbackWrapper
        {
            public TimerCallbackWrapper(AutoResetEvent triggeredTimeoutEvent)
            {
                timeoutEvent = triggeredTimeoutEvent;
            }

            public void TimerCallback(Object state)
            {
                timeoutEvent.Set();
            }

            private AutoResetEvent timeoutEvent;
        }

        /* Callback will be fired when an event message contains an end-of-exposure event. */
        static void endOfExposureCallback(NODE_HANDLE hNode)
        {
            try
            {
                long frame;
                frame = GenApi.IntegerGetValue(hNode);
                Console.WriteLine("Got end-of-exposure event. Frame number: {0}.", frame);
            }
            catch (Exception e)
            {
                string msg = GenApi.GetLastErrorMessage() + "\n" + GenApi.GetLastErrorDetail();
                Console.Error.WriteLine("Exception caught:");
                Console.Error.WriteLine(e.Message);
                if (msg != "\n")
                {
                    Console.Error.WriteLine("Last error message:");
                    Console.Error.WriteLine(msg);
                }
            }
        }

        #endregion


        #region Private Helpers

        bool IsFeatureAvailable(string name)
        {
            if (!IsConnected)
                throw new Exception("Not connected to the camera.");

            bool available;
            try
            {
                available = Pylon.DeviceFeatureIsAvailable(_camera, name);
            }
            catch (Exception)
            {
                throw new Exception("Could not get " + name + " value: " +
                                    GenApi.GetLastErrorMessage());
            }
            return available;
        }

        int GetIntegerFeature(string name)
        {
            if (!IsConnected)
                throw new Exception("Not connected to the camera.");

            int value;
            try
            {
                value = (int)Pylon.DeviceGetIntegerFeature(_camera, name);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not get " + name + " value: " +
                                    GenApi.GetLastErrorMessage());
            }
            return value;
        }   // CIA stringa

        int GetIntegerFeatureMin(string name)
        {
            if (!IsConnected)
                throw new Exception("Not connected to the camera.");

            int value;
            try
            {
                value = (int)Pylon.DeviceGetIntegerFeatureMin(_camera, name);
            }
            catch (Exception)
            {
                throw new Exception("Could not get " + name + " value: " +
                                    GenApi.GetLastErrorMessage());
            }
            return value;
        }

        int GetIntegerFeatureMax(string name)
        {
            if (!IsConnected)
                throw new Exception("Not connected to the camera.");

            int value;
            try
            {
                value = (int)Pylon.DeviceGetIntegerFeatureMax(_camera, name);
            }
            catch (Exception)
            {
                throw new Exception("Could not get " + name + " value: " +
                                    GenApi.GetLastErrorMessage());
            }
            return value;
        }

        int GetIntegerFeatureInc(string name)
        {
            if (!IsConnected)
                throw new Exception("Not connected to the camera.");

            int value;
            try
            {
                value = (int)Pylon.DeviceGetIntegerFeatureInc(_camera, name);
            }
            catch (Exception)
            {
                throw new Exception("Could not get " + name + " value: " +
                                    GenApi.GetLastErrorMessage());
            }
            return value;
        }

        void SetIntegerFeature(string name, int value)
        {
            if (!IsConnected)
                throw new Exception("Not connected to the camera.");

            try
            {
                Pylon.DeviceSetIntegerFeature(_camera, name, value);
            }
            catch (Exception)
            {
                throw new Exception("Could not set " + name + " value: " +
                                    GenApi.GetLastErrorMessage());
            }
        }


        bool GetBooleanFeature(string name)
        {
            if (!IsConnected)
                throw new Exception("Not connected to the camera.");

            bool value;
            try
            {
                value = (bool)Pylon.DeviceGetBooleanFeature(_camera, name);
            }
            catch (Exception)
            {
                throw new Exception("Could not get " + name + " value: " +
                                    GenApi.GetLastErrorMessage());
            }
            return value;
        }

        void SetBooleanFeature(string name, bool value)
        {
            if (!IsConnected)
                throw new Exception("Not connected to the camera.");

            try
            {
                Pylon.DeviceSetBooleanFeature(_camera, name, value);
            }
            catch (Exception)
            {
                throw new Exception("Could not set " + name + " value: " +
                                    GenApi.GetLastErrorMessage());
            }
        }

        void SetIntegerFeatureInc(string name, int value)
        {
            if (!IsConnected)
                throw new Exception("Not connected to the camera.");

            try
            {
                value -= value %
                    (int)Pylon.DeviceGetIntegerFeatureInc(_camera, name);
                Pylon.DeviceSetIntegerFeature(_camera, name, value);
            }
            catch (Exception)
            {
                throw new Exception("Could not set " + name + " value: " +
                                    GenApi.GetLastErrorMessage());
            }
        }

        string GetStringFeature(string name)
        {
            if (!IsConnected)
                throw new Exception("Not connected to the camera.");

            string value;
            try
            {
                value = Pylon.DeviceFeatureToString(_camera, name);
            }
            catch (Exception)
            {
                throw new Exception("Could not get " + name + " value: " +
                                    GenApi.GetLastErrorMessage());
            }
            return value;
        }

        void SetStringFeature(string name, string value)
        {
            if (!IsConnected)
                throw new Exception("Not connected to the camera.");

            try
            {
                Pylon.DeviceFeatureFromString(_camera, name, value);
            }
            catch (Exception)
            {
                throw new Exception("Could not set " + name + " value: " +
                                    GenApi.GetLastErrorMessage());
                //Error occurs than camera is disconnected
            }
        }

        void SelectTrigger(TriggerSelector selector)
        {
            if (!IsConnected)
                throw new Exception("Not connected to the camera.");

            try
            {
                switch (selector)
                {
                    case TriggerSelector.AcquisitionStart:
                        Pylon.DeviceFeatureFromString(_camera, "TriggerSelector",
                                                      "AcquisitionStart");
                        break;
                    case TriggerSelector.FrameStart:
                        Pylon.DeviceFeatureFromString(_camera, "TriggerSelector",
                                                      "FrameStart");
                        break;
                    case TriggerSelector.LineStart:
                        Pylon.DeviceFeatureFromString(_camera, "TriggerSelector",
                                                      "LineStart");
                        break;
                }
            }
            catch (Exception)
            {
                throw new Exception("Could not set TriggerSelector value: " +
                                    GenApi.GetLastErrorMessage());
            }
        }

        #endregion

    }
}

//        /// <summary>
//        /// Line is inverted or not
//        /// </summary>
//        private bool _lineInverter;

//        /// <summary>
//        /// Line source option
//        /// </summary>
//        private LineSource _lineSource;

//        /// <summary>
//        /// Synchronization context used to dispatch asynchronous methods.
//        /// </summary>
//        private SynchronizationContext _context;

//        /// <summary>
//        /// Grab mode
//        /// </summary>
//        private GrabMode _mode;

//        /// <summary>
//        /// Parameter by which to look for the requested camera.
//        /// </summary>
//        private SelectBy _selectBy;

//        /// <summary>
//        /// Id of the requested camera.
//        /// </summary>
//        private String _id;

//        /// <summary>
//        /// Flag indicating camera connection status.
//        /// </summary>
//        private bool _connected;

//        /// <summary>
//        /// Serial number of a camera.
//        /// </summary>
//        private String _serialNumber;

//        /// <summary>
//        /// Friendly name of a camera.
//        /// </summary>
//        private String _friendlyName;

//        /// <summary>
//        /// Camera object handle.
//        /// </summary>
//        private PYLON_DEVICE_HANDLE _camera;

//        /// <summary>
//        /// Stream grabber object handle.
//        /// </summary>
//        private PYLON_STREAMGRABBER_HANDLE _grabber;

//        /// <summary>
//        /// Frame arrival wait object handle.
//        /// </summary>
//        private PYLON_WAITOBJECT_HANDLE _frameEvent;

//        /// <summary>
//        /// Buffer queue change wait object handle.
//        /// </summary>
//        private PYLON_WAITOBJECT_HANDLE _enqueueEvent;

//        /// <summary>
//        /// Thread termination wait object handle.
//        /// </summary>
//        private PYLON_WAITOBJECT_HANDLE _terminateEvent;

//        /// <summary>
//        /// Buffers to hold image data.
//        /// </summary>
//        private PylonBuffer<byte>[] _buffers;

//        private Queue<SynchContext> _resultsContexts = new Queue<SynchContext>();


//        /// <summary>
//        /// Handles to stream grabber's registered buffers.
//        /// </summary>
//        private PYLON_STREAMBUFFER_HANDLE[] _handles;

//        /// <summary>
//        /// Index queue of buffers ready to be enqueued.
//        /// </summary>
//        private Queue<int> _ready;

//        /// <summary>
//        /// Mutex used to synchronize buffer accesses between threads.
//        /// </summary>
//        private Mutex _mutex;

//        /// <summary>
//        /// Thread used to grab frames from a camera.
//        /// </summary>
//        private Thread _thread;

//        /// <summary>
//        /// Event used to notify that grabbing has started.
//        /// </summary>
//        private AutoResetEvent _barrier;

//        /// <summary>
//        /// Callback handler for device removal event.
//        /// </summary>
//        private DeviceCallbackHandler _handler;

//        /// <summary>
//        /// Registered device callback object handle.
//        /// </summary>
//        private PYLON_DEVICECALLBACK_HANDLE _callback;

//        private ManualResetEventSlim _stopGrab = new ManualResetEventSlim(false);

//        #endregion

//        #region Public Properties

//        public override int Left { get; set; }
//        public override int Top { get; set; }

//        public override string Name
//        {
//            get
//            {
//                return _id;
//            }
//            set
//            {
//                if (value != String.Empty)
//                {
//                    _id = value;
//                }
//                else
//                {
//                    _id = "Unknown";
//                }
//            }
//        }

//        public override bool IsConnected
//        {
//            get { return _connected; }
//        }


//        public override bool IsGrabbing
//        {
//            get { return IsRealyGrabbing; }
//        }



//        public String Id
//        {
//            get { return _id; }
//        }

//        public String SerialNumber
//        {
//            get { return _serialNumber; }
//        }

//        public String FriendlyName
//        {
//            get { return _friendlyName; }
//        }

//        public bool LeftOffsetAvailable
//        {
//            get { return IsFeatureAvailable("OffsetX"); }
//        }

//        public bool TopOffsetAvailable
//        {
//            get { return IsFeatureAvailable("OffsetY"); }
//        }

//        public bool WidthAvailable
//        {
//            get { return IsFeatureAvailable("Width"); }
//        }

//        public bool HeightAvailable
//        {
//            get { return IsFeatureAvailable("Height"); }
//        }

//        public bool BinningHorizontalAvailable
//        {
//            get { return IsFeatureAvailable("BinningHorizontal"); }
//        }

//        public bool BinningVerticalAvailable
//        {
//            get { return IsFeatureAvailable("BinningVertical"); }
//        }

//        public bool BlackLevelAvailable
//        {
//            get { return IsFeatureAvailable("BlackLevelRaw"); }
//        }

//        public bool ExposureAvailable
//        {
//            get { return IsFeatureAvailable("ExposureTimeRaw"); }
//        }

//        public int LeftOffsetMin
//        {
//            get { return GetIntegerFeatureMin("OffsetX"); }
//        }

//        public int TopOffsetMin
//        {
//            get { return GetIntegerFeatureMin("OffsetY"); }
//        }

//        public int WidthMin
//        {
//            get { return GetIntegerFeatureMin("Width"); }
//        }

//        public int HeightMin
//        {
//            get { return GetIntegerFeatureMin("Height"); }
//        }

//        public int BinningHorizontalMin
//        {
//            get { return GetIntegerFeatureMin("BinningHorizontal"); }
//        }

//        public int BinningVerticalMin
//        {
//            get { return GetIntegerFeatureMin("BinningVertical"); }
//        }

//        public int BlackLevelMin
//        {
//            get { return GetIntegerFeatureMin("BlackLevelRaw"); }
//        }

//        public int ExposureMin
//        {
//            get { return GetIntegerFeatureMin("ExposureTimeRaw"); }
//        }

//        public int LeftOffsetMax
//        {
//            get { return GetIntegerFeatureMax("OffsetX"); }
//        }

//        public int TopOffsetMax
//        {
//            get { return GetIntegerFeatureMax("OffsetY"); }
//        }

//        public int WidthMax
//        {
//            get { return GetIntegerFeatureMax("Width"); }
//        }

//        public int HeightMax
//        {
//            get { return GetIntegerFeatureMax("Height"); }
//        }

//        public int BinningHorizontalMax
//        {
//            get { return GetIntegerFeatureMax("BinningHorizontal"); }
//        }

//        public int BinningVerticalMax
//        {
//            get { return GetIntegerFeatureMax("BinningVertical"); }
//        }

//        public int BlackLevelMax
//        {
//            get { return GetIntegerFeatureMax("BlackLevelRaw"); }
//        }

//        public int ExposureMax
//        {
//            get { return GetIntegerFeatureMax("ExposureTimeRaw"); }
//        }

//        public int LeftOffsetInc
//        {
//            get { return GetIntegerFeatureInc("OffsetX"); }
//        }

//        public int TopOffsetInc
//        {
//            get { return GetIntegerFeatureInc("OffsetY"); }
//        }

//        public int WidthInc
//        {
//            get { return GetIntegerFeatureInc("Width"); }
//        }

//        public int HeightInc
//        {
//            get { return GetIntegerFeatureInc("Height"); }
//        }

//        public int BinningHorizontalInc
//        {
//            get { return GetIntegerFeatureInc("BinningHorizontal"); }
//        }

//        public int BinningVerticalInc
//        {
//            get { return GetIntegerFeatureInc("BinningVertical"); }
//        }

//        public int BlackLevelInc
//        {
//            get { return GetIntegerFeatureInc("BlackLevelRaw"); }
//        }

//        public int ExposureInc
//        {
//            get { return GetIntegerFeatureInc("ExposureTimeRaw"); }
//        }

//        public int LeftOffset
//        {
//            get { return GetIntegerFeature("OffsetX"); }
//            set { SetIntegerFeature("OffsetX", value); }
//        }

//        public int TopOffset
//        {
//            get { return GetIntegerFeature("OffsetY"); }
//            set { SetIntegerFeature("OffsetY", value); }
//        }

//        public override int Width
//        {
//            get { return GetIntegerFeature("Width"); }
//            set { SetIntegerFeature("Width", value); }
//        }

//        public override int Height
//        {
//            get { return GetIntegerFeature("Height"); }
//            set { SetIntegerFeature("Height", value); }
//        }

//        public int BinningHorizontal
//        {
//            get { return GetIntegerFeature("BinningHorizontal"); }
//            set { SetIntegerFeature("BinningHorizontal", value); }
//        }

//        public int BinningVertical
//        {
//            get { return GetIntegerFeature("BinningVertical"); }
//            set { SetIntegerFeature("BinningVertical", value); }
//        }

//        public int BlackLevel
//        {
//            get { return GetIntegerFeature("BlackLevelRaw"); }
//            set { SetIntegerFeature("BlackLevelRaw", value); }
//        }

//       /* public override int Exposure
//        {
//            get { return GetIntegerFeature("ExposureOverlapTimeMaxRaw"); }
//            set { SetIntegerFeatureInc("ExposureOverlapTimeMaxRaw", value); }
//        }*/

//        public override int Exposure
//        {
//            get { return GetIntegerFeature("ExposureTimeRaw"); }
//            set { SetIntegerFeatureInc("ExposureTimeRaw", value); }
//        }

//        public override bool GlobalResetReleaseMode
//        {
//            get { return GetBooleanFeature("GlobalResetReleaseModeEnable"); }
//            set { SetBooleanFeature("GlobalResetReleaseModeEnable", value); }
//        }

//        public bool LineInverter
//        {
//            get { return GetBooleanFeature("LineInverter"); }
//            set { SetBooleanFeature("LineInverter", value); }
//        }

//        public string EventSelector
//        {
//            get { return GetStringFeature("EventSelector"); }
//            set { SetStringFeature("EventSelector", value); }
//        }

//        public string EventNotification
//        {
//            get { return GetStringFeature("EventNotification"); }
//            set { SetStringFeature("EventNotification", value); }
//        }

//        [XmlIgnoreAttribute]
//        public AcquisitionModeType AcquisitionMode
//        {
//            get
//            {
//                string mode = GetStringFeature("AcquisitionMode");
//                if (mode.Equals("SingleFrame"))
//                    return AcquisitionModeType.SingleFrame;
//                else if (mode.Equals("Continuous"))
//                    return AcquisitionModeType.Continuous;
//                else
//                    throw new Exception("Unknown acquisition mode type.");
//            }
//            set
//            {
//                switch (value)
//                {
//                    case AcquisitionModeType.SingleFrame:
//                        SetStringFeature("AcquisitionMode", "SingleFrame");
//                        break;
//                    case AcquisitionModeType.Continuous:
//                        SetStringFeature("AcquisitionMode", "Continuous");
//                        break;
//                }
//            }
//        }

//        #endregion

