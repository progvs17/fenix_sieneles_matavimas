﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.ComponentModel;
using Elvis.Fenix.Hardware;
using Elvis.Fenix.Hardware.Gpio;
using UniDAQ_Ns;

namespace Elvis.Fenix.Hardware.Gpio
{
    public class ICPDASUniDAQ : FxGpio
    {

        ManualResetEventSlim _stopLoop = new ManualResetEventSlim(false);

        #region Private Members

        private Mutex _sync = new Mutex();

        private ushort _totalBoards;
        private ushort _return;
        private ushort _boardNo;
        private ushort _initialCode;
        byte[] _modelName = new byte[20];

        private byte _inOutCheck = 0;

        UniDAQ.IXUD_CARD_INFO[] sCardInfo = new UniDAQ.IXUD_CARD_INFO[32];
        UniDAQ.IXUD_DEVICE_INFO[] sDeviceInfo = new UniDAQ.IXUD_DEVICE_INFO[32];

        int _lastDOPortValue;
        byte _checkingPort;
        Thread _inputChecker = null;      

        #endregion


        #region Public Class

        /// <summary>
        /// Needed to observe input/output pins.
        /// </summary>
        [StructLayout(LayoutKind.Explicit, Size = 16)]
        public class DAQ_BITS
        {
            [FieldOffset(0)]
            public byte bit0;
            [FieldOffset(1)]
            public byte bit1;
            [FieldOffset(2)]
            public byte bit2;
            [FieldOffset(3)]
            public byte bit3;
            [FieldOffset(4)]
            public byte bit4;
            [FieldOffset(5)]
            public byte bit5;
            [FieldOffset(6)]
            public byte bit6;
            [FieldOffset(7)]
            public byte bit7;
            [FieldOffset(8)]
            public byte bit8;
            [FieldOffset(9)]
            public byte bit9;
            [FieldOffset(10)]
            public byte bit10;
            [FieldOffset(11)]
            public byte bit11;
            [FieldOffset(12)]
            public byte bit12;
            [FieldOffset(13)]
            public byte bit13;
            [FieldOffset(14)]
            public byte bit14;
            [FieldOffset(15)]
            public byte bit15;
        }

        #endregion


        #region Public Members

        /// <summary>
        /// Board output pins values
        /// </summary>
        public DAQ_BITS boardOutputBits;

        /// <summary>
        /// Board input pins values
        /// </summary>
        public DAQ_BITS boardInputBits;

        #endregion


        #region Constructors

        /// <summary>
        /// Card initialization with default values (0s).
        /// Also calls method Open() - to connect to the board.
        /// </summary>
        public ICPDASUniDAQ()
        {
            // assign wBoardNo to 0 (default)
            _boardNo = 0;

            // reset
            _totalBoards = 0;

            Open();
        }

        /// <summary>
        /// Card initialization with selectable board number and default other values (0s)
        /// Also calls method Open() - to connect to the board.
        /// </summary>
        /// <param name="boardNumber">Board number: 0 - 31</param>
        public ICPDASUniDAQ(int boardNumber)
        {
            // assign wBoardNo to 0 (default)
            _boardNo = (ushort)boardNumber;

            // reset
            _totalBoards = 0;

            Open();
        }


        #endregion


        #region Public Properties

        /// <summary>
        /// Maximum available port's pins amount
        /// </summary>
        [Description("Maximum amount of pins available"), ReadOnly(true)]
        public override int MaxPinNumber { get; set; }

        /// <summary>
        /// Device ID depending on code
        /// </summary>
        [Description("Device ID"), ReadOnly(true)]
        public override string DeviceId { get; set; }

        /// <summary>
        /// Device serial number
        /// </summary>
        [Description("Device serial number"), ReadOnly(true)]
        public override string DeviceSerial { get; set; }

        /// <summary>
        /// Device's vendor id number
        /// </summary>
        [Description("Device's vendor ID number"), ReadOnly(true)]
        public override string Vendor { get; set; }

        /// <summary>
        /// Device's status
        /// </summary>
        [Description("Device's status - open/closed"), ReadOnly(true)]
        public override bool IsOpen { get; set; }

        #endregion


        #region Public Methods

        /// <summary>
        /// Opens device with defined parameters.
        /// Updates parameters: MaxPinNumber, DeviceId, DeviceSerial, Vendor.
        /// </summary>
        public override void Open()
        {
            try
            {
                _stopLoop.Reset();
                //Initial the resource and get total board number form Driver 
                _return = UniDAQ.Ixud_DriverInit(ref _totalBoards);

                if (_totalBoards > 0)   // check if there are any available boards
                {
                    // get response form the board
                    _initialCode = UniDAQ.Ixud_GetCardInfo(_boardNo, ref sDeviceInfo[_boardNo], ref sCardInfo[_boardNo], _modelName);

                    if (_initialCode == UniDAQ.Ixud_NoErr)  // check response status
                    {
                        IsOpen = true;
                        // update properties
                        MaxPinNumber = sCardInfo[_boardNo].wDIOPortWidth;
                        DeviceId = Encoding.ASCII.GetString(_modelName);
                        DeviceSerial = sDeviceInfo[_boardNo].wDeviceID.ToString();
                        Vendor = "ICP DAS";

                        //sCardInfo[_boardNo].
                        _checkingPort = sCardInfo[_boardNo].CardID;

                        // initialize port pins values
                        boardOutputBits = new DAQ_BITS();
                        boardInputBits = new DAQ_BITS();

                        // start port checking task
                        _inputChecker = new Thread(new ThreadStart(CheckStatus));
                        _inputChecker.Name = "Digital IO checking thread";

                        _inputChecker.Start();
                    }
                    else
                    {
                        IsOpen = false;
                        throw new Exception("Could not connect to device. Error code: " + _initialCode.ToString());
                    }

                }
                else
                {
                    throw new Exception("No availablle devices found");
                }

            }
            catch (Exception)
            {
                throw new Exception("Could not connect to device. Device not found!");
            }
        }

        /// <summary>
        /// Close driver port
        /// </summary>
        public override void Close()
        {
            try
            {
                _stopLoop.Set();
                _inputChecker.Abort();
                _inputChecker.Join();
                _return = (ushort)UniDAQ.Ixud_DriverClose();                
                IsOpen = false;
            }
            catch (Exception)
            {
                throw new Exception("Could not disconnect device. Device not found!");
            }
        }

        /// <summary>
        /// Toggles single pin
        /// </summary>
        /// <param name="pin">Pin number</param>
        public override void TogglePin(int pin)
        {
            if (IsOpen)
            {
                int mask = 1 << pin;
                int value = (_lastDOPortValue & mask) >> pin;
                int newValue = Math.Abs( value - 1);
                SetPin(pin, newValue);
            }
            else
                throw new Exception("Device not found! Error code: " + _return.ToString());
        }

        /// <summary>
        /// Set digital value to single pin
        /// </summary>
        /// <param name="pin">Pin number (port 0)</param>
        /// <param name="value">On (1) / Off (0)</param>
        public override void SetPin(int pin, int value)
        {
            SetPin(0, pin, value);
        }


        /// <summary>
        /// Set digital value to single pin
        /// </summary>
        /// <param name="port">Port number if not defined before</param>
        /// <param name="pin">Pin number</param>
        /// <param name="value">On (1) / Off (0)</param>
        public override void SetPin(int port, int pin, int value)
        {
            if(EnableDO)
            {
                int newPortValue = value != 0 ? _lastDOPortValue | 1 << pin : _lastDOPortValue & ~(1 << pin);
                _lastDOPortValue = newPortValue;

                if (IsOpen)
                {
                    try
                    {
                        // write to single pin
                        _sync.WaitOne();
                        _return = UniDAQ.Ixud_WriteDOBit(_boardNo, (ushort)port, (ushort)pin, (ushort)value);
                        if(_return != 0)
                        {
                            throw new Exception("Error in Ixud_WriteDOBit");
                        }
                    }
                    catch(Exception ex)
                    {
                        //throw;
                    } 
                    finally
                    {
                        _sync.ReleaseMutex();                        
                    }
                }
                else
                    throw new Exception("Device not found! Error code: " + _return.ToString());
            }            
        }


        /// <summary>
        /// Set digital value to whole port pins
        /// </summary>
        /// <param name="port">Port number</param>
        /// <param name="value">Pin statuses combination (value)</param>
        public override void SetPort(int port, int value)
        {
            
            if (EnableDO)
            {
                _lastDOPortValue = value;
                if (IsOpen)
                {
                    try
                    {
                        // write to port
                        _sync.WaitOne();
                        _return = UniDAQ.Ixud_WriteDO(_boardNo, (ushort)port, (ushort)value);
                        if (_return != 0)
                        {
                            throw new Exception("Error inIxud_WriteDO");
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        _sync.ReleaseMutex();
                    }
                }
                else
                    throw new Exception("Device not found! Error code: " + _return.ToString());
            }            
        }




        /// <summary>
        /// Gets single pin digital value
        /// </summary>
        /// <param name="port">Port number</param>
        /// <param name="pin">Pin number</param>
        /// <returns>Pin value (0/1)</returns>
        public override int GetPin(int port, int pin)
        {
            if(EnableDI)
            {
                ushort _pinValue = 0;

                if (IsOpen)
                {
                    try
                    {
                        _sync.WaitOne();
                        _initialCode = UniDAQ.Ixud_ReadDIBit(_boardNo, (ushort)port, (ushort)pin, ref _pinValue);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error in Ixud_ReadDIBit");                      
                    }
                    finally
                    {
                        _sync.ReleaseMutex();
                    }                    
                }
                else
                {
                    throw new Exception("Device not found! Error code: " + _initialCode.ToString());
                }
                return (int)_pinValue;                
            }
            return 0;
        }

        /// <summary>
        /// Gets whole port pins' values
        /// </summary>
        /// <param name="port">Port number</param>
        /// <returns>Pin statuses combination</returns>
        public override int GetPort(int port)
        {
            if (EnableDI)
            {
                uint DIValue = 0;

                if (IsOpen)
                {
                    try
                    {
                        _sync.WaitOne();
                        _initialCode = UniDAQ.Ixud_ReadDI(_boardNo, (ushort)port, ref DIValue);

                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error in Ixud_ReadDI");
                    }
                    finally
                    {
                        _sync.ReleaseMutex();
                    }
                }
                else
                {
                    throw new Exception("Device not found! Error code: " + _initialCode.ToString());
                }
                return (int)DIValue;
            }
            return 0;            
        }


        /// <summary>
        /// Reads digital output values
        /// </summary>
        /// <param name="port">Port number</param>
        /// <returns>Pin statuses combination (value)</returns>
        public int GetDOValues(int port)
        {
            uint result = 0;
            _return = UniDAQ.Ixud_SoftwareReadbackDO(_boardNo, (ushort)port, ref result);
            return (int)result;
        }


        public override int GetDOPin(int pin)
        {
            int mask = 1 << pin;
            return (_lastDOPortValue & mask) >> pin;
        }

        #endregion


        #region Private Methods
        /// <summary>
        /// Method for continously checking IN/OUT pins values
        /// </summary>
        private void CheckStatus()
        {
            Console.WriteLine("Thread started.");

            int _tempInput = 0;
            int _tempOutput = 0;
            int result = 0;

            while (!_stopLoop.IsSet)
            {
                switch (_inOutCheck)
                {
                    case 0: // input

                        // current input values
                        _tempInput = GetPort(_checkingPort);

                        result = ByteSorting("input");

                        if (_tempInput != result)
                        {
                            // event occured
                            if (onInputsChanged != null)
                            {
                                ByteUpdate("input", _tempInput);
                                //int pinNo = GetChangedPinIndex(Math.Abs(_tempInput - result));
                                // generate event
                                onInputsChanged(this, ByteSorting("input"));
                            }

                        }   // end of if
                        _inOutCheck = 1; // switch to next statement
                        break;

                    case 1: // output

                        // current output values
                        _tempOutput = GetDOValues(_checkingPort);

                        result = ByteSorting("output");

                        if (_tempOutput != result)
                        {
                            // event occured
                            if (onOutputsChanged != null)
                            {
                                ByteUpdate("output", _tempOutput);
                                //int pinNo = GetChangedPinIndex(Math.Abs(_tempInput - result));

                                onOutputsChanged(this, ByteSorting("output"));
                            }
                        }
                        _inOutCheck = 0; // switch to next statement
                        break;
                }

                Thread.Sleep(100);
            }
        }

        /// <summary>
        /// Method for encoding in/out pins values
        /// </summary>
        /// <param name="inOut">Input - "input", Output - "output"</param>
        /// <param name="value">Port pins value to encode</param>
        private void ByteUpdate(string inOut, int value)
        {
            int _tempData = value;

            byte on = 1;
            byte off = 0;

            switch (inOut)
            {
                case "input":

                    boardInputBits.bit0 = Convert.ToBoolean(_tempData & 0x01) ? on : off;
                    boardInputBits.bit1 = Convert.ToBoolean((_tempData >> 1) & 0x01) ? on : off;
                    boardInputBits.bit2 = Convert.ToBoolean((_tempData >> 2) & 0x01) ? on : off;
                    boardInputBits.bit3 = Convert.ToBoolean((_tempData >> 3) & 0x01) ? on : off;
                    boardInputBits.bit4 = Convert.ToBoolean((_tempData >> 4) & 0x01) ? on : off;
                    boardInputBits.bit5 = Convert.ToBoolean((_tempData >> 5) & 0x01) ? on : off;
                    boardInputBits.bit6 = Convert.ToBoolean((_tempData >> 6) & 0x01) ? on : off;
                    boardInputBits.bit7 = Convert.ToBoolean((_tempData >> 7) & 0x01) ? on : off;

                    boardInputBits.bit8 = Convert.ToBoolean((_tempData >> 8) & 0x01) ? on : off;
                    boardInputBits.bit9 = Convert.ToBoolean((_tempData >> 8) & 0x01) ? on : off;
                    boardInputBits.bit10 = Convert.ToBoolean((_tempData >> 10) & 0x01) ? on : off;
                    boardInputBits.bit11 = Convert.ToBoolean((_tempData >> 11) & 0x01) ? on : off;
                    boardInputBits.bit12 = Convert.ToBoolean((_tempData >> 12) & 0x01) ? on : off;
                    boardInputBits.bit13 = Convert.ToBoolean((_tempData >> 13) & 0x01) ? on : off;
                    boardInputBits.bit14 = Convert.ToBoolean((_tempData >> 14) & 0x01) ? on : off;
                    boardInputBits.bit15 = Convert.ToBoolean((_tempData >> 15) & 0x01) ? on : off;

                    break;

                case "output":

                    boardOutputBits.bit0 = Convert.ToBoolean(_tempData & 0x01) ? on : off;
                    boardOutputBits.bit1 = Convert.ToBoolean((_tempData >> 1) & 0x01) ? on : off;
                    boardOutputBits.bit2 = Convert.ToBoolean((_tempData >> 2) & 0x01) ? on : off;
                    boardOutputBits.bit3 = Convert.ToBoolean((_tempData >> 3) & 0x01) ? on : off;
                    boardOutputBits.bit4 = Convert.ToBoolean((_tempData >> 4) & 0x01) ? on : off;
                    boardOutputBits.bit5 = Convert.ToBoolean((_tempData >> 5) & 0x01) ? on : off;
                    boardOutputBits.bit6 = Convert.ToBoolean((_tempData >> 6) & 0x01) ? on : off;
                    boardOutputBits.bit7 = Convert.ToBoolean((_tempData >> 7) & 0x01) ? on : off;

                    boardOutputBits.bit8 = Convert.ToBoolean((_tempData >> 8) & 0x01) ? on : off;
                    boardOutputBits.bit9 = Convert.ToBoolean((_tempData >> 8) & 0x01) ? on : off;
                    boardOutputBits.bit10 = Convert.ToBoolean((_tempData >> 10) & 0x01) ? on : off;
                    boardOutputBits.bit11 = Convert.ToBoolean((_tempData >> 11) & 0x01) ? on : off;
                    boardOutputBits.bit12 = Convert.ToBoolean((_tempData >> 12) & 0x01) ? on : off;
                    boardOutputBits.bit13 = Convert.ToBoolean((_tempData >> 13) & 0x01) ? on : off;
                    boardOutputBits.bit14 = Convert.ToBoolean((_tempData >> 14) & 0x01) ? on : off;
                    boardOutputBits.bit15 = Convert.ToBoolean((_tempData >> 15) & 0x01) ? on : off;

                    break;

                default:
                    Console.WriteLine("Invalid input parameter");
                    break;
            }

            //return _tempData;
        }

        /// <summary>
        /// Method for decoding in/out pins values
        /// </summary>
        /// <param name="inOut">Input - "input", Output - "output"</param>
        /// <returns>Decoded port pins value</returns>
        private int ByteSorting(string inOut)
        {
            int _tempData = 0;
            //pinIndex = 20;
            //   boardInputBits 

            switch (inOut)
            {
                case "input":

                    if (boardInputBits.bit0 == 1) _tempData |= 0x0001;
                    if (boardInputBits.bit1 == 1) _tempData |= 0x0002;
                    if (boardInputBits.bit2 == 1) _tempData |= 0x0004;
                    if (boardInputBits.bit3 == 1) _tempData |= 0x0008;
                    if (boardInputBits.bit4 == 1) _tempData |= 0x0010;
                    if (boardInputBits.bit5 == 1) _tempData |= 0x0020;
                    if (boardInputBits.bit6 == 1) _tempData |= 0x0040;
                    if (boardInputBits.bit7 == 1) _tempData |= 0x0080;

                    if (boardInputBits.bit8 == 1) _tempData |= 0x0100;
                    if (boardInputBits.bit9 == 1) _tempData |= 0x0200;
                    if (boardInputBits.bit10 == 1) _tempData |= 0x0400;
                    if (boardInputBits.bit11 == 1) _tempData |= 0x0800;
                    if (boardInputBits.bit12 == 1) _tempData |= 0x1000;
                    if (boardInputBits.bit13 == 1) _tempData |= 0x2000;
                    if (boardInputBits.bit14 == 1) _tempData |= 0x4000;
                    if (boardInputBits.bit15 == 1) _tempData |= 0x8000;
                    break;

                case "output":

                    if (boardOutputBits.bit0 == 1) _tempData |= 0x0001;
                    if (boardOutputBits.bit1 == 1) _tempData |= 0x0002;
                    if (boardOutputBits.bit2 == 1) _tempData |= 0x0004;
                    if (boardOutputBits.bit3 == 1) _tempData |= 0x0008;
                    if (boardOutputBits.bit4 == 1) _tempData |= 0x0010;
                    if (boardOutputBits.bit5 == 1) _tempData |= 0x0020;
                    if (boardOutputBits.bit6 == 1) _tempData |= 0x0040;
                    if (boardOutputBits.bit7 == 1) _tempData |= 0x0080;

                    if (boardOutputBits.bit8 == 1) _tempData |= 0x0100;
                    if (boardOutputBits.bit9 == 1) _tempData |= 0x0200;
                    if (boardOutputBits.bit10 == 1) _tempData |= 0x0400;
                    if (boardOutputBits.bit11 == 1) _tempData |= 0x0800;
                    if (boardOutputBits.bit12 == 1) _tempData |= 0x1000;
                    if (boardOutputBits.bit13 == 1) _tempData |= 0x2000;
                    if (boardOutputBits.bit14 == 1) _tempData |= 0x4000;
                    if (boardOutputBits.bit15 == 1) _tempData |= 0x8000;
                    break;

                default:
                    Console.WriteLine("Invalid input parameter");
                    break;
            }

            return _tempData;
        }

        #endregion


        #region Events
        /// <summary>
        /// Occurs on output port/pin value change. 
        /// Returns output port pins[bytes] combination</param>
        /// </summary>
        public override event GpioOutputsEventHandler onOutputsChanged;

        /// <summary>
        /// Occurs on input port/pin value change. 
        /// Returns input port pins[bytes] combination
        /// </summary>     
        public override event GpioInputsEventHandler onInputsChanged;

        public override event DataReadyEventHandler onDataReady;

        #endregion




        // unused for now
        /*int GetChangedPinIndex(int difference)
        {
            int pinNumber = 20;

            switch (difference)
            {
                case 0x0001:
                    pinNumber = 0;
                    break;
                case 0x0002:
                    pinNumber = 1;
                    break;
                case 0x0004:
                    pinNumber = 2;
                    break;
                case 0x0008:
                    pinNumber = 3;
                    break;
                case 0x0010:
                    pinNumber = 4;
                    break;
                case 0x0020:
                    pinNumber = 5;
                    break;
                case 0x0040:
                    pinNumber = 6;
                    break;
                case 0x0080:
                    pinNumber = 7;
                    break;
                case 0x0100:
                    pinNumber = 8;
                    break;
                case 0x0200:
                    pinNumber = 9;
                    break;
                case 0x0400:
                    pinNumber = 10;
                    break;
                case 0x0800:
                    pinNumber = 11;
                    break;
                case 0x1000:
                    pinNumber = 12;
                    break;
                case 0x2000:
                    pinNumber = 13;
                    break;
                case 0x4000:
                    pinNumber = 14;
                    break;
                case 0x8000:
                    pinNumber = 15;
                    break;

                case 0:
                    Console.Write("All pins are low");
                    break;

                default:
                    throw new Exception("Invalid pin number!");
                    break;

            }


            return pinNumber;
        }
        */
    
     

    }
}
