﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Xml.Serialization;
using System.ComponentModel;

using Elvis.Fenix.Hardware;


namespace Elvis.Fenix.Hardware.FxLedController
{
    #region UDP Connection classes

    public struct Received
    {
        public IPEndPoint Sender;
        public string Message;
    }

    /// <summary>
    /// Connnection port numbers
    /// </summary>
    public enum Ports
    {
        ENQUIRY_SOURCE = 30310,
        ENQUIRY_DESTINATION = 30311,
        UDP_SOURCE = 30312,
        UDP_DESTINATION = 30313
    }

    public abstract class UdpBase
    {
        protected UdpClient Client;

        protected UdpBase()
        {
            Client = new UdpClient();
        }

        public async Task<Received> Receive()
        {
            var result = await Client.ReceiveAsync();

            //Console.WriteLine(Encoding.ASCII.GetString(result.Buffer, 0, result.Buffer.Length));
            return new Received()
            {
                Message = Encoding.ASCII.GetString(result.Buffer, 0, result.Buffer.Length),
                Sender = result.RemoteEndPoint
            };
        }
    }

    // Server
    public class UdpListener : UdpBase
    {
        private IPEndPoint _listenOn;

        public UdpListener()
            : this(new IPEndPoint(IPAddress.Any, (int)Ports.UDP_DESTINATION))
        {
        }

        public UdpListener(IPEndPoint endpoint)
        {
            _listenOn = endpoint;
            Client = new UdpClient(_listenOn);
        }

        public void Reply(string message, IPEndPoint endpoint)
        {
            var datagram = Encoding.ASCII.GetBytes(message);
            Client.Send(datagram, datagram.Length, endpoint);
        }

    }

    // Client
    public class UdpUser : UdpBase
    {
        private UdpUser() { }

        public static UdpUser ConnectTo(string hostname, int port)
        {
            var connection = new UdpUser();
            connection.Client.Connect(hostname, port);
            return connection;
        }

        public void Send(string message)
        {
            var datagram = Encoding.ASCII.GetBytes(message);
            Client.Send(datagram, datagram.Length);
        }

    }

    #endregion

    /// <summary>
    /// Error flag number
    /// </summary>
    public enum ErrorNumber
    {
        NoError = 0,
        Err1 = 1,
        Err2 = 2,
        Err3 = 3,
        Err4 = 4,
        Err5 = 5
    }

    public enum LedControllerTrigger
    {
        Trigger1Upper = 11,
        Trigger1Lower = 12,
        Trigger2Upper = 21,
        Trigger2Lower = 22
    }

    /*  TBD: show configuration??   */
    public class RT820F2 : FxLEDController
    {
        #region Private Fields

        private ManualResetEventSlim _stop = new ManualResetEventSlim(false);

        private string _modelId;
        private string _ipAddress;
        private Ports _port;
        private ErrorNumber _error = 0;

        private ControllerMode _mode;
        private double _pulseWidth;
        private double _pulseDelay;
        private double _retriggerDelay;
        private int _brightness;
        private int _outputChannel;
        private int _triggerInput;

        private bool updateBrightness = false;
        private bool updateTrigger = false;

        private const string replyCompleted = ">";
        

        #endregion


        #region Public Properties

        public override string SerialNumber { get; set; }

        public override string ModelId 
        {
            get { return _modelId; }
            set { _modelId = value; } 
        }

        public override string IpAddress 
        {
            get { return _ipAddress; }
            set { _ipAddress = value; } 
        }

        public override int Port 
        {
            get { return (int)_port; }
            set { _port = (Ports)value; }
        }

        public override bool IsConnected 
        { 
            get
            {
                ModelId = "NaN";
                SendHeartbeat();

                return (_error == ErrorNumber.NoError && ModelId != "NaN") ? true : false;
            }
        }

        public override ControllerMode Mode 
        {
            get { return _mode; }
            set
            {
                if(!_mode.Equals(value))
                {
                    _mode = value;
                    updateBrightness = true;
                }
            }
        }

        public override int OutputChannel
        {
            get { return _outputChannel; }
            set
            {
                if(!_outputChannel.Equals(value))
                {
                    _outputChannel = value;
                    updateBrightness = true;
                    updateTrigger = true;
                    //SetChannelBrightness(Mode, OutputChannel, PulseWidth, PulseDelay, Brightness, RetriggerDelay);
                }
            }
        }

        public override double PulseWidth
        {
            get { return _pulseWidth; }
            set
            {
                if (!_pulseWidth.Equals(value))
                {
                    _pulseWidth = value;
                    updateBrightness = true;
                    //SetChannelBrightness(Mode, OutputChannel, PulseWidth, PulseDelay, Brightness, RetriggerDelay);
                }
            }
        }

        public override double PulseDelay
        {
            get { return _pulseDelay; }
            set
            {
                if (!_pulseDelay.Equals(value))
                {
                    _pulseDelay = value;
                    updateBrightness = true;
                    //SetChannelBrightness(Mode, OutputChannel, PulseWidth, PulseDelay, Brightness, RetriggerDelay);
                }
            }
        }

        public override double RetriggerDelay
        {
            get { return _retriggerDelay; }
            set
            {
                if (!_retriggerDelay.Equals(value))
                {
                    _retriggerDelay = value;
                    updateBrightness = true;
                    //SetChannelBrightness(Mode, OutputChannel, PulseWidth, PulseDelay, Brightness, RetriggerDelay);
                }
            }
        }


        public override int Brightness
        {
            get { return _brightness; }
            set
            {
                if (!_brightness.Equals(value))
                {
                    _brightness = value;
                    updateBrightness = true;
                    //SetChannelBrightness(Mode, OutputChannel, PulseWidth, PulseDelay, Brightness, RetriggerDelay);
                }
            }
        }


        public override int TriggerInput
        {
            get { return _triggerInput; }
            set
            {
                if (!_triggerInput.Equals(value))
                {
                    _triggerInput = value;
                    updateTrigger = true;
                    //SetTriggerInput(OutputChannel, TriggerInput);
                }
            }
        }

        [XmlIgnoreAttribute, Browsable(false)]
        public override GPIO Gpio { get; set; }

        public override int Trigger1UpperDOPin { get; set; }
        public override int Trigger1LowerDOPin { get; set; }
        public override int Trigger2UpperDOPin { get; set; }
        public override int Trigger2LowerDOPin { get; set; }

        #endregion


        #region Constructors

        public RT820F2() { }

        /// <summary>
        /// Creates RT820F-2 led controller instance
        /// </summary>
        /// <param name="ipAddress">target ip address</param>
        public RT820F2(string name, string ipAddress)
        {
            Name = name;
            IpAddress = ipAddress;
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Sends a heartbeat command to get model id
        /// </summary>
        public override void Init() 
        { 
            SendHeartbeat();
        }

        /// <summary>
        /// Clear any errors present
        /// </summary>
        public override void ClearErrors() 
        {
            SendMessage("GR");
        }

        /// <summary>
        /// Only works with continuous, switched and selected modes
        /// </summary>
        /// <param name="mode">continuous, switched or selected</param>
        /// <param name="outputChannel">1 to 8 (depends on model)</param>
        /// <param name="brightness">from 0 to 100%</param>
        /// <param name="brightness2">2nd brightness setting for selected mode, from 0 to 'brightness' (the previous parameter)</param>
        public override void SetChannelBrightness(ControllerMode mode, int outputChannel, int brightness, int brightness2) 
        {
            string message = "";

            switch(mode)
            {
                case ControllerMode.Continuous:
                    message = "RS" + outputChannel.ToString() + "," + brightness.ToString();
                    break;

                case ControllerMode.Switched:
                    message = "RW" + outputChannel.ToString() + "," + brightness.ToString();
                    break;

                case ControllerMode.Selected:
                    message = "RU" + outputChannel.ToString() + "," + brightness.ToString() + "," + brightness2.ToString();
                    break;                    
            }

            SendMessage(message);
        }

        /// <summary>
        /// Only works with pulse mode
        /// </summary>
        /// <param name="mode">pulse</param>
        /// <param name="outputChannel">1 to 8 or w/e (depends on model)</param>
        /// <param name="pulseWidth">pulse width in milliseconds (0.02 to 999)</param>
        /// <param name="delayFromTrigger">delay from trigger to pulse in milliseconds (0.02 to 999)</param>
        /// <param name="settingPercent">setting in percent (0 to 999)</param>
        /// <param name="retriggerDelay">retrigger delay (this parameter is optional)</param>
        public override bool SetChannelBrightness(ControllerMode mode, int outputChannel, double pulseWidth, double delayFromTrigger, int settingPercent, double retriggerDelay)
        {
            bool updated = false;

            if(mode == ControllerMode.Pulse)
            {
                Mode = mode;
                OutputChannel = outputChannel;
                PulseWidth = pulseWidth;
                PulseDelay = delayFromTrigger;
                Brightness = settingPercent;
                RetriggerDelay = retriggerDelay;
                updated = updateBrightness;

                if (updateBrightness)
                {
                    //SendHeartbeat();

                    string message = " ";
                    CultureInfo info = CultureInfo.GetCultureInfo("en-US");

                    if (!RetriggerDelay.Equals(0))
                        message = "RT" + OutputChannel.ToString(info) + "," + PulseWidth.ToString(info) + "," + PulseDelay.ToString(info) + "," + Brightness.ToString(info) + "," + RetriggerDelay.ToString(info);
                    else
                        message = "RT" + OutputChannel.ToString(info) + "," + PulseWidth.ToString(info) + "," + PulseDelay.ToString(info) + "," + Brightness.ToString(info);

                    SendMessage(message);

                    updateBrightness = false;
                }
            }
            return updated;
        }

        /// <summary>
        /// If current rating is being set, then the voltage rating value should be 0
        /// </summary>
        /// <param name="outputChannel">1 to 8 or w/e (depends on model)</param>
        /// <param name="voltage">0 or 12 to 36</param>
        /// <param name="current">0 or 10mA to 3A</param>
        public override void SetVoltageRating(int outputChannel, double voltage, double current) 
        {
            if (current > 0)
                voltage = 0;

            SendMessage("VL" + outputChannel.ToString() + "," + voltage.ToString() + "," + current.ToString());
        }

        /// <summary>
        /// This method sets which input is used for pulse and switch output modes
        /// </summary>
        /// <param name="outputChannel">1 to 8 or w/e (depends on model)</param>
        /// <param name="triggerInput">trigger input (1 or 2)</param>
        public override void SetTriggerInput(int outputChannel, int triggerInput) 
        {
            TriggerInput = triggerInput;
            OutputChannel = outputChannel;

            if (updateTrigger)
            {
                SendMessage("RP" + OutputChannel.ToString() + "," + TriggerInput.ToString());
                System.Threading.Thread.Sleep(50);
                updateTrigger = false;

                //string message = "RP";

                //if(Gpio != null)
                //{
                //    Gpio.SetPin(Trigger1UpperDOPin, 0);
                //    Gpio.SetPin(Trigger1LowerDOPin, 0);
                //    Gpio.SetPin(Trigger2UpperDOPin, 0);
                //    Gpio.SetPin(Trigger2LowerDOPin, 0);
                //}

                //switch ((LedControllerTrigger)triggerInput)
                //{
                //    case LedControllerTrigger.Trigger1Upper:
                //        message += "1,1";
                //        if (Gpio != null) Gpio.SetPin(Trigger1UpperDOPin, 1);
                //        break;

                //    case LedControllerTrigger.Trigger1Lower:
                //        message += "1,1";
                //        if (Gpio != null) Gpio.SetPin(Trigger1LowerDOPin, 1);
                //        break;

                //    case LedControllerTrigger.Trigger2Upper:
                //        message += "1,2";
                //        if (Gpio != null) Gpio.SetPin(Trigger1UpperDOPin, 1);
                //        break;

                //    case LedControllerTrigger.Trigger2Lower:
                //        message += "1,2";
                //        if (Gpio != null) Gpio.SetPin(Trigger2LowerDOPin, 1);
                //        break;
                //}
            }

            

        }


        /// <summary>
        /// Settings are saved to memory, to be retained when unit is switched off
        /// </summary>
        public override void SaveSettingsToMemory() 
        {
            SendMessage("AW"); 
        }

        /// <summary>
        /// Clears the channel configuration and lighting ratings and sets all channels to 50% continous operation
        /// </summary>
        public override void ClearConfiguration()
        {
            SendMessage("CL");
        }

        /// <summary>
        /// Sends custom command message
        /// </summary>
        /// <param name="command">enter full command</param>
        public override void SendCustomCommand(string command)
        {
            SendMessage(command);
        }


        #endregion


        #region Private Methods

        /// <summary>
        /// Send a heartbeat message to check if device is responding
        /// </summary>
        private void SendHeartbeat()
        {
            //create a new client
            var client = UdpUser.ConnectTo(IpAddress, (int)Ports.UDP_DESTINATION);

            client.Send("VR");

            _stop.Reset();
            //wait for reply messages from server and send them to console 
            Task.Factory.StartNew(async () =>
            {
                while (true && !_stop.IsSet)
                {
                    try
                    {
                        var received = await client.Receive();
                        Console.WriteLine(received.Message);

                        if(received.Message.Contains(replyCompleted))
                        {
                            ModelId = received.Message.Substring(2, received.Message.IndexOf(" ") - 2);
                            Console.WriteLine("Beep...");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    finally
                    {
                        GC.Collect();
                        _stop.Set();
                    }
                }
            });
        }

        /// <summary>
        /// Send a message to device
        /// </summary>
        /// <param name="message">whole message text</param>
        private void SendMessage(string message)
        {
            //create a new client
            var client = UdpUser.ConnectTo(IpAddress, (int)Ports.UDP_DESTINATION);

            client.Send(message);

            _stop.Reset();
            //wait for reply messages from server and send them to console 
            Task.Factory.StartNew(async () =>
            {
                while (true && !_stop.IsSet)
                {
                    try
                    {
                        var received = await client.Receive();
                        Console.WriteLine("Message received:");
                        Console.WriteLine(received.Message);

                        if (received.Message.Contains(replyCompleted))
                        {
                            if(received.Message.Contains("Err 1")) _error = ErrorNumber.Err1;
                            else if (received.Message.Contains("Err 2")) _error = ErrorNumber.Err2;
                            else if (received.Message.Contains("Err 3")) _error = ErrorNumber.Err3;
                            else if (received.Message.Contains("Err 4")) _error = ErrorNumber.Err4;
                            else if (received.Message.Contains("Err 5")) _error = ErrorNumber.Err5;
                            else _error = ErrorNumber.NoError;

                            SetError(_error);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);

                    }
                    finally
                    {
                        GC.Collect();
                        _stop.Set();
                    }
                }
            });
            
        }

        /// <summary>
        /// Check if there was any error in sending/receiving very last message
        /// </summary>
        /// <param name="err">response from the device</param>
        private void SetError(ErrorNumber err)
        {
            string errText = "No error";

            switch(err)
            {
                case ErrorNumber.NoError:
                    errText = "Message sent succesfully!";
                    break;
                case ErrorNumber.Err1:
                    errText = "A parameter value is invalid!";
                    break;
                case ErrorNumber.Err2:
                    errText = "Command not recognised!";
                    break;
                case ErrorNumber.Err3:
                    errText = "Numeric value is wrong format!";
                    break;
                case ErrorNumber.Err4:
                    errText = "Wrong number of parameters!";
                    break;
                case ErrorNumber.Err5:
                    errText = "(Warning) Timing parameter was out of range and has been adjusted to a valid value.";
                    break;
            }

            Console.WriteLine(errText);

            if (onMessage != null)
                onMessage(this, errText);

            if(!err.Equals(ErrorNumber.NoError))
            {
                try { ClearErrors(); }
                catch { }
            }
        }

        #endregion


        #region Events

        public override event LedControllerMessageHandler onMessage;

        #endregion
    }
}
