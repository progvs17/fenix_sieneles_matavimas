﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO.Ports;
using System.ComponentModel;
using System.Xml;
using System.Xml.Serialization;
using System.Threading;

using CRC;
using Elvis.Fenix.IO;
using Elvis.Fenix.Hardware;

namespace Elvis.Fenix.Hardware.FxActuators
{
    /// <summary>
    /// ARD-KD driver class
    /// </summary>
    public class ARDKD : FxActuator
    {
        #region Private Members

        private SerialPort _actuatorSerialPort;
        private byte _slaveAddress;
        private int _speed;// = 2000;// = 1500;
        private int _acceleration; // = 2000;// = 2000;
        private int _decceleration; // = 2000;// = 2000;
        int _currentPosition;
        private string _status = "NaN";
        private bool _isConnected = false;
        bool _isMoving = false;

        FxModbusPortManager _comManager;
        Thread _movementChecker = null;
        
        /// <summary>
        /// 0 - incremental, 1 - absolute
        /// </summary>
        private byte _operationMode = 0;

        #endregion


        #region Public Members

        /// <summary>
        /// output register structure 0x007F
        /// </summary>
        [StructLayout(LayoutKind.Explicit, Size = 16)]
        public class DriverOutputRegister
        {
            [FieldOffset(0)]
            public bool M0_R;
            [FieldOffset(1)]
            public bool M1_R;
            [FieldOffset(2)]
            public bool M2_R;
            [FieldOffset(3)]
            public bool START_R;
            [FieldOffset(4)]
            public bool HOME_P;
            [FieldOffset(5)]
            public bool READY;
            [FieldOffset(6)]
            public bool WNG;
            [FieldOffset(7)]
            public bool ALM;
            [FieldOffset(8)]
            public bool S_BSY;
            [FieldOffset(9)]
            public bool AREA1;
            [FieldOffset(10)]
            public bool AREA2;
            [FieldOffset(11)]
            public bool AREA3;
            [FieldOffset(12)]
            public bool TIM;
            [FieldOffset(13)]
            public bool MOVE;
            [FieldOffset(14)]
            public bool END;
            [FieldOffset(15)]
            public bool TLC;
        }

        /// <summary>
        /// input register structure 0x007D
        /// </summary>
        [StructLayout(LayoutKind.Explicit, Size = 16)]
        public class DriverInputRegister
        {
            [FieldOffset(0)]
            public bool M0;
            [FieldOffset(1)]
            public bool M1;
            [FieldOffset(2)]
            public bool M2;
            [FieldOffset(3)]
            public bool START;
            [FieldOffset(4)]
            public bool HOME;
            [FieldOffset(5)]
            public bool STOP;
            [FieldOffset(6)]
            public bool FREE;
            [FieldOffset(7)]
            public bool res;
            [FieldOffset(8)]
            public bool MS0;
            [FieldOffset(9)]
            public bool MS1;
            [FieldOffset(10)]
            public bool MS2;
            [FieldOffset(11)]
            public bool SSTART;
            [FieldOffset(12)]
            public bool posJOG;
            [FieldOffset(13)]
            public bool negJOG;
            [FieldOffset(14)]
            public bool FWD;
            [FieldOffset(15)]
            public bool RVS;
        }

        #endregion


        #region Public Porperties

        /// <summary>
        /// Used to assign already open/predefined serial port
        /// </summary>
        [Browsable(false), XmlIgnore]
        public override SerialPort ComPort
        {
            get { return _actuatorSerialPort; }
            set { _actuatorSerialPort = value; }
        }

        /// <summary>
        /// Driver inputs
        /// </summary>
        [Browsable(false), XmlIgnore]
        public DriverInputRegister InputStatus { get; set; }

        /// <summary>
        /// Driver outputs
        /// </summary>
        [Browsable(false), XmlIgnore]
        public DriverOutputRegister OutputStatus { get; set; }

        /// <summary>
        /// Gets current rotary position.
        /// Sets absolute rotary position from 0 to 1000
        /// </summary>
        [Description("Current position from 0 to 1000")]
        public override double CurrentPosition 
        {
            get
            {
                if (!_isMoving)
                    return (GetCurrentPosition() % 1000) * 0.36; //} // !_isMoving ? (GetCurrentPosition() % 1000) * 0.36 : (_currentPosition % 1000) * 0.36; }
                else
                    return _currentPosition * 0.36;
            }
            set 
            {
                if (((_currentPosition % 1000) * 0.36) != value)
                {
                    MoveTo(value);
                }
                //SetOperationMode(0);
                
            } 
        }

        /// <summary>
        /// Not used in this class (8,388,607)
        /// </summary>
        [Description("Not used in this class. Max position is +-8,388,607")]
        public override double MaxPosition { get; set; }

        /// <summary>
        /// Sets/gets motor speed.
        /// <para> Default is 1000 [Hz]. </para>
        /// <para> Range: 1 to 1,000,000. </para>
        /// </summary>
        [Description("Default is 1000 [Hz]. Range: 1 to 1,000,000.")]
        public override int Speed
        {
            get
            {
                return GetSpeed();
                //return !_isMoving ? GetSpeed() : _speed;
            }
            set
            {
                if(_speed != value && value > 0)
                    SetSpeed(value);
            }
        }

        /// <summary>
        /// Sets/gets acceleration
        /// <para> Default is 1000 [Hz]. </para>
        /// <para> Range: 1 to 1,000,000. </para>
        /// </summary>
        [Description("Default is 1000 [Hz]. Range: 1 to 1,000,000.")]
        public override int Acceleration 
        {
            get 
            {
                return GetAcceleration();
                //return !_isMoving ? GetAcceleration() : _acceleration; 
            }
            set 
            {
                if (value != _acceleration && value > 0)
                    SetAcceleration(value); 
            }
        }

        /// <summary>
        /// Sets/gets decceleration
        /// <para> Default is 1000 [Hz]. </para>
        /// <para> Range: 1 to 1,000,000. </para>
        /// </summary>
        [Description("Default is 1000 [Hz]. Range: 1 to 1,000,000.")]
        public override int Decceleration 
        {
            get 
            {
                return GetDecceleration();
                //return !_isMoving ? GetDecceleration() : _decceleration; 
            }
            set 
            {
                if (value != _decceleration && value > 0)
                    SetDecceleration(value); 
            }
        }

        public override bool InitComplete
        {
            get
            {
                return _initComplete;
            }
        }

        /// <summary>
        /// Status about motor movement. True - moving, false - not moving
        /// </summary>
        [Description("Motor movement status")]
        public override bool IsMoving 
        { 
            get 
            {
               // UpdateOutputRegister();
               // _isMoving = !OutputStatus.END;
                
                return _isMoving; 
            } 
        }

        /// <summary>
        /// Device's connection status
        /// </summary>
        [Description("Device's connection status.")]
        public override bool IsConnected 
        { 
            get 
            {
                UpdateOutputRegister();

                if (_actuatorSerialPort.IsOpen && (Status != "Error") && (OutputStatus.READY || OutputStatus.END))
                    _isConnected = true;
                else
                    _isConnected = false;

                return _isConnected;
            } 
        }

        /// <summary>
        /// Indicates driver's status - Ready, Error
        /// </summary>
        [Description("Indicates driver's status.")]
        public override string Status 
        {
            get 
            {
                UpdateOutputRegister();

                if (!OutputStatus.ALM && !OutputStatus.WNG) // && !OutputStatus.MOVE)
                    _status = "Ready";
                else
                {
                    _status = "Error";

                    if(onMessage != null)
                    {
                        if (OutputStatus.ALM) onMessage(this, "alarm");
                        if (OutputStatus.WNG) onMessage(this, "warning");

                        onMessage(this, ReadRegister(0x0080, 2).ToString());
                    }
                    //try
                    //{
                    //    ResetAlarm();
                    //}                    
                    //catch { }
                }

                return _status; 
            } 
        }

        #endregion


        #region Constructors

        /// <summary>
        /// Empty constructor
        /// </summary>
        public ARDKD()
        {  }

        /// <summary>
        /// Creates ARD-KD rotary actuator control object with existing Serial port and inputed slave address.
        /// Make sure existing serial port is already open before assigning. If it is, you do not need to connect this device again.
        /// </summary>
        /// <param name="comPort">Already open/existing serial port</param>
        /// <param name="slaveAddr">Slave address</param>
        public ARDKD(string name, SerialPort comPort, byte slaveAddr)
        {
            Name = name;
            // assign COM port
            _actuatorSerialPort = comPort;

            ConstructorsRoutine(slaveAddr);
        }

        /// <summary>
        /// Creates ARD-KD rotary actuator control object with inputed serial port, slave address and default baudrate(19200)
        /// </summary>
        /// <param name="comPort">Com port name</param>
        /// <param name="slaveAddr">Slave address</param>
        public ARDKD(string name, string comPort, byte slaveAddr)
        {
            Name = name;
            int baudRate = 19200; // 19200;
            // assign COM port
            _actuatorSerialPort = new SerialPort(comPort, baudRate, Parity.Even, 8, StopBits.One);
            
            ConstructorsRoutine(slaveAddr);
        }

        /// <summary>
        /// Creates ARD-KD rotary actuator control object with inputed serial port name, baudrate and slave address
        /// </summary>
        /// <param name="comPort">Com port name</param>
        /// <param name="baudRate">Custom baud rate (default is 19200)</param>
        /// <param name="slaveAddr">Slave address</param>
        public ARDKD(string name, string comPort, int baudRate, byte slaveAddr)
        {
            Name = name;
            // assign COM port
            _actuatorSerialPort = new SerialPort(comPort, baudRate, Parity.Even, 8, StopBits.One);
         
            ConstructorsRoutine(slaveAddr);
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Connects to device
        /// </summary>
        public override void Connect()
        {
            OpenCom();
        }

        /// <summary>
        /// Disconnects device
        /// </summary>
        public override void Disconnect()
        {
            CloseCom();
        }


        /// <summary>
        /// Driver initialization. Not neccesary if rotation amount/position is set.
        /// </summary>
        public override void Init() 
        {
            _initComplete = false;

            //Delay(100);

            if (onMessage != null)
                onMessage(this, "Configure sequence started.");

            ResetAlarm();

            MoveToHomePosition();

            //if (_isMoving)
            //    while (_isMoving) ;
            
            if (onMessage != null)
                onMessage(this, "Configure sequence completed!");

            _initComplete = true;
        }

        /// <summary>
        /// Move by the amount inputed
        /// </summary>
        /// <param name="value">Moving amount, can be positive or negative values (for rotating backwards)</param>
        public override void Move(double value) 
        {
            if (!_isMoving)
            {
                int val = (int)(value / 0.36);

                try
                {
                    // check operation mode for incremental
                    if (_operationMode != 0)
                        SetOperationMode(0);

                    // set position
                    SetPosition(val);

                    // make sure any other bits arent set in status register
                    ClearInputRegisterStructure();

                    // select position No.1
                    InputStatus.M0 = true;
                    // set start bit
                    InputStatus.START = true;
                    // update register (send)
                    UpdateInputRegister();

                    // generate movement start event
                    _isMoving = true;
                    Console.WriteLine("Actuator " + _slaveAddress.ToString() + " started");
                    if (onActuatorStart != null)
                        onActuatorStart(this);

                    // detect when actuator stops
                    CheckIfActuatorStopped();

                }
                catch
                {
                    if (onMessage != null)
                        onMessage(this, "Command 'Move' could not be completed");

                    //throw new Exception("Command 'Move' could not be completed");
                }
            }
        }

        /// <summary>
        /// Move to precise location from 0 - 360 deg  //0 - 1000
        /// </summary>
        /// <param name="value">Range: 0 - 1000 (0 - 360 degrees rotation)</param>
        public override void MoveTo(double value)
        {
            if (!_isMoving)
            {
                int val = (int)(value / 0.36);
                try
                {
                    // check operation mode for incremental
                    if (_operationMode != 1)
                        SetOperationMode(1);

                    if (val <= 1000)
                    {
                        SetPosition(val);

                        // make sure any other bits arent set in status register
                        ClearInputRegisterStructure();

                        // select position No.1
                        InputStatus.M0 = true;
                        // set start bit
                        InputStatus.START = true;
                        // update register (send)
                        UpdateInputRegister();

                        // generate movement start event
                        _isMoving = true;
                        Console.WriteLine("Actuator " + _slaveAddress.ToString() + " started");

                        if (onActuatorStart != null)
                            onActuatorStart(this);

                        // detect when actuator stops
                        CheckIfActuatorStopped();

                    }
                    else
                    {
                        Console.WriteLine("Inputed value is out of range");
                        if (onMessage != null)
                        {
                            onMessage(this, "Inputed value is out of range");
                        }
                    }
                }   // end of try
                catch
                {
                    if (onMessage != null)
                        onMessage(this, "Command 'MoveTo' could not be completed");

                    //throw new Exception("Command 'MoveTo' could not be completed");
                }
            }

        }

        /// <summary>
        /// Reset error/alarm
        /// </summary>
        public void ResetAlarm()
        {
            ClearInputRegisterStructure();

            WriteToRegister(0x10, 0x0180, 0, 1);
        }

        /// <summary>
        /// Stops any present rotation
        /// </summary>
        public override void Stop() 
        {
            if (_movementChecker == null)
            {
                StopRotation();
                //Delay(5);
            }
            _isMoving = false;
            if (onActuatorStop != null)
                onActuatorStop(this);
        }

        public override void MoveToHomePosition() 
        {
            //if (onMessage != null)
            //    onMessage(this, "-180 " + _isMoving.ToString());
            //MoveTo(-180);
            //if (onMessage != null)
            //    onMessage(this, _isMoving.ToString());
            //Delay(5);

            //if (onMessage != null)
            //    onMessage(this, "still " + _isMoving.ToString());
            //while (_isMoving) ;

            //if (onMessage != null)
            //    onMessage(this, "stoppped?");

            MoveTo(0);
            //Delay(500);
            
        }


        #endregion


        #region Events

        public override event ActuatorStartHanlder onActuatorStart;
        public override event ActuatorStopHanlder onActuatorStop;
        public override event ActuatorMessageHandler onMessage;
        private bool _initComplete;

        #endregion


        #region Private Methods

        private void CheckIfActuatorStopped()
        {
            if (_movementChecker != null)
                _movementChecker = null;

            _movementChecker = new Thread(new ThreadStart(CheckForActuatorStop));
            _movementChecker.Start();
            //_movementChecker.Join();
        }


        /// <summary>
        /// initialize routine
        /// </summary>
        /// <param name="slaveAddr">Slave address</param>
        private void ConstructorsRoutine(byte slaveAddr)
        {
            // set slave address
            _slaveAddress = slaveAddr;

            InputStatus = new DriverInputRegister();
            OutputStatus = new DriverOutputRegister();

             _comManager = new FxModbusPortManager();
        }


        /// <summary>
        /// Opens the COM port
        /// </summary>
        private void OpenCom()
        {   // check if port is not already open
            if (!_actuatorSerialPort.IsOpen)
            {
                try
                {
                    _actuatorSerialPort.Open();
                    _actuatorSerialPort.ReceivedBytesThreshold = 3;

                    Console.WriteLine("Connected to COM");
                    //generate event
                    if (onMessage != null)
                        onMessage(this, "COM port opened succesfully.");             
                }
                catch (Exception ex)
                {
                    if (onMessage != null)
                        onMessage(this, "Failed to open com port");

                   // throw new Exception(ex.Message);
                }

            }
            else
            {
                Console.WriteLine("Com port already open!");
                //generate error event
                if (onMessage != null)
                    onMessage(this, "Com port already open!");
            }
        }

 
        /// <summary>
        /// Closes the COM port
        /// </summary>
        private void CloseCom()
        {
            try
            {
                _actuatorSerialPort.Close();
            }
            catch
            {
                if (onMessage != null)
                    onMessage(this, "Serial port already closed");

                Console.WriteLine("Cannot close com port.");
            }

        }

        #region Modbus

        /// <summary>
        /// Sends a modbus message
        /// </summary>
        /// <param name="message">Message packet</param>
        private void SendModbusMessage(byte[] message)
        {
            Delay(5);
            _comManager.SendMessage(_actuatorSerialPort, message);
        }

        /// <summary>
        /// Write to single address register
        /// </summary>
        /// <param name="functionCode">To set single parameter, input 0x06</param>
        /// <param name="registerAddress">Address, in HEX</param>
        /// <param name="registerValue">Value</param>
        private void WriteToRegister(byte functionCode, int registerAddress, int registerValue)
        {
            // create Modbus instance for CRC checking
            CRC_Modbus CRC = new CRC_Modbus();

            // create Modbus message buffer
            byte[] modbus_bytes = new byte[8];

            // address
            int start_address_higer = registerAddress / 256;
            int start_address_lower = registerAddress % 256;
            // value
            int value_higer = registerValue / 256;
            int value_lower = registerValue % 256;

            modbus_bytes[0] = _slaveAddress;
            modbus_bytes[1] = functionCode;
            modbus_bytes[2] = (byte)start_address_higer;
            modbus_bytes[3] = (byte)start_address_lower;
            modbus_bytes[4] = (byte)value_higer;
            modbus_bytes[5] = (byte)value_lower;

            // generate CRC
            int crc_result = CRC.crc16(modbus_bytes, (modbus_bytes.Length - 2));
            byte[] crc_bytes = BitConverter.GetBytes(crc_result);
            // apply CRC to message
            modbus_bytes[modbus_bytes.Length - 2] = crc_bytes[1];
            modbus_bytes[modbus_bytes.Length - 1] = crc_bytes[0];

            SendModbusMessage(modbus_bytes);
           
            // get answer
            byte[] read_array = _comManager.ReceivedData;   
  
            if (read_array != null && read_array.Length > 4)
            {
                if(read_array[1] == 0x90)
                {
                    if(onMessage != null)
                    {
                        onMessage(this, read_array[2].ToString());
                    }
                }
            }
        

        }   // end of private void WriteToRegister

        /// <summary>
        /// Write to double address register
        /// </summary>
        /// <param name="functionCode">To set multiple parameters, input 0x10</param>
        /// <param name="registerAddress">Address, in HEX</param>
        /// <param name="registerValue1">1st register value</param>
        /// <param name="registerValue2">2nd register value</param>
        private void WriteToRegister(byte functionCode, int registerAddress, int registerValue1, int registerValue2)
        {
            // create Modbus instance for CRC checking
            CRC_Modbus CRC = new CRC_Modbus();

            // create Modbus message buffer
            byte[] modbus_bytes = new byte[13];

            // address
            int start_address_higer = registerAddress / 256;
            int start_address_lower = registerAddress % 256;
            // value 1 
            int value_higer1 = registerValue1 / 256;
            int value_lower1 = registerValue1 % 256;
            // value 2
            int value_higer2 = registerValue2 / 256;
            int value_lower2 = registerValue2 % 256;

            modbus_bytes[0] = _slaveAddress;
            modbus_bytes[1] = functionCode;
            modbus_bytes[2] = (byte)start_address_higer;
            modbus_bytes[3] = (byte)start_address_lower;
            modbus_bytes[4] = 0x00;
            modbus_bytes[5] = 0x02; // number of registers
            modbus_bytes[6] = 0x04; // twice the number of registers
            modbus_bytes[7] = (byte)value_higer1;
            modbus_bytes[8] = (byte)value_lower1;
            modbus_bytes[9] = (byte)value_higer2;
            modbus_bytes[10] = (byte)value_lower2;

            // generate CRC
            int crc_result = CRC.crc16(modbus_bytes, (modbus_bytes.Length - 2));
            byte[] crc_bytes = BitConverter.GetBytes(crc_result);
            // apply CRC to message
            modbus_bytes[modbus_bytes.Length - 2] = crc_bytes[1];
            modbus_bytes[modbus_bytes.Length - 1] = crc_bytes[0];

            SendModbusMessage(modbus_bytes);

            // get answer
            byte[] read_array = _comManager.ReceivedData; // ModbusResponse(8);

            if (read_array != null && read_array.Length > 4)
            {
                if (read_array[1] == 0x90)
                {
                    if (onMessage != null)
                    {
                        onMessage(this, read_array[2].ToString());
                    }
                }
            }

        }   // end of public void WriteToRegister


        /// <summary>
        /// Read value from register
        /// </summary>
        /// <param name="registerAddress">Address, in HEX</param>
        /// <param name="registerCount">How many registers to read (preferably 2)</param>
        /// <returns>int 32 signed value</returns>
        private int ReadRegister(int registerAddress, int registerCount)
        {
            // clear in buffer
            //_actuatorSerialPort.DiscardInBuffer();

            // create Modbus instance for CRC checking
            CRC_Modbus CRC = new CRC_Modbus();

            // create Modbus message buffer
            byte[] modbus_bytes = new byte[8];

            // address
            int start_address_higer = registerAddress / 256;
            int start_address_lower = registerAddress % 256;
            // count
            int value_higer = registerCount / 256;
            int value_lower = registerCount % 256;

            modbus_bytes[0] = _slaveAddress;
            modbus_bytes[1] = 0x03;
            modbus_bytes[2] = (byte)start_address_higer;
            modbus_bytes[3] = (byte)start_address_lower;

            modbus_bytes[4] = (byte)value_higer;
            modbus_bytes[5] = (byte)value_lower; // read x register(s)         

            // generate CRC
            int crc_result = CRC.crc16(modbus_bytes, (modbus_bytes.Length - 2));
            byte[] crc_bytes = BitConverter.GetBytes(crc_result);
            // apply CRC to message
            modbus_bytes[modbus_bytes.Length - 2] = crc_bytes[1];
            modbus_bytes[modbus_bytes.Length - 1] = crc_bytes[0];

            SendModbusMessage(modbus_bytes);

            // get answer
            byte[] read_array = _comManager.ReceivedData; // ModbusResponse((5 + 2 * registerCount));

            Int32 answer = 0;

            if (read_array != null && read_array.Length > 5)
                {
                    // make answer into a WORD value
                    answer += read_array[3];
                    answer = answer << 8;
                    answer += read_array[4];
            
                //add dword value if 2 registers were read
                if (registerCount > 1 && read_array.Length > 7)
                {
                    answer = answer << 8;
                    answer += read_array[5];
                    answer = answer << 8;
                    answer += read_array[6];
                }
            }

            return answer;
        }

#endregion

        /// <summary>
        /// Updates driver's output register @ 0x007F
        /// </summary>
        private void UpdateOutputRegister()
        {
            int outputByte = ReadRegister(0x007F, 0x0001);
            // upper byte
            OutputStatus.TLC = Convert.ToBoolean((outputByte >> 15) & 0x01) ? true : false;
            OutputStatus.END = Convert.ToBoolean((outputByte >> 14) & 0x01) ? true : false;
            OutputStatus.MOVE = Convert.ToBoolean((outputByte >> 13) & 0x01) ? true : false;
            OutputStatus.TIM = Convert.ToBoolean((outputByte >> 12) & 0x01) ? true : false;
            OutputStatus.AREA3 = Convert.ToBoolean((outputByte >> 11) & 0x01) ? true : false;
            OutputStatus.AREA2 = Convert.ToBoolean((outputByte >> 10) & 0x01) ? true : false;
            OutputStatus.AREA1 = Convert.ToBoolean((outputByte >> 9) & 0x01) ? true : false;
            OutputStatus.S_BSY = Convert.ToBoolean((outputByte >> 8) & 0x01) ? true : false;
            // lower byte
            OutputStatus.ALM = Convert.ToBoolean((outputByte >> 7) & 0x01) ? true : false;
            OutputStatus.WNG = Convert.ToBoolean((outputByte >> 6) & 0x01) ? true : false;
            OutputStatus.READY = Convert.ToBoolean((outputByte >> 5) & 0x01) ? true : false;
            OutputStatus.HOME_P = Convert.ToBoolean((outputByte >> 4) & 0x01) ? true : false;
            OutputStatus.START_R = Convert.ToBoolean((outputByte >> 3) & 0x01) ? true : false;
            OutputStatus.M2_R = Convert.ToBoolean((outputByte >> 2) & 0x01) ? true : false;
            OutputStatus.M1_R = Convert.ToBoolean((outputByte >> 1) & 0x01) ? true : false;
            OutputStatus.M0_R = Convert.ToBoolean(outputByte & 0x01) ? true : false;
        }

        /// <summary>
        /// Update driver's input register @ 0x007D, and send to driver
        /// </summary>
        private void UpdateInputRegister()
        {
            int packet = 0;
            // upper byte
            if (InputStatus.RVS) packet |= 0x8000;
            if (InputStatus.FWD) packet |= 0x4000;
            if (InputStatus.negJOG) packet |= 0x2000;
            if (InputStatus.posJOG) packet |= 0x1000;
            if (InputStatus.SSTART) packet |= 0x0800;
            if (InputStatus.MS2) packet |= 0x0400;
            if (InputStatus.MS1) packet |= 0x0200;
            if (InputStatus.MS1) packet |= 0x0100;
            // lower
            if (InputStatus.res) packet |= 0x0080;
            if (InputStatus.FREE) packet |= 0x0040;
            if (InputStatus.STOP) packet |= 0x0020;
            if (InputStatus.HOME) packet |= 0x0010;
            if (InputStatus.START) packet |= 0x0008;
            if (InputStatus.M2) packet |= 0x0004;
            if (InputStatus.M1) packet |= 0x0002;
            if (InputStatus.M0) packet |= 0x0001;
            // send to controller
            WriteToRegister(0x06, 0x007D, packet);

        }

        /// <summary>
        /// Gets current position
        /// </summary>
        /// <returns>Current position</returns>
        private int GetCurrentPosition()
        {
            return _currentPosition = ReadRegister(0x00CD, 0x0001);   // (position % 1000);
            //return _currentPosition = ReadRegister(0x0402, 0x0002);   // (position % 1000);
        }

        /// <summary>
        /// Set position step or absolute value, depending on operation mode (default is 0)
        /// </summary>
        /// <param name="value">Range: −8,388,608 to 8,388,607</param>
        private void SetPosition(int value)
        {
            WriteToRegister(0x10, 0x0402, ((value >> 16) & 0xFFFF), (value & 0xFFFF));
            _currentPosition = value;
        }

        /// <summary>
        /// Set operation speed [hz] (default is 1000)
        /// </summary>
        /// <param name="value">Range: 1 to 1,000,000</param>
        private void SetSpeed(int value)
        {
            WriteToRegister(0x10, 0x0482, ((value >> 16) & 0xFFFF), (value & 0xFFFF));
            _speed = value;
        }

        /// <summary>
        /// Reads current speed from register
        /// </summary>
        /// <returns>Speed value</returns>
        private int GetSpeed()
        {
            _speed = ReadRegister(0x0482, 0x02);
            return _speed;
        }

        /// <summary>
        /// Set acceleration [hz] (default is 1000)
        /// </summary>
        /// <param name="value">Range: 1 to 1,000,000</param>
        private void SetAcceleration(int value)
        {
            WriteToRegister(0x10, 0x0602, ((value >> 16) & 0xFFFF), (value & 0xFFFF));
            _acceleration = value;
        }

        /// <summary>
        /// Reads current acceleration from register
        /// </summary>
        /// <returns>Acceleration value</returns>
        private int GetAcceleration()
        {
            _acceleration = ReadRegister(0x0602, 0x02);
            return _acceleration;
        }

        /// <summary>
        /// Set decceleration [hz] (default is 1000)
        /// </summary>
        /// <param name="value">Range: 1 to 1,000,000</param>
        private void SetDecceleration(int value)
        {
            WriteToRegister(0x10, 0x0682, ((value >> 16) & 0xFFFF), (value & 0xFFFF));
            _decceleration = value;
        }

        /// <summary>
        /// Reads current decceleration from register
        /// </summary>
        /// <returns>Decceleration value</returns>
        private int GetDecceleration()
        {
            _decceleration = ReadRegister(0x0682, 0x02);
            return _decceleration;
        }

        /// <summary>
        /// Set operation mode (default is 0)
        /// </summary>
        /// <param name="mode">0 - incremental, 1 - absolute</param>
        private void SetOperationMode(byte mode)
        {
            if (mode == 1)
                _operationMode = 1;
            else
                _operationMode = 0;

            WriteToRegister(0x10, 0x0502, ((mode >> 16) & 0xFFFF), (mode & 0xFFFF));
        }

        /// <summary>
        /// Clear all input status registers structure
        /// </summary>
        private void ClearInputRegisterStructure()
        {
            // lower byte
            InputStatus.M0 = false;
            InputStatus.M1 = false;
            InputStatus.M2 = false;
            InputStatus.START = false;
            InputStatus.HOME = false;
            InputStatus.STOP = false;
            InputStatus.FREE = false;
            //DriverINPUT.res = false
            // upper byte
            InputStatus.MS0 = false;
            InputStatus.MS1 = false;
            InputStatus.MS2 = false;
            InputStatus.SSTART = false;
            InputStatus.posJOG = false;
            InputStatus.negJOG = false;
            InputStatus.FWD = false;
            InputStatus.RVS = false;
            
            // clear the register status
            WriteToRegister(0x06, 0x007D, 0);
        }

        /// <summary>
        /// Stop any rotation
        /// </summary>
        private void StopRotation()
        {
            ClearInputRegisterStructure();
            InputStatus.STOP = true;
            UpdateInputRegister();
        }

        /// <summary>
        /// Communication timeout
        /// </summary>
        /// <param name="timeout">Range: 0 to 10000 ms (default is 0)</param>
        private void SetCommunicationTimeout(int timeout)
        {
            WriteToRegister(0x10, 0x1200, ((timeout >> 16) & 0xFFFF), (timeout & 0xFFFF));
        }

        /// <summary>
        /// Continously checks if actuator has ended last operation
        /// </summary>
        private void CheckForActuatorStop()
        {
            UpdateOutputRegister();
            bool stopped = false;
            int interval = (DateTime.Now.Second + 5) - 60;
            

            while((DateTime.Now.Second - 60) < interval)
            {
                //if (onMessage != null)
                //    onMessage(this, "OutputStatus.END: " + OutputStatus.END.ToString());

                if (!OutputStatus.END)
                {
                    Delay(3);
                    UpdateOutputRegister();
                }
                else
                {
                    stopped = true;
                    break;
                }

                //Delay(10);
                //stopped = true;
                //break;

            }

            _isMoving = false;

            if(stopped)
            {
                if (onActuatorStop != null)
                    onActuatorStop(this);

                Console.WriteLine("Actuator " + _slaveAddress.ToString() + " stopped");
            }
            else
            {
                if (onActuatorStop != null)
                    onActuatorStop(this);

                if (onMessage != null)
                    onMessage(this, "Failed to move actuator: " + _slaveAddress.ToString());
            }


            //if (_movementChecker != null && _movementChecker.IsAlive)
            //    _movementChecker.Join(200);
        }

        /// <summary>
        /// Delay function
        /// </summary>
        /// <param name="time">value in ms</param>
        private void Delay(int time)
        {
            System.Threading.Thread.Sleep(time);
        }

        #endregion


    }



}
