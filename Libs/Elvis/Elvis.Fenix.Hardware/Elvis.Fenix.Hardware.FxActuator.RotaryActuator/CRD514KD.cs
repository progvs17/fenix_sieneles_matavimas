﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO.Ports;
using System.ComponentModel;
using System.Xml;
using System.Xml.Serialization;
using System.Threading;

using CRC;
using Elvis.Fenix.IO;
using Elvis.Fenix.Hardware;

namespace Elvis.Fenix.Hardware.FxActuators
{
    /// <summary>
    /// CRD-514-KD driver class
    /// </summary>
    public class CRD514KD : FxActuator
    {

        #region Private Members

        private SerialPort _actuatorSerialPort;
        private byte _slaveAddress;
        private string _status = "NaN";
        private bool _isMoving = false;
        private bool _homePositioningOnBootDone = false;
        private bool _homesDetected = false;
        private int _homePositioningStep = 30;
        private bool _initComplete = false;

        int _currentPosition = 0;
        int _accel;// = 30000;// = 1000;
        int _deccel;// = 30000;// = 1000;
        int _speed;// = 10000;// = 8000;
        private double _maxPosition = 110;

        FxModbusPortManager _comManager;
        Thread _movementChecker = null;

        #endregion


        #region Public Members

        public int HomeOffset = 0;
        public bool HomeingSuccessful = true;

        // output register structure 0x001E
        [StructLayout(LayoutKind.Explicit, Size = 16)]
        public class CommandRegisters
        {
            [FieldOffset(0)]
            public bool M0;
            [FieldOffset(1)]
            public bool M1;
            [FieldOffset(2)]
            public bool M2;
            [FieldOffset(3)]
            public bool M3;
            [FieldOffset(4)]
            public bool M4;
            [FieldOffset(5)]
            public bool M5;
            [FieldOffset(6)]
            public bool res6;
            [FieldOffset(7)]
            public bool res7;
            [FieldOffset(8)]
            public bool START;
            [FieldOffset(9)]
            public bool FWD;
            [FieldOffset(10)]
            public bool RVS;
            [FieldOffset(11)]
            public bool HOME;
            [FieldOffset(12)]
            public bool STOP;
            [FieldOffset(13)]
            public bool C_ON;
            [FieldOffset(14)]
            public bool res14;
            [FieldOffset(15)]
            public bool res15;
        }

        // status condition structure 0x0020
        [StructLayout(LayoutKind.Explicit, Size = 16)]
        public class StatusRegisters
        {
            [FieldOffset(0)]
            public bool M0_R;
            [FieldOffset(1)]
            public bool M1_R;
            [FieldOffset(2)]
            public bool M2_R;
            [FieldOffset(3)]
            public bool M3_R;
            [FieldOffset(4)]
            public bool M4_R;
            [FieldOffset(5)]
            public bool M5_R;
            [FieldOffset(6)]
            public bool WNG;
            [FieldOffset(7)]
            public bool ALM;
            [FieldOffset(8)]
            public bool START_R;
            [FieldOffset(9)]
            public bool STEPOUT;
            [FieldOffset(10)]
            public bool MOVE;
            [FieldOffset(11)]
            public bool HOME_P;
            [FieldOffset(12)]
            public bool res12;
            [FieldOffset(13)]
            public bool READY;
            [FieldOffset(14)]
            public bool res14;
            [FieldOffset(15)]
            public bool AREA;
        }

        #endregion


        #region Public Properties

        // <summary>
        /// Used to assign already open/predefined serial port
        /// </summary>
        [Browsable(false), XmlIgnore]
        public override SerialPort ComPort
        {
            get { return _actuatorSerialPort; }
            set { _actuatorSerialPort = value; }
        }

        /// <summary>
        /// Driver inputs
        /// </summary>
        [Browsable(false), XmlIgnore]
        public CommandRegisters CommandRegister { get; set; }

        /// <summary>
        /// Driver outputs
        /// </summary>
        [Browsable(false), XmlIgnore]
        public StatusRegisters StatusRegister { get; set; }

        /// <summary>
        /// Monitors the command position.
        /// </summary>
        [Description("Current position")]
        public override double CurrentPosition 
        {
            get 
            {
                if (!IsMoving)
                    return (-0.036 * GetPosition()) + HomeOffset; // !_isMoving ? (-0.036 * GetPosition()) : (-0.036 * _currentPosition); 
                else
                    return (-0.036 * _currentPosition) + HomeOffset;
            }
            set 
            {
                if ((-0.036 * _currentPosition) != value)
                    MoveTo(value); 
            }
        }

        /// <summary>
        /// Not implemented for usage
        /// </summary>
        [Description("Maximum position in positive & negative directions")]
        public override double MaxPosition 
        {
            get { return _maxPosition; }
            set { _maxPosition = value; } 
        }

        /// <summary>
        /// Sets/gets motor speed
        /// <para> Default is 1000 [Hz]. </para>
        /// <para> Range: 1 to 500,000. </para>
        /// </summary>
        [Description("Default is 1000 [Hz]. Range: 1 to 500,000.")]
        public override int Speed 
        {
            get { return GetSpeed(); }// !_isMoving ? GetSpeed() : _speed; }
            set 
            {
                if (_speed != value && value > 0)
                    SetSpeed(value); 
            }
        }

        /// <summary>
        /// Sets/gets acceleration
        /// <para> Default is 30.000. (30000). </para>
        /// <para> Range: 0.001 to 1000.000 ms/kHz. </para>
        /// </summary>
        [Description("Default is 30.000. (30000). Range: 0.001 to 1000.000 ms/kHz.")]
        public override int Acceleration 
        {
            get { return GetAcceleration(); } // !_isMoving ? GetAcceleration() : _accel; }
            set 
            {
                if (_accel != value && value > 0)
                    SetAcceleration(value);
            }
        }

        /// <summary>
        /// Sets/gets decceleration.
        /// <para> Default is 30.000. (30000) </para>
        /// <para> Range: 0.001 to 1000.000 ms/kHz. </para>
        /// </summary>
        [Description("Default is 30.000. (30000). Range: 0.001 to 1000.000 ms/kHz.")]
        public override int Decceleration
        {
            get { return GetDecceleration(); } // !_isMoving ? GetDecceleration() : _deccel; }
            set 
            {
                if (_deccel != value && value > 0)
                    SetDecceleration(value); 
            }
        }

        public override bool InitComplete
        {
            get { return _initComplete; }
        }

        /// <summary>
        /// Status about motor movement. True - moving, false - not moving
        /// </summary>
        [Description("Motor movement status")]
        public override bool IsMoving
        {
            get
            {
                return _isMoving;
            }
        }

        /// <summary>
        /// Device's connection status
        /// </summary>`
        [Description("Device's connection status")]
        public override bool IsConnected 
        {
            get 
            {
                if (!StatusRegister.READY)
                    ResetAlarm();

                UpdateStatusRegister();

                if (_actuatorSerialPort.IsOpen && Status == "Ready") // && StatusRegister.READY)
                    return true;
                else
                    return false; 
            }
        }

        /// <summary>
        /// Indicates driver's status - Ready, Error
        /// </summary>
        [Description("Indicates driver's status")]
        public override string Status 
        {
            get
            {
                if (!_isMoving)
                    UpdateStatusRegister();

                if (!StatusRegister.ALM && !StatusRegister.WNG) // && StatusRegister.READY)
                    _status = "Ready";
                else
                {
                    _status = "Error";

                    ClearCommunicationErrors();
                    //try
                    //{
                    //    ResetAlarm();
                    //}
                    //catch { }
                }

                return _status;
            }
        }

        #endregion


        #region Constructors

        /// <summary>
        /// Empty constructor
        /// </summary>
        public CRD514KD()
        {}

        /// <summary>
        /// Creates CRD-514-KD rotary actuator control object with existing Serial port and inputed slave address.
        /// Make sure existing serial port is already open before assigning. If it is, you do not need to connect this device again.
        /// </summary>
        /// <param name="comPort">Already open/existing serial port</param>
        /// <param name="slaveAddr">Slave address</param>
        public CRD514KD(string name, SerialPort comPort, byte slaveAddr)
        {
            Name = name;
            // assign COM port
            _actuatorSerialPort = comPort;

            ConstructorsRoutine(slaveAddr);
        }

        /// <summary>
        /// Creates CRD-514-KD  rotary actuator control object with inputed serial port, slave address and default baudrate(19200)
        /// </summary>
        /// <param name="comPort">Com port name</param>
        /// <param name="slaveAddr">Slave address</param>
        public CRD514KD(string name, string comPort, byte slaveAddr)
        {
            Name = name;
            int baudRate = 19200; // 19200;
            // assign COM port
            _actuatorSerialPort = new SerialPort(comPort, baudRate, Parity.Even, 8, StopBits.One);
            
            ConstructorsRoutine(slaveAddr);
        }

        /// <summary>
        /// Creates CRD-514-KD  rotary actuator control object with inputed serial port name, baudrate and slave address
        /// </summary>
        /// <param name="comPort">Com port name</param>
        /// <param name="baudRate">Custom baud rate (default is 19200)</param>
        /// <param name="slaveAddr">Slave address</param>
        public CRD514KD(string name, string comPort, int baudRate, byte slaveAddr)
        {
            Name = name;
            // assign COM port
            _actuatorSerialPort = new SerialPort(comPort, baudRate, Parity.Even, 8, StopBits.One);
         
            ConstructorsRoutine(slaveAddr);
           
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Connects to device
        /// </summary>
        public override void Connect()
        {
            OpenCom();
        }

        /// <summary>
        /// Disconnects device
        /// </summary>
        public override void Disconnect()
        {
            CloseCom();
        }

        /// <summary>
        /// Driver initialization. Must be done on every power up!
        /// </summary>
        public override void Init()
        {
            _initComplete = false;

            Delay(100);

            if (onMessage != null)
                onMessage(this, "Configure sequence started.");

            ResetAlarm();

            ConfigureSequence();

            MoveToHomePosition();
            //Delay(999);
            //while (_isMoving) ;

            if (onMessage != null)
                onMessage(this, "Configure sequence completed!");

            _initComplete = true;
        }

        /// <summary>
        /// Move by the amount inputed
        /// </summary>
        /// <param name="value">Moving amount, can be positive or negative values (for rotating backwards)</param>
        public override void Move(double value) 
        {
            //double desiredPosition = 0;
            // check if inputed step is positive or negative
            if (value >= 0)
            {
                value = CurrentPosition + value >= MaxPosition ? 0 : value;
                //value = desiredPosition >= MaxPosition ? MaxPosition : desiredPosition; // over positive
            }
            else
            {
                value = CurrentPosition - value <= -MaxPosition ? 0 : value;
                //desiredPosition = CurrentPosition - Math.Abs(value);
                //value = desiredPosition <= -MaxPosition ? MaxPosition : desiredPosition; // below negative
            }

            if (!_isMoving) 
            {
                // set poz
                int val = (int)(value / (-0.036));

                val = val + HomeOffset;

                SetPosition(val);
                // send rotate command
                RotateActuator();

                // start waiting for actuator stop
                CheckIfActuatorStopped();
            }
        }

        /// <summary>
        /// Moves to position specified
        /// </summary>
        /// <param name="value">Absolute position value</param>
        public override void MoveTo(double value) 
        {
            value = value >= MaxPosition ? MaxPosition : value; // over positive
            value = value <= -MaxPosition ? MaxPosition : value; // below negative

            if (!_isMoving)
            {
                //int val = (int)(value / 0.36);

                int val = (int)((value - CurrentPosition) / (-0.036));

                val = val + HomeOffset;
                // set position
                //SetPosition((int)(value - CurrentPosition));
                SetPosition(val);

                // send rotate command
                RotateActuator();

                // start waiting for actuator stop
                CheckIfActuatorStopped();
            }
            
        }

        /// <summary>
        /// Stops any present rotation
        /// </summary>
        public override void Stop()
        {
            if (_movementChecker == null)
            {
                CommandRegister.START = false;
                UpdateCommandRegister();

                StopRotation();
            }

            //Delay(5);
            _isMoving = false;
            if (onActuatorStop != null)
                onActuatorStop(this);
        }

        /// <summary>
        /// Returns to home position
        /// </summary>
        public override void MoveToHomePosition()
        {
            bool _default = true;

            HomeingSuccessful = false;

            ClearCommandRegister();

            if(!_homePositioningOnBootDone)
            {
                if (_default)
                {
                    _homePositioningStep = 3;
                    SetHomeOperationDirection(1);
                    SetHomeOperationOffset(40);
                }
                else
                {
                    _homePositioningStep = -3;
                    SetHomeOperationDirection(0);
                    SetHomeOperationOffset(-40);
                }

                // check for home signal clockwise
                for (int h = 0; h < 34; h++)
                {
                    ReadIOStatus();

                    if (!_homesDetected)
                    {
                        Delay(5); // 20
                        Move(_homePositioningStep);
                        while (IsMoving) ;
                    }
                    else
                    {                 
                        break;
                    }
                }

                // if home signal not found, check counter clockwise
                if(!_homesDetected)
                {
                    Move(-105);
                    while (IsMoving) ;

                    _homePositioningStep = -3;

                    for (int h = 0; h < 34; h++)
                    {
                        ReadIOStatus();

                        if (!_homesDetected)
                        {
                            Delay(5); // 20
                            Move(_homePositioningStep);
                            while (IsMoving) ;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                    
            }

            if (_homesDetected)
            {
                if (CurrentPosition < 0 && _homePositioningOnBootDone)
                {
                    SetHomeOperationDirection(1);
                    SetHomeOperationOffset(40); //200);
                }
                else if (CurrentPosition > 0 && _homePositioningOnBootDone)
                {
                    SetHomeOperationDirection(0);
                    SetHomeOperationOffset(-40); // - 200);
                }

                SetCONBit();
                // enable start
                CommandRegister.C_ON = true;
                CommandRegister.HOME = true;
                UpdateCommandRegister();
                // disable start
                CommandRegister.HOME = false;
                UpdateCommandRegister();

                if (!_homePositioningOnBootDone) _homePositioningOnBootDone = true;

                HomeingSuccessful = true;
            }
            else
            {
                // error??
                Move(105);
                while (IsMoving) ;
            }
        }

        #endregion


        #region Events

        public override event ActuatorStartHanlder onActuatorStart;
        public override event ActuatorStopHanlder onActuatorStop;
        public override event ActuatorMessageHandler onMessage;
        

        #endregion


        #region Private Methods

        private void CheckIfActuatorStopped()
        {
            if (_movementChecker != null)
                _movementChecker = null;

            _movementChecker = new Thread(new ThreadStart(CheckForActuatorStop));
            _movementChecker.Start();
            //_movementChecker.Join();
        }

        /// <summary>
        /// initialize routine
        /// </summary>
        /// <param name="slaveAddr">Slave address</param>
        private void ConstructorsRoutine(byte slaveAddr)
        {
            // set slave address
            _slaveAddress = slaveAddr;

            CommandRegister = new CommandRegisters();
            StatusRegister = new StatusRegisters();

            _comManager = new FxModbusPortManager();

            //ComPort.DataReceived += ComPort_DataReceived;
        }

        /// <summary>
        /// Opens the COM port
        /// </summary>
        private void OpenCom()
        {   // check if port is not already open
            if (!_actuatorSerialPort.IsOpen)
            {
                try
                {
                    _actuatorSerialPort.Open();
                    _actuatorSerialPort.ReceivedBytesThreshold = 3;

                    Console.WriteLine("Connected to COM");
                    //generate event
                    if (onMessage != null)
                        onMessage(this, "COM port opened succesfully.");
                }
                catch (Exception ex)
                {
                    if (onMessage != null)
                        onMessage(this, "Failed to open com port!");
                    //throw new Exception(ex.Message);
                }
            }   // end of if
            else
            {
                Console.WriteLine("Com port already open.");
                //generate error event
                if (onMessage != null)
                    onMessage(this, "Com port already open.");
            }
        }

        /// <summary>
        /// Closes the COM port
        /// </summary>
        private void CloseCom()
        {
            try
            {
                _actuatorSerialPort.Close();
            }
            catch
            {
                if (onMessage != null)
                    onMessage(this, "Serial port already closed");

                Console.WriteLine("Cannot close com port.");
            }

        }


        #region Modbus

        /// <summary>
        /// Sends a modbus message
        /// </summary>
        /// <param name="message">Message packet</param>
        private void SendModbusMessage(byte[] message)
        {
            Delay(5);
            _comManager.SendMessage(_actuatorSerialPort, message);
        }
 
        /// <summary>
        /// Write to single address register
        /// </summary>
        /// <param name="functionCode">To set single parameter, input 0x06</param>
        /// <param name="registerAddress">Address, in HEX</param>
        /// <param name="registerValue">Value</param>
        private void WriteToRegister(byte functionCode, int registerAddress, int registerValue)
        {
            // create Modbus instance for CRC checking
            CRC_Modbus CRC = new CRC_Modbus();

            // create Modbus message buffer
            byte[] modbus_bytes = new byte[8];

            // address
            int start_address_higer = registerAddress / 256;
            int start_address_lower = registerAddress % 256;
            // value
            int value_higer = registerValue / 256;
            int value_lower = registerValue % 256;

            modbus_bytes[0] = _slaveAddress;
            modbus_bytes[1] = functionCode;
            modbus_bytes[2] = (byte)start_address_higer;
            modbus_bytes[3] = (byte)start_address_lower;
            modbus_bytes[4] = (byte)value_higer;
            modbus_bytes[5] = (byte)value_lower;

            // generate CRC
            int crc_result = CRC.crc16(modbus_bytes, (modbus_bytes.Length - 2));
            byte[] crc_bytes = BitConverter.GetBytes(crc_result);
            // apply CRC to message
            modbus_bytes[modbus_bytes.Length - 2] = crc_bytes[1];
            modbus_bytes[modbus_bytes.Length - 1] = crc_bytes[0];

            SendModbusMessage(modbus_bytes);

            // get answer
            byte[] read_array = _comManager.ReceivedData; //modbusResponse; // ModbusResponse(8);

        }   // end of private void WriteToRegister

        /// <summary>
        /// Write to double address register
        /// </summary>
        /// <param name="functionCode">To set multiple parameters, input 0x10</param>
        /// <param name="registerAddress">Address, in HEX</param>
        /// <param name="registerValue1">1st register value</param>
        /// <param name="registerValue2">2nd register value</param>
        private void WriteToRegister(byte functionCode, int registerAddress, int registerValue1, int registerValue2)
        {
            // create Modbus instance for CRC checking
            CRC_Modbus CRC = new CRC_Modbus();

            // create Modbus message buffer
            byte[] modbus_bytes = new byte[13];

            // address
            int start_address_higer = registerAddress / 256;
            int start_address_lower = registerAddress % 256;
            // value 1 
            int value_higer1 = registerValue1 / 256;
            int value_lower1 = registerValue1 % 256;
            // value 2
            int value_higer2 = registerValue2 / 256;
            int value_lower2 = registerValue2 % 256;

            modbus_bytes[0] = _slaveAddress;
            modbus_bytes[1] = functionCode;
            modbus_bytes[2] = (byte)start_address_higer;
            modbus_bytes[3] = (byte)start_address_lower;
            modbus_bytes[4] = 0x00;
            modbus_bytes[5] = 0x02; // number of registers
            modbus_bytes[6] = 0x04; // twice the number of registers
            modbus_bytes[7] = (byte)value_higer1;
            modbus_bytes[8] = (byte)value_lower1;
            modbus_bytes[9] = (byte)value_higer2;
            modbus_bytes[10] = (byte)value_lower2;

            // generate CRC
            int crc_result = CRC.crc16(modbus_bytes, (modbus_bytes.Length - 2));
            byte[] crc_bytes = BitConverter.GetBytes(crc_result);
            // apply CRC to message
            modbus_bytes[modbus_bytes.Length - 2] = crc_bytes[1];
            modbus_bytes[modbus_bytes.Length - 1] = crc_bytes[0];

            SendModbusMessage(modbus_bytes);

            // get answer
            byte[] read_array = _comManager.ReceivedData; // ModbusResponse(8);

        }   // end of public void WriteToRegister

        /// <summary>
        /// Read value from register
        /// </summary>
        /// <param name="registerAddress">Address, in HEX</param>
        /// <param name="registerCount">How many registers to read (preferably 2)</param>
        /// <returns>WORD type value</returns>
        private int ReadRegister(int registerAddress, int registerCount)
        {

            // create Modbus instance for CRC checking
            CRC_Modbus CRC = new CRC_Modbus();

            // create Modbus message buffer
            byte[] modbus_bytes = new byte[8];

            // address
            int start_address_higer = registerAddress / 256;
            int start_address_lower = registerAddress % 256;
            // count
            int value_higer = registerCount / 256;
            int value_lower = registerCount % 256;

            modbus_bytes[0] = _slaveAddress;
            modbus_bytes[1] = 0x03;
            modbus_bytes[2] = (byte)start_address_higer;
            modbus_bytes[3] = (byte)start_address_lower;

            modbus_bytes[4] = (byte)value_higer;
            modbus_bytes[5] = (byte)value_lower; // read x register(s)         

            // generate CRC
            int crc_result = CRC.crc16(modbus_bytes, (modbus_bytes.Length - 2));
            byte[] crc_bytes = BitConverter.GetBytes(crc_result);
            // apply CRC to message
            modbus_bytes[modbus_bytes.Length - 2] = crc_bytes[1];
            modbus_bytes[modbus_bytes.Length - 1] = crc_bytes[0];

            SendModbusMessage(modbus_bytes);

            // get answer
            byte[] read_array = _comManager.ReceivedData; // ModbusResponse((5 + 2 * registerCount));

            Int32 answer = 0;

            if (read_array != null && read_array.Length > 5)
            {
                // make answer into a WORD value
                answer += read_array[3];
                answer = answer << 8;
                answer += read_array[4];

                //add dword value if 2 registers were read
                if (registerCount > 1 && read_array.Length > 7)
                {
                    answer = answer << 8;
                    answer += read_array[5];
                    answer = answer << 8;
                    answer += read_array[6];
                }
            }

            return answer;
        }

        #endregion


        /// <summary>
        /// Set position step or absolute value, depending on operation mode (default is 0)
        /// </summary>
        /// <param name="value">Range: −8,388,608 to 8,388,607</param>
        private void SetPosition(int value)
        {
            WriteToRegister(0x10, 0x0402, ((value >> 16) & 0xFFFF), (value & 0xFFFF));
            _currentPosition = value;
        }

        /// <summary>
        /// Monitors the command position.  
        /// </summary>
        /// <returns>−2,147,483,648 to 2,147,483,647 step</returns>
        private int GetPosition()
        {
            return _currentPosition = ReadRegister(0x0118, 2);
        }

        /// <summary>
        /// Set operation speed [hz] (default is 1000)
        /// </summary>
        /// <param name="value">Range: 1 to 500,000</param>
        private void SetSpeed(int value)
        {
            WriteToRegister(0x10, 0x0502, ((value >> 16) & 0xFFFF), (value & 0xFFFF));
            _speed = value;
        }

        /// <summary>
        /// Reads current speed from register
        /// </summary>
        /// <returns>Speed value</returns>
        private int GetSpeed()
        {
            return _speed = ReadRegister(0x0502, 0x02);
        }

        /// <summary>
        /// Set acceleration [hz] (default is 30.000)
        /// </summary>
        /// <param name="value">Range: 0.001 to 1000.000 ms/kHz</param>
        private void SetAcceleration(int value)
        {
            WriteToRegister(0x10, 0x0902, ((value >> 16) & 0xFFFF), (value & 0xFFFF));
            _accel = value;
        }

        /// <summary>
        /// Reads current acceleration from register
        /// </summary>
        /// <returns>Acceleration value</returns>
        private int GetAcceleration()
        {
            return _accel = ReadRegister(0x0902, 0x02);
        }

        /// <summary>
        /// Set decceleration [hz] (default is 30.000)
        /// </summary>
        /// <param name="value">Range: 0.001 to 1000.000 ms/kHz</param>
        private void SetDecceleration(int value)
        {
            WriteToRegister(0x10, 0x0A02, ((value >> 16) & 0xFFFF), (value & 0xFFFF));
            _deccel = value;
        }

        /// <summary>
        /// Reads current decceleration from register
        /// </summary>
        /// <returns>Decceleration value</returns>
        private int GetDecceleration()
        {
            return _deccel = ReadRegister(0x0A02, 0x02);
        }


        /// <summary>
        /// Set operation mode (default is 0)
        /// </summary>
        /// <param name="mode">0 - incremental, 1 - absolute</param>
        private void SetOperationMode(byte mode)
        {         
            WriteToRegister(0x06, 0x0601, mode);
            //WriteToRegister(0x10, 0x0601, ((mode >> 16) & 0xFFFF), (mode & 0xFFFF));
        }

        /// <summary>
        /// starting direction of home-seeking
        /// </summary>
        /// <param name="direction">0 - negative, 1 - positive</param>
        private void SetHomeOperationDirection(byte direction)
        {
            WriteToRegister(0x06, 0x0242, direction);
            //WriteToRegister(0x10, 0x0601, ((mode >> 16) & 0xFFFF), (mode & 0xFFFF));
        }

        /// <summary>
        /// Offset step
        /// </summary>
        /// <param name="offset"></param>
        private void SetHomeOperationOffset(int offset)
        {
            WriteToRegister(0x10, 0x0240, ((offset >> 16) & 0xFFFF), (offset & 0xFFFF));
        }

        /// <summary>
        /// Update Command1 registers
        /// </summary>
        private void UpdateCommandRegister()
        {
            int packet = 0;
            // upper
            //if (CommandRegister.res15) packet |= 0x8000;
            //if (CommandRegister.res14) packet |= 0x4000;
            if (CommandRegister.C_ON) packet |= 0x2000;
            if (CommandRegister.STOP) packet |= 0x1000;
            if (CommandRegister.HOME) packet |= 0x0800;
            if (CommandRegister.RVS) packet |= 0x0400;
            if (CommandRegister.FWD) packet |= 0x0200;
            if (CommandRegister.START) packet |= 0x0100;
            // lower
            //if (CommandRegister.res7) packet |= 0x0080;
            //if (CommandRegister.res6) packet |= 0x0040;
            if (CommandRegister.M5) packet |= 0x0020;
            if (CommandRegister.M4) packet |= 0x0010;
            if (CommandRegister.M3) packet |= 0x0008;
            if (CommandRegister.M2) packet |= 0x0004;
            if (CommandRegister.M1) packet |= 0x0002;
            if (CommandRegister.M0) packet |= 0x0001;

            // send to controller
            WriteToRegister(0x06, 0x001E, packet);

        }

        /// <summary>
        /// Update Status1 registers
        /// </summary>
        private void UpdateStatusRegister()
        {
            int outputByte = ReadRegister(0x0020, 0x0001);

            // upper byte
            StatusRegister.AREA = Convert.ToBoolean((outputByte >> 15) & 0x01) ? true : false;
            StatusRegister.res14 = Convert.ToBoolean((outputByte >> 14) & 0x01) ? true : false;
            StatusRegister.READY = Convert.ToBoolean((outputByte >> 13) & 0x01) ? true : false;
            StatusRegister.res12 = Convert.ToBoolean((outputByte >> 12) & 0x01) ? true : false;
            StatusRegister.HOME_P = Convert.ToBoolean((outputByte >> 11) & 0x01) ? true : false;
            StatusRegister.MOVE = Convert.ToBoolean((outputByte >> 10) & 0x01) ? true : false;
            StatusRegister.STEPOUT = Convert.ToBoolean((outputByte >> 9) & 0x01) ? true : false;
            StatusRegister.START_R = Convert.ToBoolean((outputByte >> 8) & 0x01) ? true : false;
            // lower byte
            StatusRegister.ALM = Convert.ToBoolean((outputByte >> 7) & 0x01) ? true : false;
            StatusRegister.WNG = Convert.ToBoolean((outputByte >> 6) & 0x01) ? true : false;
            StatusRegister.M5_R = Convert.ToBoolean((outputByte >> 5) & 0x01) ? true : false;
            StatusRegister.M4_R = Convert.ToBoolean((outputByte >> 4) & 0x01) ? true : false;
            StatusRegister.M3_R = Convert.ToBoolean((outputByte >> 3) & 0x01) ? true : false;
            StatusRegister.M2_R = Convert.ToBoolean((outputByte >> 2) & 0x01) ? true : false;
            StatusRegister.M1_R = Convert.ToBoolean((outputByte >> 1) & 0x01) ? true : false;
            StatusRegister.M0_R = Convert.ToBoolean(outputByte & 0x01) ? true : false;
        }

        private void ReadIOStatus()
        {
            int inputOutputByte = ReadRegister(0x0133, 0x0002);

            if (inputOutputByte.Equals(0))
                return;

            int check = inputOutputByte & 0x2000;
            _homesDetected = check.Equals(0x2000) ? true : false;
        }

        // <summary>
        /// Clear all driver Command1 registers
        /// </summary>
        private void ClearCommandRegister()
        {
            // clear all structure
            CommandRegister.C_ON = false;
            CommandRegister.M0 = false;
            CommandRegister.M1 = false;
            CommandRegister.M2 = false;
            CommandRegister.M3 = false;
            CommandRegister.M4 = false;
            CommandRegister.M5 = false;
            CommandRegister.START = false;
            CommandRegister.HOME = false;
            CommandRegister.STOP = false;
            CommandRegister.FWD = false;
            CommandRegister.RVS = false;

            // send to controller
            WriteToRegister(0x06, 0x001E, 0x0000);
        }

        /// <summary>
        /// Set C-ON bit
        /// </summary>
        private void SetCONBit()
        {
            ClearCommandRegister();
            CommandRegister.C_ON = true;
            UpdateCommandRegister();
        }

        /// <summary>
        /// Enables rotation
        /// </summary>
        private void RotateActuator()
        {
            SetCONBit();
            // enable start
            CommandRegister.C_ON = true;
            CommandRegister.START = true;
            CommandRegister.M0 = true;
            UpdateCommandRegister();
            // disable start
            CommandRegister.START = false;
            UpdateCommandRegister();

            // generate movement start event
            _isMoving = true;
            Console.WriteLine("Actuator " + _slaveAddress.ToString() + " started");
            if (onActuatorStart != null)
                onActuatorStart(this);

        }

        /// <summary>
        /// Stop any rotation
        /// </summary>
        private void StopRotation()
        {
            CommandRegister.STOP = true;
            UpdateCommandRegister();
            // clear register
            ClearCommandRegister();
        }

        /// <summary>
        /// Set controller to communicate via Modbus
        /// </summary>
        private void ConfigureSequence()
        {
            // set inputs control by RS-485
            WriteToRegister(0x06, 0x0200, 0);

            WriteToRegister(0x06, 0x0202, 0);

            WriteToRegister(0x06, 0x0204, 0);

            WriteToRegister(0x06, 0x020A, 0);

            WriteToRegister(0x06, 0x020B, 0);

            WriteToRegister(0x06, 0x020C, 0);

            WriteToRegister(0x06, 0x020D, 0);

            // added
            WriteToRegister(0x06, 0x031A, 500);
            WriteToRegister(0x06, 0x031B, 1000);
            WriteToRegister(0x06, 0x031C, 10);

        }


        void ClearCommunicationErrors()
        {
            WriteToRegister(0x06, 0x0049, 1);
        }

        /// <summary>
        /// Reset error/alarm
        /// </summary>
        public void ResetAlarm()
        {
            //ClearInputRegisterStructure();

            WriteToRegister(0x06, 0x0040, 1);
            WriteToRegister(0x06, 0x0041, 1);
            WriteToRegister(0x06, 0x0042, 1);
            WriteToRegister(0x06, 0x0049, 1);

            Delay(50);
  
            WriteToRegister(0x06, 0x0040, 0);
            WriteToRegister(0x06, 0x0041, 0);
            WriteToRegister(0x06, 0x0042, 0);
            WriteToRegister(0x06, 0x0049, 0);
        }

        /// <summary>
        /// Continously checks if actuator has ended last operation
        /// </summary>
        private void CheckForActuatorStop()
        {

            UpdateStatusRegister();
            bool stopped = false;
            int interval = (DateTime.Now.Second + 5) - 60;

            while ((DateTime.Now.Second - 60) < interval)
            {
               // if (!_homePositioningOnBootDone) ReadIOStatus();

                if (!StatusRegister.READY)
                {
                    Delay(3);
                    UpdateStatusRegister();
                }
                else
                {
                    stopped = true;
                    break;
                }
            }

            if (stopped)
            {
                if (onActuatorStop != null)
                    onActuatorStop(this);

                Console.WriteLine("Actuator " + _slaveAddress.ToString() + " stopped");
            }
            else
            {
                if (onActuatorStop != null)
                    onActuatorStop(this);

                if (onMessage != null)
                    onMessage(this, "Failed to move actuator: " + _slaveAddress.ToString());
            }

            _isMoving = false;

            //if (_movementChecker != null && _movementChecker.IsAlive)
            //    _movementChecker.Join(200);

            //if(_homePositioningOnBootDone) _isMoving = false;
        }


        /// <summary>
        /// Delay function
        /// </summary>
        /// <param name="time">value in ms</param>
        private void Delay(int time)
        {
            System.Threading.Thread.Sleep(time);
        }

        #endregion

        public void Reset()
        {
            WriteToRegister(0x06, 0x0046, 1);
            Thread.Sleep(500);
            WriteToRegister(0x06, 0x0046, 0);
        }


        private void WriteParametersToRom()
        {

            WriteToRegister(0x06, 0x0045, 1);
            Delay(500);
            WriteToRegister(0x06, 0x0045, 0);
        }

        private void ResetAllDataInRom()
        {

            WriteToRegister(0x06, 0x0046, 1);
            Delay(500);
            WriteToRegister(0x06, 0x0046, 0);
        }

    }


}
