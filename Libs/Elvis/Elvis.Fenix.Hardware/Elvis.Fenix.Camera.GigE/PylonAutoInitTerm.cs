﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elvis.Fenix.Hardware.Camera.GigE
{
    public class PylonAutoInitTerm
    {
        #region Constructors

        public PylonAutoInitTerm()
        {
            PylonC.NET.Pylon.Initialize();
        }

        #endregion

        #region Finalizers

        ~PylonAutoInitTerm()
        {
            PylonC.NET.Pylon.Terminate();
        }

        #endregion
    }
}
