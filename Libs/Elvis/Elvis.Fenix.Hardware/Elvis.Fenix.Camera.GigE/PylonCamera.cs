﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using PylonC.NET;
using System.Threading;

using Elvis.Fenix.Hardware;

namespace Elvis.Fenix.Hardware.Camera.GigE
{
	public enum GrabMode
	{
		SYNCH,
		ASYNCH
	}


	public struct SynchContext
	{
		public int ContextId { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
	}


	public class PylonCamera : FxCamera, IDisposable
	{
		public override int Left { get; set; }
		public override int Top { get; set; }

		private PYLON_WAITOBJECT_HANDLE waitSet;
        PYLON_WAITOBJECTS_HANDLE wos; /* Wait objects. */

		#region Constants

		private const int TotalBuffers = 1;

		#endregion

		#region Static Members

		/// <summary>
		/// Initializes the Pylon runtime system and releases its resources
		/// before application terminates.
		/// </summary>
		private static PylonAutoInitTerm autoInitTerm = new PylonAutoInitTerm();

		#endregion

		#region Private Members

        /// <summary>
        /// Line is inverted or not
        /// </summary>
        private bool _lineInverter;

        /// <summary>
        /// Line source option
        /// </summary>
        private LineSource _lineSource;

		/// <summary>
		/// Synchronization context used to dispatch asynchronous methods.
		/// </summary>
		private SynchronizationContext _context;

		/// <summary>
		/// Grab mode
		/// </summary>
		private GrabMode _mode;

		/// <summary>
		/// Parameter by which to look for the requested camera.
		/// </summary>
		private SelectBy _selectBy;

		/// <summary>
		/// Id of the requested camera.
		/// </summary>
		private String _id;

		/// <summary>
		/// Flag indicating camera connection status.
		/// </summary>
		private bool _connected;

		/// <summary>
		/// Serial number of a camera.
		/// </summary>
		private String _serialNumber;

		/// <summary>
		/// Friendly name of a camera.
		/// </summary>
		private String _friendlyName;

		/// <summary>
		/// Camera object handle.
		/// </summary>
		private PYLON_DEVICE_HANDLE _camera;

		/// <summary>
		/// Stream grabber object handle.
		/// </summary>
		private PYLON_STREAMGRABBER_HANDLE _grabber;

		/// <summary>
		/// Frame arrival wait object handle.
		/// </summary>
		private PYLON_WAITOBJECT_HANDLE _frameEvent;

		/// <summary>
		/// Buffer queue change wait object handle.
		/// </summary>
		private PYLON_WAITOBJECT_HANDLE _enqueueEvent;

		/// <summary>
		/// Thread termination wait object handle.
		/// </summary>
		private PYLON_WAITOBJECT_HANDLE _terminateEvent;

		/// <summary>
		/// Buffers to hold image data.
		/// </summary>
		private PylonBuffer<byte>[] _buffers;

		private Queue<SynchContext> _resultsContexts = new Queue<SynchContext>();


		/// <summary>
		/// Handles to stream grabber's registered buffers.
		/// </summary>
		private PYLON_STREAMBUFFER_HANDLE[] _handles;

		/// <summary>
		/// Index queue of buffers ready to be enqueued.
		/// </summary>
		private Queue<int> _ready;

		/// <summary>
		/// Mutex used to synchronize buffer accesses between threads.
		/// </summary>
		private Mutex _mutex;

		/// <summary>
		/// Thread used to grab frames from a camera.
		/// </summary>
		private Thread _thread;

		/// <summary>
		/// Event used to notify that grabbing has started.
		/// </summary>
		private AutoResetEvent _barrier;

		/// <summary>
		/// Callback handler for device removal event.
		/// </summary>
		private DeviceCallbackHandler _handler;

		/// <summary>
		/// Registered device callback object handle.
		/// </summary>
		private PYLON_DEVICECALLBACK_HANDLE _callback;

		private ManualResetEventSlim _stopGrab = new ManualResetEventSlim(false);

		#endregion

		#region Events

		public event FrameReadyHandler onFrameReady;
		public event CameraConnectedHandler onCameraConnected;
		public event CameraDisconectedHandler onCameraDisconected;
		public override event GrabStartedHandler onGrabStarted;
		public event GrabStoppedHandler onGrabStopped;

		#endregion

		#region Constructors

		public PylonCamera() { }

		public PylonCamera(SynchronizationContext context, GrabMode mode)
		{
			_mode = mode;
			_context = context;
			_selectBy = SelectBy.SerialNumber;
			_id = null;

			Initialize();
		}

		public PylonCamera(SynchronizationContext context, SelectBy selectBy, String id, GrabMode mode)
		{
			_mode = mode;
			_context = context;
			_selectBy = selectBy;
			_id = id;

			Initialize();
		}

        public PylonCamera(SynchronizationContext context, SelectBy selectBy, String id, GrabMode mode, LineSource lineSource, bool lineInverter)
		{
			_mode = mode;
			_context = context;
			_selectBy = selectBy;
			_id = id;

            _lineSource = lineSource;
            _lineInverter = lineInverter;

			Initialize();
		}

		#endregion

		#region Destructors

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				try
				{
					Disconnect();
				}
				catch (Exception)
				{
					System.Diagnostics.Debug.WriteLine(GenApi.GetLastErrorMessage());
				}

				if(_mode == GrabMode.ASYNCH)
				{
					Pylon.WaitObjectDestroy(_enqueueEvent);
					Pylon.WaitObjectDestroy(_terminateEvent);
				}				
			}
		}

		#endregion

		#region Public Types

		public enum SelectBy
		{
			SerialNumber,
			UserDefinedId,
		}

		public enum AcquisitionModeType
		{
			SingleFrame,
			Continuous,
		}

		public enum TriggerSelector
		{
			AcquisitionStart,
			FrameStart,
			LineStart,
		}

		public enum TriggerSourceType
		{
			Unknown = -1,
			Software,
			Line1,
			Line2,
			Line3,
		}

		public enum TriggerModeType
		{
			Off,
			On,
		}

		public enum ConfigurationSet
		{
			Default,
			UserSet1,
			UserSet2,
			UserSet3,
		}

        public enum LineSource
        {
            ExposureActive,
            FrameTriggerWait,
            TimerActive,
            UserOutput,
            TriggerReady,
            AcquisitionTriggerWait,
            AcquisitionTriggerReady,
            FlashWindow
        }

		#endregion

		#region Public Properties

        public override string Name
        {
            get
            {
                return _id;
            }
            set
            {
                if (value != String.Empty)
                {
                    _id = value;
                }
                else
                {
                    _id = "Unknown";
                }
            }
        }

		public override bool IsConnected
		{
			get { return _connected; }
		}

		public override bool IsGrabbing
		{
			get { return IsRealyGrabbing; }
			//get { return (_thread != null && _thread.IsAlive); }
		}

		public String Id
		{
			get { return _id; }
		}

		public String SerialNumber
		{
			get { return _serialNumber; }
		}

		public String FriendlyName
		{
			get { return _friendlyName; }
		}

		public bool LeftOffsetAvailable
		{
			get { return IsFeatureAvailable("OffsetX"); }
		}

		public bool TopOffsetAvailable
		{
			get { return IsFeatureAvailable("OffsetY"); }
		}

		public bool WidthAvailable
		{
			get { return IsFeatureAvailable("Width"); }
		}

		public bool HeightAvailable
		{
			get { return IsFeatureAvailable("Height"); }
		}

		public bool BinningHorizontalAvailable
		{
			get { return IsFeatureAvailable("BinningHorizontal"); }
		}

		public bool BinningVerticalAvailable
		{
			get { return IsFeatureAvailable("BinningVertical"); }
		}

		public bool BlackLevelAvailable
		{
			get { return IsFeatureAvailable("BlackLevelRaw"); }
		}

		public bool ExposureAvailable
		{
			get { return IsFeatureAvailable("ExposureTimeRaw"); }
		}

		public int LeftOffsetMin
		{
			get { return GetIntegerFeatureMin("OffsetX"); }
		}

		public int TopOffsetMin
		{
			get { return GetIntegerFeatureMin("OffsetY"); }
		}

		public int WidthMin
		{
			get { return GetIntegerFeatureMin("Width"); }
		}

		public int HeightMin
		{
			get { return GetIntegerFeatureMin("Height"); }
		}

		public int BinningHorizontalMin
		{
			get { return GetIntegerFeatureMin("BinningHorizontal"); }
		}

		public int BinningVerticalMin
		{
			get { return GetIntegerFeatureMin("BinningVertical"); }
		}

		public int BlackLevelMin
		{
			get { return GetIntegerFeatureMin("BlackLevelRaw"); }
		}

		public int ExposureMin
		{
			get { return GetIntegerFeatureMin("ExposureTimeRaw"); }
		}

		public int LeftOffsetMax
		{
			get { return GetIntegerFeatureMax("OffsetX"); }
		}

		public int TopOffsetMax
		{
			get { return GetIntegerFeatureMax("OffsetY"); }
		}

		public int WidthMax
		{
			get { return GetIntegerFeatureMax("Width"); }
		}

		public int HeightMax
		{
			get { return GetIntegerFeatureMax("Height"); }
		}

		public int BinningHorizontalMax
		{
			get { return GetIntegerFeatureMax("BinningHorizontal"); }
		}

		public int BinningVerticalMax
		{
			get { return GetIntegerFeatureMax("BinningVertical"); }
		}

		public int BlackLevelMax
		{
			get { return GetIntegerFeatureMax("BlackLevelRaw"); }
		}

		public int ExposureMax
		{
			get { return GetIntegerFeatureMax("ExposureTimeRaw"); }
		}

		public int LeftOffsetInc
		{
			get { return GetIntegerFeatureInc("OffsetX"); }
		}

		public int TopOffsetInc
		{
			get { return GetIntegerFeatureInc("OffsetY"); }
		}

		public int WidthInc
		{
			get { return GetIntegerFeatureInc("Width"); }
		}

		public int HeightInc
		{
			get { return GetIntegerFeatureInc("Height"); }
		}

		public int BinningHorizontalInc
		{
			get { return GetIntegerFeatureInc("BinningHorizontal"); }
		}

		public int BinningVerticalInc
		{
			get { return GetIntegerFeatureInc("BinningVertical"); }
		}

		public int BlackLevelInc
		{
			get { return GetIntegerFeatureInc("BlackLevelRaw"); }
		}

		public int ExposureInc
		{
			get { return GetIntegerFeatureInc("ExposureTimeRaw"); }
		}

		public int LeftOffset
		{
			get { return GetIntegerFeature("OffsetX"); }
			set { SetIntegerFeature("OffsetX", value); }
		}

		public int TopOffset
		{
			get { return GetIntegerFeature("OffsetY"); }
			set { SetIntegerFeature("OffsetY", value); }
		}

		public override int Width
		{
			get { return GetIntegerFeature("Width"); }
			set { SetIntegerFeature("Width", value); }
		}

		public override int Height
		{
			get { return GetIntegerFeature("Height"); }
			set { SetIntegerFeature("Height", value); }
		}

		public int BinningHorizontal
		{
			get { return GetIntegerFeature("BinningHorizontal"); }
			set { SetIntegerFeature("BinningHorizontal", value); }
		}

		public int BinningVertical
		{
			get { return GetIntegerFeature("BinningVertical"); }
			set { SetIntegerFeature("BinningVertical", value); }
		}

		public int BlackLevel
		{
			get { return GetIntegerFeature("BlackLevelRaw"); }
			set { SetIntegerFeature("BlackLevelRaw", value); }
		}

       /* public override int Exposure
        {
            get { return GetIntegerFeature("ExposureOverlapTimeMaxRaw"); }
            set { SetIntegerFeatureInc("ExposureOverlapTimeMaxRaw", value); }
        }*/

        public override int Exposure
        {
            get { return GetIntegerFeature("ExposureTimeRaw"); }
            set { SetIntegerFeatureInc("ExposureTimeRaw", value); }
        }

        public override bool GlobalResetReleaseMode
        {
            get { return GetBooleanFeature("GlobalResetReleaseModeEnable"); }
            set { SetBooleanFeature("GlobalResetReleaseModeEnable", value); }
        }

        public bool LineInverter
        {
            get { return GetBooleanFeature("LineInverter"); }
            set { SetBooleanFeature("LineInverter", value); }
        }

		

		
		[XmlIgnoreAttribute]
		public AcquisitionModeType AcquisitionMode
		{
			get
			{
				string mode = GetStringFeature("AcquisitionMode");
				if (mode.Equals("SingleFrame"))
					return AcquisitionModeType.SingleFrame;
				else if (mode.Equals("Continuous"))
					return AcquisitionModeType.Continuous;
				else
					throw new Exception("Unknown acquisition mode type.");
			}
			set
			{
				switch (value)
				{
					case AcquisitionModeType.SingleFrame:
						SetStringFeature("AcquisitionMode", "SingleFrame");
						break;
					case AcquisitionModeType.Continuous:
						SetStringFeature("AcquisitionMode", "Continuous");
						break;
				}
			}
		}

		#endregion

		#region Public Methods

		public override void Connect()
		{
			if (IsConnected)
				return;

			try
			{
				InitializeCamera();

                // set up line source & line inverter
                SetLineSource(_lineSource);
                LineInverter = _lineInverter;

				ClearError(ErrorFlags.CONNECT_ERROR);
			}
			catch (Exception)
			{
				SetError(ErrorFlags.CONNECT_ERROR);
				/*
				throw new Exception("Could not connect to the camera: " +
									GenApi.GetLastErrorMessage());
				 * */
			}
		}

		public override void Disconnect()
		{
			if (!IsConnected)
				return;

			StopGrab();
			ReleaseBuffers();
			ReleaseCamera();
		}

		public void Run()
		{
			if (_mode == GrabMode.SYNCH)
			{
				throw new Exception("No way Chose. Run method can be called only on asynch mode");
			}

            //while (!_stopGrab.IsSet)
            //{
            //    GrabFrameToBuffer();
            //}

            if (_thread != null)
            {
                if (_thread.IsAlive) _thread.Abort(500);

                _thread = null;
            }

            _thread = new Thread(new ThreadStart(GrabFrameToBuffer));
            _thread.Start();
		}

		public override void StartGrab()
		{
			if (!IsConnected || IsRealyGrabbing)
			{
				return;
			}                

			try
			{

				if (_mode == GrabMode.ASYNCH)
				{
                    wos = Pylon.WaitObjectsCreate();

					/* Create a wait object set. */
					waitSet = Pylon.StreamGrabberGetWaitObject(_grabber);
                    
                    Pylon.WaitObjectsAdd(wos, waitSet);
                    Pylon.WaitObjectsAdd(wos, _frameEvent);
                    Pylon.WaitObjectsAdd(wos, _enqueueEvent);
                    Pylon.WaitObjectsAdd(wos, _terminateEvent);
					SetupAsynchGrab();

				}
				else
				{
					SetupSynchGrab();
				}
				
				ClearError(ErrorFlags.START_ERROR);
			}
			catch (Exception)
			{
				SetError(ErrorFlags.START_ERROR);
				System.Diagnostics.Debug.WriteLine(GenApi.GetLastErrorMessage());
			}

			IsRealyGrabbing = true;
		}

		public override void StopGrab()
		{
			if (!IsRealyGrabbing)
			{
				return;
			}

			if (_mode == GrabMode.ASYNCH)
			{
                Pylon.WaitObjectsRemoveAll(wos);             
                Pylon.WaitObjectsDestroy(wos);
                Pylon.WaitObjectDestroy(waitSet);

				try
				{
                    _stopGrab.Set();
					CleanupGrab();
				}
				catch (Exception)
				{
					System.Diagnostics.Debug.WriteLine(GenApi.GetLastErrorMessage());
				}
				finally
				{
					IsRealyGrabbing = false;
				}
				Pylon.WaitObjectSignal(_terminateEvent);
			}			
		}

		public override void ReleaseFrame(FxFrame frame)
		{
			if (frame == null)
			{
				/* Invalid frame object has been passed. */
				return;
			}

			if (!IsConnected)
			{
				/* Not connected to the camera. */
				return;
			}

			_mutex.WaitOne();

			/*
			 * Check whether image data buffers have not been reallocated and
			 * we still need to enqueue the frame buffer.
			 */
			if (_buffers != null) // check if buffer exists
			{
				if (frame.Data == _buffers[(int)frame.Context].Pointer)
				{
					/*
					 * Enqueue frame buffer and notify capture thread if camera is
					 * still grabbing images.
					 */
					_ready.Enqueue((int)frame.Context);
					//if (IsGrabbing)
					Pylon.WaitObjectSignal(_enqueueEvent);
				}
			}

			_mutex.ReleaseMutex();
		}

		public TriggerSourceType GetTriggerSource(TriggerSelector selector)
		{
			SelectTrigger(selector);

			string source = GetStringFeature("TriggerSource");
			if (source.Equals("Line1"))
				return TriggerSourceType.Line1;
			else if (source.Equals("Line2"))
				return TriggerSourceType.Line2;
			else if (source.Equals("Line3"))
				return TriggerSourceType.Line3;
			else if (source.Equals("Software"))
				return TriggerSourceType.Software;
			else
				throw new Exception("Unknown trigger source type.");
		}

		public void SetTriggerSource(TriggerSelector selector,
									 TriggerSourceType source)
		{
			SelectTrigger(selector);

			switch (source)
			{
				case TriggerSourceType.Line1:
					SetStringFeature("TriggerSource", "Line1");
					break;
				case TriggerSourceType.Line2:
					SetStringFeature("TriggerSource", "Line2");
					break;
				case TriggerSourceType.Line3:
					SetStringFeature("TriggerSource", "Line3");
					break;
				case TriggerSourceType.Software:
					SetStringFeature("TriggerSource", "Software");
					break;
			}
		}

        public LineSource GetLineSource()
        {
            string source = GetStringFeature("LineSource");

            if (source.Equals("ExposureActive"))
                return LineSource.ExposureActive;
            else if (source.Equals("FrameTriggerWait"))
                return LineSource.FrameTriggerWait;
            else if (source.Equals("TimerActive"))
                return LineSource.TimerActive;
            else if (source.Equals("UserOutput"))
                return LineSource.UserOutput;
            else if (source.Equals("TriggerReady"))
                return LineSource.TriggerReady;
            else if (source.Equals("AcquisitionTriggerWait"))
                return LineSource.AcquisitionTriggerWait;
            else if (source.Equals("AcquisitionTriggerReady"))
                return LineSource.AcquisitionTriggerReady;
            else if (source.Equals("FlashWindow"))
                return LineSource.FlashWindow;
            else
                throw new Exception("Invalid line source");
        }

        public void SetLineSource(LineSource source)
        {
            switch(source)
            {
                case LineSource.ExposureActive:
                    SetStringFeature("LineSource", "ExposureActive");
                    break;
                case LineSource.FrameTriggerWait:
                    SetStringFeature("LineSource", "FrameTriggerWait");
                    break;
                case LineSource.TimerActive:
                    SetStringFeature("LineSource", "TimerActive");
                    break;
                case LineSource.UserOutput:
                    SetStringFeature("LineSource", "UserOutput");
                    break;
                case LineSource.TriggerReady:
                    SetStringFeature("LineSource", "TriggerReady");
                    break;
                case LineSource.AcquisitionTriggerWait:
                    SetStringFeature("LineSource", "AcquisitionTriggerWait");
                    break;
                case LineSource.AcquisitionTriggerReady:
                    SetStringFeature("LineSource", "AcquisitionTriggerReady");
                    break;
                case LineSource.FlashWindow:
                    SetStringFeature("LineSource", "FlashWindow");
                    break;
            }
        }

		public TriggerModeType GetTriggerMode(TriggerSelector selector)
		{
			SelectTrigger(selector);
			string mode = GetStringFeature("TriggerMode");
			return mode.Equals("On") ? TriggerModeType.On : TriggerModeType.Off;
		}

		public void SetTriggerMode(TriggerSelector selector,
								   TriggerModeType mode)
		{
			SelectTrigger(selector);
			SetStringFeature("TriggerMode", mode == TriggerModeType.On ? "On" :
							 "Off");
		}

		public void SoftwareTrigger()
		{
			if (!IsConnected)
				throw new Exception("Not connected to the camera.");

			try
			{
				Pylon.DeviceExecuteCommandFeature(_camera, "TriggerSoftware");
			}
			catch (Exception)
			{
				throw new Exception("Could not execute TriggerSoftware: " +
									GenApi.GetLastErrorMessage());
			}
		}

		public void LoadConfigurationSet(ConfigurationSet configurationSet)
		{
			try
			{
				switch (configurationSet)
				{
					case ConfigurationSet.Default:
						Pylon.DeviceFeatureFromString(_camera, "UserSetSelector",
													  "Default");                   
						break;
					case ConfigurationSet.UserSet1:
						Pylon.DeviceFeatureFromString(_camera, "UserSetSelector",
													  "UserSet1");
						break;
					case ConfigurationSet.UserSet2:
						Pylon.DeviceFeatureFromString(_camera, "UserSetSelector",
													  "UserSet2");
						break;
					case ConfigurationSet.UserSet3:
						Pylon.DeviceFeatureFromString(_camera, "UserSetSelector",
													  "UserSet3");
						break;
				}

				Pylon.DeviceExecuteCommandFeature(_camera, "UserSetLoad");
			}
			catch (Exception)
			{
				throw new Exception("Could not load configuration set: " +
									GenApi.GetLastErrorMessage());
			}
		}

		public void SaveConfigurationSet(ConfigurationSet configurationSet)
		{
			try
			{
				switch (configurationSet)
				{
					case ConfigurationSet.Default:
						Pylon.DeviceFeatureFromString(_camera, "UserSetSelector",
													  "Default");
						break;
					case ConfigurationSet.UserSet1:
						Pylon.DeviceFeatureFromString(_camera, "UserSetSelector",
													  "UserSet1");
						break;
					case ConfigurationSet.UserSet2:
						Pylon.DeviceFeatureFromString(_camera, "UserSetSelector",
													  "UserSet2");
						break;
					case ConfigurationSet.UserSet3:
						Pylon.DeviceFeatureFromString(_camera, "UserSetSelector",
													  "UserSet3");
						break;
				}

				Pylon.DeviceExecuteCommandFeature(_camera, "UserSetSave");
			}
			catch (Exception)
			{
				throw new Exception("Could not save configuration set: " +
									GenApi.GetLastErrorMessage());
			}
		}

		#endregion

		#region Private Methods

		void Initialize()
		{
			_connected = false;
			_serialNumber = "";
			_friendlyName = "";
			_camera = null;
			_grabber = null;
			_frameEvent = null;
			_enqueueEvent = Pylon.WaitObjectCreate();
			_terminateEvent = Pylon.WaitObjectCreate();
			_buffers = null;
			_handles = null;
			_ready = new Queue<int>();
			_mutex = new Mutex();
			_thread = null;
			_barrier = new AutoResetEvent(false);
			_handler = new DeviceCallbackHandler();
			_handler.CallbackEvent +=
				new DeviceCallbackHandler.DeviceCallback(HandleRemoval);
		}

		void InitializeCamera()
		{
			System.Diagnostics.Debug.WriteLine("Looking for cameras...");

			Pylon.Initialize();

			/* Get all attached devices. */
			uint devices = Pylon.EnumerateDevices();
			if (devices == 0)
			{
				if (string.IsNullOrEmpty(_id))
					throw new Exception("No camera was found.");
			}

			/* Look for the requested camera. */
			uint device = devices;
			if (_id != null && _id.Length > 0)
			{
				for (uint i = 0; i < devices; ++i)
				{
					PYLON_DEVICE_INFO_HANDLE info =
						Pylon.GetDeviceInfoHandle(i);

					string id = "";
					switch (_selectBy)
					{
						case SelectBy.SerialNumber:
							id = Pylon.DeviceInfoGetPropertyValueByName(
								info, Pylon.cPylonDeviceInfoSerialNumberKey);
							break;

						case SelectBy.UserDefinedId:
							id = Pylon.DeviceInfoGetPropertyValueByName(
								info, Pylon.cPylonDeviceInfoUserDefinedNameKey);
							break;
					}

					if (_id.Equals(id))
					{
						device = i;
						_serialNumber = Pylon.DeviceInfoGetPropertyValueByName(
							info, Pylon.cPylonDeviceInfoSerialNumberKey);
						_friendlyName = Pylon.DeviceInfoGetPropertyValueByName(
							info, Pylon.cPylonDeviceInfoFriendlyNameKey);
						System.Diagnostics.Debug.WriteLine(_friendlyName +
														   " found.");
						break;
					}
				}
			}
			else
			{
				/* Select the first available device. */
				device = 0;

				PYLON_DEVICE_INFO_HANDLE info =
						Pylon.GetDeviceInfoHandle(device);
				_serialNumber = Pylon.DeviceInfoGetPropertyValueByName(
					info, Pylon.cPylonDeviceInfoSerialNumberKey);
				_friendlyName = Pylon.DeviceInfoGetPropertyValueByName(
					info, Pylon.cPylonDeviceInfoFriendlyNameKey);
				System.Diagnostics.Debug.WriteLine(_friendlyName + " found.");
			}

			if (device == devices)
			{
				/* No suitable camera was found. */
				throw new Exception("No suitable camera was found.");
			}

			_camera = Pylon.CreateDeviceByIndex(device);
			try
			{
				Pylon.DeviceOpen(_camera, Pylon.cPylonAccessModeControl | Pylon.cPylonAccessModeStream);
			}
			catch (Exception)
			{
				Pylon.DestroyDevice(_camera);
				throw;
			}

			_callback = Pylon.DeviceRegisterRemovalCallback(_camera, _handler);

#if DEBUG
			/* For GigE cameras, the application periodically sends heartbeat
			 * signals to the camera to keep the connection to the camera alive.
			 * If the camera doesn't receive heartbeat signals within the time 
			 * period specified by the heartbeat timeout counter, the camera
			 * resets the connection. When the application is stopped by the
			 * debugger, the application cannot create the heartbeat signals.
			 * For that reason, the pylon runtime extends the heartbeat timeout
			 * in debug mode to 5 minutes to allow debugging. For GigE cameras,
			 * we will set the heartbeat timeout to a shorter period before
			 * testing the callbacks. The heartbeat mechanism is also used for
			 * detection of device removal. When the pylon runtime doesn't
			 * receive an acknowledge for the heartbeat signal, it is assumed
			 * that the device has been removed. A removal callback will be
			 * fired in that case. By decreasing the heartbeat timeout in debug
			 * mode for GigE cameras, the surprise removal will be noticed
			 * sooner than set by the pylon runtime.
			 */
			try
			{
				/* Get the node map for the transport layer parameters. */
				NODEMAP_HANDLE nodemap = Pylon.DeviceGetTLNodeMap(_camera);

				/* Get the node for the heartbeat timeout parameter. */
				NODE_HANDLE node = GenApi.NodeMapGetNode(nodemap, "HeartbeatTimeout");

				/* Set the new hearbeat timeout value. */
				GenApi.IntegerSetValue(node, 1000);
			}
			catch (Exception)
			{
			}
#endif // DEBUG

			if(_mode == GrabMode.ASYNCH)
			{
				_grabber = Pylon.DeviceGetStreamGrabber(_camera, 0);
				Pylon.StreamGrabberOpen(_grabber);
				_frameEvent = Pylon.StreamGrabberGetWaitObject(_grabber);
			}		

			_connected = true;
		}

		void ReleaseCamera()
		{
			try
			{
				if (_grabber != null && Pylon.StreamGrabberIsOpen(_grabber))
					Pylon.StreamGrabberClose(_grabber);

				if (_camera != null)
				{
					Pylon.DeviceDeregisterRemovalCallback(_camera, _callback);
					if (Pylon.DeviceIsOpen(_camera))
						Pylon.DeviceClose(_camera);
					Pylon.DestroyDevice(_camera);
				}
			}
			catch(Exception ex)
			{

			}			

			_frameEvent = null;
			_grabber = null;
			_camera = null;
			_connected = false;
		}

		void AllocateBuffers()
		{
			/* Determine the required data buffer size. */
			uint payloadSize = (uint)Pylon.DeviceGetIntegerFeature(
				_camera, "PayloadSize");

			/* Check the state of previously allocated buffers. */
			_mutex.WaitOne();
			try
			{
				if (_buffers != null)
				{
					bool correct = true;
					for (int i = 0; i < _buffers.Length; ++i)
					{
						if (_buffers[i] == null ||
							_buffers[i].Array.Length != payloadSize)
						{
							correct = false;
							break;
						}
					}

					if (correct)
					{
						/*
						 * All of the previously allocated buffers are
						 * of correct size, just use them.
						 */
						return;
					}
				}
			}
			finally
			{
				_mutex.ReleaseMutex();
			}

			/* Release any previously allocated buffers. */
			ReleaseBuffers();

			/* Allocate image data buffers. */
			_mutex.WaitOne();

			_buffers = new PylonBuffer<Byte>[TotalBuffers];
			for (int i = 0; i < TotalBuffers; ++i)
			{
				_buffers[i] = new PylonBuffer<Byte>(payloadSize, true);
				_ready.Enqueue(i);
			}

			_mutex.ReleaseMutex();
		}

		void ReleaseBuffers()
		{
			if (_buffers == null)
				return;

			_mutex.WaitOne();

			/* Release all of the idle image data buffers. */
			while (_ready.Count > 0)
			{
				int index = _ready.Dequeue();
				PylonBuffer<Byte> buffer = _buffers[index];
				buffer.Dispose();
			}
			_buffers = null;

			_mutex.ReleaseMutex();
		}


		void SetupSynchGrab()
		{
			/* Set the pixel format to Mono8, where gray values will be output as 8 bit values for each pixel. */
			/* ... Check first to see if the device supports the Mono8 format. */
			bool isAvail = Pylon.DeviceFeatureIsAvailable(_camera, "EnumEntry_PixelFormat_Mono8");

			if (!isAvail)
			{
				/* Feature is not available. */
				throw new Exception("Device doesn't support the Mono8 pixel format.");
			}

			/* ... Set the pixel format to Mono8. */
			Pylon.DeviceFeatureFromString(_camera, "PixelFormat", "Mono8");

			/* Disable acquisition start trigger if available. */
			isAvail = Pylon.DeviceFeatureIsAvailable(_camera, "EnumEntry_TriggerSelector_AcquisitionStart");
			if (isAvail)
			{
				Pylon.DeviceFeatureFromString(_camera, "TriggerSelector", "AcquisitionStart");
				Pylon.DeviceFeatureFromString(_camera, "TriggerMode", "Off");
			}

			/* Disable frame start trigger if available */
			isAvail = Pylon.DeviceFeatureIsAvailable(_camera, "EnumEntry_TriggerSelector_FrameStart");
			if (isAvail)
			{
				Pylon.DeviceFeatureFromString(_camera, "TriggerSelector", "FrameStart");
				Pylon.DeviceFeatureFromString(_camera, "TriggerMode", "Off");
			}

			isAvail = Pylon.DeviceFeatureIsWritable(_camera, "GevSCPSPacketSize");

			if (isAvail)
			{
				/* ... The device supports the packet size feature. Set a value. */
				Pylon.DeviceSetIntegerFeature(_camera, "GevSCPSPacketSize", 1500);
			}
		}

		void SetupAsynchGrab()
		{
			/* Determine the required size of the data buffer. */
			uint payloadSize = (uint)Pylon.DeviceGetIntegerFeature(_camera, "PayloadSize");

			/* Set the maximum number of buffers for a stream grabber to use. */
			Pylon.StreamGrabberSetMaxNumBuffer(_grabber, TotalBuffers);

			/* Set the maximum buffer size for a stream grabber. */
			Pylon.StreamGrabberSetMaxBufferSize(_grabber, payloadSize);

			/* Prepare a stream grabber for grabbing. */
			Pylon.StreamGrabberPrepareGrab(_grabber);

			/* Allocate and attach image data buffers to a stream grabber. */
			AllocateBuffers();
			
			_handles = new PYLON_STREAMBUFFER_HANDLE[TotalBuffers];
			for (int i = 0; i < TotalBuffers; ++i)
			{
				_handles[i] = Pylon.StreamGrabberRegisterBuffer(
					_grabber, ref _buffers[i]);
			}
					
			_mutex.WaitOne();

			while (_ready.Count > 0)
			{
				int index = _ready.Dequeue();
				Pylon.StreamGrabberQueueBuffer(_grabber, _handles[index],
											   index);
			}
			_mutex.ReleaseMutex();
			

			/* Let the camera acquire images. */
			Pylon.DeviceExecuteCommandFeature(_camera, "AcquisitionStart");
		}

		void CleanupGrab()
		{
			/* Stop the camera acquisition. */
			try
			{
				Pylon.DeviceExecuteCommandFeature(_camera, "AcquisitionStop");
			}
			catch (Exception)
			{
				/*
				 * Command execution will fail if camera has been disconnected
				 * while grabbing images, thus this guard is needed.
				 */
				System.Diagnostics.Debug.WriteLine(
					"CleanupGrab(): " + GenApi.GetLastErrorMessage());
			}

			/*
			 * Cancel grab operation ensuring that all pending buffers are put
			 * into the stream grabber's output queue.
			 */
			Pylon.StreamGrabberCancelGrab(_grabber);

			/* Retrieve buffers from the stream grabber's output queue. */
			_mutex.WaitOne();

			PylonGrabResult_t result;
			while (Pylon.StreamGrabberRetrieveResult(_grabber, out result))
				_ready.Enqueue(result.Context);

			_mutex.ReleaseMutex();

			/* Detach image data buffers. */
			for (int i = 0; i < TotalBuffers; ++i)
				Pylon.StreamGrabberDeregisterBuffer(_grabber, _handles[i]);
			_handles = null;

			/* Shut down a stream grabber. */
			Pylon.StreamGrabberFinishGrab(_grabber);
		}


		public void GrabFrameToBuffer()
		{

            while (!_stopGrab.IsSet)
            {
                if (onGrabStarted != null)
                {
                    onGrabStarted(this);
                }

                uint signalled;
                while (Pylon.WaitObjectsWaitForAny(wos, System.UInt32.MaxValue, out signalled))
                {
                    if (signalled == 2)
                    {
                        Pylon.WaitObjectReset(_terminateEvent);
                        break;
                    }
                    else if (signalled == 1)
                    {
                        _mutex.WaitOne();
                        try
                        {
                            while (_ready.Count > 0)
                            {
                                int index = _ready.Dequeue();
                                Pylon.StreamGrabberQueueBuffer(
                                    _grabber, _handles[index], index);
                            }
                        }
                        catch (Exception e)
                        {
                            System.Diagnostics.Debug.WriteLine(e.Message);
                        }
                        finally
                        {
                            _mutex.ReleaseMutex();
                        }

                        Pylon.WaitObjectReset(_enqueueEvent);
                        continue;
                    }


                    PylonGrabResult_t result;
                    Pylon.StreamGrabberRetrieveResult(_grabber, out result);



                    if (result.Status == EPylonGrabStatus.Grabbed)
                    {
                        SynchContext con = new SynchContext();
                        con.ContextId = result.Context;
                        con.Width = result.SizeX;
                        con.Height = result.SizeY;

                        _resultsContexts.Enqueue(con);

                        //PylonBuffer<Byte> buffer = _buffers[result.Context];
                        //return new PylonFrame(result, buffer);

                        //return new FxFrame(result.Context, buffer.Pointer, result.SizeX, result.SizeY, 1);

                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("Skipping frame!");


                    }

                    Pylon.StreamGrabberQueueBuffer(
                            _grabber, _handles[result.Context], result.Context);
                }
                //return null;
            }
		}


		public override FxFrame RetrieveFrame()
		{
			if(_resultsContexts.Count > 0)
			{
				SynchContext context = _resultsContexts.Dequeue();
				PylonBuffer<Byte> buffer = _buffers[context.ContextId];
				return new FxFrame(context, buffer.Pointer, context.Width, context.Height, 1);
			}
			return null;            
		}       


		public override FxFrame  GrabFrame()
		{
			if(_mode == GrabMode.ASYNCH)
			{
				throw new Exception("No way Hose. This GrabFrame method can be used only in synch mode");
			}

			if(onGrabStarted != null)
			{
				onGrabStarted(this);
			}

			bool isReady;  
			PylonGrabResult_t grabResult;
			PylonBuffer<Byte> imgBuf = null; 

			try
			{
				isReady = Pylon.DeviceGrabSingleFrame(_camera, 0, ref imgBuf, out grabResult, 500);

				if (!isReady)
				{
					System.Diagnostics.Debug.WriteLine("Camera Error: " + GenApi.GetLastErrorMessage());
					SetError(ErrorFlags.GRAB_ERROR);
				}

				if (grabResult.Status == EPylonGrabStatus.Grabbed && imgBuf != null)
				{
					FxFrame frame = new FxFrame(grabResult.Context, imgBuf.Pointer, grabResult.SizeX, grabResult.SizeY, 1);
					return frame;
				}
				else
				{
					System.Diagnostics.Debug.WriteLine("Camera Error: " + GenApi.GetLastErrorMessage());
					SetError(ErrorFlags.GRAB_ERROR);
				}

			}
			catch(Exception ex)
			{

				System.Diagnostics.Debug.WriteLine("Camera Error: " + GenApi.GetLastErrorMessage());

			}

			StopGrab();
			Disconnect();
			Connect();

			return null;


			/*
			EPylonWaitExResult res;

			res = Pylon.WaitObjectWaitEx(waitSet, 100, true);

			if(res == EPylonWaitExResult.waitex_timeout)
			{
				System.Diagnostics.Debug.WriteLine("Camera Error: " + GenApi.GetLastErrorMessage());
				SetError(ErrorFlags.GRAB_ERROR);
				StopGrab();
				Disconnect();
				Connect();
			}

			PylonGrabResult_t result;
			isReady = Pylon.StreamGrabberRetrieveResult(_grabber, out result);

			if (!isReady)
			{
				System.Diagnostics.Debug.WriteLine("Camera Error: " + GenApi.GetLastErrorMessage());
				SetError(ErrorFlags.GRAB_ERROR);
			}

		   
			if (result.Status == EPylonGrabStatus.Grabbed)
			{

				_mutex.WaitOne();
				try
				{
									 
				  
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine(e.Message);
				}
				finally
				{
					_mutex.ReleaseMutex();
				}

				PylonBuffer<Byte> buffer = _buffers[result.Context];

				Pylon.ImageWindowDisplayImage<Byte>(0, buffer, result);


				ClearError(ErrorFlags.GRAB_ERROR);
				FxFrame frame = new FxFrame(result.Context, buffer.Pointer, result.SizeX, result.SizeY, 1);


			   // Pylon.StreamGrabberQueueBuffer(_grabber, _handles[result.Context], result.Context);

				while (_ready.Count > 0)
				{
					int index = _ready.Dequeue();
					Pylon.StreamGrabberQueueBuffer(_grabber, _handles[index], index);
				} 


				_ready.Enqueue((int)frame.Context);
				//Pylon.WaitObjectSignal(_enqueueEvent);
			 * */



			/*

			while (Pylon.WaitObjectsWaitForAny(waitSet, 10000, out signalled))
			{
				if (signalled == 2)
				{
					Pylon.WaitObjectReset(_terminateEvent);
					break;
				}
				else if (signalled == 1)
				{
					_mutex.WaitOne();
					try
					{
						while (_ready.Count > 0)
						{
							int index = _ready.Dequeue();
							Pylon.StreamGrabberQueueBuffer(
								_grabber, _handles[index], index);
						}
					}
					catch (Exception e)
					{
						System.Diagnostics.Debug.WriteLine(e.Message);
					}
					finally
					{
						_mutex.ReleaseMutex();
					}

					Pylon.WaitObjectReset(_enqueueEvent);
					continue;
				}

				///* Retrieve buffer after processing by stream grabber. **
				PylonGrabResult_t result;
				Pylon.StreamGrabberRetrieveResult(_grabber, out result);

							 
			}

			System.Diagnostics.Debug.WriteLine("Camera error: " + GenApi.GetLastErrorMessage());
 
			SetError(ErrorFlags.GRAB_ERROR);

			

			*/

			
		}

		/*
		public Object GetFrame()
		{
			Grab();


			PylonFrame frame = null;
			PylonGrabResult_t result;
			Pylon.StreamGrabberRetrieveResult(_grabber, out result);
			if (result != null && result.Context > 0)
			{
				PylonBuffer<Byte> buffer = _buffers[result.Context];
				frame = new PylonFrame(result, buffer);
			}

			return frame;
		}
		 * */

		void HandleRemoval(PYLON_DEVICE_HANDLE device)
		{
			try
			{
				Disconnect();

				if (onCameraDisconected != null)
					_context.Post(delegate { onCameraDisconected(this); }, null);
			}
			catch (Exception)
			{
				System.Diagnostics.Debug.WriteLine(
					GenApi.GetLastErrorMessage());
			}
		}

		#endregion

		#region Private Helpers

		bool IsFeatureAvailable(string name)
		{
			if (!IsConnected)
				throw new Exception("Not connected to the camera.");

			bool available;
			try
			{
				available = Pylon.DeviceFeatureIsAvailable(_camera, name);
			}
			catch (Exception)
			{
				throw new Exception("Could not get " + name + " value: " +
									GenApi.GetLastErrorMessage());
			}
			return available;
		}

		int GetIntegerFeature(string name)
		{
			if (!IsConnected)
				throw new Exception("Not connected to the camera.");

			int value;
			try
			{
				value = (int)Pylon.DeviceGetIntegerFeature(_camera, name);
			}
			catch (Exception ex)
			{
				throw new Exception("Could not get " + name + " value: " +
									GenApi.GetLastErrorMessage());
			}
			return value;
		}   // CIA stringa

		int GetIntegerFeatureMin(string name)
		{
			if (!IsConnected)
				throw new Exception("Not connected to the camera.");

			int value;
			try
			{
				value = (int)Pylon.DeviceGetIntegerFeatureMin(_camera, name);
			}
			catch (Exception)
			{
				throw new Exception("Could not get " + name + " value: " +
									GenApi.GetLastErrorMessage());
			}
			return value;
		}

		int GetIntegerFeatureMax(string name)
		{
			if (!IsConnected)
				throw new Exception("Not connected to the camera.");

			int value;
			try
			{
				value = (int)Pylon.DeviceGetIntegerFeatureMax(_camera, name);
			}
			catch (Exception)
			{
				throw new Exception("Could not get " + name + " value: " +
									GenApi.GetLastErrorMessage());
			}
			return value;
		}

		int GetIntegerFeatureInc(string name)
		{
			if (!IsConnected)
				throw new Exception("Not connected to the camera.");

			int value;
			try
			{
				value = (int)Pylon.DeviceGetIntegerFeatureInc(_camera, name);
			}
			catch (Exception)
			{
				throw new Exception("Could not get " + name + " value: " +
									GenApi.GetLastErrorMessage());
			}
			return value;
		}

		void SetIntegerFeature(string name, int value)
		{
			if (!IsConnected)
				throw new Exception("Not connected to the camera.");

			try
			{
				Pylon.DeviceSetIntegerFeature(_camera, name, value);
			}
			catch (Exception)
			{
				throw new Exception("Could not set " + name + " value: " +
									GenApi.GetLastErrorMessage());
			}
		}


		bool GetBooleanFeature(string name)
		{
			if (!IsConnected)
				throw new Exception("Not connected to the camera.");

			bool value;
			try
			{
				value = (bool)Pylon.DeviceGetBooleanFeature(_camera, name);
			}
			catch (Exception)
			{
				throw new Exception("Could not get " + name + " value: " +
									GenApi.GetLastErrorMessage());
			}
			return value;
		}

		void SetBooleanFeature(string name, bool value)
		{
			if (!IsConnected)
				throw new Exception("Not connected to the camera.");

			try
			{
				Pylon.DeviceSetBooleanFeature(_camera, name, value);
			}
			catch (Exception)
			{
				throw new Exception("Could not set " + name + " value: " +
									GenApi.GetLastErrorMessage());
			}
		}

		void SetIntegerFeatureInc(string name, int value)
		{
			if (!IsConnected)
				throw new Exception("Not connected to the camera.");

			try
			{
				value -= value %
					(int)Pylon.DeviceGetIntegerFeatureInc(_camera, name);
				Pylon.DeviceSetIntegerFeature(_camera, name, value);
			}
			catch (Exception)
			{
				throw new Exception("Could not set " + name + " value: " +
									GenApi.GetLastErrorMessage());
			}
		}

		string GetStringFeature(string name)
		{
			if (!IsConnected)
				throw new Exception("Not connected to the camera.");

			string value;
			try
			{
				value = Pylon.DeviceFeatureToString(_camera, name);
			}
			catch (Exception)
			{
				throw new Exception("Could not get " + name + " value: " +
									GenApi.GetLastErrorMessage());
			}
			return value;
		}

		void SetStringFeature(string name, string value)
		{
			if (!IsConnected)
				throw new Exception("Not connected to the camera.");

			try
			{
				Pylon.DeviceFeatureFromString(_camera, name, value);
			}
			catch (Exception)
			{
				throw new Exception("Could not set " + name + " value: " +
									GenApi.GetLastErrorMessage());
				//Error occurs than camera is disconnected
			}
		}

		void SelectTrigger(TriggerSelector selector)
		{
			if (!IsConnected)
				throw new Exception("Not connected to the camera.");

			try
			{
				switch (selector)
				{
					case TriggerSelector.AcquisitionStart:
						Pylon.DeviceFeatureFromString(_camera, "TriggerSelector",
													  "AcquisitionStart");
						break;
					case TriggerSelector.FrameStart:
						Pylon.DeviceFeatureFromString(_camera, "TriggerSelector",
													  "FrameStart");
						break;
					case TriggerSelector.LineStart:
						Pylon.DeviceFeatureFromString(_camera, "TriggerSelector",
													  "LineStart");
						break;
				}
			}
			catch (Exception)
			{
				throw new Exception("Could not set TriggerSelector value: " +
									GenApi.GetLastErrorMessage());
			}
		}

		#endregion

		protected bool IsRealyGrabbing { get; set; }

		

	}
}
