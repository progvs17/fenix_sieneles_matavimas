﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Elvis.Fenix.Hardware.Camera.GigE
{
    using PylonC.NET;

    public class PylonFrame : Elvis.Generals.IImage.IImage
    {

        #region Private Members

        private PylonGrabResult_t _frame = null;
        private PylonBuffer<byte> _buffer = null;

        #endregion

        #region Constructors

        public PylonFrame(PylonGrabResult_t frame, PylonBuffer<byte> buffer)
        {
            _frame = frame;
            _buffer = buffer;
        }

        #endregion

        #region Public Properties

        public PYLON_STREAMBUFFER_HANDLE Buffer
        {
            get { return _frame.hBuffer; }
        }

        public System.IntPtr Data
        {
            get { return _buffer.Pointer; }
        }

        public int Width
        {
            get { return _frame.SizeX; }
        }

        public int Height
        {
            get { return _frame.SizeY; }
        }

        public int Context
        {
            get { return _frame.Context; }
        }

        #endregion
    }
}

