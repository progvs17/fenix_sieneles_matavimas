﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.ComponentModel;

namespace Elvis.Fenix.Hardware
{
    public delegate void LedControllerMessageHandler(Object sender, string message);

    public enum ControllerMode
    {
        Continuous = 0,
        Switched = 1,
        Selected = 2,
        Pulse = 3
    }

    public abstract class FxLEDController
    {
        public FxLEDController() { }

        private string name;

        [Browsable(false)]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value != String.Empty)
                {
                    name = value;
                }
                else
                {
                    name = "Unknown";
                }
            }
        }


        [XmlIgnoreAttribute]
        public abstract string SerialNumber { get; set; }
        [XmlIgnoreAttribute]
        public abstract string ModelId { get; set; }
        [XmlIgnoreAttribute]
        public abstract string IpAddress { get; set; }
        [XmlIgnoreAttribute]
        public abstract int Port { get; set; }
        [XmlIgnoreAttribute]
        public abstract bool IsConnected { get; }

        [XmlIgnoreAttribute, Browsable(false)]
        public abstract FxGpio Gpio { get; set; }

        /******** TBD if needed *********/
        public abstract int Trigger1UpperDOPin { get; set; }
        public abstract int Trigger1LowerDOPin { get; set; }
        public abstract int Trigger2UpperDOPin { get; set; }
        public abstract int Trigger2LowerDOPin { get; set; }

        public abstract ControllerMode Mode { get; set; }
        public abstract int OutputChannel { get; set; }
        public abstract double PulseWidth { get; set; }
        public abstract double PulseDelay { get; set; }
        public abstract double RetriggerDelay { get; set; }
        public abstract int Brightness { get; set; }
        public abstract int TriggerInput { get; set; }
        /********************************/
        

        public virtual void Init() { throw new NotImplementedException(); }

        /// <summary>
        /// Only works with continuous, switched and selected modes
        /// </summary>
        /// <param name="mode">continuous, switched or selected</param>
        /// <param name="outputChannel">1 to 8 (depends on model)</param>
        /// <param name="brightness">from 0 to 100%</param>
        /// <param name="brightness2">2nd brightness setting for selected mode, from 0 to 'brightness' (the previous parameter)</param>
        public virtual void SetChannelBrightness(ControllerMode mode, int outputChannel, int brightness, int brightness2 = 0) { throw new NotImplementedException(); }

        /// <summary>
        /// Only works with pulse mode
        /// </summary>
        /// <param name="mode">pulse</param>
        /// <param name="outputChannel">1 to 8 or w/e (depends on model)</param>
        /// <param name="pulseWidth">pulse width in milliseconds (0.02 to 999)</param>
        /// <param name="delayFromTrigger">delay from trigger to pulse in milliseconds (0.02 to 999)</param>
        /// <param name="settingPercent">setting in percent (0 to 999)</param>
        /// <param name="retriggerDelay">retrigger delay (this parameter is optional)</param>
        public virtual bool SetChannelBrightness(ControllerMode mode, int outputChannel, double pulseWidth, double delayFromTrigger, int settingPercent, double retriggerDelay = 0) { throw new NotImplementedException(); }

        public virtual void SetVoltageRating(int outputChannel, double voltage, double current) { throw new NotImplementedException(); }

        // cia LedControllerTrigger triggerInput
        public virtual void SetTriggerInput(int outputChannel, int triggerInput) { throw new NotImplementedException(); }
        public virtual void SaveSettingsToMemory() { throw new NotImplementedException(); }
        public virtual void ClearConfiguration() { throw new NotImplementedException(); }
        public virtual void ClearErrors() { throw new NotImplementedException(); }

        public virtual void SendCustomCommand(string command) { throw new NotImplementedException(); }


        public abstract event LedControllerMessageHandler onMessage;
    }
}
