﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Xml.Serialization;
using System.ComponentModel;
using Elvis.Fenix.General;


namespace Elvis.Fenix.Hardware
{
    public delegate void ActuatorStartHanlder(Object sender);
    public delegate void ActuatorStopHanlder(Object sender);
    public delegate void ActuatorMessageHandler(Object sender, string message);

    public abstract class FxActuator : IFxBindable
    {
        private string name;

        [Browsable(false)]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value != String.Empty)
                {
                    name = value;
                }
                else
                {
                    name = "Unknown";
                }
            }
        }

        [XmlIgnore]
        public abstract SerialPort ComPort { get; set; }

        public abstract double CurrentPosition { get; set; }
        public abstract double MaxPosition { get; set; }       
        public abstract int Speed { get; set; }
        public abstract int Acceleration { get; set; }
        public abstract int Decceleration { get; set; }

        [XmlIgnore]
        public abstract bool IsMoving { get; }
        [XmlIgnore]
        public abstract bool IsConnected { get; }
        [XmlIgnore]
        public abstract string Status { get; }
        [XmlIgnore]
        public abstract bool InitComplete { get; }


        public FxActuator() { }


        public virtual void Connect() { throw new NotImplementedException(); }
        public virtual void Disconnect() { throw new NotImplementedException(); }
        public virtual void Init() { throw new NotImplementedException(); }
        public virtual void Move(double value) { throw new NotImplementedException(); }
        public virtual void MoveTo(double value) { throw new NotImplementedException(); }
        public virtual void Stop() { throw new NotImplementedException(); }
        public virtual void MoveToHomePosition() { throw new NotImplementedException(); }


        public abstract event ActuatorStartHanlder onActuatorStart;
        public abstract event ActuatorStopHanlder onActuatorStop;
        public abstract event ActuatorMessageHandler onMessage;


    }
}
