﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elvis.Fenix.Hardware
{
    [Flags]
    public enum ErrorFlags
    {
        INIT_ERROR = 0x01,
        CONNECT_ERROR = 0x02,
        START_ERROR = 0x04,
        GRAB_ERROR = 0x08,
        ALL = 0xFF,
    }

    public delegate void FrameReadyHandler(Object sender);
    public delegate void CameraConnectedHandler(Object sender);
    public delegate void CameraDisconectedHandler(Object sender);
    public delegate void GrabStartedHandler(Object sender);
    public delegate void GrabStoppedHandler(Object sender);
    public delegate void ErrorChangedHandler(Object sender, ErrorFlags flags);

    public abstract class FxCamera
    {
        

        public ErrorFlags Errors { set; get; }

        public abstract string Name { get; set; }

        public abstract int Width { get; set; }
        public abstract int Height { get; set; }
        public abstract int Left { get; set; }
        public abstract int Top { get; set; }
        public abstract int Exposure { set; get; }
        public abstract bool GlobalResetReleaseMode { set; get; }

        public abstract bool IsConnected { get; }
        public abstract bool IsGrabbing { get;}

        public abstract void Connect();
        public abstract void Disconnect();
        public abstract void StartGrab();
        public abstract void StopGrab();
        public abstract FxFrame GrabFrame();
        public abstract FxFrame RetrieveFrame();
        public abstract void ReleaseFrame(FxFrame frame);

        public void SetError(ErrorFlags err)
        {
            Errors |= err;
            if(onErrorChanged != null)
            {
                onErrorChanged(this, Errors);
            }
        }

        public void ClearError(ErrorFlags err)
        {
            Errors &= ~err;
            if (onErrorChanged != null)
            {
                onErrorChanged(this, Errors);
            }
        }

        public virtual event FrameReadyHandler onFrameReady;
        public event CameraConnectedHandler onCameraConnected;
        public event CameraDisconectedHandler onCameraDisconected;
        public virtual event GrabStartedHandler onGrabStarted;
        public event GrabStoppedHandler onGrabStopped;
        public event ErrorChangedHandler onErrorChanged;
    }
}
