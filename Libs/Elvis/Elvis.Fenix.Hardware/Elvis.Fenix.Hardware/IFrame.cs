﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elvis.Fenix.Hardware
{
    public interface IFrame
    {
        IntPtr Data { get; }
        int Width { get; }
        int Height { get; }
        int Stride { get; }
        int Format  { get; }
    }
}
