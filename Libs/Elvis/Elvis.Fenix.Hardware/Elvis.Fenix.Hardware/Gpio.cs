﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elvis.Fenix.Hardware
{
    public delegate void GpioOutputsEventHandler(Object sender, int pins);
    public delegate void GpioInputsEventHandler(Object sender, int pins);
    public delegate void DataReadyEventHandler(Object sender, double[] data);

    public abstract class FxGpio
    {
        public abstract string DeviceId { get; set; }
        public abstract string DeviceSerial { get; set; }
        public abstract string Vendor { get; set; }
        public abstract int MaxPinNumber { get; set; }

        public virtual bool EnableDI { set; get; }
        public virtual bool EnableDO { set; get; }
        public virtual bool IsOpen { get; set; }
        
        public virtual void Open() { throw new NotImplementedException(); }
        public virtual void Close() { throw new NotImplementedException(); }
        public virtual void TogglePin(int pin) { throw new NotImplementedException(); }
        public virtual void SetPin(int pin, int value) { throw new NotImplementedException(); }
        public virtual void SetPin(int port, int pin, int value) { throw new NotImplementedException(); }
        public virtual void SetPort(int port, int value) { throw new NotImplementedException(); }
        public virtual int GetPin(int port, int pin) { throw new NotImplementedException(); }
        public virtual int GetPort(int port) { throw new NotImplementedException(); }
        public virtual int GetDOPin(int port) { throw new NotImplementedException(); }

        public virtual void SetReferenceVoltage(short channel, ushort voltageRange) { throw new NotImplementedException(); }
        public virtual double ReadAnalogVoltage(ushort channel) { throw new NotImplementedException(); }
        public virtual int ReadAnalogValue(ushort channel) { throw new NotImplementedException(); }

        public abstract event GpioOutputsEventHandler onOutputsChanged;
        public abstract event GpioInputsEventHandler onInputsChanged;
        public abstract event DataReadyEventHandler onDataReady;

        //public GPIO()
        //{

        //}
    }
}
