﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elvis.Fenix.Hardware
{
    public delegate void FrameReadyHandler(Object sender);
    public delegate void CameraConnectedHandler(Object sender);
    public delegate void CameraDisconectedHandler(Object sender);
    public delegate void GrabStartedHandler(Object sender);
    public delegate void GrabStoppedHandler(Object sender);

    public abstract class FxCamera
    {
        public abstract int Width { get; set; }
        public abstract int Height { get; set; }
        public abstract int Left { get; set; }
        public abstract int Top { get; set; }

        public virtual void Connect() { throw new NotImplementedException(); }
        public virtual void Disconnect() { throw new NotImplementedException(); }
        public virtual void StartGrab() { throw new NotImplementedException(); }
        public virtual void StopGrab(){ throw new NotImplementedException(); }
        public virtual Object GetFrame() { throw new NotImplementedException(); }
        public virtual void ReleaseFrame(Object frame) { throw new NotImplementedException(); }

        public event FrameReadyHandler onFrameReady;
        public event CameraConnectedHandler onCameraConnected;
        public event CameraDisconectedHandler onCameraDisconected;
        public event GrabStartedHandler onGrabStarted;
        public event GrabStoppedHandler onGrabStopped;        
    }
}
