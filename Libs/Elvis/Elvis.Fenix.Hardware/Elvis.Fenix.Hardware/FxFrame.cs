﻿using Elvis.Fenix.Hardware;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elvis.Fenix.Hardware
{
    public class FxFrame : IFrame
    {
        public Object Context { get; set; }
        public IntPtr Data { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Stride { get; set; }
        public int Format { get; set; }

        public FxFrame()
        {
        }
        public FxFrame(Object context, IntPtr data, int width, int height, int pixelFormat)
        {
            Context = context;
            Data = data;
            Width = width;
            Height = height;
            Format = pixelFormat;
        }

        public Image ToImage(FxFrame frame)
        {
            Bitmap img = new Bitmap(frame.Width, frame.Height, PixelFormat.Format24bppRgb);
            Rectangle rect = new Rectangle(0, 0, frame.Width, frame.Height);
            BitmapData data = img.LockBits(rect, ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

            int step = data.Stride / data.Width;

            int size = frame.Width * frame.Height;
            byte[] values = new byte[size];
            System.Runtime.InteropServices.Marshal.Copy(frame.Data, values, 0, size);

            byte[] rgbValues = new byte[size * step];
            for (int ind = 0; ind < size; ind++)
            {
                rgbValues[ind * step] = values[ind];
                rgbValues[ind * step + 1] = values[ind];
                rgbValues[ind * step + 2] = values[ind];
            }
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, data.Scan0, size * step);

            img.UnlockBits(data);

            return img;
        }
    }
}
