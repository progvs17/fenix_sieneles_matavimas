﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Runtime.InteropServices;
using System.IO.Ports;
using System.ComponentModel;
using System.Xml;
using System.Xml.Serialization;

using CRC;
using Elvis.Fenix.IO;
using Elvis.Fenix.Hardware;

namespace Elvis.Fenix.Hardware.FxActuators
{
    public class IAILinearActuator : FxActuator
    {
        #region Private Members

        private SerialPort _actuatorSerialPort;
        private byte _slaveAddress;
        private int _currentPosition; // = 0;
        int _accel; // = 3;
        int _deccel; // = 3;
        int _speed; // = 200;
        double _maxPosition;
        bool _isMoving = false;

        FxModbusPortManager _comManager;
        Thread _movementChecker = null;

        #endregion


        #region Public Members

        /// <summary>
        /// 9005 register status structure
        /// </summary>
        [StructLayout(LayoutKind.Explicit, Size = 16)]
        public class DSS1REGS
        {
            [FieldOffset(0)]
            public bool res0;
            [FieldOffset(1)]
            public bool CLBS;
            [FieldOffset(2)]
            public bool CEND;
            [FieldOffset(3)]
            public bool PEND;
            [FieldOffset(4)]
            public bool HEND;
            [FieldOffset(5)]
            public bool STP;
            [FieldOffset(6)]
            public bool res6;
            [FieldOffset(7)]
            public bool BRKL;
            [FieldOffset(8)]
            public bool ABER;
            [FieldOffset(9)]
            public bool ALML;
            [FieldOffset(10)]
            public bool ALMH;
            [FieldOffset(11)]
            public bool PSFL;
            [FieldOffset(12)]
            public bool OnOff;
            [FieldOffset(13)]
            public bool PWR;
            [FieldOffset(14)]
            public bool SFTY;
            [FieldOffset(15)]
            public bool EMG;
        }

        #endregion


        #region Public Properties

        /// <summary>
        /// Status register @ address 0x9005
        /// </summary>
        [Browsable(false), XmlIgnore]
        public DSS1REGS RegisterDSS1 { get; set; }

   
        /// <summary>
        /// Used to assign already open/predefined serial port
        /// </summary>
        [Browsable(false), XmlIgnore]
        public override SerialPort ComPort 
        {
            get { return _actuatorSerialPort; }
            set { _actuatorSerialPort = value; }
        }

        
        /// <summary>
        /// Returns current position in 1 mm accuracy.
        /// Sets absolute position (actuator moves to that position).
        /// </summary>
        [Description("Current position in 1 mm accuracy")]
        public override double CurrentPosition
        {
            get { return _currentPosition = GetCurrentPosition() >= 654 ? 0 : GetCurrentPosition(); }
            set 
            {
                if(_actuatorSerialPort.IsOpen)
                    MoveTo(value);

                _currentPosition = (int)value;
            }
        }

        /// <summary>
        /// Maximum available position in mm's
        /// </summary>
        [Description("Maximum available position in mm's")]
        public override double MaxPosition 
        { 
            get
            {
                return _maxPosition;
            }
            set
            {
                _maxPosition = value;
            }
        }

        /// <summary>
        /// 1 mm/sec sensitive, default value is 100 mm/sec.
        /// Can be changed while moving, just resend last absolute position command
        /// </summary>
        [Description("1 mm/sec sensitive, default value is 100 mm/sec")]
        public override int Speed 
        { 
            get
            {
                return _speed;
            }
            set
            {
                _speed = value;
            }
        }

        /// <summary>
        /// 0.1 G sensitive, default value is 0.1 G.
        /// Can be changed while moving, just resend last absolute position command
        /// </summary>
        [Description("0.1 G sensitive, default value is 0.1 G")]
        public override int Acceleration 
        { 
            get
            {
                return _accel;
            }
            set
            {
                _accel = value;
            }
        }

        /// <summary>
        /// Decceleration = Acceleration.
        /// Can be changed while moving, just resend last absolute position command
        /// </summary>
        [Description("Decceleration = Acceleration")]
        public override int Decceleration 
        {
            get { return Acceleration; }
            set { Acceleration = value; } 
        }

        public override bool InitComplete
        {
            get { return _initComplete; }
        }

        /// <summary>
        /// Status about motor movement. True - moving, false - not moving
        /// </summary>
        [Description("Motor movement status")]
        public override bool IsMoving 
        {
            get 
            {
                //UpdateStatusByte();

                return _isMoving; //= !RegisterDSS1.PEND;
            } 
        }

        /// <summary>
        /// Driver's COM port & motor status
        /// </summary>
        [Description("Driver's COM port status")]
        public override bool IsConnected 
        {
            get 
            {
                UpdateStatusByte();

                if (RegisterDSS1.OnOff && RegisterDSS1.PWR && _actuatorSerialPort.IsOpen)
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Returns motor status
        /// </summary>
        [Description("Returns motor status")]
        public override string Status 
        {
            get 
            {
                string response = "NaN";

                ResetAlarms();
                // update register
                UpdateStatusByte();
                // check condition
                if (RegisterDSS1.OnOff && RegisterDSS1.PWR && !RegisterDSS1.ALMH && !RegisterDSS1.ALML && !RegisterDSS1.ABER && !RegisterDSS1.EMG)
                    response = "Ready";
                else
                    response = "Error";
                //else if (RegisterDSS1.OnOff && RegisterDSS1.PWR && !RegisterDSS1.PEND)
                //    response = "Moving";
                
                return response; 
            } 
        }

        #endregion


        #region Constructors

        /// <summary>
        /// Empty constructor
        /// </summary>
        public IAILinearActuator()
        { }

        /// <summary>
        /// Creates IAI linear actuator control object with inputed serial port name, baudrate and slave address
        /// </summary>
        /// <param name="comPort">Com port name</param>
        /// <param name="baudRate">Custom baud rate (default is 38400)</param>
        /// <param name="slaveAddr">Slave address</param>
        public IAILinearActuator(string name, string comPort, int baudRate, byte slaveAddr)
        {
            Name = name;
            // assign COM port
            _actuatorSerialPort = new SerialPort(comPort, baudRate, Parity.None, 8, StopBits.One);
            // init defaults
            ConstructorRoutine(slaveAddr);
        }

        /// <summary>
        /// Creates IAI linear actuator control object with inputed serial port, slave address and default baudrate(38400) 
        /// </summary>
        /// <param name="comPort">Com port name</param>
        /// <param name="slaveAddr">Slave address</param>
        public IAILinearActuator(string name, string comPort, byte slaveAddr)
        {
            Name = name;
            int baudRate = 38400;
            // assign COM port
            _actuatorSerialPort = new SerialPort(comPort, baudRate, Parity.None, 8, StopBits.One);
            // init defaults
            ConstructorRoutine(slaveAddr);
        }

        /// <summary>
        /// Creates IAI linear actuator control object with existing Serial port and inputed slave address.
        /// Make sure existing serial port is already open before assigning. If it is, you do not need to connect this device again.
        /// </summary>
        /// <param name="comPort">Already open/existing serial port</param>
        /// <param name="slaveAddr">Slave address</param>
        public IAILinearActuator(string name, SerialPort comPort, byte slaveAddr)
        {
            Name = name;
            // assign COM port
            _actuatorSerialPort = comPort;
            // init defaults
            ConstructorRoutine(slaveAddr);
             
        }


        #endregion


        #region Public Methods

        /// <summary>
        /// Connects to device
        /// </summary>
        public override void Connect() 
        {
            OpenCom();
        }

        /// <summary>
        /// Disconnects device
        /// </summary>
        public override void Disconnect() 
        {
            CloseCom();
        }

        /// <summary>
        /// Driver initialization. Must be done on every power up!
        /// </summary>
        public override void Init() 
        {
            _initComplete = false;

            if (onMessage != null)
                onMessage(this, "Configure sequence started.");

            Delay(100);
            
            // turn on if previously disabled
            EnableServo(true);

            // reset any alarm
            ResetAlarms();

            // return to home position
            HomePositioning();

            // indicate movement
            _isMoving = true;
            // returning to home pos
            if (onActuatorStart != null)
                onActuatorStart(this);

            // set timeout 30 seconds
            var timeout = (DateTime.Now.Second + 30);

            // update status byte, check if home position is reached
            UpdateStatusByte();
            while (!RegisterDSS1.HEND && (DateTime.Now.Second < timeout))
            {
                Delay(5);
                UpdateStatusByte();                
            }
        
            // actuator is ready to use
            if (onActuatorStop != null)
                onActuatorStop(this);

            // indicate stop
            _isMoving = false;

            if (onMessage != null)
                onMessage(this, "Configure sequence completed!");

            _initComplete = true;
        }


        /// <summary>
        /// Stops any movement
        /// </summary>
        public override void Stop()
        {
            if (_movementChecker == null)
            {
                StopMoving();
                Delay(5);
            }
            _isMoving = false;
            if (onActuatorStop != null)
                onActuatorStop(this);
        }

        /// <summary>
        /// Move by the amount inputed
        /// </summary>
        /// <param name="value">Moving amount</param>
        public override void Move(double value)
        {
            if (!_isMoving)
            {

                int val = (int)value;

                try
                {
                    int desiredPosition = 0;
                    // check if inputed step is positive or negative
                    if (value >= 0)
                        desiredPosition = (int)CurrentPosition + val;
                    else
                        desiredPosition = (int)CurrentPosition - Math.Abs(val);

                    // check range
                    if ((desiredPosition >= 0) && (desiredPosition <= MaxPosition))
                    {
                        // add value to current position
                        WriteToRegister(0x10, 0x9900, ((desiredPosition) * 100), (_speed * 100), (_accel * 10));

                        // generate movement start event
                        _isMoving = true;
                        Console.WriteLine("Actuator " + _slaveAddress.ToString() + " started");
                        if (onActuatorStart != null)
                            onActuatorStart(this);

                        // start waiting for actuator stop
                        CheckIfActuatorStopped();
                        //CheckForActuatorStop();

                    }
                    else
                    {
                        Console.WriteLine("Valid range exceeded!");

                        if (onMessage != null)
                            onMessage(this, "Valid range exceeded!");
                    }
                }
                catch
                {
                    if (onMessage != null)
                        onMessage(this, "Command 'Move' could not be completed");

                    //throw new Exception("Command 'Move' could not be completed");
                }
            }
        }

        /// <summary>
        /// Moves to absolute position
        /// </summary>
        /// <param name="value">Absolute position in mm's</param>
        public override void MoveTo(double value) 
        {
            if (!_isMoving)
            {
                int val = (int)value;

                //if (onMessage != null)
                //{
                //    onMessage(this, "Entered value: " + value.ToString());
                //    onMessage(this, "Max Pos: " + MaxPosition.ToString());
                //}

                try
                {
                    // check for valid range
                    if (val <= MaxPosition)
                    {
                        // move to absolute position
                        WriteToRegister(0x10, 0x9900, (val * 100), (_speed * 100), (_accel * 10));

                        // generate movement start event
                        _isMoving = true;
                        Console.WriteLine("Actuator " + _slaveAddress.ToString() + " started");
                        if (onActuatorStart != null)
                            onActuatorStart(this);

                        // start waiting for actuator stop
                        CheckIfActuatorStopped();
                        //CheckForActuatorStop();
                    }
                    else
                    {
                        Console.WriteLine("Maximum range exceeded!");
                        // generate error event
                        if (onMessage != null)
                            onMessage(this, "Maximum range exceeded!" + " " + MaxPosition.ToString());
                    }
                }
                catch
                {
                    if (onMessage != null)
                        onMessage(this, "Command 'MoveTo' could not be completed");

                    //throw new TimeoutException();
                    //throw new Exception("Command 'MoveTo' could not be completed");
                }
            }
        }

        public override void MoveToHomePosition() 
        {
            HomePositioning();
        }



        /// <summary>
        /// Gets alarm message code
        /// </summary>
        /// <returns>Error/alarm code value</returns>
        public int ReadError()
        {
            return ReadRegister(0x0503);
        }

        #endregion


        #region Events

        public override event ActuatorStartHanlder onActuatorStart;
        public override event ActuatorStopHanlder onActuatorStop;
        public override event ActuatorMessageHandler onMessage;
        private bool _initComplete;

        #endregion


        #region Private Methods

        private void CheckIfActuatorStopped()
        {
            if (_movementChecker != null)
                _movementChecker = null;

            _movementChecker = new Thread(new ThreadStart(CheckForActuatorStop));

            _movementChecker.Start();
            //_movementChecker.Join();
        }

        /// <summary>
        /// Constructor initialization routine
        /// </summary>
        private void ConstructorRoutine(byte slaveAddr)
        {
            // assign new status register
            RegisterDSS1 = new DSS1REGS();

            // set default values
            //_speed = 100;
            //_accel = 1;

            // set slave address
            _slaveAddress = slaveAddr;

            // setup serial port for messaging
            _comManager = new FxModbusPortManager();

        }

        /// <summary>
        /// Opens the COM port
        /// </summary>
        private void OpenCom()
        {   // check if port is not already open
            if (!_actuatorSerialPort.IsOpen)
            {
                try
                {
                    _actuatorSerialPort.Open();
                    _actuatorSerialPort.ReceivedBytesThreshold = 3;

                    //generate event
                    if (onMessage != null)
                        onMessage(this, "COM port opened succesfully.");
                }
                catch (Exception ex)
                {
                    if (onMessage != null)
                        onMessage(this, "COM port could not be opened!");
                    //throw new Exception(ex.Message);
                }

            }
            else
            {
                Console.WriteLine("Com port already open.");
                //generate error event
                if (onMessage != null)
                    onMessage(this, "Com port already open.");
            }
        }

        /// <summary>
        /// Closes the COM port
        /// </summary>
        private void CloseCom()
        {
            try
            {
                _actuatorSerialPort.Close();
            }
            catch
            {
                if (onMessage != null)
                    onMessage(this, "Com port already closed.");
                Console.WriteLine("Com port already closed.");
            }
  
        }


        #region Modbus

        /// <summary>
        /// Writes data to certain register
        /// </summary>
        /// <param name="functionCode">Enter 0x05 for config message and 0x10 for position</param>
        /// <param name="registerAddress">Register address, in HEX format</param>
        /// <param name="registerValue">Register value, in decimal</param>
        private void WriteToRegister(byte functionCode, int registerAddress, int registerValue)
        {
            int messageLength = 8;

            if (functionCode == 0x05)   // config
            {
                messageLength = 8;
            }

            if (functionCode == 0x10)   // move to position [mm]
            {
                messageLength = 13;
            }

            // create Modbus instance for CRC checking
            CRC_Modbus CRC = new CRC_Modbus();

            // split register address into 2 bytes
            int start_address_higer = registerAddress / 256;
            int start_address_lower = registerAddress % 256;
            // split register value into 2 bytes
            int value_higer = registerValue / 256;
            int value_lower = registerValue % 256;

            // create Modbus message buffer
            byte[] modbus_bytes = new byte[messageLength];
            // form a Modbus packet
            modbus_bytes[0] = _slaveAddress;
            modbus_bytes[1] = functionCode;
            modbus_bytes[2] = (byte)start_address_higer;
            modbus_bytes[3] = (byte)start_address_lower;

            // adapt modbus packet according to message length
            switch (messageLength)
            {
                case 8:     // config
                    modbus_bytes[4] = (byte)value_higer;
                    modbus_bytes[5] = (byte)value_lower;
                    break;

                case 13:    // position
                    modbus_bytes[4] = 0x00;
                    modbus_bytes[5] = 0x02; // bytes
                    modbus_bytes[6] = 0x04;
                    modbus_bytes[7] = 0x00; // poz upper - not needed
                    modbus_bytes[8] = 0x00; // poz upper - not needed
                    modbus_bytes[9] = (byte)value_higer;
                    modbus_bytes[10] = (byte)value_lower;
                    break;

            }   // end of switch (messageLength)

            // generate CRC
            int crc_result = CRC.crc16(modbus_bytes, (modbus_bytes.Length - 2));
            byte[] crc_bytes = BitConverter.GetBytes(crc_result);
            // apply CRC to message
            modbus_bytes[messageLength - 2] = crc_bytes[1];
            modbus_bytes[messageLength - 1] = crc_bytes[0];

            SendModbusMessage(modbus_bytes);

            // get answer
            byte[] read_array = new byte[7];

            read_array = _comManager.ReceivedData; // ModbusResponse(7);
        }

        /// <summary>
        /// Sends a modbus message
        /// </summary>
        /// <param name="message">Message packet</param>
        private void SendModbusMessage(byte[] message)
        {
            Delay(5);
            _comManager.SendMessage(_actuatorSerialPort, message);
        }

        /// <summary>
        /// This method is used to move actuator with custom position, speed and accel/deccel parameters
        /// </summary>
        /// <param name="functionCode">input 0x10</param>
        /// <param name="registerAddress">0x9900</param>
        /// <param name="position"></param>
        /// <param name="speed"></param>
        /// <param name="acceleration"></param>
        private void WriteToRegister(byte functionCode, int registerAddress, int position, int speed, int acceleration)
        {
            int messageLength = 23;

            // create Modbus instance for CRC checking
            CRC_Modbus CRC = new CRC_Modbus();

            // split register address into 2 bytes
            int start_address_higer = registerAddress / 256;
            int start_address_lower = registerAddress % 256;
            // split register values into 2 bytes
            int value_higer1 = position / 256;
            int value_lower1 = position % 256;

            // check for defaul speed - input is null
            if (speed == 0)
                speed = 0x2710;

            int value_higer2 = speed / 256;
            int value_lower2 = speed % 256;

            // check for defaul acceleration - input is null
            if (acceleration == 0)
                acceleration = 0x000A;
            int value_higer3 = acceleration / 256;
            int value_lower3 = acceleration % 256;

            // create Modbus message buffer
            byte[] modbus_bytes = new byte[messageLength];
            // form a Modbus packet
            modbus_bytes[0] = _slaveAddress;
            modbus_bytes[1] = functionCode;
            modbus_bytes[2] = (byte)start_address_higer;
            modbus_bytes[3] = (byte)start_address_lower;

            modbus_bytes[4] = 0x00;
            modbus_bytes[5] = 0x07; // bytes
            modbus_bytes[6] = 0x0E; // 2 * bytes
            modbus_bytes[7] = 0x00; // poz upper - not needed
            modbus_bytes[8] = 0x00; // poz upper - not needed
            modbus_bytes[9] = (byte)value_higer1;
            modbus_bytes[10] = (byte)value_lower1;

            modbus_bytes[11] = 0x00; // positioning band
            modbus_bytes[12] = 0x00;
            modbus_bytes[13] = 0x00;
            modbus_bytes[14] = 0x0A;

            modbus_bytes[15] = 0x00; // speed
            modbus_bytes[16] = 0x00;
            modbus_bytes[17] = (byte)value_higer2;
            modbus_bytes[18] = (byte)value_lower2;

            modbus_bytes[19] = (byte)value_higer3;    // acc / decc
            modbus_bytes[20] = (byte)value_lower3;    // 0.1 * 100 = 10

            // generate CRC
            int crc_result = CRC.crc16(modbus_bytes, (modbus_bytes.Length - 2));
            byte[] crc_bytes = BitConverter.GetBytes(crc_result);
            // apply CRC to message
            modbus_bytes[messageLength - 2] = crc_bytes[1];
            modbus_bytes[messageLength - 1] = crc_bytes[0];

            SendModbusMessage(modbus_bytes);

            // get answer
            byte[] read_array = new byte[7];

            read_array = _comManager.ReceivedData; // ModbusResponse(7); 
        }

        /// <summary>
        /// Reads register specified
        /// </summary>
        /// <param name="registerAddress">Register address, in HEX</param>
        /// <returns>Requested addresses value</returns>
        private int ReadRegister(int registerAddress)
        {
            // clear in buffer
            //_actuatorSerialPort.DiscardInBuffer();

            // create Modbus instance for CRC checking
            CRC_Modbus CRC = new CRC_Modbus();

            // create Modbus message buffer
            byte[] modbus_bytes = new byte[8];

            // address
            int start_address_higer = registerAddress / 256;
            int start_address_lower = registerAddress % 256;

            modbus_bytes[0] = _slaveAddress;
            modbus_bytes[1] = 0x03;
            modbus_bytes[2] = (byte)start_address_higer;
            modbus_bytes[3] = (byte)start_address_lower;

            modbus_bytes[4] = 0x00;
            modbus_bytes[5] = 0x01; // read 1 register         

            // generate CRC
            int crc_result = CRC.crc16(modbus_bytes, (modbus_bytes.Length - 2));
            byte[] crc_bytes = BitConverter.GetBytes(crc_result);
            // apply CRC to message
            modbus_bytes[modbus_bytes.Length - 2] = crc_bytes[1];
            modbus_bytes[modbus_bytes.Length - 1] = crc_bytes[0];

            SendModbusMessage(modbus_bytes);

            // get answer
            byte[] read_array = new byte[7];
            int answer = 0;

            read_array = _comManager.ReceivedData; // ModbusResponse(7);

            if (read_array != null && read_array.Length > 5)
            {
                answer |= read_array[3];
                answer = answer << 8;
                answer |= read_array[4];
            }

            return answer;
        }

        #endregion

        /// <summary>
        /// Gets current position
        /// </summary>
        /// <returns>Current position in 1 mm accuracy</returns>
        private int GetCurrentPosition()
        {
            int position = ReadRegister(0x9001);
            

            //return position / 100;
            return (int)Math.Round(((double)position / 100));
        }

        /// <summary>
        /// Turn servo motor ON or OFF
        /// </summary>
        /// <param name="ON">ON - true, OFF - false</param>
        private void EnableServo(bool ON)
        {
            if (ON)
                WriteToRegister(0x05, 0x0403, 0xFF00);
            else
                WriteToRegister(0x05, 0x0403, 0x0000);
        }

        /// <summary>
        /// Resets alarms
        /// </summary>
        private void ResetAlarms()
        {
            System.Threading.Thread.Sleep(40);
            WriteToRegister(0x05, 0x0407, 0xFF00);
            System.Threading.Thread.Sleep(40);
            WriteToRegister(0x05, 0x0407, 0x0000);
        }

        /// <summary>
        /// Moves slider to home position, needed for initilization
        /// </summary>
        private void HomePositioning()
        {
            System.Threading.Thread.Sleep(40);
            WriteToRegister(0x05, 0x040B, 0xFF00);
            System.Threading.Thread.Sleep(40);
            WriteToRegister(0x05, 0x040B, 0x0000);
        }

        /// <summary>
        /// Updates status byte from register 0x9005
        /// </summary>
        private void UpdateStatusByte()
        {
            int status = 0;
            status = ReadRegister(0x9005);

            // upper byte
            RegisterDSS1.EMG = Convert.ToBoolean((status >> 15) & 0x01) ? true : false;
            RegisterDSS1.SFTY = Convert.ToBoolean((status >> 14) & 0x01) ? true : false;
            RegisterDSS1.PWR = Convert.ToBoolean((status >> 13) & 0x01) ? true : false;
            RegisterDSS1.OnOff = Convert.ToBoolean((status >> 12) & 0x01) ? true : false;
            RegisterDSS1.PSFL = Convert.ToBoolean((status >> 11) & 0x01) ? true : false;
            RegisterDSS1.ALMH = Convert.ToBoolean((status >> 10) & 0x01) ? true : false;
            RegisterDSS1.ALML = Convert.ToBoolean((status >> 9) & 0x01) ? true : false;
            RegisterDSS1.ABER = Convert.ToBoolean((status >> 8) & 0x01) ? true : false;
            // lower byte
            RegisterDSS1.BRKL = Convert.ToBoolean((status >> 7) & 0x01) ? true : false;
            RegisterDSS1.res6 = Convert.ToBoolean((status >> 6) & 0x01) ? true : false;
            RegisterDSS1.STP = Convert.ToBoolean((status >> 5) & 0x01) ? true : false;
            RegisterDSS1.HEND = Convert.ToBoolean((status >> 4) & 0x01) ? true : false;
            RegisterDSS1.PEND = Convert.ToBoolean((status >> 3) & 0x01) ? true : false;
            RegisterDSS1.CEND = Convert.ToBoolean((status >> 2) & 0x01) ? true : false;
            RegisterDSS1.CLBS = Convert.ToBoolean((status >> 1) & 0x01) ? true : false;
            RegisterDSS1.res0 = Convert.ToBoolean(status & 0x01) ? true : false;

        }

        /// <summary>
        /// Deccelerates & stops moving
        /// </summary>
        private void StopMoving()
        {
            WriteToRegister(0x05, 0x042C, 0xFF00);
        }


        /// <summary>
        /// Continously checks if actuator has ended last operation
        /// </summary>
        private void CheckForActuatorStop()
        {
 
            UpdateStatusByte();

            bool stopped = false;
            int interval = (DateTime.Now.Second + 8) - 60;


            while ((DateTime.Now.Second - 60) < interval)
            {
                if (!RegisterDSS1.PEND)
                {
                    Delay(3);
                    UpdateStatusByte();
                }
                else
                {
                    stopped = true;
                    break;
                }
            }


            if (stopped)
            {
                if (onActuatorStop != null)
                    onActuatorStop(this);

                Console.WriteLine("Actuator " + _slaveAddress.ToString() + " stopped");
            }
            else
            {
                //if (onActuatorStop != null)
                //    onActuatorStop(this);

                if (onMessage != null)
                    onMessage(this, "Failed to move actuator: " + _slaveAddress.ToString());
            }

            _isMoving = false;

            //_movementChecker.Join();
        }


        /// <summary>
        /// Delay function
        /// </summary>
        /// <param name="time">value in ms</param>
        private void Delay(int time)
        {
            System.Threading.Thread.Sleep(time);
        }

        #endregion



    }
}
