﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using Elvis.Fenix.Hardware;
using Elvis.Fenix.Hardware.FxActuators;
using System.Threading;

namespace ActuatorTestUI
{
    public partial class Form1 : Form
    {
        FxActuator _maskSlider;
        FxActuator _cameraSlider;

        FxActuator _maskRotation;
        FxActuator _maskReduction;

        

        bool goOn = false;

        public Form1()
        {
            InitializeComponent();
            ScanComPorts();
        }

        // scan for com ports
        public void ScanComPorts()
        {
            foreach (string portname in System.IO.Ports.SerialPort.GetPortNames())
            {
                portList.Items.Add(portname);
                comboBox1.Items.Add(portname);
            }

            if (portList.Items.Count > 0)
            {
                portList.SelectedIndex = 0;
                comboBox1.SelectedIndex = 0;
            }
        }


        #region Linear Actuators

        private void btnConnect_Click(object sender, EventArgs e)
        {


            // 0x03
            _cameraSlider = new IAILinearActuator("camera", portList.SelectedItem.ToString(), 0x03);
            _cameraSlider.MaxPosition = 200;    // 20cm
            _cameraSlider.Connect();

            propertyGrid3.SelectedObject = _cameraSlider;

            // 0x02
            _maskSlider = new IAILinearActuator("mask", _cameraSlider.ComPort, 0x02);
            _maskSlider.MaxPosition = 400; // 40cm*/
            //_maskSlider.Connect();

            propertyGrid2.SelectedObject = _maskSlider;
          
            //subscribe to events
            _cameraSlider.onActuatorStart += _cameraSlider_onActuatorStart;
            _cameraSlider.onActuatorStop += _cameraSlider_onActuatorStop;
            if (_maskSlider != null)
            {
                _maskSlider.onActuatorStart += _maskSlider_onActuatorStart;
                _maskSlider.onActuatorStop += _maskSlider_onActuatorStop;
            }
        }

        #region event logging
        void _maskSlider_onActuatorStop(object sender)
        {
           // tbEventLog.AppendText("IAI mask actuator stopped. \n");
            this.Invoke(new MethodInvoker(delegate() { tbEventLog.AppendText("IAI mask actuator stopped. \n"); }));
            goOn = true;
        }

        void _maskSlider_onActuatorStart(object sender)
        {
            this.Invoke(new MethodInvoker(delegate() { tbEventLog.AppendText("IAI mask actuator started. \n"); }));
            //tbEventLog.AppendText("IAI mask actuator started. \n");
            goOn = false;
        }

        void _cameraSlider_onActuatorStop(object sender)
        {
           // tbEventLog.AppendText("IAI camera actuator stopped. \n");
            this.Invoke(new MethodInvoker(delegate() { tbEventLog.AppendText("IAI camera actuator stopped. \n"); }));
            goOn = true;
        }

        void _cameraSlider_onActuatorStart(object sender)
        {
            this.Invoke(new MethodInvoker(delegate() { tbEventLog.AppendText("IAI camera actuator started. \n"); }));
            //tbEventLog.AppendText("IAI camera actuator started. \n");
            goOn = false;
        }


        private void btbMove_Click(object sender, EventArgs e)
        {
            _maskSlider.MoveTo(int.Parse(textBox3.Text));
        }
        #endregion

        // move by
        private void button4_Click(object sender, EventArgs e)
        {
            _maskSlider.Move(int.Parse(textBox3.Text));
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            _maskSlider.Init();
        }




        // move2
        private void button3_Click(object sender, EventArgs e)
        {
            _cameraSlider.MoveTo(int.Parse(textBox4.Text));
        }

        // move by
        private void button5_Click(object sender, EventArgs e)
        {
            _cameraSlider.Move(int.Parse(textBox4.Text));
        }

        // reset2
        private void button2_Click(object sender, EventArgs e)
        {
            _cameraSlider.Init();
        }


        #endregion



        #region Rotary Actuators

        // connect
        private void button6_Click(object sender, EventArgs e)
        {
            _maskRotation = new ARDKD("rotary", comboBox1.SelectedItem.ToString(), 0x01);
            _maskRotation.Connect();
            _maskRotation.Init();

            propRotaryMask.SelectedObject = _maskRotation as FxActuator;

            _maskReduction = new CRD514KD("reduction", _maskRotation.ComPort, 0x02);

            //_maskReduction.Reset();

            _maskReduction.Init();

            propertyGrid1.SelectedObject = _maskReduction as FxActuator;

            // subscribe to events
            _maskRotation.onActuatorStart += _maskRotation_onActuatorStart;
            _maskRotation.onActuatorStop += _maskRotation_onActuatorStop;
            _maskReduction.onActuatorStart += _maskReduction_onActuatorStart;
            _maskReduction.onActuatorStop += _maskReduction_onActuatorStop;

        }


        #region event logging

        void _maskReduction_onActuatorStop(object sender)
        {
            //tbEventLog.AppendText("CRD-514-KD mask actuator stopped. \n");
            this.Invoke(new MethodInvoker(delegate() { tbEventLog.AppendText("CRD-514-KD mask actuator stopped. \n"); }));
            goOn = true;
        }

        void _maskReduction_onActuatorStart(object sender)
        {
            this.Invoke(new MethodInvoker(delegate() { tbEventLog.AppendText("CRD-514-KD mask actuator started. \n"); }));
            //tbEventLog.AppendText("CRD-514-KD mask actuator started. \n");
            goOn = false;
        }

        void _maskRotation_onActuatorStop(object sender)
        {
            //tbEventLog.AppendText("ARD-KD mask actuator stopped. \n");
            this.Invoke(new MethodInvoker(delegate() { tbEventLog.AppendText("ARD-KD mask actuator stopped. \n"); }));
            goOn = true;
        }

        void _maskRotation_onActuatorStart(object sender)
        {
            this.Invoke(new MethodInvoker(delegate() { tbEventLog.AppendText("ARD-KD mask actuator started. \n"); }));
            //tbEventLog.AppendText("ARD-KD mask actuator started. \n");
            goOn = false;
        }

        #endregion



        // move by
        private void button7_Click(object sender, EventArgs e)
        {
            _maskRotation.Move(int.Parse(textBox1.Text));
            //_maskRotation.Move((int)numericUpDown11.Value);
        }

        // move to
        private void button8_Click(object sender, EventArgs e)
        {
            _maskRotation.MoveTo(int.Parse(textBox1.Text));
            //_maskRotation.MoveTo((int)numericUpDown11.Value);
        }


        // reduction
        private void button9_Click(object sender, EventArgs e)
        {
            _maskReduction.Move(int.Parse(textBox2.Text));
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            _maskReduction.MoveTo(int.Parse(textBox2.Text));
        }

        #endregion

        Thread dooo;
        private void btnComplex_Click(object sender, EventArgs e)
        {

            dooo = new Thread(new ThreadStart(action));

            dooo.Start();
            
        }


        public void action()
        {
            _maskSlider.MoveTo(10);
            while (!goOn) ;

            _maskReduction.MoveTo(25);
            while (!goOn) ;

            _maskRotation.Move(365);
            while (!goOn) ;

            _maskReduction.MoveTo(0);
            while (!goOn) ;

            _maskSlider.MoveTo(200);
            while (!goOn) ;

            //_maskSlider.MoveTo(10);
            //while (_maskSlider.IsMoving) ;

            //_maskReduction.MoveTo(25);
            //while (_maskReduction.IsMoving) ;

            //_maskRotation.Move(365);
            //while (_maskRotation.IsMoving) ;

            //_maskReduction.MoveTo(0);
            //while (_maskReduction.IsMoving) ;

            //_maskSlider.MoveTo(200);
            //while (_maskSlider.IsMoving) ;
        }

        private void redHomeBtn_Click(object sender, EventArgs e)
        {
            _maskReduction.MoveToHomePosition();
        }

        private void stopBtn_Click(object sender, EventArgs e)
        {
            _maskReduction.Stop();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                _maskRotation.onActuatorStart -= _maskRotation_onActuatorStart;
                _maskRotation.onActuatorStop -= _maskRotation_onActuatorStop;
                _maskReduction.onActuatorStart -= _maskReduction_onActuatorStart;
                _maskReduction.onActuatorStop -= _maskReduction_onActuatorStop;
                _cameraSlider.onActuatorStart -= _cameraSlider_onActuatorStart;
                _cameraSlider.onActuatorStop -= _cameraSlider_onActuatorStop;

                _maskSlider.onActuatorStart -= _maskSlider_onActuatorStart;
                _maskSlider.onActuatorStop -= _maskSlider_onActuatorStop;
            }
            catch { }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            ARDKD ard = new ARDKD();
            ard = _maskRotation as ARDKD;

            ard.ResetAlarm();
        }
        

        


        

        

        

        

        


    }
}
