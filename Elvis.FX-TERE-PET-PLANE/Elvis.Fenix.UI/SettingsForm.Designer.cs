﻿namespace Elvis.Fenix
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.multiPanel = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.imgLogPanel = new System.Windows.Forms.Panel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.allMasksAreGoodCheck = new System.Windows.Forms.CheckBox();
            this.saveSettingsButton = new System.Windows.Forms.Button();
            this.settingsChangeOnGpioCheck = new System.Windows.Forms.CheckBox();
            this.label58 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.enableAutostartCheck = new System.Windows.Forms.CheckBox();
            this.stopAfterNGcheck = new System.Windows.Forms.CheckBox();
            this.stopAfterGDcheck = new System.Windows.Forms.CheckBox();
            this.pathTb = new System.Windows.Forms.TextBox();
            this.enableDebugCheck = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.prefixText = new System.Windows.Forms.TextBox();
            this.enNegImgLogCheck = new System.Windows.Forms.CheckBox();
            this.logFolderBrowseButton = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.enAllImgLogCheck = new System.Windows.Forms.CheckBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.yOffSpin = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.xOffSpin = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.hOffSpin = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.wOffSpin = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.cropImagesCheck = new System.Windows.Forms.CheckBox();
            this.horizontalImagesSpin = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.borderWidthSpin = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.croppedImageSizeSpin = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gpioPanel = new System.Windows.Forms.Panel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.diLabel16 = new System.Windows.Forms.Label();
            this.diLabel15 = new System.Windows.Forms.Label();
            this.diLabel14 = new System.Windows.Forms.Label();
            this.diLabel13 = new System.Windows.Forms.Label();
            this.diLabel12 = new System.Windows.Forms.Label();
            this.diLabel11 = new System.Windows.Forms.Label();
            this.diLabel10 = new System.Windows.Forms.Label();
            this.diLabel9 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.enDICheck = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.diLabel8 = new System.Windows.Forms.Label();
            this.diLabel7 = new System.Windows.Forms.Label();
            this.diLabel6 = new System.Windows.Forms.Label();
            this.diLabel5 = new System.Windows.Forms.Label();
            this.diLabel4 = new System.Windows.Forms.Label();
            this.diLabel3 = new System.Windows.Forms.Label();
            this.diLabel2 = new System.Windows.Forms.Label();
            this.diLabel1 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.input3Spin = new System.Windows.Forms.NumericUpDown();
            this.label47 = new System.Windows.Forms.Label();
            this.maskBit3InSpin = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.maskBit2InSpin = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.maskBit0InSpin = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.maskBit1InSpin = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.stopInSpin = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.manualInSpin = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.bottleInSpin = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.doButton16 = new System.Windows.Forms.Button();
            this.doButton15 = new System.Windows.Forms.Button();
            this.doButton14 = new System.Windows.Forms.Button();
            this.doButton13 = new System.Windows.Forms.Button();
            this.doButton12 = new System.Windows.Forms.Button();
            this.doButton11 = new System.Windows.Forms.Button();
            this.doButton10 = new System.Windows.Forms.Button();
            this.doButton9 = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.faultOutSpin = new System.Windows.Forms.NumericUpDown();
            this.label48 = new System.Windows.Forms.Label();
            this.NGOutSpin = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.runOutSpin = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.bottleCatchOutSpin = new System.Windows.Forms.NumericUpDown();
            this.label26 = new System.Windows.Forms.Label();
            this.actuatorOnOutSpin = new System.Windows.Forms.NumericUpDown();
            this.label27 = new System.Windows.Forms.Label();
            this.nozzlesOutSpin = new System.Windows.Forms.NumericUpDown();
            this.label28 = new System.Windows.Forms.Label();
            this.bottleFlowOutSpin = new System.Windows.Forms.NumericUpDown();
            this.hardwareEnOutSpin = new System.Windows.Forms.NumericUpDown();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.doButton8 = new System.Windows.Forms.Button();
            this.doButton7 = new System.Windows.Forms.Button();
            this.doButton6 = new System.Windows.Forms.Button();
            this.doButton5 = new System.Windows.Forms.Button();
            this.doButton4 = new System.Windows.Forms.Button();
            this.doButton3 = new System.Windows.Forms.Button();
            this.doButton2 = new System.Windows.Forms.Button();
            this.doButton1 = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.enDOCheck = new System.Windows.Forms.CheckBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.red2OffsetSpin = new System.Windows.Forms.NumericUpDown();
            this.label53 = new System.Windows.Forms.Label();
            this.red1OffsetSpin = new System.Windows.Forms.NumericUpDown();
            this.label52 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.cameraLinDeccSpin = new System.Windows.Forms.NumericUpDown();
            this.cameraLinSpeedSpin = new System.Windows.Forms.NumericUpDown();
            this.label44 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.cameraLinAccSpin = new System.Windows.Forms.NumericUpDown();
            this.label34 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.maskLinSpeedSpin = new System.Windows.Forms.NumericUpDown();
            this.label46 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.maskLinDeccSpin = new System.Windows.Forms.NumericUpDown();
            this.maskLinAccSpin = new System.Windows.Forms.NumericUpDown();
            this.label41 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.reductionDeccSpin = new System.Windows.Forms.NumericUpDown();
            this.label38 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.reductionAccSpin = new System.Windows.Forms.NumericUpDown();
            this.label35 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.rotationSpeedSpin = new System.Windows.Forms.NumericUpDown();
            this.reductionSpeedSpin = new System.Windows.Forms.NumericUpDown();
            this.label36 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.rotationAccSpin = new System.Windows.Forms.NumericUpDown();
            this.label32 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.rotationDeccSpin = new System.Windows.Forms.NumericUpDown();
            this.panel5 = new System.Windows.Forms.Panel();
            this.bypassInitCheck = new System.Windows.Forms.CheckBox();
            this.linearPortList = new System.Windows.Forms.ComboBox();
            this.rotaryPortList = new System.Windows.Forms.ComboBox();
            this.label51 = new System.Windows.Forms.Label();
            this.linearSerialTb = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.rotarySerialTb = new System.Windows.Forms.TextBox();
            this.testActuatorsBtn = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.actuatorsBtn = new System.Windows.Forms.PictureBox();
            this.gpioBtn = new System.Windows.Forms.PictureBox();
            this.imgLogBtn = new System.Windows.Forms.PictureBox();
            this.gpioOutputBtn = new System.Windows.Forms.PictureBox();
            this.updateTimer = new System.Windows.Forms.Timer(this.components);
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.tableLayoutPanel1.SuspendLayout();
            this.multiPanel.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.imgLogPanel.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.yOffSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xOffSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hOffSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wOffSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.horizontalImagesSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderWidthSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.croppedImageSizeSpin)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.gpioPanel.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.panel9.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.input3Spin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maskBit3InSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maskBit2InSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maskBit0InSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maskBit1InSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopInSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.manualInSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottleInSpin)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.faultOutSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NGOutSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.runOutSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottleCatchOutSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.actuatorOnOutSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nozzlesOutSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottleFlowOutSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hardwareEnOutSpin)).BeginInit();
            this.panel6.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.red2OffsetSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.red1OffsetSpin)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cameraLinDeccSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraLinSpeedSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraLinAccSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maskLinSpeedSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maskLinDeccSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maskLinAccSpin)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reductionDeccSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reductionAccSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotationSpeedSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reductionSpeedSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotationAccSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotationDeccSpin)).BeginInit();
            this.panel5.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.actuatorsBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpioBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLogBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpioOutputBtn)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.multiPanel, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(515, 566);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // multiPanel
            // 
            this.multiPanel.BackColor = System.Drawing.Color.White;
            this.multiPanel.Controls.Add(this.tabControl);
            this.multiPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.multiPanel.Location = new System.Drawing.Point(71, 3);
            this.multiPanel.Name = "multiPanel";
            this.multiPanel.Padding = new System.Windows.Forms.Padding(1);
            this.multiPanel.Size = new System.Drawing.Size(441, 560);
            this.multiPanel.TabIndex = 0;
            // 
            // tabControl
            // 
            this.tabControl.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Controls.Add(this.tabPage4);
            this.tabControl.Location = new System.Drawing.Point(0, -3);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(444, 566);
            this.tabControl.TabIndex = 0;
            this.tabControl.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.tabPage1.Controls.Add(this.imgLogPanel);
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(436, 540);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            // 
            // imgLogPanel
            // 
            this.imgLogPanel.Controls.Add(this.tableLayoutPanel6);
            this.imgLogPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imgLogPanel.Location = new System.Drawing.Point(3, 3);
            this.imgLogPanel.Name = "imgLogPanel";
            this.imgLogPanel.Size = new System.Drawing.Size(430, 534);
            this.imgLogPanel.TabIndex = 0;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(108)))));
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.panel14, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.panel4, 0, 2);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 4;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 169F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 167F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(430, 534);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.panel14.BackgroundImage = global::Elvis.Fenix.Properties.Resources.ui_block_settings;
            this.panel14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel14.Controls.Add(this.allMasksAreGoodCheck);
            this.panel14.Controls.Add(this.saveSettingsButton);
            this.panel14.Controls.Add(this.settingsChangeOnGpioCheck);
            this.panel14.Controls.Add(this.label58);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(0, 344);
            this.panel14.Margin = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(430, 189);
            this.panel14.TabIndex = 3;
            // 
            // allMasksAreGoodCheck
            // 
            this.allMasksAreGoodCheck.AutoSize = true;
            this.allMasksAreGoodCheck.BackColor = System.Drawing.Color.Transparent;
            this.allMasksAreGoodCheck.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.allMasksAreGoodCheck.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.allMasksAreGoodCheck.Location = new System.Drawing.Point(159, 68);
            this.allMasksAreGoodCheck.Name = "allMasksAreGoodCheck";
            this.allMasksAreGoodCheck.Size = new System.Drawing.Size(164, 22);
            this.allMasksAreGoodCheck.TabIndex = 37;
            this.allMasksAreGoodCheck.Text = "Treat all items as good";
            this.allMasksAreGoodCheck.UseVisualStyleBackColor = false;
            this.allMasksAreGoodCheck.Visible = false;
            // 
            // saveSettingsButton
            // 
            this.saveSettingsButton.BackColor = System.Drawing.Color.Transparent;
            this.saveSettingsButton.BackgroundImage = global::Elvis.Fenix.Properties.Resources.save_settings;
            this.saveSettingsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.saveSettingsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.saveSettingsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveSettingsButton.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.saveSettingsButton.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.saveSettingsButton.Location = new System.Drawing.Point(27, 31);
            this.saveSettingsButton.Name = "saveSettingsButton";
            this.saveSettingsButton.Size = new System.Drawing.Size(50, 50);
            this.saveSettingsButton.TabIndex = 26;
            this.saveSettingsButton.UseVisualStyleBackColor = false;
            this.saveSettingsButton.Click += new System.EventHandler(this.saveSettingsButton_Click);
            // 
            // settingsChangeOnGpioCheck
            // 
            this.settingsChangeOnGpioCheck.AutoSize = true;
            this.settingsChangeOnGpioCheck.BackColor = System.Drawing.Color.Transparent;
            this.settingsChangeOnGpioCheck.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.settingsChangeOnGpioCheck.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.settingsChangeOnGpioCheck.Location = new System.Drawing.Point(159, 47);
            this.settingsChangeOnGpioCheck.Name = "settingsChangeOnGpioCheck";
            this.settingsChangeOnGpioCheck.Size = new System.Drawing.Size(220, 22);
            this.settingsChangeOnGpioCheck.TabIndex = 20;
            this.settingsChangeOnGpioCheck.Text = "Enable settings change on GPIO";
            this.settingsChangeOnGpioCheck.UseVisualStyleBackColor = false;
            this.settingsChangeOnGpioCheck.Visible = false;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.label58.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label58.Location = new System.Drawing.Point(8, 84);
            this.label58.Name = "label58";
            this.label58.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label58.Size = new System.Drawing.Size(88, 18);
            this.label58.TabIndex = 25;
            this.label58.Text = "Save settings";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.panel1.BackgroundImage = global::Elvis.Fenix.Properties.Resources.ui_block_settings;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.enableAutostartCheck);
            this.panel1.Controls.Add(this.stopAfterNGcheck);
            this.panel1.Controls.Add(this.stopAfterGDcheck);
            this.panel1.Controls.Add(this.pathTb);
            this.panel1.Controls.Add(this.enableDebugCheck);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.prefixText);
            this.panel1.Controls.Add(this.enNegImgLogCheck);
            this.panel1.Controls.Add(this.logFolderBrowseButton);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.enAllImgLogCheck);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 8);
            this.panel1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(430, 168);
            this.panel1.TabIndex = 1;
            // 
            // enableAutostartCheck
            // 
            this.enableAutostartCheck.AutoSize = true;
            this.enableAutostartCheck.BackColor = System.Drawing.Color.Transparent;
            this.enableAutostartCheck.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.enableAutostartCheck.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.enableAutostartCheck.Location = new System.Drawing.Point(27, 142);
            this.enableAutostartCheck.Name = "enableAutostartCheck";
            this.enableAutostartCheck.Size = new System.Drawing.Size(128, 22);
            this.enableAutostartCheck.TabIndex = 36;
            this.enableAutostartCheck.Text = "Enable autostart";
            this.enableAutostartCheck.UseVisualStyleBackColor = false;
            // 
            // stopAfterNGcheck
            // 
            this.stopAfterNGcheck.AutoSize = true;
            this.stopAfterNGcheck.BackColor = System.Drawing.Color.Transparent;
            this.stopAfterNGcheck.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.stopAfterNGcheck.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.stopAfterNGcheck.Location = new System.Drawing.Point(256, 142);
            this.stopAfterNGcheck.Name = "stopAfterNGcheck";
            this.stopAfterNGcheck.Size = new System.Drawing.Size(109, 22);
            this.stopAfterNGcheck.TabIndex = 28;
            this.stopAfterNGcheck.Text = "Stop after NG";
            this.stopAfterNGcheck.UseVisualStyleBackColor = false;
            // 
            // stopAfterGDcheck
            // 
            this.stopAfterGDcheck.AutoSize = true;
            this.stopAfterGDcheck.BackColor = System.Drawing.Color.Transparent;
            this.stopAfterGDcheck.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.stopAfterGDcheck.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.stopAfterGDcheck.Location = new System.Drawing.Point(256, 121);
            this.stopAfterGDcheck.Name = "stopAfterGDcheck";
            this.stopAfterGDcheck.Size = new System.Drawing.Size(108, 22);
            this.stopAfterGDcheck.TabIndex = 27;
            this.stopAfterGDcheck.Text = "Stop after GD";
            this.stopAfterGDcheck.UseVisualStyleBackColor = false;
            // 
            // pathTb
            // 
            this.pathTb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(102)))), ((int)(((byte)(120)))));
            this.pathTb.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.pathTb.Font = new System.Drawing.Font("Calibri", 9F);
            this.pathTb.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.pathTb.Location = new System.Drawing.Point(27, 100);
            this.pathTb.Name = "pathTb";
            this.pathTb.Size = new System.Drawing.Size(383, 15);
            this.pathTb.TabIndex = 35;
            this.pathTb.Text = "File: ";
            // 
            // enableDebugCheck
            // 
            this.enableDebugCheck.AutoSize = true;
            this.enableDebugCheck.BackColor = System.Drawing.Color.Transparent;
            this.enableDebugCheck.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.enableDebugCheck.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.enableDebugCheck.Location = new System.Drawing.Point(27, 121);
            this.enableDebugCheck.Name = "enableDebugCheck";
            this.enableDebugCheck.Size = new System.Drawing.Size(133, 22);
            this.enableDebugCheck.TabIndex = 21;
            this.enableDebugCheck.Text = "Enable debug log";
            this.enableDebugCheck.UseVisualStyleBackColor = false;
            this.enableDebugCheck.CheckStateChanged += new System.EventHandler(this.enableDebugCheck_CheckStateChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label3.Location = new System.Drawing.Point(253, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 18);
            this.label3.TabIndex = 19;
            this.label3.Text = "Prefix: ";
            // 
            // prefixText
            // 
            this.prefixText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(112)))), ((int)(((byte)(130)))));
            this.prefixText.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.prefixText.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.prefixText.Location = new System.Drawing.Point(310, 41);
            this.prefixText.Name = "prefixText";
            this.prefixText.Size = new System.Drawing.Size(100, 26);
            this.prefixText.TabIndex = 18;
            // 
            // enNegImgLogCheck
            // 
            this.enNegImgLogCheck.AutoSize = true;
            this.enNegImgLogCheck.BackColor = System.Drawing.Color.Transparent;
            this.enNegImgLogCheck.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.enNegImgLogCheck.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.enNegImgLogCheck.Location = new System.Drawing.Point(92, 68);
            this.enNegImgLogCheck.Name = "enNegImgLogCheck";
            this.enNegImgLogCheck.Size = new System.Drawing.Size(78, 22);
            this.enNegImgLogCheck.TabIndex = 17;
            this.enNegImgLogCheck.Text = "Only NG";
            this.enNegImgLogCheck.UseVisualStyleBackColor = false;
            // 
            // logFolderBrowseButton
            // 
            this.logFolderBrowseButton.BackColor = System.Drawing.Color.Transparent;
            this.logFolderBrowseButton.BackgroundImage = global::Elvis.Fenix.Properties.Resources.load;
            this.logFolderBrowseButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.logFolderBrowseButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.logFolderBrowseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.logFolderBrowseButton.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.logFolderBrowseButton.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.logFolderBrowseButton.Location = new System.Drawing.Point(27, 41);
            this.logFolderBrowseButton.Name = "logFolderBrowseButton";
            this.logFolderBrowseButton.Size = new System.Drawing.Size(50, 50);
            this.logFolderBrowseButton.TabIndex = 4;
            this.logFolderBrowseButton.UseVisualStyleBackColor = false;
            this.logFolderBrowseButton.Click += new System.EventHandler(this.logFolderBrowseButton_Click);
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label11.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label11.Location = new System.Drawing.Point(22, 1);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(378, 35);
            this.label11.TabIndex = 11;
            this.label11.Text = "Data log";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // enAllImgLogCheck
            // 
            this.enAllImgLogCheck.AutoSize = true;
            this.enAllImgLogCheck.BackColor = System.Drawing.Color.Transparent;
            this.enAllImgLogCheck.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.enAllImgLogCheck.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.enAllImgLogCheck.Location = new System.Drawing.Point(92, 43);
            this.enAllImgLogCheck.Name = "enAllImgLogCheck";
            this.enAllImgLogCheck.Size = new System.Drawing.Size(111, 22);
            this.enAllImgLogCheck.TabIndex = 16;
            this.enAllImgLogCheck.Text = "Enable saving";
            this.enAllImgLogCheck.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.panel4.BackgroundImage = global::Elvis.Fenix.Properties.Resources.ui_block_settings;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel4.Controls.Add(this.yOffSpin);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.xOffSpin);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.hOffSpin);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.wOffSpin);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.cropImagesCheck);
            this.panel4.Controls.Add(this.horizontalImagesSpin);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.borderWidthSpin);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.croppedImageSizeSpin);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 177);
            this.panel4.Margin = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(430, 166);
            this.panel4.TabIndex = 2;
            // 
            // yOffSpin
            // 
            this.yOffSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.yOffSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.yOffSpin.ForeColor = System.Drawing.Color.White;
            this.yOffSpin.Location = new System.Drawing.Point(352, 113);
            this.yOffSpin.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.yOffSpin.Name = "yOffSpin";
            this.yOffSpin.Size = new System.Drawing.Size(46, 23);
            this.yOffSpin.TabIndex = 35;
            this.yOffSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.yOffSpin.Value = new decimal(new int[] {
            130,
            0,
            0,
            0});
            this.yOffSpin.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.label22.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label22.Location = new System.Drawing.Point(286, 114);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label22.Size = new System.Drawing.Size(54, 18);
            this.label22.TabIndex = 34;
            this.label22.Text = "Y offset";
            this.label22.Visible = false;
            // 
            // xOffSpin
            // 
            this.xOffSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.xOffSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.xOffSpin.ForeColor = System.Drawing.Color.White;
            this.xOffSpin.Location = new System.Drawing.Point(352, 84);
            this.xOffSpin.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.xOffSpin.Name = "xOffSpin";
            this.xOffSpin.Size = new System.Drawing.Size(46, 23);
            this.xOffSpin.TabIndex = 33;
            this.xOffSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.xOffSpin.Value = new decimal(new int[] {
            130,
            0,
            0,
            0});
            this.xOffSpin.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.label21.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label21.Location = new System.Drawing.Point(286, 85);
            this.label21.Name = "label21";
            this.label21.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label21.Size = new System.Drawing.Size(55, 18);
            this.label21.TabIndex = 32;
            this.label21.Text = "X offset";
            this.label21.Visible = false;
            // 
            // hOffSpin
            // 
            this.hOffSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.hOffSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.hOffSpin.ForeColor = System.Drawing.Color.White;
            this.hOffSpin.Location = new System.Drawing.Point(352, 54);
            this.hOffSpin.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.hOffSpin.Name = "hOffSpin";
            this.hOffSpin.Size = new System.Drawing.Size(46, 23);
            this.hOffSpin.TabIndex = 31;
            this.hOffSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.hOffSpin.Value = new decimal(new int[] {
            130,
            0,
            0,
            0});
            this.hOffSpin.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.label20.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label20.Location = new System.Drawing.Point(286, 55);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label20.Size = new System.Drawing.Size(56, 18);
            this.label20.TabIndex = 30;
            this.label20.Text = "H offset";
            this.label20.Visible = false;
            // 
            // wOffSpin
            // 
            this.wOffSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.wOffSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.wOffSpin.ForeColor = System.Drawing.Color.White;
            this.wOffSpin.Location = new System.Drawing.Point(352, 25);
            this.wOffSpin.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.wOffSpin.Name = "wOffSpin";
            this.wOffSpin.Size = new System.Drawing.Size(46, 23);
            this.wOffSpin.TabIndex = 29;
            this.wOffSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.wOffSpin.Value = new decimal(new int[] {
            130,
            0,
            0,
            0});
            this.wOffSpin.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.label19.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label19.Location = new System.Drawing.Point(286, 26);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label19.Size = new System.Drawing.Size(60, 18);
            this.label19.TabIndex = 28;
            this.label19.Text = "W offset";
            this.label19.Visible = false;
            // 
            // cropImagesCheck
            // 
            this.cropImagesCheck.AutoSize = true;
            this.cropImagesCheck.BackColor = System.Drawing.Color.Transparent;
            this.cropImagesCheck.Checked = true;
            this.cropImagesCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cropImagesCheck.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.cropImagesCheck.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.cropImagesCheck.Location = new System.Drawing.Point(24, 56);
            this.cropImagesCheck.Name = "cropImagesCheck";
            this.cropImagesCheck.Size = new System.Drawing.Size(157, 22);
            this.cropImagesCheck.TabIndex = 27;
            this.cropImagesCheck.Text = "Crop grabbed images";
            this.cropImagesCheck.UseVisualStyleBackColor = false;
            this.cropImagesCheck.Visible = false;
            // 
            // horizontalImagesSpin
            // 
            this.horizontalImagesSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.horizontalImagesSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.horizontalImagesSpin.ForeColor = System.Drawing.Color.White;
            this.horizontalImagesSpin.Location = new System.Drawing.Point(159, 21);
            this.horizontalImagesSpin.Name = "horizontalImagesSpin";
            this.horizontalImagesSpin.Size = new System.Drawing.Size(46, 23);
            this.horizontalImagesSpin.TabIndex = 26;
            this.horizontalImagesSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.horizontalImagesSpin.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.horizontalImagesSpin.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.label12.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label12.Location = new System.Drawing.Point(24, 25);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label12.Size = new System.Drawing.Size(119, 18);
            this.label12.TabIndex = 25;
            this.label12.Text = "Horizontal images";
            this.label12.Visible = false;
            // 
            // borderWidthSpin
            // 
            this.borderWidthSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.borderWidthSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.borderWidthSpin.ForeColor = System.Drawing.Color.White;
            this.borderWidthSpin.Location = new System.Drawing.Point(159, 113);
            this.borderWidthSpin.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.borderWidthSpin.Name = "borderWidthSpin";
            this.borderWidthSpin.Size = new System.Drawing.Size(46, 23);
            this.borderWidthSpin.TabIndex = 24;
            this.borderWidthSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.borderWidthSpin.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.borderWidthSpin.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.label10.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label10.Location = new System.Drawing.Point(24, 118);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label10.Size = new System.Drawing.Size(89, 18);
            this.label10.TabIndex = 23;
            this.label10.Text = "Border width";
            this.label10.Visible = false;
            // 
            // croppedImageSizeSpin
            // 
            this.croppedImageSizeSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.croppedImageSizeSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.croppedImageSizeSpin.ForeColor = System.Drawing.Color.White;
            this.croppedImageSizeSpin.Location = new System.Drawing.Point(159, 84);
            this.croppedImageSizeSpin.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.croppedImageSizeSpin.Name = "croppedImageSizeSpin";
            this.croppedImageSizeSpin.Size = new System.Drawing.Size(46, 23);
            this.croppedImageSizeSpin.TabIndex = 22;
            this.croppedImageSizeSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.croppedImageSizeSpin.Value = new decimal(new int[] {
            130,
            0,
            0,
            0});
            this.croppedImageSizeSpin.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.label9.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label9.Location = new System.Drawing.Point(24, 88);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label9.Size = new System.Drawing.Size(129, 18);
            this.label9.TabIndex = 21;
            this.label9.Text = "Cropped image size";
            this.label9.Visible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.tabPage2.Controls.Add(this.gpioPanel);
            this.tabPage2.Location = new System.Drawing.Point(4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(436, 540);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            // 
            // gpioPanel
            // 
            this.gpioPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.gpioPanel.Controls.Add(this.tableLayoutPanel5);
            this.gpioPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gpioPanel.Location = new System.Drawing.Point(3, 3);
            this.gpioPanel.Name = "gpioPanel";
            this.gpioPanel.Size = new System.Drawing.Size(430, 534);
            this.gpioPanel.TabIndex = 1;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.panel9, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.panel2, 0, 2);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 176F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 176F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 176F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(430, 534);
            this.tableLayoutPanel5.TabIndex = 6;
            // 
            // panel9
            // 
            this.panel9.BackgroundImage = global::Elvis.Fenix.Properties.Resources.ui_block_settings;
            this.panel9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel9.Controls.Add(this.tableLayoutPanel8);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(3, 179);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(424, 170);
            this.panel9.TabIndex = 1;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel8.ColumnCount = 4;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel8.Controls.Add(this.diLabel16, 3, 1);
            this.tableLayoutPanel8.Controls.Add(this.diLabel15, 2, 1);
            this.tableLayoutPanel8.Controls.Add(this.diLabel14, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.diLabel13, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.diLabel12, 3, 0);
            this.tableLayoutPanel8.Controls.Add(this.diLabel11, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.diLabel10, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.diLabel9, 0, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(17, 46);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.Padding = new System.Windows.Forms.Padding(3);
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 52.12766F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.87234F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(393, 78);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // diLabel16
            // 
            this.diLabel16.AutoSize = true;
            this.diLabel16.BackColor = System.Drawing.Color.IndianRed;
            this.diLabel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diLabel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.diLabel16.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.diLabel16.Location = new System.Drawing.Point(294, 43);
            this.diLabel16.Margin = new System.Windows.Forms.Padding(3);
            this.diLabel16.Name = "diLabel16";
            this.diLabel16.Size = new System.Drawing.Size(93, 29);
            this.diLabel16.TabIndex = 7;
            this.diLabel16.Text = "DI15";
            this.diLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // diLabel15
            // 
            this.diLabel15.AutoSize = true;
            this.diLabel15.BackColor = System.Drawing.Color.IndianRed;
            this.diLabel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diLabel15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.diLabel15.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.diLabel15.Location = new System.Drawing.Point(198, 43);
            this.diLabel15.Margin = new System.Windows.Forms.Padding(3);
            this.diLabel15.Name = "diLabel15";
            this.diLabel15.Size = new System.Drawing.Size(90, 29);
            this.diLabel15.TabIndex = 6;
            this.diLabel15.Text = "DI14";
            this.diLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // diLabel14
            // 
            this.diLabel14.AutoSize = true;
            this.diLabel14.BackColor = System.Drawing.Color.IndianRed;
            this.diLabel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.diLabel14.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.diLabel14.Location = new System.Drawing.Point(102, 43);
            this.diLabel14.Margin = new System.Windows.Forms.Padding(3);
            this.diLabel14.Name = "diLabel14";
            this.diLabel14.Size = new System.Drawing.Size(90, 29);
            this.diLabel14.TabIndex = 5;
            this.diLabel14.Text = "DI13";
            this.diLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // diLabel13
            // 
            this.diLabel13.AutoSize = true;
            this.diLabel13.BackColor = System.Drawing.Color.IndianRed;
            this.diLabel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.diLabel13.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.diLabel13.Location = new System.Drawing.Point(6, 43);
            this.diLabel13.Margin = new System.Windows.Forms.Padding(3);
            this.diLabel13.Name = "diLabel13";
            this.diLabel13.Size = new System.Drawing.Size(90, 29);
            this.diLabel13.TabIndex = 4;
            this.diLabel13.Text = "DI12";
            this.diLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // diLabel12
            // 
            this.diLabel12.AutoSize = true;
            this.diLabel12.BackColor = System.Drawing.Color.IndianRed;
            this.diLabel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.diLabel12.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.diLabel12.Location = new System.Drawing.Point(294, 6);
            this.diLabel12.Margin = new System.Windows.Forms.Padding(3);
            this.diLabel12.Name = "diLabel12";
            this.diLabel12.Size = new System.Drawing.Size(93, 31);
            this.diLabel12.TabIndex = 3;
            this.diLabel12.Text = "DI11";
            this.diLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // diLabel11
            // 
            this.diLabel11.AutoSize = true;
            this.diLabel11.BackColor = System.Drawing.Color.IndianRed;
            this.diLabel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.diLabel11.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.diLabel11.Location = new System.Drawing.Point(198, 6);
            this.diLabel11.Margin = new System.Windows.Forms.Padding(3);
            this.diLabel11.Name = "diLabel11";
            this.diLabel11.Size = new System.Drawing.Size(90, 31);
            this.diLabel11.TabIndex = 2;
            this.diLabel11.Text = "DI10";
            this.diLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // diLabel10
            // 
            this.diLabel10.AutoSize = true;
            this.diLabel10.BackColor = System.Drawing.Color.IndianRed;
            this.diLabel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.diLabel10.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.diLabel10.Location = new System.Drawing.Point(102, 6);
            this.diLabel10.Margin = new System.Windows.Forms.Padding(3);
            this.diLabel10.Name = "diLabel10";
            this.diLabel10.Size = new System.Drawing.Size(90, 31);
            this.diLabel10.TabIndex = 1;
            this.diLabel10.Text = "DI9";
            this.diLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // diLabel9
            // 
            this.diLabel9.AutoSize = true;
            this.diLabel9.BackColor = System.Drawing.Color.IndianRed;
            this.diLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.diLabel9.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.diLabel9.Location = new System.Drawing.Point(6, 6);
            this.diLabel9.Margin = new System.Windows.Forms.Padding(3);
            this.diLabel9.Name = "diLabel9";
            this.diLabel9.Size = new System.Drawing.Size(90, 31);
            this.diLabel9.TabIndex = 0;
            this.diLabel9.Text = "DI8";
            this.diLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.panel3.BackgroundImage = global::Elvis.Fenix.Properties.Resources.ui_block_settings;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel3.Controls.Add(this.enDICheck);
            this.panel3.Controls.Add(this.tableLayoutPanel9);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(424, 170);
            this.panel3.TabIndex = 0;
            // 
            // enDICheck
            // 
            this.enDICheck.AutoSize = true;
            this.enDICheck.BackColor = System.Drawing.Color.Transparent;
            this.enDICheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.enDICheck.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.enDICheck.Location = new System.Drawing.Point(154, 25);
            this.enDICheck.Name = "enDICheck";
            this.enDICheck.Size = new System.Drawing.Size(72, 22);
            this.enDICheck.TabIndex = 5;
            this.enDICheck.Text = "Enable";
            this.enDICheck.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel9.ColumnCount = 4;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel9.Controls.Add(this.diLabel8, 3, 1);
            this.tableLayoutPanel9.Controls.Add(this.diLabel7, 2, 1);
            this.tableLayoutPanel9.Controls.Add(this.diLabel6, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.diLabel5, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.diLabel4, 3, 0);
            this.tableLayoutPanel9.Controls.Add(this.diLabel3, 2, 0);
            this.tableLayoutPanel9.Controls.Add(this.diLabel2, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.diLabel1, 0, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(19, 68);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.Padding = new System.Windows.Forms.Padding(3);
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 52.12766F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.87234F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(393, 78);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // diLabel8
            // 
            this.diLabel8.AutoSize = true;
            this.diLabel8.BackColor = System.Drawing.Color.IndianRed;
            this.diLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.diLabel8.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.diLabel8.Location = new System.Drawing.Point(294, 43);
            this.diLabel8.Margin = new System.Windows.Forms.Padding(3);
            this.diLabel8.Name = "diLabel8";
            this.diLabel8.Size = new System.Drawing.Size(93, 29);
            this.diLabel8.TabIndex = 7;
            this.diLabel8.Text = "DI7";
            this.diLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // diLabel7
            // 
            this.diLabel7.AutoSize = true;
            this.diLabel7.BackColor = System.Drawing.Color.IndianRed;
            this.diLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.diLabel7.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.diLabel7.Location = new System.Drawing.Point(198, 43);
            this.diLabel7.Margin = new System.Windows.Forms.Padding(3);
            this.diLabel7.Name = "diLabel7";
            this.diLabel7.Size = new System.Drawing.Size(90, 29);
            this.diLabel7.TabIndex = 6;
            this.diLabel7.Text = "DI6";
            this.diLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // diLabel6
            // 
            this.diLabel6.AutoSize = true;
            this.diLabel6.BackColor = System.Drawing.Color.IndianRed;
            this.diLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.diLabel6.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.diLabel6.Location = new System.Drawing.Point(102, 43);
            this.diLabel6.Margin = new System.Windows.Forms.Padding(3);
            this.diLabel6.Name = "diLabel6";
            this.diLabel6.Size = new System.Drawing.Size(90, 29);
            this.diLabel6.TabIndex = 5;
            this.diLabel6.Text = "DI5";
            this.diLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // diLabel5
            // 
            this.diLabel5.AutoSize = true;
            this.diLabel5.BackColor = System.Drawing.Color.IndianRed;
            this.diLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.diLabel5.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.diLabel5.Location = new System.Drawing.Point(6, 43);
            this.diLabel5.Margin = new System.Windows.Forms.Padding(3);
            this.diLabel5.Name = "diLabel5";
            this.diLabel5.Size = new System.Drawing.Size(90, 29);
            this.diLabel5.TabIndex = 4;
            this.diLabel5.Text = "DI4";
            this.diLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // diLabel4
            // 
            this.diLabel4.AutoSize = true;
            this.diLabel4.BackColor = System.Drawing.Color.IndianRed;
            this.diLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.diLabel4.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.diLabel4.Location = new System.Drawing.Point(294, 6);
            this.diLabel4.Margin = new System.Windows.Forms.Padding(3);
            this.diLabel4.Name = "diLabel4";
            this.diLabel4.Size = new System.Drawing.Size(93, 31);
            this.diLabel4.TabIndex = 3;
            this.diLabel4.Text = "DI3";
            this.diLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // diLabel3
            // 
            this.diLabel3.AutoSize = true;
            this.diLabel3.BackColor = System.Drawing.Color.IndianRed;
            this.diLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.diLabel3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.diLabel3.Location = new System.Drawing.Point(198, 6);
            this.diLabel3.Margin = new System.Windows.Forms.Padding(3);
            this.diLabel3.Name = "diLabel3";
            this.diLabel3.Size = new System.Drawing.Size(90, 31);
            this.diLabel3.TabIndex = 2;
            this.diLabel3.Text = "DI2";
            this.diLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // diLabel2
            // 
            this.diLabel2.AutoSize = true;
            this.diLabel2.BackColor = System.Drawing.Color.IndianRed;
            this.diLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.diLabel2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.diLabel2.Location = new System.Drawing.Point(102, 6);
            this.diLabel2.Margin = new System.Windows.Forms.Padding(3);
            this.diLabel2.Name = "diLabel2";
            this.diLabel2.Size = new System.Drawing.Size(90, 31);
            this.diLabel2.TabIndex = 1;
            this.diLabel2.Text = "DI1";
            this.diLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // diLabel1
            // 
            this.diLabel1.AutoSize = true;
            this.diLabel1.BackColor = System.Drawing.Color.IndianRed;
            this.diLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.diLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.diLabel1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.diLabel1.Location = new System.Drawing.Point(6, 6);
            this.diLabel1.Margin = new System.Windows.Forms.Padding(3);
            this.diLabel1.Name = "diLabel1";
            this.diLabel1.Size = new System.Drawing.Size(90, 31);
            this.diLabel1.TabIndex = 0;
            this.diLabel1.Text = "DI0";
            this.diLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label14.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label14.Location = new System.Drawing.Point(22, 23);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(116, 23);
            this.label14.TabIndex = 1;
            this.label14.Text = "Digital inputs";
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::Elvis.Fenix.Properties.Resources.ui_block5;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel2.Controls.Add(this.input3Spin);
            this.panel2.Controls.Add(this.label47);
            this.panel2.Controls.Add(this.maskBit3InSpin);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.maskBit2InSpin);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.maskBit0InSpin);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.maskBit1InSpin);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.stopInSpin);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.manualInSpin);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.bottleInSpin);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.panel2.Location = new System.Drawing.Point(3, 355);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(424, 170);
            this.panel2.TabIndex = 2;
            // 
            // input3Spin
            // 
            this.input3Spin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.input3Spin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.input3Spin.ForeColor = System.Drawing.Color.White;
            this.input3Spin.Location = new System.Drawing.Point(73, 126);
            this.input3Spin.Name = "input3Spin";
            this.input3Spin.Size = new System.Drawing.Size(46, 23);
            this.input3Spin.TabIndex = 39;
            this.input3Spin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label47.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label47.Location = new System.Drawing.Point(20, 130);
            this.label47.Name = "label47";
            this.label47.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label47.Size = new System.Drawing.Size(33, 12);
            this.label47.TabIndex = 38;
            this.label47.Text = "Input 3";
            // 
            // maskBit3InSpin
            // 
            this.maskBit3InSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.maskBit3InSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.maskBit3InSpin.ForeColor = System.Drawing.Color.White;
            this.maskBit3InSpin.Location = new System.Drawing.Point(317, 126);
            this.maskBit3InSpin.Name = "maskBit3InSpin";
            this.maskBit3InSpin.Size = new System.Drawing.Size(46, 23);
            this.maskBit3InSpin.TabIndex = 35;
            this.maskBit3InSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label6.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label6.Location = new System.Drawing.Point(261, 130);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 34;
            this.label6.Text = "Config pin 3";
            // 
            // maskBit2InSpin
            // 
            this.maskBit2InSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.maskBit2InSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.maskBit2InSpin.ForeColor = System.Drawing.Color.White;
            this.maskBit2InSpin.Location = new System.Drawing.Point(317, 97);
            this.maskBit2InSpin.Name = "maskBit2InSpin";
            this.maskBit2InSpin.Size = new System.Drawing.Size(46, 23);
            this.maskBit2InSpin.TabIndex = 33;
            this.maskBit2InSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label5.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label5.Location = new System.Drawing.Point(261, 101);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 32;
            this.label5.Text = "Config pin 2";
            // 
            // maskBit0InSpin
            // 
            this.maskBit0InSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.maskBit0InSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.maskBit0InSpin.ForeColor = System.Drawing.Color.White;
            this.maskBit0InSpin.Location = new System.Drawing.Point(317, 39);
            this.maskBit0InSpin.Name = "maskBit0InSpin";
            this.maskBit0InSpin.Size = new System.Drawing.Size(46, 23);
            this.maskBit0InSpin.TabIndex = 31;
            this.maskBit0InSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(261, 43);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 30;
            this.label1.Text = "Config pin 0";
            // 
            // maskBit1InSpin
            // 
            this.maskBit1InSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.maskBit1InSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.maskBit1InSpin.ForeColor = System.Drawing.Color.White;
            this.maskBit1InSpin.Location = new System.Drawing.Point(317, 68);
            this.maskBit1InSpin.Name = "maskBit1InSpin";
            this.maskBit1InSpin.Size = new System.Drawing.Size(46, 23);
            this.maskBit1InSpin.TabIndex = 29;
            this.maskBit1InSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(261, 72);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 28;
            this.label2.Text = "Config pin 1";
            // 
            // stopInSpin
            // 
            this.stopInSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.stopInSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.stopInSpin.ForeColor = System.Drawing.Color.White;
            this.stopInSpin.Location = new System.Drawing.Point(73, 98);
            this.stopInSpin.Name = "stopInSpin";
            this.stopInSpin.Size = new System.Drawing.Size(46, 23);
            this.stopInSpin.TabIndex = 27;
            this.stopInSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label17.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label17.Location = new System.Drawing.Point(20, 102);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label17.Size = new System.Drawing.Size(29, 12);
            this.label17.TabIndex = 26;
            this.label17.Text = "STOP";
            // 
            // manualInSpin
            // 
            this.manualInSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.manualInSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.manualInSpin.ForeColor = System.Drawing.Color.White;
            this.manualInSpin.Location = new System.Drawing.Point(73, 69);
            this.manualInSpin.Name = "manualInSpin";
            this.manualInSpin.Size = new System.Drawing.Size(46, 23);
            this.manualInSpin.TabIndex = 23;
            this.manualInSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label4.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label4.Location = new System.Drawing.Point(20, 73);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(47, 12);
            this.label4.TabIndex = 22;
            this.label4.Text = "MANUAL";
            // 
            // bottleInSpin
            // 
            this.bottleInSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.bottleInSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.bottleInSpin.ForeColor = System.Drawing.Color.White;
            this.bottleInSpin.Location = new System.Drawing.Point(73, 40);
            this.bottleInSpin.Name = "bottleInSpin";
            this.bottleInSpin.Size = new System.Drawing.Size(46, 23);
            this.bottleInSpin.TabIndex = 19;
            this.bottleInSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label13.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label13.Location = new System.Drawing.Point(20, 44);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label13.Size = new System.Drawing.Size(51, 12);
            this.label13.TabIndex = 18;
            this.label13.Text = "BOTTLE IN";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label8.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label8.Location = new System.Drawing.Point(17, 16);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(126, 23);
            this.label8.TabIndex = 6;
            this.label8.Text = "System DI pins";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.tabPage3.Controls.Add(this.tableLayoutPanel3);
            this.tabPage3.Location = new System.Drawing.Point(4, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(436, 540);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.panel7, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.panel8, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.panel6, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 176F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 176F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 176F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(430, 534);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.panel7.BackgroundImage = global::Elvis.Fenix.Properties.Resources.ui_block_settings;
            this.panel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel7.Controls.Add(this.tableLayoutPanel7);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 179);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(424, 170);
            this.panel7.TabIndex = 3;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel7.ColumnCount = 4;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.Controls.Add(this.doButton16, 3, 1);
            this.tableLayoutPanel7.Controls.Add(this.doButton15, 2, 1);
            this.tableLayoutPanel7.Controls.Add(this.doButton14, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.doButton13, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.doButton12, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this.doButton11, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.doButton10, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.doButton9, 0, 0);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(23, 38);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.Padding = new System.Windows.Forms.Padding(3);
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.48544F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.51456F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(393, 91);
            this.tableLayoutPanel7.TabIndex = 5;
            // 
            // doButton16
            // 
            this.doButton16.BackColor = System.Drawing.Color.IndianRed;
            this.doButton16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doButton16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doButton16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.doButton16.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.doButton16.Location = new System.Drawing.Point(294, 48);
            this.doButton16.Name = "doButton16";
            this.doButton16.Size = new System.Drawing.Size(93, 37);
            this.doButton16.TabIndex = 7;
            this.doButton16.Text = "DO15";
            this.doButton16.UseVisualStyleBackColor = false;
            this.doButton16.Click += new System.EventHandler(this.SetDO);
            // 
            // doButton15
            // 
            this.doButton15.BackColor = System.Drawing.Color.IndianRed;
            this.doButton15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doButton15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doButton15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.doButton15.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.doButton15.Location = new System.Drawing.Point(198, 48);
            this.doButton15.Name = "doButton15";
            this.doButton15.Size = new System.Drawing.Size(90, 37);
            this.doButton15.TabIndex = 6;
            this.doButton15.Text = "DO14";
            this.doButton15.UseVisualStyleBackColor = false;
            this.doButton15.Click += new System.EventHandler(this.SetDO);
            // 
            // doButton14
            // 
            this.doButton14.BackColor = System.Drawing.Color.IndianRed;
            this.doButton14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doButton14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doButton14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.doButton14.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.doButton14.Location = new System.Drawing.Point(102, 48);
            this.doButton14.Name = "doButton14";
            this.doButton14.Size = new System.Drawing.Size(90, 37);
            this.doButton14.TabIndex = 5;
            this.doButton14.Text = "DO13";
            this.doButton14.UseVisualStyleBackColor = false;
            this.doButton14.Click += new System.EventHandler(this.SetDO);
            // 
            // doButton13
            // 
            this.doButton13.BackColor = System.Drawing.Color.IndianRed;
            this.doButton13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doButton13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doButton13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.doButton13.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.doButton13.Location = new System.Drawing.Point(6, 48);
            this.doButton13.Name = "doButton13";
            this.doButton13.Size = new System.Drawing.Size(90, 37);
            this.doButton13.TabIndex = 4;
            this.doButton13.Text = "DO12";
            this.doButton13.UseVisualStyleBackColor = false;
            this.doButton13.Click += new System.EventHandler(this.SetDO);
            // 
            // doButton12
            // 
            this.doButton12.BackColor = System.Drawing.Color.IndianRed;
            this.doButton12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doButton12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doButton12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.doButton12.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.doButton12.Location = new System.Drawing.Point(294, 6);
            this.doButton12.Name = "doButton12";
            this.doButton12.Size = new System.Drawing.Size(93, 36);
            this.doButton12.TabIndex = 3;
            this.doButton12.Text = "DO11";
            this.doButton12.UseVisualStyleBackColor = false;
            this.doButton12.Click += new System.EventHandler(this.SetDO);
            // 
            // doButton11
            // 
            this.doButton11.BackColor = System.Drawing.Color.IndianRed;
            this.doButton11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doButton11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doButton11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.doButton11.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.doButton11.Location = new System.Drawing.Point(198, 6);
            this.doButton11.Name = "doButton11";
            this.doButton11.Size = new System.Drawing.Size(90, 36);
            this.doButton11.TabIndex = 2;
            this.doButton11.Text = "DO10";
            this.doButton11.UseVisualStyleBackColor = false;
            this.doButton11.Click += new System.EventHandler(this.SetDO);
            // 
            // doButton10
            // 
            this.doButton10.BackColor = System.Drawing.Color.IndianRed;
            this.doButton10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doButton10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doButton10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.doButton10.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.doButton10.Location = new System.Drawing.Point(102, 6);
            this.doButton10.Name = "doButton10";
            this.doButton10.Size = new System.Drawing.Size(90, 36);
            this.doButton10.TabIndex = 1;
            this.doButton10.Text = "DO9";
            this.doButton10.UseVisualStyleBackColor = false;
            this.doButton10.Click += new System.EventHandler(this.SetDO);
            // 
            // doButton9
            // 
            this.doButton9.BackColor = System.Drawing.Color.IndianRed;
            this.doButton9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doButton9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doButton9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.doButton9.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.doButton9.Location = new System.Drawing.Point(6, 6);
            this.doButton9.Name = "doButton9";
            this.doButton9.Size = new System.Drawing.Size(90, 36);
            this.doButton9.TabIndex = 0;
            this.doButton9.Text = "DO8";
            this.doButton9.UseVisualStyleBackColor = false;
            this.doButton9.Click += new System.EventHandler(this.SetDO);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.panel10);
            this.panel8.Location = new System.Drawing.Point(3, 355);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(424, 163);
            this.panel8.TabIndex = 2;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.panel10.BackgroundImage = global::Elvis.Fenix.Properties.Resources.ui_block_settings;
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel10.Controls.Add(this.faultOutSpin);
            this.panel10.Controls.Add(this.label48);
            this.panel10.Controls.Add(this.NGOutSpin);
            this.panel10.Controls.Add(this.label24);
            this.panel10.Controls.Add(this.runOutSpin);
            this.panel10.Controls.Add(this.label25);
            this.panel10.Controls.Add(this.bottleCatchOutSpin);
            this.panel10.Controls.Add(this.label26);
            this.panel10.Controls.Add(this.actuatorOnOutSpin);
            this.panel10.Controls.Add(this.label27);
            this.panel10.Controls.Add(this.nozzlesOutSpin);
            this.panel10.Controls.Add(this.label28);
            this.panel10.Controls.Add(this.bottleFlowOutSpin);
            this.panel10.Controls.Add(this.hardwareEnOutSpin);
            this.panel10.Controls.Add(this.label29);
            this.panel10.Controls.Add(this.label30);
            this.panel10.Controls.Add(this.label15);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(424, 163);
            this.panel10.TabIndex = 1;
            // 
            // faultOutSpin
            // 
            this.faultOutSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.faultOutSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.faultOutSpin.ForeColor = System.Drawing.Color.White;
            this.faultOutSpin.Location = new System.Drawing.Point(84, 82);
            this.faultOutSpin.Name = "faultOutSpin";
            this.faultOutSpin.Size = new System.Drawing.Size(46, 23);
            this.faultOutSpin.TabIndex = 35;
            this.faultOutSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label48.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label48.Location = new System.Drawing.Point(20, 86);
            this.label48.Name = "label48";
            this.label48.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label48.Size = new System.Drawing.Size(26, 12);
            this.label48.TabIndex = 34;
            this.label48.Text = "Fault";
            // 
            // NGOutSpin
            // 
            this.NGOutSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.NGOutSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.NGOutSpin.ForeColor = System.Drawing.Color.White;
            this.NGOutSpin.Location = new System.Drawing.Point(323, 111);
            this.NGOutSpin.Name = "NGOutSpin";
            this.NGOutSpin.Size = new System.Drawing.Size(46, 23);
            this.NGOutSpin.TabIndex = 31;
            this.NGOutSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label24.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label24.Location = new System.Drawing.Point(266, 115);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label24.Size = new System.Drawing.Size(19, 12);
            this.label24.TabIndex = 30;
            this.label24.Text = "NG";
            // 
            // runOutSpin
            // 
            this.runOutSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.runOutSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.runOutSpin.ForeColor = System.Drawing.Color.White;
            this.runOutSpin.Location = new System.Drawing.Point(323, 82);
            this.runOutSpin.Name = "runOutSpin";
            this.runOutSpin.Size = new System.Drawing.Size(46, 23);
            this.runOutSpin.TabIndex = 29;
            this.runOutSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label25.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label25.Location = new System.Drawing.Point(266, 86);
            this.label25.Name = "label25";
            this.label25.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label25.Size = new System.Drawing.Size(26, 12);
            this.label25.TabIndex = 28;
            this.label25.Text = "RUN";
            // 
            // bottleCatchOutSpin
            // 
            this.bottleCatchOutSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.bottleCatchOutSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.bottleCatchOutSpin.ForeColor = System.Drawing.Color.White;
            this.bottleCatchOutSpin.Location = new System.Drawing.Point(323, 53);
            this.bottleCatchOutSpin.Name = "bottleCatchOutSpin";
            this.bottleCatchOutSpin.Size = new System.Drawing.Size(46, 23);
            this.bottleCatchOutSpin.TabIndex = 27;
            this.bottleCatchOutSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label26.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label26.Location = new System.Drawing.Point(266, 57);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(56, 12);
            this.label26.TabIndex = 26;
            this.label26.Text = "Bottle Catch";
            // 
            // actuatorOnOutSpin
            // 
            this.actuatorOnOutSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.actuatorOnOutSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.actuatorOnOutSpin.ForeColor = System.Drawing.Color.White;
            this.actuatorOnOutSpin.Location = new System.Drawing.Point(203, 111);
            this.actuatorOnOutSpin.Name = "actuatorOnOutSpin";
            this.actuatorOnOutSpin.Size = new System.Drawing.Size(46, 23);
            this.actuatorOnOutSpin.TabIndex = 25;
            this.actuatorOnOutSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label27.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label27.Location = new System.Drawing.Point(151, 115);
            this.label27.Name = "label27";
            this.label27.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label27.Size = new System.Drawing.Size(41, 12);
            this.label27.TabIndex = 24;
            this.label27.Text = "Actuator";
            // 
            // nozzlesOutSpin
            // 
            this.nozzlesOutSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.nozzlesOutSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.nozzlesOutSpin.ForeColor = System.Drawing.Color.White;
            this.nozzlesOutSpin.Location = new System.Drawing.Point(203, 82);
            this.nozzlesOutSpin.Name = "nozzlesOutSpin";
            this.nozzlesOutSpin.Size = new System.Drawing.Size(46, 23);
            this.nozzlesOutSpin.TabIndex = 23;
            this.nozzlesOutSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label28.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label28.Location = new System.Drawing.Point(151, 86);
            this.label28.Name = "label28";
            this.label28.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label28.Size = new System.Drawing.Size(39, 12);
            this.label28.TabIndex = 22;
            this.label28.Text = "Nozzles";
            // 
            // bottleFlowOutSpin
            // 
            this.bottleFlowOutSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.bottleFlowOutSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.bottleFlowOutSpin.ForeColor = System.Drawing.Color.White;
            this.bottleFlowOutSpin.Location = new System.Drawing.Point(203, 53);
            this.bottleFlowOutSpin.Name = "bottleFlowOutSpin";
            this.bottleFlowOutSpin.Size = new System.Drawing.Size(46, 23);
            this.bottleFlowOutSpin.TabIndex = 21;
            this.bottleFlowOutSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // hardwareEnOutSpin
            // 
            this.hardwareEnOutSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.hardwareEnOutSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.hardwareEnOutSpin.ForeColor = System.Drawing.Color.White;
            this.hardwareEnOutSpin.Location = new System.Drawing.Point(84, 53);
            this.hardwareEnOutSpin.Name = "hardwareEnOutSpin";
            this.hardwareEnOutSpin.Size = new System.Drawing.Size(46, 23);
            this.hardwareEnOutSpin.TabIndex = 20;
            this.hardwareEnOutSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label29.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label29.Location = new System.Drawing.Point(149, 57);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(52, 12);
            this.label29.TabIndex = 19;
            this.label29.Text = "Bottle Flow";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label30.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label30.Location = new System.Drawing.Point(20, 57);
            this.label30.Name = "label30";
            this.label30.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label30.Size = new System.Drawing.Size(61, 12);
            this.label30.TabIndex = 18;
            this.label30.Text = "Hardware EN";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label15.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label15.Location = new System.Drawing.Point(20, 15);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(134, 23);
            this.label15.TabIndex = 7;
            this.label15.Text = "System DO pins";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.panel6.BackgroundImage = global::Elvis.Fenix.Properties.Resources.ui_block_settings;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel6.Controls.Add(this.tableLayoutPanel4);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.enDOCheck);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(424, 170);
            this.panel6.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.Controls.Add(this.doButton8, 3, 1);
            this.tableLayoutPanel4.Controls.Add(this.doButton7, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.doButton6, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.doButton5, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.doButton4, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.doButton3, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.doButton2, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.doButton1, 0, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(23, 58);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.Padding = new System.Windows.Forms.Padding(3);
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.48544F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.51456F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(393, 91);
            this.tableLayoutPanel4.TabIndex = 5;
            // 
            // doButton8
            // 
            this.doButton8.BackColor = System.Drawing.Color.IndianRed;
            this.doButton8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doButton8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doButton8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.doButton8.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.doButton8.Location = new System.Drawing.Point(294, 48);
            this.doButton8.Name = "doButton8";
            this.doButton8.Size = new System.Drawing.Size(93, 37);
            this.doButton8.TabIndex = 7;
            this.doButton8.Text = "DO7";
            this.doButton8.UseVisualStyleBackColor = false;
            this.doButton8.Click += new System.EventHandler(this.SetDO);
            // 
            // doButton7
            // 
            this.doButton7.BackColor = System.Drawing.Color.IndianRed;
            this.doButton7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doButton7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doButton7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.doButton7.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.doButton7.Location = new System.Drawing.Point(198, 48);
            this.doButton7.Name = "doButton7";
            this.doButton7.Size = new System.Drawing.Size(90, 37);
            this.doButton7.TabIndex = 6;
            this.doButton7.Text = "DO6";
            this.doButton7.UseVisualStyleBackColor = false;
            this.doButton7.Click += new System.EventHandler(this.SetDO);
            // 
            // doButton6
            // 
            this.doButton6.BackColor = System.Drawing.Color.IndianRed;
            this.doButton6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doButton6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.doButton6.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.doButton6.Location = new System.Drawing.Point(102, 48);
            this.doButton6.Name = "doButton6";
            this.doButton6.Size = new System.Drawing.Size(90, 37);
            this.doButton6.TabIndex = 5;
            this.doButton6.Text = "DO5";
            this.doButton6.UseVisualStyleBackColor = false;
            this.doButton6.Click += new System.EventHandler(this.SetDO);
            // 
            // doButton5
            // 
            this.doButton5.BackColor = System.Drawing.Color.IndianRed;
            this.doButton5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doButton5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.doButton5.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.doButton5.Location = new System.Drawing.Point(6, 48);
            this.doButton5.Name = "doButton5";
            this.doButton5.Size = new System.Drawing.Size(90, 37);
            this.doButton5.TabIndex = 4;
            this.doButton5.Text = "DO4";
            this.doButton5.UseVisualStyleBackColor = false;
            this.doButton5.Click += new System.EventHandler(this.SetDO);
            // 
            // doButton4
            // 
            this.doButton4.BackColor = System.Drawing.Color.IndianRed;
            this.doButton4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doButton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.doButton4.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.doButton4.Location = new System.Drawing.Point(294, 6);
            this.doButton4.Name = "doButton4";
            this.doButton4.Size = new System.Drawing.Size(93, 36);
            this.doButton4.TabIndex = 3;
            this.doButton4.Text = "DO3";
            this.doButton4.UseVisualStyleBackColor = false;
            this.doButton4.Click += new System.EventHandler(this.SetDO);
            // 
            // doButton3
            // 
            this.doButton3.BackColor = System.Drawing.Color.IndianRed;
            this.doButton3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.doButton3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.doButton3.Location = new System.Drawing.Point(198, 6);
            this.doButton3.Name = "doButton3";
            this.doButton3.Size = new System.Drawing.Size(90, 36);
            this.doButton3.TabIndex = 2;
            this.doButton3.Text = "DO2";
            this.doButton3.UseVisualStyleBackColor = false;
            this.doButton3.Click += new System.EventHandler(this.SetDO);
            // 
            // doButton2
            // 
            this.doButton2.BackColor = System.Drawing.Color.IndianRed;
            this.doButton2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.doButton2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.doButton2.Location = new System.Drawing.Point(102, 6);
            this.doButton2.Name = "doButton2";
            this.doButton2.Size = new System.Drawing.Size(90, 36);
            this.doButton2.TabIndex = 1;
            this.doButton2.Text = "DO1";
            this.doButton2.UseVisualStyleBackColor = false;
            this.doButton2.Click += new System.EventHandler(this.SetDO);
            // 
            // doButton1
            // 
            this.doButton1.BackColor = System.Drawing.Color.IndianRed;
            this.doButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.doButton1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.doButton1.Location = new System.Drawing.Point(6, 6);
            this.doButton1.Name = "doButton1";
            this.doButton1.Size = new System.Drawing.Size(90, 36);
            this.doButton1.TabIndex = 0;
            this.doButton1.Text = "DO0";
            this.doButton1.UseVisualStyleBackColor = false;
            this.doButton1.Click += new System.EventHandler(this.SetDO);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label23.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label23.Location = new System.Drawing.Point(26, 22);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(128, 23);
            this.label23.TabIndex = 6;
            this.label23.Text = "Digital outputs";
            // 
            // enDOCheck
            // 
            this.enDOCheck.AutoSize = true;
            this.enDOCheck.BackColor = System.Drawing.Color.Transparent;
            this.enDOCheck.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.enDOCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.enDOCheck.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.enDOCheck.Location = new System.Drawing.Point(164, 24);
            this.enDOCheck.Name = "enDOCheck";
            this.enDOCheck.Size = new System.Drawing.Size(72, 22);
            this.enDOCheck.TabIndex = 7;
            this.enDOCheck.Text = "Enable";
            this.enDOCheck.UseVisualStyleBackColor = false;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.tabPage4.Controls.Add(this.tableLayoutPanel10);
            this.tabPage4.Location = new System.Drawing.Point(4, 4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(436, 540);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "tabPage4";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.panel13, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.panel12, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.panel11, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 5;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(430, 534);
            this.tableLayoutPanel10.TabIndex = 0;
            // 
            // panel13
            // 
            this.panel13.BackgroundImage = global::Elvis.Fenix.Properties.Resources.ui_block_settings;
            this.panel13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel13.Controls.Add(this.red2OffsetSpin);
            this.panel13.Controls.Add(this.label53);
            this.panel13.Controls.Add(this.red1OffsetSpin);
            this.panel13.Controls.Add(this.label52);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(3, 387);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(424, 122);
            this.panel13.TabIndex = 3;
            // 
            // red2OffsetSpin
            // 
            this.red2OffsetSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.red2OffsetSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.red2OffsetSpin.ForeColor = System.Drawing.Color.White;
            this.red2OffsetSpin.Location = new System.Drawing.Point(109, 52);
            this.red2OffsetSpin.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.red2OffsetSpin.Minimum = new decimal(new int[] {
            99999,
            0,
            0,
            -2147483648});
            this.red2OffsetSpin.Name = "red2OffsetSpin";
            this.red2OffsetSpin.Size = new System.Drawing.Size(65, 23);
            this.red2OffsetSpin.TabIndex = 32;
            this.red2OffsetSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label53.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label53.Location = new System.Drawing.Point(21, 56);
            this.label53.Name = "label53";
            this.label53.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label53.Size = new System.Drawing.Size(82, 12);
            this.label53.TabIndex = 31;
            this.label53.Text = "Reduction 2 Offset";
            // 
            // red1OffsetSpin
            // 
            this.red1OffsetSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.red1OffsetSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.red1OffsetSpin.ForeColor = System.Drawing.Color.White;
            this.red1OffsetSpin.Location = new System.Drawing.Point(109, 23);
            this.red1OffsetSpin.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.red1OffsetSpin.Minimum = new decimal(new int[] {
            99999,
            0,
            0,
            -2147483648});
            this.red1OffsetSpin.Name = "red1OffsetSpin";
            this.red1OffsetSpin.Size = new System.Drawing.Size(65, 23);
            this.red1OffsetSpin.TabIndex = 30;
            this.red1OffsetSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label52.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label52.Location = new System.Drawing.Point(21, 27);
            this.label52.Name = "label52";
            this.label52.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label52.Size = new System.Drawing.Size(82, 12);
            this.label52.TabIndex = 29;
            this.label52.Text = "Reduction 1 Offset";
            // 
            // panel12
            // 
            this.panel12.BackgroundImage = global::Elvis.Fenix.Properties.Resources.ui_block_settings;
            this.panel12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel12.Controls.Add(this.cameraLinDeccSpin);
            this.panel12.Controls.Add(this.cameraLinSpeedSpin);
            this.panel12.Controls.Add(this.label44);
            this.panel12.Controls.Add(this.label33);
            this.panel12.Controls.Add(this.cameraLinAccSpin);
            this.panel12.Controls.Add(this.label34);
            this.panel12.Controls.Add(this.label45);
            this.panel12.Controls.Add(this.label43);
            this.panel12.Controls.Add(this.maskLinSpeedSpin);
            this.panel12.Controls.Add(this.label46);
            this.panel12.Controls.Add(this.label42);
            this.panel12.Controls.Add(this.maskLinDeccSpin);
            this.panel12.Controls.Add(this.maskLinAccSpin);
            this.panel12.Controls.Add(this.label41);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(3, 259);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(424, 122);
            this.panel12.TabIndex = 2;
            // 
            // cameraLinDeccSpin
            // 
            this.cameraLinDeccSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.cameraLinDeccSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.cameraLinDeccSpin.ForeColor = System.Drawing.Color.White;
            this.cameraLinDeccSpin.Location = new System.Drawing.Point(323, 93);
            this.cameraLinDeccSpin.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.cameraLinDeccSpin.Name = "cameraLinDeccSpin";
            this.cameraLinDeccSpin.Size = new System.Drawing.Size(65, 23);
            this.cameraLinDeccSpin.TabIndex = 37;
            this.cameraLinDeccSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.cameraLinDeccSpin.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // cameraLinSpeedSpin
            // 
            this.cameraLinSpeedSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.cameraLinSpeedSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.cameraLinSpeedSpin.ForeColor = System.Drawing.Color.White;
            this.cameraLinSpeedSpin.Location = new System.Drawing.Point(323, 35);
            this.cameraLinSpeedSpin.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.cameraLinSpeedSpin.Name = "cameraLinSpeedSpin";
            this.cameraLinSpeedSpin.Size = new System.Drawing.Size(65, 23);
            this.cameraLinSpeedSpin.TabIndex = 33;
            this.cameraLinSpeedSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.cameraLinSpeedSpin.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label44.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label44.Location = new System.Drawing.Point(244, 97);
            this.label44.Name = "label44";
            this.label44.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label44.Size = new System.Drawing.Size(62, 12);
            this.label44.TabIndex = 36;
            this.label44.Text = "Decceleration";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label33.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label33.Location = new System.Drawing.Point(46, 10);
            this.label33.Name = "label33";
            this.label33.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label33.Size = new System.Drawing.Size(103, 23);
            this.label33.TabIndex = 8;
            this.label33.Text = "Mask linear";
            // 
            // cameraLinAccSpin
            // 
            this.cameraLinAccSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.cameraLinAccSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.cameraLinAccSpin.ForeColor = System.Drawing.Color.White;
            this.cameraLinAccSpin.Location = new System.Drawing.Point(323, 64);
            this.cameraLinAccSpin.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.cameraLinAccSpin.Name = "cameraLinAccSpin";
            this.cameraLinAccSpin.Size = new System.Drawing.Size(65, 23);
            this.cameraLinAccSpin.TabIndex = 35;
            this.cameraLinAccSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.cameraLinAccSpin.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label34.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label34.Location = new System.Drawing.Point(256, 10);
            this.label34.Name = "label34";
            this.label34.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label34.Size = new System.Drawing.Size(120, 23);
            this.label34.TabIndex = 9;
            this.label34.Text = "Camera linear";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label45.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label45.Location = new System.Drawing.Point(244, 68);
            this.label45.Name = "label45";
            this.label45.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label45.Size = new System.Drawing.Size(57, 12);
            this.label45.TabIndex = 34;
            this.label45.Text = "Acceleration";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label43.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label43.Location = new System.Drawing.Point(21, 39);
            this.label43.Name = "label43";
            this.label43.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label43.Size = new System.Drawing.Size(31, 12);
            this.label43.TabIndex = 26;
            this.label43.Text = "Speed";
            // 
            // maskLinSpeedSpin
            // 
            this.maskLinSpeedSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.maskLinSpeedSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.maskLinSpeedSpin.ForeColor = System.Drawing.Color.White;
            this.maskLinSpeedSpin.Location = new System.Drawing.Point(100, 35);
            this.maskLinSpeedSpin.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.maskLinSpeedSpin.Name = "maskLinSpeedSpin";
            this.maskLinSpeedSpin.Size = new System.Drawing.Size(65, 23);
            this.maskLinSpeedSpin.TabIndex = 27;
            this.maskLinSpeedSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.maskLinSpeedSpin.Value = new decimal(new int[] {
            600,
            0,
            0,
            0});
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label46.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label46.Location = new System.Drawing.Point(244, 39);
            this.label46.Name = "label46";
            this.label46.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label46.Size = new System.Drawing.Size(31, 12);
            this.label46.TabIndex = 32;
            this.label46.Text = "Speed";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label42.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label42.Location = new System.Drawing.Point(21, 68);
            this.label42.Name = "label42";
            this.label42.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label42.Size = new System.Drawing.Size(57, 12);
            this.label42.TabIndex = 28;
            this.label42.Text = "Acceleration";
            // 
            // maskLinDeccSpin
            // 
            this.maskLinDeccSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.maskLinDeccSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.maskLinDeccSpin.ForeColor = System.Drawing.Color.White;
            this.maskLinDeccSpin.Location = new System.Drawing.Point(100, 93);
            this.maskLinDeccSpin.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.maskLinDeccSpin.Name = "maskLinDeccSpin";
            this.maskLinDeccSpin.Size = new System.Drawing.Size(65, 23);
            this.maskLinDeccSpin.TabIndex = 31;
            this.maskLinDeccSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.maskLinDeccSpin.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // maskLinAccSpin
            // 
            this.maskLinAccSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.maskLinAccSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.maskLinAccSpin.ForeColor = System.Drawing.Color.White;
            this.maskLinAccSpin.Location = new System.Drawing.Point(100, 64);
            this.maskLinAccSpin.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.maskLinAccSpin.Name = "maskLinAccSpin";
            this.maskLinAccSpin.Size = new System.Drawing.Size(65, 23);
            this.maskLinAccSpin.TabIndex = 29;
            this.maskLinAccSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.maskLinAccSpin.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label41.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label41.Location = new System.Drawing.Point(21, 97);
            this.label41.Name = "label41";
            this.label41.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label41.Size = new System.Drawing.Size(62, 12);
            this.label41.TabIndex = 30;
            this.label41.Text = "Decceleration";
            // 
            // panel11
            // 
            this.panel11.BackgroundImage = global::Elvis.Fenix.Properties.Resources.ui_block_settings;
            this.panel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel11.Controls.Add(this.reductionDeccSpin);
            this.panel11.Controls.Add(this.label38);
            this.panel11.Controls.Add(this.label31);
            this.panel11.Controls.Add(this.reductionAccSpin);
            this.panel11.Controls.Add(this.label35);
            this.panel11.Controls.Add(this.label39);
            this.panel11.Controls.Add(this.rotationSpeedSpin);
            this.panel11.Controls.Add(this.reductionSpeedSpin);
            this.panel11.Controls.Add(this.label36);
            this.panel11.Controls.Add(this.label40);
            this.panel11.Controls.Add(this.rotationAccSpin);
            this.panel11.Controls.Add(this.label32);
            this.panel11.Controls.Add(this.label37);
            this.panel11.Controls.Add(this.rotationDeccSpin);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(3, 131);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(424, 122);
            this.panel11.TabIndex = 1;
            // 
            // reductionDeccSpin
            // 
            this.reductionDeccSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.reductionDeccSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.reductionDeccSpin.ForeColor = System.Drawing.Color.White;
            this.reductionDeccSpin.Location = new System.Drawing.Point(323, 91);
            this.reductionDeccSpin.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.reductionDeccSpin.Name = "reductionDeccSpin";
            this.reductionDeccSpin.Size = new System.Drawing.Size(65, 23);
            this.reductionDeccSpin.TabIndex = 32;
            this.reductionDeccSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.reductionDeccSpin.Value = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label38.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label38.Location = new System.Drawing.Point(244, 95);
            this.label38.Name = "label38";
            this.label38.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label38.Size = new System.Drawing.Size(62, 12);
            this.label38.TabIndex = 31;
            this.label38.Text = "Decceleration";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label31.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label31.Location = new System.Drawing.Point(46, 7);
            this.label31.Name = "label31";
            this.label31.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label31.Size = new System.Drawing.Size(78, 23);
            this.label31.TabIndex = 7;
            this.label31.Text = "Rotation";
            // 
            // reductionAccSpin
            // 
            this.reductionAccSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.reductionAccSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.reductionAccSpin.ForeColor = System.Drawing.Color.White;
            this.reductionAccSpin.Location = new System.Drawing.Point(323, 62);
            this.reductionAccSpin.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.reductionAccSpin.Name = "reductionAccSpin";
            this.reductionAccSpin.Size = new System.Drawing.Size(65, 23);
            this.reductionAccSpin.TabIndex = 30;
            this.reductionAccSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.reductionAccSpin.Value = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label35.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label35.Location = new System.Drawing.Point(21, 37);
            this.label35.Name = "label35";
            this.label35.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label35.Size = new System.Drawing.Size(31, 12);
            this.label35.TabIndex = 20;
            this.label35.Text = "Speed";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label39.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label39.Location = new System.Drawing.Point(244, 66);
            this.label39.Name = "label39";
            this.label39.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label39.Size = new System.Drawing.Size(57, 12);
            this.label39.TabIndex = 29;
            this.label39.Text = "Acceleration";
            // 
            // rotationSpeedSpin
            // 
            this.rotationSpeedSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.rotationSpeedSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.rotationSpeedSpin.ForeColor = System.Drawing.Color.White;
            this.rotationSpeedSpin.Location = new System.Drawing.Point(100, 33);
            this.rotationSpeedSpin.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.rotationSpeedSpin.Name = "rotationSpeedSpin";
            this.rotationSpeedSpin.Size = new System.Drawing.Size(65, 23);
            this.rotationSpeedSpin.TabIndex = 21;
            this.rotationSpeedSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.rotationSpeedSpin.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // reductionSpeedSpin
            // 
            this.reductionSpeedSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.reductionSpeedSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.reductionSpeedSpin.ForeColor = System.Drawing.Color.White;
            this.reductionSpeedSpin.Location = new System.Drawing.Point(323, 33);
            this.reductionSpeedSpin.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.reductionSpeedSpin.Name = "reductionSpeedSpin";
            this.reductionSpeedSpin.Size = new System.Drawing.Size(65, 23);
            this.reductionSpeedSpin.TabIndex = 28;
            this.reductionSpeedSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.reductionSpeedSpin.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label36.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label36.Location = new System.Drawing.Point(21, 66);
            this.label36.Name = "label36";
            this.label36.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label36.Size = new System.Drawing.Size(57, 12);
            this.label36.TabIndex = 22;
            this.label36.Text = "Acceleration";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label40.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label40.Location = new System.Drawing.Point(244, 37);
            this.label40.Name = "label40";
            this.label40.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label40.Size = new System.Drawing.Size(31, 12);
            this.label40.TabIndex = 27;
            this.label40.Text = "Speed";
            // 
            // rotationAccSpin
            // 
            this.rotationAccSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.rotationAccSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.rotationAccSpin.ForeColor = System.Drawing.Color.White;
            this.rotationAccSpin.Location = new System.Drawing.Point(100, 62);
            this.rotationAccSpin.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.rotationAccSpin.Name = "rotationAccSpin";
            this.rotationAccSpin.Size = new System.Drawing.Size(65, 23);
            this.rotationAccSpin.TabIndex = 23;
            this.rotationAccSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.rotationAccSpin.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label32.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label32.Location = new System.Drawing.Point(256, 7);
            this.label32.Name = "label32";
            this.label32.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label32.Size = new System.Drawing.Size(90, 23);
            this.label32.TabIndex = 26;
            this.label32.Text = "Reduction";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label37.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label37.Location = new System.Drawing.Point(21, 95);
            this.label37.Name = "label37";
            this.label37.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label37.Size = new System.Drawing.Size(62, 12);
            this.label37.TabIndex = 24;
            this.label37.Text = "Decceleration";
            // 
            // rotationDeccSpin
            // 
            this.rotationDeccSpin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.rotationDeccSpin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.rotationDeccSpin.ForeColor = System.Drawing.Color.White;
            this.rotationDeccSpin.Location = new System.Drawing.Point(100, 91);
            this.rotationDeccSpin.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.rotationDeccSpin.Name = "rotationDeccSpin";
            this.rotationDeccSpin.Size = new System.Drawing.Size(65, 23);
            this.rotationDeccSpin.TabIndex = 25;
            this.rotationDeccSpin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.rotationDeccSpin.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // panel5
            // 
            this.panel5.BackgroundImage = global::Elvis.Fenix.Properties.Resources.ui_block_settings;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Controls.Add(this.bypassInitCheck);
            this.panel5.Controls.Add(this.linearPortList);
            this.panel5.Controls.Add(this.rotaryPortList);
            this.panel5.Controls.Add(this.label51);
            this.panel5.Controls.Add(this.linearSerialTb);
            this.panel5.Controls.Add(this.label50);
            this.panel5.Controls.Add(this.rotarySerialTb);
            this.panel5.Controls.Add(this.testActuatorsBtn);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(424, 122);
            this.panel5.TabIndex = 0;
            // 
            // bypassInitCheck
            // 
            this.bypassInitCheck.AutoSize = true;
            this.bypassInitCheck.BackColor = System.Drawing.Color.Transparent;
            this.bypassInitCheck.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.bypassInitCheck.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.bypassInitCheck.Location = new System.Drawing.Point(16, 92);
            this.bypassInitCheck.Name = "bypassInitCheck";
            this.bypassInitCheck.Size = new System.Drawing.Size(145, 22);
            this.bypassInitCheck.TabIndex = 26;
            this.bypassInitCheck.Text = "Bypass initalization";
            this.bypassInitCheck.UseVisualStyleBackColor = false;
            // 
            // linearPortList
            // 
            this.linearPortList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.linearPortList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.linearPortList.ForeColor = System.Drawing.SystemColors.Window;
            this.linearPortList.FormattingEnabled = true;
            this.linearPortList.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8"});
            this.linearPortList.Location = new System.Drawing.Point(70, 59);
            this.linearPortList.Name = "linearPortList";
            this.linearPortList.Size = new System.Drawing.Size(85, 21);
            this.linearPortList.TabIndex = 24;
            // 
            // rotaryPortList
            // 
            this.rotaryPortList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.rotaryPortList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rotaryPortList.ForeColor = System.Drawing.SystemColors.Window;
            this.rotaryPortList.FormattingEnabled = true;
            this.rotaryPortList.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8"});
            this.rotaryPortList.Location = new System.Drawing.Point(70, 27);
            this.rotaryPortList.Name = "rotaryPortList";
            this.rotaryPortList.Size = new System.Drawing.Size(85, 21);
            this.rotaryPortList.TabIndex = 23;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label51.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label51.Location = new System.Drawing.Point(13, 59);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(46, 18);
            this.label51.TabIndex = 22;
            this.label51.Text = "Linear";
            // 
            // linearSerialTb
            // 
            this.linearSerialTb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(112)))), ((int)(((byte)(130)))));
            this.linearSerialTb.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.linearSerialTb.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.linearSerialTb.Location = new System.Drawing.Point(175, 56);
            this.linearSerialTb.Name = "linearSerialTb";
            this.linearSerialTb.Size = new System.Drawing.Size(100, 26);
            this.linearSerialTb.TabIndex = 21;
            this.linearSerialTb.Text = "COM1";
            this.linearSerialTb.Visible = false;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label50.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label50.Location = new System.Drawing.Point(13, 27);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(48, 18);
            this.label50.TabIndex = 20;
            this.label50.Text = "Rotary";
            // 
            // rotarySerialTb
            // 
            this.rotarySerialTb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(112)))), ((int)(((byte)(130)))));
            this.rotarySerialTb.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.rotarySerialTb.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.rotarySerialTb.Location = new System.Drawing.Point(175, 24);
            this.rotarySerialTb.Name = "rotarySerialTb";
            this.rotarySerialTb.Size = new System.Drawing.Size(100, 26);
            this.rotarySerialTb.TabIndex = 19;
            this.rotarySerialTb.Text = "COM2";
            this.rotarySerialTb.Visible = false;
            // 
            // testActuatorsBtn
            // 
            this.testActuatorsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.testActuatorsBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.testActuatorsBtn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.testActuatorsBtn.Location = new System.Drawing.Point(287, 24);
            this.testActuatorsBtn.Name = "testActuatorsBtn";
            this.testActuatorsBtn.Size = new System.Drawing.Size(101, 63);
            this.testActuatorsBtn.TabIndex = 1;
            this.testActuatorsBtn.Text = "TEST ACTUATORS";
            this.testActuatorsBtn.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.actuatorsBtn, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.gpioBtn, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.imgLogBtn, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.gpioOutputBtn, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(62, 560);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // actuatorsBtn
            // 
            this.actuatorsBtn.BackgroundImage = global::Elvis.Fenix.Properties.Resources.actuator_btn;
            this.actuatorsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.actuatorsBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actuatorsBtn.Location = new System.Drawing.Point(3, 183);
            this.actuatorsBtn.Name = "actuatorsBtn";
            this.actuatorsBtn.Size = new System.Drawing.Size(56, 54);
            this.actuatorsBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.actuatorsBtn.TabIndex = 3;
            this.actuatorsBtn.TabStop = false;
            this.actuatorsBtn.Visible = false;
            this.actuatorsBtn.Click += new System.EventHandler(this.actuatorsBtn_Click);
            // 
            // gpioBtn
            // 
            this.gpioBtn.BackgroundImage = global::Elvis.Fenix.Properties.Resources.gpio_input;
            this.gpioBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.gpioBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gpioBtn.Location = new System.Drawing.Point(3, 63);
            this.gpioBtn.Name = "gpioBtn";
            this.gpioBtn.Size = new System.Drawing.Size(56, 54);
            this.gpioBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.gpioBtn.TabIndex = 0;
            this.gpioBtn.TabStop = false;
            this.gpioBtn.Click += new System.EventHandler(this.gpioBtn_Click);
            // 
            // imgLogBtn
            // 
            this.imgLogBtn.BackgroundImage = global::Elvis.Fenix.Properties.Resources.save;
            this.imgLogBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.imgLogBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imgLogBtn.Location = new System.Drawing.Point(3, 3);
            this.imgLogBtn.Name = "imgLogBtn";
            this.imgLogBtn.Size = new System.Drawing.Size(56, 54);
            this.imgLogBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLogBtn.TabIndex = 1;
            this.imgLogBtn.TabStop = false;
            this.imgLogBtn.Click += new System.EventHandler(this.imgLogBtn_Click);
            // 
            // gpioOutputBtn
            // 
            this.gpioOutputBtn.BackgroundImage = global::Elvis.Fenix.Properties.Resources.gpio_output;
            this.gpioOutputBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.gpioOutputBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gpioOutputBtn.Location = new System.Drawing.Point(3, 123);
            this.gpioOutputBtn.Name = "gpioOutputBtn";
            this.gpioOutputBtn.Size = new System.Drawing.Size(56, 54);
            this.gpioOutputBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.gpioOutputBtn.TabIndex = 2;
            this.gpioOutputBtn.TabStop = false;
            this.gpioOutputBtn.Click += new System.EventHandler(this.gpioOutputBtn_Click);
            // 
            // updateTimer
            // 
            this.updateTimer.Enabled = true;
            this.updateTimer.Tick += new System.EventHandler(this.updateTimer_Tick);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.ClientSize = new System.Drawing.Size(515, 566);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(531, 600);
            this.MinimumSize = new System.Drawing.Size(16, 500);
            this.Name = "SettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SettingsForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsForm_FormClosing);
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.multiPanel.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.imgLogPanel.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.yOffSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xOffSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hOffSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wOffSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.horizontalImagesSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderWidthSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.croppedImageSizeSpin)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.gpioPanel.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.input3Spin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maskBit3InSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maskBit2InSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maskBit0InSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maskBit1InSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopInSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.manualInSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottleInSpin)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.faultOutSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NGOutSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.runOutSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottleCatchOutSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.actuatorOnOutSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nozzlesOutSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottleFlowOutSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hardwareEnOutSpin)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.red2OffsetSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.red1OffsetSpin)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cameraLinDeccSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraLinSpeedSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraLinAccSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maskLinSpeedSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maskLinDeccSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maskLinAccSpin)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reductionDeccSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reductionAccSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotationSpeedSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reductionSpeedSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotationAccSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotationDeccSpin)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.actuatorsBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpioBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLogBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpioOutputBtn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox gpioBtn;
        private System.Windows.Forms.PictureBox imgLogBtn;
        private System.Windows.Forms.Timer updateTimer;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.PictureBox gpioOutputBtn;
        private System.Windows.Forms.Panel multiPanel;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel imgLogPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox prefixText;
        private System.Windows.Forms.CheckBox enNegImgLogCheck;
        private System.Windows.Forms.Button logFolderBrowseButton;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox enAllImgLogCheck;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.CheckBox cropImagesCheck;
        private System.Windows.Forms.NumericUpDown horizontalImagesSpin;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown borderWidthSpin;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown croppedImageSizeSpin;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel gpioPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label diLabel16;
        private System.Windows.Forms.Label diLabel15;
        private System.Windows.Forms.Label diLabel14;
        private System.Windows.Forms.Label diLabel13;
        private System.Windows.Forms.Label diLabel12;
        private System.Windows.Forms.Label diLabel11;
        private System.Windows.Forms.Label diLabel10;
        private System.Windows.Forms.Label diLabel9;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox enDICheck;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label diLabel8;
        private System.Windows.Forms.Label diLabel7;
        private System.Windows.Forms.Label diLabel6;
        private System.Windows.Forms.Label diLabel5;
        private System.Windows.Forms.Label diLabel4;
        private System.Windows.Forms.Label diLabel3;
        private System.Windows.Forms.Label diLabel2;
        private System.Windows.Forms.Label diLabel1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.NumericUpDown maskBit3InSpin;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown maskBit2InSpin;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown maskBit0InSpin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown maskBit1InSpin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown stopInSpin;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown manualInSpin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown bottleInSpin;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Button doButton16;
        private System.Windows.Forms.Button doButton15;
        private System.Windows.Forms.Button doButton14;
        private System.Windows.Forms.Button doButton13;
        private System.Windows.Forms.Button doButton12;
        private System.Windows.Forms.Button doButton11;
        private System.Windows.Forms.Button doButton10;
        private System.Windows.Forms.Button doButton9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.NumericUpDown NGOutSpin;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.NumericUpDown runOutSpin;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.NumericUpDown bottleCatchOutSpin;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.NumericUpDown actuatorOnOutSpin;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.NumericUpDown nozzlesOutSpin;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.NumericUpDown bottleFlowOutSpin;
        private System.Windows.Forms.NumericUpDown hardwareEnOutSpin;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button doButton8;
        private System.Windows.Forms.Button doButton7;
        private System.Windows.Forms.Button doButton6;
        private System.Windows.Forms.Button doButton5;
        private System.Windows.Forms.Button doButton4;
        private System.Windows.Forms.Button doButton3;
        private System.Windows.Forms.Button doButton2;
        private System.Windows.Forms.Button doButton1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.CheckBox enDOCheck;
        private System.Windows.Forms.NumericUpDown wOffSpin;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown hOffSpin;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown yOffSpin;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.NumericUpDown xOffSpin;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox settingsChangeOnGpioCheck;
        private System.Windows.Forms.PictureBox actuatorsBtn;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button testActuatorsBtn;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.NumericUpDown cameraLinDeccSpin;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.NumericUpDown cameraLinAccSpin;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.NumericUpDown cameraLinSpeedSpin;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.NumericUpDown maskLinDeccSpin;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.NumericUpDown maskLinAccSpin;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.NumericUpDown maskLinSpeedSpin;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.NumericUpDown reductionDeccSpin;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.NumericUpDown reductionAccSpin;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.NumericUpDown reductionSpeedSpin;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.NumericUpDown rotationDeccSpin;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.NumericUpDown rotationAccSpin;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.NumericUpDown rotationSpeedSpin;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.NumericUpDown faultOutSpin;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox linearSerialTb;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox rotarySerialTb;
        private System.Windows.Forms.ComboBox linearPortList;
        private System.Windows.Forms.ComboBox rotaryPortList;
        private System.Windows.Forms.CheckBox bypassInitCheck;
        private System.Windows.Forms.NumericUpDown input3Spin;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.NumericUpDown red2OffsetSpin;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.NumericUpDown red1OffsetSpin;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.CheckBox enableDebugCheck;
        private System.Windows.Forms.TextBox pathTb;
        private System.Windows.Forms.CheckBox enableAutostartCheck;
        private System.Windows.Forms.CheckBox allMasksAreGoodCheck;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button saveSettingsButton;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.CheckBox stopAfterNGcheck;
        private System.Windows.Forms.CheckBox stopAfterGDcheck;
    }
}