﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

using Elvis.Fenix.FunctionBlocks;
using Elvis.Fenix.Hardware;
using Elvis.Fenix.General;

namespace Elvis.Fenix
{
    public enum ViewPanel
    {
        MAIN_PANEL,
        SETTINGS_PANEL,
        GPIO_PANEL,
        LOGS_PANEL,
        IMAGE_LOG_PANEL,
        GPIO_OUT_PANEL,
        ACTUATORS_PANEL
    }

    public enum StatusIcon
    {
        GPIO_ICON = 0,
        CAMERA_ICON = 1,
        ACTUATOR_ICON = 2
    }

    public enum StatusLabel
    {
        GPIO_LABEL = 0,
        CAMERA_LABEL = 1,
        ACTUATOR_LABEL = 2
    }

    public enum StatusValue
    {
        ERROR = 0,
        OK = 1
    }

    public partial class MainForm : Form
    {
        Thread introThread;
        SplashForm introForm;
        bool exitIntro = false;

        Mutex _mutex = new Mutex();
        static Object syncIcons = new Object();
        static Object syncBtns = new Object();
        static Object syncStats = new object();
        

        FenixCore _core;
        DateTime _lastTime;
        Label[] diLabel = new Label[8];
        Button[] doButton = new Button[8];
        SceneForm sceneForm;
        SettingsForm settingsForm;

        private string adminPassword = "y4uKBypt";
        private bool adminAccess = true; //false;
        private bool formShown = false;
        private bool canUpdate = true;

        public MainForm()
        {
            SplashForm.Print("Initializing components...");

            InitializeComponent();

            _lastTime = DateTime.Now;


            UpdateMultiPanel(ViewPanel.MAIN_PANEL);

            this.Text = "PET BOTTLE WALL THICKNESS INSPECTION SYSTEM Ver." + typeof(MainForm).Assembly.GetName().Version;

            SplashForm.Print("Initilizing core...");

            _core = new FenixCore(act => Invoke(act));
            _core.onErroStatusChanged += new ErrorStateChangedEventHandler(UpdateStatusIcons);
            _core.onMessage += (sndr, msg) => { AppendLogMessage(sndr, msg); };
            _core.Init();
            _core.onSceneCompleted += new SceneCompletedHandler(SceneCompleted);

            UpdateLabel(sceneNameLabel, _core.settings.SceneName);

            foreach (var block in _core.scene.blocks)
            {
                block.onTaskCompleted += new TaskCompleteHandler(BlockCompleted);

                //if(block.GetType() == (new FxRotateAndGrabImage()).GetType())
                //{
                //    (block as FxRotateAndGrabImage).onImageGrabbed += MainForm_onImageGrabbed;
                //}
            }

            //_core.onBlockComplete += new BlockCompleteHandler(UpdateHImageAfterBlock);

            //filenameLabel.DataBindings.Add(new Binding("Text", _core._imSaveBlock, "FileName"));
            filenameTb.DataBindings.Add(new Binding("Text", _core._imSaveBlock, "FileName"));


            //if (_core.settings.EnAutoRun)
            //{
            //    _core.Start();
            //    _core.EnableDI = true;
            //    _core.EnableDO = true;
            //    runBtn.BackgroundImage = Properties.Resources.run_active;
            //}

        }

        //void MainForm_onImageGrabbed(object sender)
        //{
        //    UpdateHImageAfterBlock(sender);
        //    //throw new NotImplementedException();
        //}

        private void MainForm_Load(object sender, EventArgs e)
        {
            string imPath = @"C:\chartBackground.bmp";

            Bitmap bmp = Properties.Resources.brown_bottle;
            bmp.Save(imPath);

            mainFormChart.BackImage = imPath; 

            // test chart (mainFormChart)
            mainFormChart.Series["Series1"].Points.AddXY(0.39, 1);
            mainFormChart.Series["Series1"].Points.AddXY(0.4, 2);
            mainFormChart.Series["Series1"].Points.AddXY(0.35, 3);
            mainFormChart.Series["Series1"].Points.AddXY(0.45, 4);

            mainFormChart.Series["Series2"].Points.AddXY(0.35, 1);
            mainFormChart.Series["Series2"].Points.AddXY(0.35, 2);
            mainFormChart.Series["Series2"].Points.AddXY(0.31, 3);
            mainFormChart.Series["Series2"].Points.AddXY(0.28, 4);

            mainFormChart.Series["Series3"].Points.AddXY(0.39, 1);
            mainFormChart.Series["Series3"].Points.AddXY(0.4, 2);
            mainFormChart.Series["Series3"].Points.AddXY(0.35, 3);
            mainFormChart.Series["Series3"].Points.AddXY(0.45, 4);

            mainFormChart.Series["Series4"].Points.AddXY(0.34, 1);
            mainFormChart.Series["Series4"].Points.AddXY(0.35, 2);
            mainFormChart.Series["Series4"].Points.AddXY(0.31, 3);
            mainFormChart.Series["Series4"].Points.AddXY(0.28, 4);

            mainFormChart.Series["Series5"].Points.AddXY(0.36, 1);
            mainFormChart.Series["Series5"].Points.AddXY(0.35, 2);
            mainFormChart.Series["Series5"].Points.AddXY(0.31, 3);
            mainFormChart.Series["Series5"].Points.AddXY(0.28, 4);

            UpdateChart();

        }

        private void BlockCompleted(Object sender)
        {
            FxSVMPredictor svm = sender as FxSVMPredictor;
            FxBottlePlaneData data = sender as FxBottlePlaneData;

            if (data != null)
            {
                //UpdateWindowAfterBlock(sender);


                // invoke
                if (mainFormChart.InvokeRequired)
                {
                    mainFormChart.Invoke(new Action(() => UpdateWindowAfterBlock(sender)));
                }
                else
                {
                    UpdateWindowAfterBlock(sender);
                }

            }
        }

        private void UpdateWindowAfterBlock(Object sender)
        {
            if (canUpdate)
            {
                canUpdate = false;

                lock (mainFormChart)
                {
                    FxBottlePlaneData data = sender as FxBottlePlaneData;

                    if (data != null && data.BottleData != null && data.BottleData.Count > 0)
                    {
                        try
                        {
                            // clear points
                            foreach (System.Windows.Forms.DataVisualization.Charting.Series s in mainFormChart.Series)
                            {
                                s.Points.Clear();
                            }

                            int counter = 1;

                            mainFormChart.Series["Series1"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 1);
                            mainFormChart.Series["Series1"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 2);
                            mainFormChart.Series["Series1"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 3);
                            mainFormChart.Series["Series1"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 4);

                            mainFormChart.Series["Series2"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 1);
                            mainFormChart.Series["Series2"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 2);
                            mainFormChart.Series["Series2"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 3);
                            mainFormChart.Series["Series2"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 4);

                            mainFormChart.Series["Series3"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 1);
                            mainFormChart.Series["Series3"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 2);
                            mainFormChart.Series["Series3"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 3);
                            mainFormChart.Series["Series3"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 4);

                            mainFormChart.Series["Series4"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 1);
                            mainFormChart.Series["Series4"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 2);
                            mainFormChart.Series["Series4"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 3);
                            mainFormChart.Series["Series4"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 4);

                            mainFormChart.Series["Series5"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 1);
                            mainFormChart.Series["Series5"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 2);
                            mainFormChart.Series["Series5"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 3);
                            mainFormChart.Series["Series5"].Points.AddXY(data.CalculateActualWidth(data.BottleData[counter++]), 4);

                            UpdateChart();
                        }


                        catch (Exception ex)
                        {

                            AppendLogMessage(sender, new FxMessage(FxMessageType.ERROR_MSG, "Error", ex.Message));
                        }

                    }
                    canUpdate = true;
                }
            }
        }


        private void UpdateChart()
        {
            if (mainFormChart.InvokeRequired)
            {
                mainFormChart.Invoke(new Action(() => UpdateChart()));
            }
            else
            {
                mainFormChart.ResetAutoValues();

                foreach (System.Windows.Forms.DataVisualization.Charting.Series s in mainFormChart.Series)
                {
                    s.Color = Color.Blue;
                    s.LabelBackColor = Color.White;
                    s.SmartLabelStyle.MovingDirection = System.Windows.Forms.DataVisualization.Charting.LabelAlignmentStyles.Right;

                    foreach (var item in s.Points)
                    {
                        item.Label = item.XValue.ToString("f") + "mm";
                    }
                }

                foreach (System.Windows.Forms.DataVisualization.Charting.ChartArea ca in mainFormChart.ChartAreas)
                {
                    ca.AxisY.Minimum = 1;
                    ca.AxisY.Maximum = 4;
                    ca.AxisX.Minimum = 0.0;
                    ca.AxisX.Maximum = 0.7;

                    ca.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
                    ca.AxisX.MajorGrid.Enabled = false;
                    ca.AxisY.MajorGrid.Enabled = false;

                    ca.AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
                    ca.AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;

                    ca.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
                    ca.BorderColor = Color.DarkGray;
                    //ca.BorderWidth = 2;
                }
            }
        }


        private void SceneCompleted(Object sender)
        {
            FxScene scene = sender as FxScene;

            if(scene != null)
            {
                UpdateWindowAfterScene(sender);
            }
        }

        private void UpdateWindowAfterScene(Object sender)
        {
            lock (syncStats)
            {
                FxScene scene = sender as FxScene;

                if (scene != null && scene.CountScene)
                {
                    UpdateJudgeResultLabel(scene.Judgement);
                    UpdateStatistic(_core.TotalCount, _core.OKCount, _core.NGCount);
                    UpdateLabel(_calcTimeLabel, scene.ElapsedTime.ToString("F0") + " ms");
                    UpdateJudgementStatus(scene.Judgement);
                }


                if (_core.runSceneOnce)
                {
                    _core.Stop();
                    _core.runSceneOnce = false;
                }
            }

        }


        bool checkedForStart = false;

        private void MainForm_Shown(object sender, EventArgs e)
        {
            formShown = true;

            if(windowViewSelector.Items.Count > 0)
            {
                windowViewSelector.SelectedIndex = 0;
            }

            if (_core.settings.EnAutoRun && !checkedForStart)
            {
                checkedForStart = true;
                Thread.Sleep(2000);

                runBtn_Click(this, null);
                //_core.Start();
            }
            
        }

        

             
        void UpdateMultiPanel(ViewPanel panel)
        {
           
        }

       


        void UpdateJudgementStatus(bool value)
        {
            _judgementLabel.Text = value == true ? "OK" : "NG";
            _judgementLabel.ForeColor = value == true ? Color.LawnGreen : Color.Red;
        }

        void UpdateDILabel(Label label, bool value)
        {
            label.BackColor = value == true ? Color.YellowGreen : Color.IndianRed;
        }

        void UpdateDOButton(Button button, bool value)
        {
            button.BackColor = value == true ? Color.YellowGreen : Color.IndianRed;
        }

        void UpdateStatusIcons(Object sender, ErrorFlags status)
        {
            lock (syncIcons)
            {
                UpdateStatusIcon(StatusIcon.CAMERA_ICON, (_core.Errors & ErrorFlags.CAMERA_ERROR) == ErrorFlags.CAMERA_ERROR);
                UpdateStatusIcon(StatusIcon.GPIO_ICON, (_core.Errors & ErrorFlags.GPIO_ERROR) == ErrorFlags.GPIO_ERROR);
                UpdateStatusIcon(StatusIcon.ACTUATOR_ICON, (_core.Errors & ErrorFlags.ACTUATOR_ERROR) == ErrorFlags.ACTUATOR_ERROR);
            }

        }

        void UpdateStatusIcon(StatusIcon icon, bool isError)
        {
            switch (icon)
            {
                case StatusIcon.CAMERA_ICON:
                    if (isError) UpdatePictureBox(cameraErrorIcon, Properties.Resources.camera_error);
                    else UpdatePictureBox(cameraErrorIcon, Properties.Resources.camera_ok);
                    break;

                case StatusIcon.GPIO_ICON:
                    if (isError) UpdatePictureBox(gpioErrorIcon, Properties.Resources.pci_error);
                    else UpdatePictureBox(gpioErrorIcon, Properties.Resources.pci_ok);
                    break;

                case StatusIcon.ACTUATOR_ICON:
                    if (isError) UpdatePictureBox(actuatorErrorIcon, Properties.Resources.actuator_error);
                    else UpdatePictureBox(actuatorErrorIcon, Properties.Resources.actuator);
                    break;
            }
        }

        void UpdatePictureBox(PictureBox pict, Image img)
        {
            if(pict.InvokeRequired)
            {
                pict.Invoke(new Action(() => { UpdatePictureBox(pict, img); } ));
            }

            pict.Image = img;
            pict.Invalidate();
        }

        void AppendLogMessage(Object sender, FxMessage msg)
        {
            if(!formShown)
            {
                SplashForm.Print(msg.Title + ": " + msg.Message);
            }

            if (logMsg.InvokeRequired)
            {
                logMsg.Invoke(new Action(() => AppendLogMessage(sender, msg)));
            }
            else
            {
                switch (msg.Type)
                {
                    case FxMessageType.INFO_MSG:
                        logMsg.SelectionColor = Color.White;
                        break;

                    case FxMessageType.WARNING_MSG:
                        logMsg.SelectionColor = Color.Yellow;
                        break;

                    case FxMessageType.ERROR_MSG:
                        logMsg.SelectionColor = Color.Coral;
                        break;

                    case FxMessageType.MASK_OK_MSG:
                        logMsg.SelectionColor = Color.SpringGreen;
                        break;
                }

                logMsg.AppendText( DateTime.Now.ToString("yy-MM-dd HH:mm:ss") + " : [" + msg.Title  + "] " +  msg.Message + "\n");
                logMsg.ScrollToCaret();
            }
        }

        void UpdateJudgeResultLabel(bool value)
        {
            if(_judgementLabel.InvokeRequired)
            {
                _judgementLabel.Invoke(new Action(() => { UpdateJudgeResultLabel(value); }));
            }
            else
            {
                _judgementLabel.Text = value == true ? "OK" : "NG";
                _judgementLabel.ForeColor = value == true ? Color.LawnGreen : Color.Red;
            }
        }

        void UpdateStatistic(int total, int gdCount, int ngCount)
        {
            UpdateLabel(_statisticTotalLabel, "Total: " + total.ToString());
            UpdateLabel(_statisticGoodLabel, "OK: " + gdCount.ToString());
            UpdateLabel(_statisticNegativeLabel, "NG: " + ngCount.ToString());
            double ef = 100 * (1 - ((double)ngCount / (double)total));
            UpdateLabel(_statisticEfficientLabel, "Efficiency: " + ef.ToString("F2") + " %");
        }

        void UpdateLabel(Label label, string text)
        {
            if (label.InvokeRequired)
            {
                label.Invoke(new Action(() => { UpdateLabel(label, text); }));
            }
            else
            {
                label.Text = text;
            }
        }

        void UpdateControls()
        {
            if(_core != null)
            {
                lock (syncBtns)
                {
                    UpdateLabel(sceneNameLabel, "Scene: " + _core.settings.SceneName);

                    enGpioBtn.BackgroundImage = _core.EnableDI && _core.EnableDO ? Properties.Resources.gpio_active : Properties.Resources.gpio;
                    enImgLogBtn.BackgroundImage = _core.EnableImgLog ? Properties.Resources.save_active : Properties.Resources.save;
                    runBtn.BackgroundImage = _core.IsStarted() ? Properties.Resources.run_active : Properties.Resources.run;

                    enFileSourceBtn.Image = _core._imSaveBlock.EnableImageLoader ? Properties.Resources.file_source_icon_active : Properties.Resources.file_source_icon;


                    if(adminAccess)
                    {
                        settingsBtn.Visible = true;
                        flowBtn.Visible = true;
                        singleRun1Btn.Visible = true;
                        //singleRun2Btn.Visible = true;
                        //enFileSourceBtn.Visible = true;
                        enGpioBtn.Visible = true;
                    }
                    else
                    {
                        settingsBtn.Visible = false;
                        flowBtn.Visible = false;
                        singleRun1Btn.Visible = false;
                        //singleRun2Btn.Visible = false;
                        enFileSourceBtn.Visible = false;
                        enGpioBtn.Visible = false;
                    }
                }
            }            
        }

        // Events

        private void button1_Click(object sender, EventArgs e)
        {
            _core.Start();
        }
        
         private void _homeButton_Click(object sender, EventArgs e)
        {
            UpdateMultiPanel(ViewPanel.MAIN_PANEL);
        }

        private void _settingsButton_Click(object sender, EventArgs e)
        {
            UpdateMultiPanel(ViewPanel.SETTINGS_PANEL);

        }

        private void _gpioButton_Click(object sender, EventArgs e)
        {
            UpdateMultiPanel(ViewPanel.GPIO_PANEL);
        }

        private void _logButton_Click(object sender, EventArgs e)
        {
            UpdateMultiPanel(ViewPanel.LOGS_PANEL);
        }

        private void SceneFormButton_Click(object sender, EventArgs e)
        {
            if (_core != null && _core.IsStarted())
            {
                MessageBox.Show("Stop the main proccess", "Error!");
                return;
            }

#if !DEBUG
            if (_core != null && !_core.ManualEnabled())
            {
                MessageBox.Show("Switch to MANUAL", "Error!");
                return;
            }
#endif

            _core.Stop();

            _core.onSceneCompleted -= new SceneCompletedHandler(SceneCompleted);


            foreach (var block in _core.scene.blocks)
            {
                block.onTaskCompleted -= new TaskCompleteHandler(BlockCompleted);
                block.SingleRun = false;
            }

            //_core.onBlockComplete -= new BlockCompleteHandler(UpdateHImageAfterBlock);
            //_core.EnableDI = false;
            //_core.EnableDO = false;  


            if(sceneForm == null )
            {
                sceneForm = new SceneForm(this, _core);                
            }
            
            sceneForm.ShowDialog();
            //sceneForm.Dispose();
            
            sceneForm = null;
            _core.onSceneCompleted += new SceneCompletedHandler(SceneCompleted);


            foreach (var block in _core.scene.blocks)
            {
                block.onTaskCompleted += new TaskCompleteHandler(BlockCompleted);
                block.SingleRun = true;
            }

        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _core.Stop();
            Thread.Sleep(1000);
            //_core.SaveCommonSettings();
            _core.Dispose();

            //lock (hWindow)
            //{ 
            //}            
        }

        private void _stopButton_Click(object sender, EventArgs e)
        {
            _core.Stop();
        }

        private void logFolderBrowseButton_Click(object sender, EventArgs e)
        {
            if(logFolderDialog.ShowDialog() == DialogResult.OK)
            {
                _core._imgLogger.FilePath = logFolderDialog.SelectedPath;
            }
        }

        private void enImgLogCheck_CheckedChanged(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //if (_core != null && _core.IsStarted())
            //{
            //    MessageBox.Show("Stop the main proccess", "Error!");
            //    return;
            //}

            if (settingsForm == null)
            {
                settingsForm = new SettingsForm(_core);
            }
            settingsForm.ShowDialog();
        }

        private void runBtn_Click(object sender, EventArgs e)
        {
            if(!_core.IsStarted())
            {
                _core.runSceneOnce = false;
                _core.Start();

                if (_core.IsStarted())
                {
                    _core.EnableDI = true;
                    _core.EnableDO = true;
                    runBtn.BackgroundImage = Properties.Resources.run_active;
                    Thread.Sleep(1000);
                }
            }
            else
            {
                _core.Stop();
                runBtn.BackgroundImage = Properties.Resources.run;
            }
        }       

        private void enGpioBtn_Click(object sender, EventArgs e)
        {
            if(!_core.EnableDI)
            {
                _core.EnableDI = true;
                _core.EnableDO = true;
                enGpioBtn.BackgroundImage = Properties.Resources.gpio_active;
            }
            else
            {
                _core.EnableDI = false;
                _core.EnableDO = false;
                enGpioBtn.BackgroundImage = Properties.Resources.gpio;
            }
        }

        private void enImgLogBtn_Click(object sender, EventArgs e)
        {
            if(!_core.EnableImgLog)
            {
                _core.EnableImgLog = true;
                enImgLogBtn.BackgroundImage = Properties.Resources.save_active;
            }
            else
            {
                _core.EnableImgLog = false;
                enImgLogBtn.BackgroundImage = Properties.Resources.save;
            }
        }

        private void controlUpdate_Tick(object sender, EventArgs e)
        {
            if (!formShown) return;

            UpdateControls();
            UpdateLabel(_statusIndicationLabel, _core.CoreStatus());
        }

        private void folderBtn_Click(object sender, EventArgs e)
        {
            if (_core != null && _core.IsStarted())
                _core.Stop();

            if (_core._imSaveBlock != null)
            {
                FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
                folderBrowser.SelectedPath = _core._imSaveBlock.FilePath;
                folderBrowser.Description = "Select session folder";

                if (folderBrowser.ShowDialog() == DialogResult.OK)
                {
                    _core._imSaveBlock.SelectedSessionImagesDir = folderBrowser.SelectedPath;
                    //filenameLabel.Text = folderBrowser.SelectedPath;
                    filenameTb.Text = folderBrowser.SelectedPath;

                    _core._imSaveBlock.LoadSessionFolders();
                }
            }
        }

        private void prevBtn_Click(object sender, EventArgs e)
        {
            if (_core != null && _core.IsStarted())
            {
                MessageBox.Show("Stop the main proccess", "Error!");
                return;
            }
                
                //_core.Stop();

            if (_core._imSaveBlock != null)
                _core._imSaveBlock.LoadPrevSessionFolder();
        }

        private void NextBtn_Click(object sender, EventArgs e)
        {
            if (_core != null && _core.IsStarted())
            {
                MessageBox.Show("Stop the main proccess", "Error!");
                return;
            }
                //_core.Stop();

            if (_core._imSaveBlock != null)
                _core._imSaveBlock.LoadNextSessionFolder();
        }

        private void enFileSourceBtn_Click(object sender, EventArgs e)
        {

            if (_core._imSaveBlock.EnableImageLoader)
            {
                enFileSourceBtn.Image = Properties.Resources.file_source_icon;
                _core._imSaveBlock.EnableImageLoader = false;
            }
            else
            {
                enFileSourceBtn.Image = Properties.Resources.file_source_icon_active;
                _core._imSaveBlock.EnableImageLoader = true;
            }


        }

        private void panel8_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel8_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            _core.ClearStatistic();

            UpdateStatistic(_core.TotalCount, _core.OKCount, _core.NGCount);
        }


        int hits = 0;
        //double firstClickX = 0;
        //double firstClickY = 0;
        bool allowUpdate = true;
        int imageToUpdateNumber = 0;


        //HImage freezedImage;
        //List<OperationList> freezedOperationList;

        private void windowViewSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (windowViewSelector.SelectedItem == "Normal")
            {
                allowUpdate = true;
            }
            else if (windowViewSelector.SelectedItem == "Freeze")
            {
                allowUpdate = false;
                //if (_core.scene.SourceImage != null) freezedImage = _core.scene.SourceImage;
                //if (_core.scene.OperationsList != null && _core.scene.OperationsList.Count > 0)
                //    freezedOperationList = _core.scene.OperationsList;
            }
        }


        private void actuatorsInitBtn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not implemented", "Error!");
        }
        

        private void singleRun1Btn_Click(object sender, EventArgs e)
        {
            if (_core.IsStarted())
            {
                MessageBox.Show("Stop the main proccess", "Error!");
                return;
            }

            _core.runSceneOnce = true;
            _core._startBlock.NextFxBlock = _core._startBlock.Position1;
            _core._startBlock.CanStart = true;
            _core.scenePath = 1;
            _core.Start();
        }

        private void singleRun2Btn_Click(object sender, EventArgs e)
        {
            if (_core.IsStarted())
            {
                MessageBox.Show("Stop the main proccess", "Error!");
                return;
            }

            _core.runSceneOnce = true;
            _core._startBlock.NextFxBlock = _core._startBlock.Position2;
            _core.scenePath = 2;
            _core.Start();
        }

        private void loginBtn_Click(object sender, EventArgs e)
        {
            if(!adminAccess)
            {
                // prompt for password
                string input = Prompt.ShowDialog("Enter password:", "Login"); // password y4uKBypt

                if(input.Equals(adminPassword)) // if password is correct, login
                {
                    loginBtn.Image = Properties.Resources.lock_unlocked_icon_small;
                    adminAccess = true;
                }
            }
            else
            {
                // logout
                loginBtn.Image = Properties.Resources.lock_icon_small;
                adminAccess = false;
            }

            
        }

        // password input box prompt
        public static class Prompt
        {
            public static string ShowDialog(string text, string caption)
            {
                Form prompt = new Form();
                prompt.Width = 240;
                prompt.Height = 150;
                prompt.FormBorderStyle = FormBorderStyle.FixedToolWindow;
                prompt.Text = caption;
                prompt.StartPosition = FormStartPosition.CenterScreen;
                Label textLabel = new Label() { Left = 65, Top = 20, Text = text };
                TextBox textBox = new TextBox() { Left = 10, Top = 50, Width = 200 };
                textBox.UseSystemPasswordChar = true;
                textBox.PasswordChar = '*';
                Button confirmation = new Button() { Text = "OK", Left = 10, Width = 200, Top = 75 };
                confirmation.Click += (sender, e) => { prompt.Close(); };
                prompt.Controls.Add(textBox);
                prompt.Controls.Add(confirmation);
                prompt.Controls.Add(textLabel);
                prompt.AcceptButton = confirmation;
                prompt.ShowDialog();
                return textBox.Text;
            }
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (!formShown) return;

            //if (hits == 0) UpdateScreenImage(_core.scene.SourceImage);
            //else UpdateScreenImage(_core.scene.OperationsList[imageToUpdateNumber]);
        }

        private void cameraErrorIcon_DoubleClick(object sender, EventArgs e)
        {
            
        }

        

        
    }

    
}
