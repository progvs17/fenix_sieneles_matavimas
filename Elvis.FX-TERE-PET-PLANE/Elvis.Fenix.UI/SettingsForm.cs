﻿using Elvis.Fenix.General;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elvis.Fenix
{
    public partial class SettingsForm : Form
    {
        private FenixCore core;
        private Button[] DOButtons = new Button[16];
        private Label[] DILabels = new Label[16];

        public SettingsForm(FenixCore core)
        {
            InitializeComponent();
            this.core = core;            

            enDICheck.DataBindings.Add(new Binding("Checked", core, "EnableDI", false, DataSourceUpdateMode.OnPropertyChanged));
            enDOCheck.DataBindings.Add(new Binding("Checked", core, "EnableDO", false, DataSourceUpdateMode.OnPropertyChanged));
            enAllImgLogCheck.DataBindings.Add(new Binding("Checked", core, "EnableImgLog", false, DataSourceUpdateMode.OnPropertyChanged));
            enNegImgLogCheck.DataBindings.Add(new Binding("Checked", core.settings, "EnNgImgLog", false, DataSourceUpdateMode.OnPropertyChanged));

            enableDebugCheck.DataBindings.Add(new Binding("Checked", core.settings, "EnableDebugLog", false, DataSourceUpdateMode.OnPropertyChanged));
            enableAutostartCheck.DataBindings.Add(new Binding("Checked", core.settings, "EnAutoRun", false, DataSourceUpdateMode.OnPropertyChanged));
            allMasksAreGoodCheck.DataBindings.Add(new Binding("Checked", core.settings, "TreatAllMasksAsGood", false, DataSourceUpdateMode.OnPropertyChanged));

            stopAfterGDcheck.DataBindings.Add(new Binding("Checked", core.settings, "StopAfterGD", false, DataSourceUpdateMode.OnPropertyChanged));
            stopAfterNGcheck.DataBindings.Add(new Binding("Checked", core.settings, "StopAfterNG", false, DataSourceUpdateMode.OnPropertyChanged));


            //pathLabel.DataBindings.Add(new Binding("Text", core._imSaveBlock, "FilePath", false, DataSourceUpdateMode.OnPropertyChanged));
            pathTb.DataBindings.Add(new Binding("Text", core._imSaveBlock, "FilePath", false, DataSourceUpdateMode.OnPropertyChanged));
            prefixText.DataBindings.Add(new Binding("Text", core._imSaveBlock, "Prefix", false, DataSourceUpdateMode.OnPropertyChanged));

            // gpio  
            bottleInSpin.DataBindings.Add(new Binding("Value", core.settings, "BottleInDIPin", false, DataSourceUpdateMode.OnPropertyChanged));
            manualInSpin.DataBindings.Add(new Binding("Value", core.settings, "ManualInDIPin", false, DataSourceUpdateMode.OnPropertyChanged));
            stopInSpin.DataBindings.Add(new Binding("Value", core.settings, "SafetyStopInDIPin", false, DataSourceUpdateMode.OnPropertyChanged));

            hardwareEnOutSpin.DataBindings.Add(new Binding("Value", core.settings, "HardwareEnableDOPin", false, DataSourceUpdateMode.OnPropertyChanged));
            faultOutSpin.DataBindings.Add(new Binding("Value", core.settings, "SystemFaultLightDO", false, DataSourceUpdateMode.OnPropertyChanged));
            bottleFlowOutSpin.DataBindings.Add(new Binding("Value", core.settings, "BottleFlowDOPin", false, DataSourceUpdateMode.OnPropertyChanged));
            bottleCatchOutSpin.DataBindings.Add(new Binding("Value", core.settings, "BottleCatchDOPin", false, DataSourceUpdateMode.OnPropertyChanged));
            nozzlesOutSpin.DataBindings.Add(new Binding("Value", core.settings, "NozzleDOPin", false, DataSourceUpdateMode.OnPropertyChanged));
            runOutSpin.DataBindings.Add(new Binding("Value", core.settings, "RunDOPin", false, DataSourceUpdateMode.OnPropertyChanged));
            actuatorOnOutSpin.DataBindings.Add(new Binding("Value", core.settings, "ActuatorOnDOPin", false, DataSourceUpdateMode.OnPropertyChanged));

            NGOutSpin.DataBindings.Add(new Binding("Value", core.settings, "NGDOPin", false, DataSourceUpdateMode.OnPropertyChanged));

            // hardware
            //rotarySerialTb.DataBindings.Add(new Binding("Text", core.settings, "RotaryActuatorsSerialPort", false, DataSourceUpdateMode.OnPropertyChanged));
            //rotationSpeedSpin.DataBindings.Add(new Binding("Value", core.settings, "RotarySpeed", false, DataSourceUpdateMode.OnPropertyChanged));
            //rotationAccSpin.DataBindings.Add(new Binding("Value", core.settings, "RotaryAcc", false, DataSourceUpdateMode.OnPropertyChanged));
            //rotationDeccSpin.DataBindings.Add(new Binding("Value", core.settings, "RotaryDecc", false, DataSourceUpdateMode.OnPropertyChanged));
            //reductionSpeedSpin.DataBindings.Add(new Binding("Value", core.settings, "ReductionSpeed", false, DataSourceUpdateMode.OnPropertyChanged));
            //reductionAccSpin.DataBindings.Add(new Binding("Value", core.settings, "ReductionAcc", false, DataSourceUpdateMode.OnPropertyChanged));
            //reductionDeccSpin.DataBindings.Add(new Binding("Value", core.settings, "ReductionDecc", false, DataSourceUpdateMode.OnPropertyChanged));

            //red1OffsetSpin.DataBindings.Add(new Binding("Value", core.settings, "Reduction1Offset", false, DataSourceUpdateMode.OnPropertyChanged));
            //red2OffsetSpin.DataBindings.Add(new Binding("Value", core.settings, "Reduction2Offset", false, DataSourceUpdateMode.OnPropertyChanged));

            //linearSerialTb.DataBindings.Add(new Binding("Text", core.settings, "LinearActuatorsSerialPort", false, DataSourceUpdateMode.OnPropertyChanged));
            //maskLinSpeedSpin.DataBindings.Add(new Binding("Value", core.settings, "MaskSpeed", false, DataSourceUpdateMode.OnPropertyChanged));
            //maskLinAccSpin.DataBindings.Add(new Binding("Value", core.settings, "MaskAcc", false, DataSourceUpdateMode.OnPropertyChanged));
            //maskLinDeccSpin.DataBindings.Add(new Binding("Value", core.settings, "MaskDecc", false, DataSourceUpdateMode.OnPropertyChanged));
            //cameraLinSpeedSpin.DataBindings.Add(new Binding("Value", core.settings, "CameraSpeed", false, DataSourceUpdateMode.OnPropertyChanged));
            //cameraLinAccSpin.DataBindings.Add(new Binding("Value", core.settings, "CameraAcc", false, DataSourceUpdateMode.OnPropertyChanged));
            //cameraLinDeccSpin.DataBindings.Add(new Binding("Value", core.settings, "CameraDecc", false, DataSourceUpdateMode.OnPropertyChanged));

            //bypassInitCheck.DataBindings.Add(new Binding("Checked", core.settings, "BypassActuatorsInit", false, DataSourceUpdateMode.OnPropertyChanged));
            
            // image cropping
            horizontalImagesSpin.DataBindings.Add(new Binding("Value", core.settings, "HorizontalImagesToTile", false, DataSourceUpdateMode.OnPropertyChanged));
            cropImagesCheck.DataBindings.Add(new Binding("Checked", core.settings, "CropTiledImages", false, DataSourceUpdateMode.OnPropertyChanged));
            croppedImageSizeSpin.DataBindings.Add(new Binding("Value", core.settings, "CroppedImageSize", false, DataSourceUpdateMode.OnPropertyChanged));
            borderWidthSpin.DataBindings.Add(new Binding("Value", core.settings, "TiledImagesBorderWidth", false, DataSourceUpdateMode.OnPropertyChanged));

            wOffSpin.DataBindings.Add(new Binding("Value", core.settings, "WidthOffset", false, DataSourceUpdateMode.OnPropertyChanged));
            hOffSpin.DataBindings.Add(new Binding("Value", core.settings, "HeigthOffset", false, DataSourceUpdateMode.OnPropertyChanged));
            xOffSpin.DataBindings.Add(new Binding("Value", core.settings, "XOffset", false, DataSourceUpdateMode.OnPropertyChanged));
            yOffSpin.DataBindings.Add(new Binding("Value", core.settings, "YOffset", false, DataSourceUpdateMode.OnPropertyChanged));


            DOButtons[0] = doButton1;
            DOButtons[1] = doButton2;
            DOButtons[2] = doButton3;
            DOButtons[3] = doButton4;
            DOButtons[4] = doButton5;
            DOButtons[5] = doButton6;
            DOButtons[6] = doButton7;
            DOButtons[7] = doButton8;
            DOButtons[8] = doButton9;
            DOButtons[9] = doButton10;
            DOButtons[10] = doButton11;
            DOButtons[11] = doButton12;
            DOButtons[12] = doButton13;
            DOButtons[13] = doButton14;
            DOButtons[14] = doButton15;
            DOButtons[15] = doButton16;

            DILabels[0] = diLabel1;
            DILabels[1] = diLabel2;
            DILabels[2] = diLabel3;
            DILabels[3] = diLabel4;
            DILabels[4] = diLabel5;
            DILabels[5] = diLabel6;
            DILabels[6] = diLabel7;
            DILabels[7] = diLabel8;
            DILabels[8] = diLabel9;
            DILabels[9] = diLabel10;
            DILabels[10] = diLabel11;
            DILabels[11] = diLabel12;
            DILabels[12] = diLabel13;
            DILabels[13] = diLabel14;
            DILabels[14] = diLabel15;
            DILabels[15] = diLabel16;
           
            UpdateMultiPanel(ViewPanel.IMAGE_LOG_PANEL);
        }

        void UpdateMultiPanel(ViewPanel panel )
        {
            gpioBtn.BackgroundImage = Properties.Resources.gpio_input;
            gpioOutputBtn.BackgroundImage = Properties.Resources.gpio_output;
            imgLogBtn.BackgroundImage = Properties.Resources.save;
            actuatorsBtn.BackgroundImage = Properties.Resources.actuator_btn;

            switch(panel)
            {
                case ViewPanel.GPIO_PANEL:
                    {
                        multiPanel.Controls.Clear();
                        multiPanel.Controls.Add(gpioPanel);
                        multiPanel.Invalidate();
                        gpioBtn.BackgroundImage = Properties.Resources.gpio_input_active;
                        UpdateDIOText();
                    }
                    break;

                case ViewPanel.GPIO_OUT_PANEL:
                    {
                        multiPanel.Controls.Clear();
                        multiPanel.Controls.Add(tableLayoutPanel3);
                        multiPanel.Invalidate();
                        gpioOutputBtn.BackgroundImage = Properties.Resources.gpio_output_active;
                        UpdateDIOText();
                    }
                    break;

                case ViewPanel.IMAGE_LOG_PANEL:
                    {
                        multiPanel.Controls.Clear();
                        multiPanel.Controls.Add(imgLogPanel);
                        multiPanel.Invalidate();
                        imgLogBtn.BackgroundImage = Properties.Resources.save_active;
                    }
                    break;

                case ViewPanel.ACTUATORS_PANEL:
                    {
                        multiPanel.Controls.Clear();
                        multiPanel.Controls.Add(tableLayoutPanel10);
                        multiPanel.Invalidate();
                        actuatorsBtn.BackgroundImage = Properties.Resources.actuator_btn_active;
                    }
                    break;
            }
        }


        public void UpdateDIPanel(Object sender, int mask)
        {
            int pin = 0;
            foreach (var label in DILabels)
            {
                int value = (mask & (1 << pin)) >> pin;
                UpdateDILabel(label, value);
                pin++;
            }
        }

        public void UpdateDOPanel(Object sender, int mask)
        {

            int pin = 0;
            foreach(var button in DOButtons)
            {
               int value = (mask & (1 << pin)) >> pin;
               UpdateDOButton(button, value);
               pin++;               
            }
        }

        public void UpdateDOButton(Button button,  int value)
        {
            if(button.InvokeRequired)
            {
                button.Invoke(new Action(() => UpdateDOButton(button, value) ));
            }
            else
            {
                button.BackColor = value == 0 ? Color.IndianRed : Color.YellowGreen;
            }

        }

        public void UpdateDILabel(Label label, int value)
        {
            if (label.InvokeRequired)
            {
                label.Invoke(new Action(() => UpdateDILabel(label, value)));
            }
            else
            {
                label.BackColor = value == 0 ? Color.IndianRed : Color.YellowGreen;
            }
        }

        private void SetDO(object sender, EventArgs e)
        {
            Button button = sender as Button;

            int index = 0;
            foreach(var btn in DOButtons)
            {
                if (btn == sender)
                {
                    core.ToggleGpioPin(index); // + 8);
                    break;
                }                   
                index++;
            }

        }

        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            updateTimer.Enabled = false;
            if(core._gpio != null)
            {
                core._gpio.onInputsChanged -= new Hardware.GpioInputsEventHandler(UpdateDIPanel);
                core._gpio.onOutputsChanged -= new Hardware.GpioOutputsEventHandler(UpdateDOPanel);
            }
            //core.SaveConfigData();
            //core.SaveCommonSettings();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            updateTimer.Enabled = true;
            if (core._gpio != null)
            {
                core._gpio.onInputsChanged += new Hardware.GpioInputsEventHandler(UpdateDIPanel);
                core._gpio.onOutputsChanged += new Hardware.GpioOutputsEventHandler(UpdateDOPanel);
            }

            UpdateDIOText();          

        }

        void UpdateDIOText()
        {
            int i = 0;
            foreach (var doBtn in DOButtons)
            {
                doBtn.Text = "DO" + i.ToString();

                doBtn.Text = core.settings.HardwareEnableDOPin == i ? "HW EN" : doBtn.Text;
                doBtn.Text = core.settings.BottleFlowDOPin == i ? "Flow" : doBtn.Text;
                doBtn.Text = core.settings.BottleCatchDOPin == i ? "Catch" : doBtn.Text;
                doBtn.Text = core.settings.NozzleDOPin == i ? "Nozzles" : doBtn.Text;
                doBtn.Text = core.settings.RunDOPin == i ? "RUN" : doBtn.Text;
                doBtn.Text = core.settings.ActuatorOnDOPin == i ? "Act ON" : doBtn.Text;
                doBtn.Text = core.settings.NGDOPin == i ? "NG" : doBtn.Text;
                doBtn.Text = core.settings.SystemFaultLightDO == i ? "Fault" : doBtn.Text;

                i++;
            }

            i = 0;
            foreach (var diLbl in DILabels)
            {
                diLbl.Text = "DI" + i.ToString();

                diLbl.Text = core.settings.BottleInDIPin == i ? "Bottle IN" : diLbl.Text;
                diLbl.Text = core.settings.ManualInDIPin == i ? "MANUAL" : diLbl.Text;
                diLbl.Text = core.settings.SafetyStopInDIPin == i ? "STOP" : diLbl.Text;
                //diLbl.Text = core.settings.Mount2DIPin == i ? "Mount 2" : diLbl.Text;
                //diLbl.Text = core.settings.Unmount1DIPin == i ? "Unmount 1" : diLbl.Text;
                //diLbl.Text = core.settings.Unmount2DIPin == i ? "Unmount 2" : diLbl.Text;

                //diLbl.Text = core.settings.SceneDIPin0 == i ? "Scene 0" : diLbl.Text;
                //diLbl.Text = core.settings.SceneDIPin1 == i ? "Scene 1" : diLbl.Text;
                //diLbl.Text = core.settings.SceneDIPin2 == i ? "Scene 2" : diLbl.Text;
                //diLbl.Text = core.settings.SceneDIPin3 == i ? "Scene 3" : diLbl.Text;

                //diLbl.Text = core.settings.Opto1DIPin == i ? "Opto 1" : diLbl.Text;
                //diLbl.Text = core.settings.Opto2DIPin == i ? "Opto 2" : diLbl.Text;

                i++;
            }
        }

        private void imgLogBtn_Click(object sender, EventArgs e)
        {
            UpdateMultiPanel(ViewPanel.IMAGE_LOG_PANEL);
        }

        private void gpioBtn_Click(object sender, EventArgs e)
        {
            UpdateMultiPanel(ViewPanel.GPIO_PANEL);
        }

        private void gpioOutputBtn_Click(object sender, EventArgs e)
        {
            UpdateMultiPanel(ViewPanel.GPIO_OUT_PANEL);
        }

        private void actuatorsBtn_Click(object sender, EventArgs e)
        {
            UpdateMultiPanel(ViewPanel.ACTUATORS_PANEL);
        }

        private void updateTimer_Tick(object sender, EventArgs e)
        {
            if (core._gpio != null)
            {
                if (core.EnableDI)
                {
                    int pin = 0;
                    foreach (var lbl in DILabels)
                    {
                        lbl.BackColor = core._gpio.GetPin(0, pin) == 1 ? Color.YellowGreen : Color.IndianRed;
                        pin++;
                    }
                }
                else
                {
                    foreach (var lbl in DILabels)
                    {
                        lbl.BackColor = Color.Gray;
                    }
                }


                if (core.EnableDO)
                {
                    int pin = 0;
                    foreach (var btn in DOButtons)
                    {
                        btn.BackColor = core._gpio.GetDOPin(pin) == 1 ? Color.YellowGreen : Color.IndianRed;
                        pin++;
                    }
                }
                else
                {
                    foreach (var btn in DOButtons)
                    {
                        btn.BackColor = Color.Gray;
                    }
                }

            }

            UpdateDIOText();

        }

        private void logFolderBrowseButton_Click(object sender, EventArgs e)
        {
            //folderBrowserDialog.RootFolder = Environment.SpecialFolder.MyComputer;

            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                core._imSaveBlock.FilePath = folderBrowserDialog.SelectedPath;
                pathTb.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void readyOutSpin_ValueChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void OnDIOChanged(object sender, EventArgs e)
        {
        }


        // save settings button
        private void saveSettingsButton_Click(object sender, EventArgs e)
        {
            core.SaveConfigData();
            core.SaveCommonSettings();
            core.SaveConfigData();
            core.AnnounceSettingsSavedMessage();            
        }

        private void enableDebugCheck_CheckStateChanged(object sender, EventArgs e)
        {
            CheckBox cb = sender as CheckBox;
            if (cb != null)
            {
                core.AnnounceDebugLogStatus(cb.Checked);
            }
        }
    }
}
