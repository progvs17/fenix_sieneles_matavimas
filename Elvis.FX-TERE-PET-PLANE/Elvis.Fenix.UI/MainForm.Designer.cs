﻿namespace Elvis.Fenix
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.logMsg = new System.Windows.Forms.RichTextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.enGpioBtn = new System.Windows.Forms.Button();
            this.enImgLogBtn = new System.Windows.Forms.Button();
            this.singleRun1Btn = new System.Windows.Forms.Button();
            this.settingsBtn = new System.Windows.Forms.Button();
            this.runBtn = new System.Windows.Forms.Button();
            this.flowBtn = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.loginBtn = new System.Windows.Forms.PictureBox();
            this.actuatorErrorIcon = new System.Windows.Forms.PictureBox();
            this.gpioErrorIcon = new System.Windows.Forms.PictureBox();
            this.cameraErrorIcon = new System.Windows.Forms.PictureBox();
            this.multiPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.actuatorsInitBtn = new System.Windows.Forms.Button();
            this.filenameTb = new System.Windows.Forms.TextBox();
            this.singleRun2Btn = new System.Windows.Forms.Button();
            this.enFileSourceBtn = new System.Windows.Forms.Button();
            this.prevBtn = new System.Windows.Forms.Button();
            this.NextBtn = new System.Windows.Forms.Button();
            this.folderBtn = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this._statusIndicationLabel = new System.Windows.Forms.Label();
            this.sceneNameLabel = new System.Windows.Forms.Label();
            this._calcTimeLabel = new System.Windows.Forms.Label();
            this._statisticEfficientLabel = new System.Windows.Forms.Label();
            this._statisticNegativeLabel = new System.Windows.Forms.Label();
            this._statisticGoodLabel = new System.Windows.Forms.Label();
            this._statisticTotalLabel = new System.Windows.Forms.Label();
            this._judgementLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.windowViewSelector = new System.Windows.Forms.ComboBox();
            this.mainFormChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.logFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.controlUpdate = new System.Windows.Forms.Timer(this.components);
            this.openImageDialog = new System.Windows.Forms.OpenFileDialog();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loginBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.actuatorErrorIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpioErrorIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraErrorIcon)).BeginInit();
            this.multiPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainFormChart)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 450F));
            this.tableLayoutPanel2.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1180, 856);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(733, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(444, 850);
            this.panel2.TabIndex = 10;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.logMsg, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.multiPanel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel8, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 171F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(444, 850);
            this.tableLayoutPanel1.TabIndex = 13;
            // 
            // logMsg
            // 
            this.logMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(102)))), ((int)(((byte)(120)))));
            this.logMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.logMsg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logMsg.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.logMsg.ForeColor = System.Drawing.Color.White;
            this.logMsg.Location = new System.Drawing.Point(10, 405);
            this.logMsg.Margin = new System.Windows.Forms.Padding(5);
            this.logMsg.Name = "logMsg";
            this.logMsg.ReadOnly = true;
            this.logMsg.Size = new System.Drawing.Size(424, 391);
            this.logMsg.TabIndex = 0;
            this.logMsg.Text = "";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.panel6.BackgroundImage = global::Elvis.Fenix.Properties.Resources.ui_block_half;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel6.Controls.Add(this.enGpioBtn);
            this.panel6.Controls.Add(this.enImgLogBtn);
            this.panel6.Controls.Add(this.singleRun1Btn);
            this.panel6.Controls.Add(this.settingsBtn);
            this.panel6.Controls.Add(this.runBtn);
            this.panel6.Controls.Add(this.flowBtn);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(8, 179);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(428, 88);
            this.panel6.TabIndex = 18;
            // 
            // enGpioBtn
            // 
            this.enGpioBtn.BackColor = System.Drawing.Color.Transparent;
            this.enGpioBtn.BackgroundImage = global::Elvis.Fenix.Properties.Resources.gpio;
            this.enGpioBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.enGpioBtn.FlatAppearance.BorderSize = 0;
            this.enGpioBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.enGpioBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.enGpioBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.enGpioBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.enGpioBtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.enGpioBtn.Location = new System.Drawing.Point(358, 13);
            this.enGpioBtn.Margin = new System.Windows.Forms.Padding(5);
            this.enGpioBtn.Name = "enGpioBtn";
            this.enGpioBtn.Padding = new System.Windows.Forms.Padding(5);
            this.enGpioBtn.Size = new System.Drawing.Size(60, 60);
            this.enGpioBtn.TabIndex = 25;
            this.enGpioBtn.UseVisualStyleBackColor = false;
            this.enGpioBtn.Visible = false;
            this.enGpioBtn.Click += new System.EventHandler(this.enGpioBtn_Click);
            // 
            // enImgLogBtn
            // 
            this.enImgLogBtn.BackColor = System.Drawing.Color.Transparent;
            this.enImgLogBtn.BackgroundImage = global::Elvis.Fenix.Properties.Resources.save;
            this.enImgLogBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.enImgLogBtn.FlatAppearance.BorderSize = 0;
            this.enImgLogBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.enImgLogBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.enImgLogBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.enImgLogBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.enImgLogBtn.ForeColor = System.Drawing.Color.IndianRed;
            this.enImgLogBtn.Location = new System.Drawing.Point(290, 13);
            this.enImgLogBtn.Margin = new System.Windows.Forms.Padding(10);
            this.enImgLogBtn.Name = "enImgLogBtn";
            this.enImgLogBtn.Padding = new System.Windows.Forms.Padding(10);
            this.enImgLogBtn.Size = new System.Drawing.Size(60, 60);
            this.enImgLogBtn.TabIndex = 21;
            this.enImgLogBtn.UseVisualStyleBackColor = false;
            this.enImgLogBtn.Click += new System.EventHandler(this.enImgLogBtn_Click);
            // 
            // singleRun1Btn
            // 
            this.singleRun1Btn.BackColor = System.Drawing.Color.Transparent;
            this.singleRun1Btn.BackgroundImage = global::Elvis.Fenix.Properties.Resources.single_run_only;
            this.singleRun1Btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.singleRun1Btn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.singleRun1Btn.FlatAppearance.BorderSize = 0;
            this.singleRun1Btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.singleRun1Btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.singleRun1Btn.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.singleRun1Btn.ForeColor = System.Drawing.Color.IndianRed;
            this.singleRun1Btn.Location = new System.Drawing.Point(151, 13);
            this.singleRun1Btn.Margin = new System.Windows.Forms.Padding(0);
            this.singleRun1Btn.Name = "singleRun1Btn";
            this.singleRun1Btn.Size = new System.Drawing.Size(60, 60);
            this.singleRun1Btn.TabIndex = 33;
            this.singleRun1Btn.UseVisualStyleBackColor = false;
            this.singleRun1Btn.Click += new System.EventHandler(this.singleRun1Btn_Click);
            // 
            // settingsBtn
            // 
            this.settingsBtn.BackColor = System.Drawing.Color.Transparent;
            this.settingsBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.settingsBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.settingsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.settingsBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.settingsBtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.settingsBtn.Image = global::Elvis.Fenix.Properties.Resources.settings;
            this.settingsBtn.Location = new System.Drawing.Point(15, 13);
            this.settingsBtn.Margin = new System.Windows.Forms.Padding(5);
            this.settingsBtn.Name = "settingsBtn";
            this.settingsBtn.Padding = new System.Windows.Forms.Padding(5);
            this.settingsBtn.Size = new System.Drawing.Size(60, 60);
            this.settingsBtn.TabIndex = 18;
            this.settingsBtn.UseVisualStyleBackColor = false;
            this.settingsBtn.Visible = false;
            this.settingsBtn.Click += new System.EventHandler(this.button2_Click);
            // 
            // runBtn
            // 
            this.runBtn.BackColor = System.Drawing.Color.Transparent;
            this.runBtn.BackgroundImage = global::Elvis.Fenix.Properties.Resources.run;
            this.runBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.runBtn.FlatAppearance.BorderSize = 0;
            this.runBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.runBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.runBtn.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.runBtn.ForeColor = System.Drawing.Color.IndianRed;
            this.runBtn.Location = new System.Drawing.Point(220, 13);
            this.runBtn.Margin = new System.Windows.Forms.Padding(0);
            this.runBtn.Name = "runBtn";
            this.runBtn.Size = new System.Drawing.Size(60, 60);
            this.runBtn.TabIndex = 2;
            this.runBtn.UseVisualStyleBackColor = false;
            this.runBtn.Click += new System.EventHandler(this.runBtn_Click);
            // 
            // flowBtn
            // 
            this.flowBtn.BackColor = System.Drawing.Color.Transparent;
            this.flowBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.flowBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.flowBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.flowBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.flowBtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.flowBtn.Image = global::Elvis.Fenix.Properties.Resources.flow;
            this.flowBtn.Location = new System.Drawing.Point(83, 13);
            this.flowBtn.Margin = new System.Windows.Forms.Padding(5);
            this.flowBtn.Name = "flowBtn";
            this.flowBtn.Padding = new System.Windows.Forms.Padding(5);
            this.flowBtn.Size = new System.Drawing.Size(60, 60);
            this.flowBtn.TabIndex = 4;
            this.flowBtn.UseVisualStyleBackColor = false;
            this.flowBtn.Visible = false;
            this.flowBtn.Click += new System.EventHandler(this.SceneFormButton_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.tableLayoutPanel4.ColumnCount = 5;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.01961F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.7451F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.7451F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.7451F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.7451F));
            this.tableLayoutPanel4.Controls.Add(this.loginBtn, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.actuatorErrorIcon, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.gpioErrorIcon, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.cameraErrorIcon, 3, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(8, 804);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(428, 38);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // loginBtn
            // 
            this.loginBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loginBtn.Image = global::Elvis.Fenix.Properties.Resources.lock_icon_small;
            this.loginBtn.Location = new System.Drawing.Point(212, 3);
            this.loginBtn.Name = "loginBtn";
            this.loginBtn.Size = new System.Drawing.Size(48, 32);
            this.loginBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.loginBtn.TabIndex = 5;
            this.loginBtn.TabStop = false;
            this.loginBtn.Click += new System.EventHandler(this.loginBtn_Click);
            // 
            // actuatorErrorIcon
            // 
            this.actuatorErrorIcon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actuatorErrorIcon.Image = global::Elvis.Fenix.Properties.Resources.actuator_error;
            this.actuatorErrorIcon.Location = new System.Drawing.Point(266, 3);
            this.actuatorErrorIcon.Name = "actuatorErrorIcon";
            this.actuatorErrorIcon.Size = new System.Drawing.Size(48, 32);
            this.actuatorErrorIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.actuatorErrorIcon.TabIndex = 4;
            this.actuatorErrorIcon.TabStop = false;
            this.actuatorErrorIcon.Visible = false;
            // 
            // gpioErrorIcon
            // 
            this.gpioErrorIcon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gpioErrorIcon.Image = global::Elvis.Fenix.Properties.Resources.pci_error;
            this.gpioErrorIcon.Location = new System.Drawing.Point(374, 3);
            this.gpioErrorIcon.Name = "gpioErrorIcon";
            this.gpioErrorIcon.Size = new System.Drawing.Size(51, 32);
            this.gpioErrorIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.gpioErrorIcon.TabIndex = 3;
            this.gpioErrorIcon.TabStop = false;
            // 
            // cameraErrorIcon
            // 
            this.cameraErrorIcon.Image = global::Elvis.Fenix.Properties.Resources.camera_error;
            this.cameraErrorIcon.Location = new System.Drawing.Point(320, 3);
            this.cameraErrorIcon.Name = "cameraErrorIcon";
            this.cameraErrorIcon.Size = new System.Drawing.Size(48, 31);
            this.cameraErrorIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.cameraErrorIcon.TabIndex = 0;
            this.cameraErrorIcon.TabStop = false;
            this.cameraErrorIcon.Visible = false;
            this.cameraErrorIcon.DoubleClick += new System.EventHandler(this.cameraErrorIcon_DoubleClick);
            // 
            // multiPanel
            // 
            this.multiPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.multiPanel.Controls.Add(this.panel1);
            this.multiPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.multiPanel.Location = new System.Drawing.Point(5, 272);
            this.multiPanel.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.multiPanel.Name = "multiPanel";
            this.multiPanel.Padding = new System.Windows.Forms.Padding(2);
            this.multiPanel.Size = new System.Drawing.Size(434, 126);
            this.multiPanel.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::Elvis.Fenix.Properties.Resources.ui_block_load;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.actuatorsInitBtn);
            this.panel1.Controls.Add(this.filenameTb);
            this.panel1.Controls.Add(this.singleRun2Btn);
            this.panel1.Controls.Add(this.enFileSourceBtn);
            this.panel1.Controls.Add(this.prevBtn);
            this.panel1.Controls.Add(this.NextBtn);
            this.panel1.Controls.Add(this.folderBtn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(2, 2);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(430, 122);
            this.panel1.TabIndex = 0;
            // 
            // actuatorsInitBtn
            // 
            this.actuatorsInitBtn.BackColor = System.Drawing.Color.Transparent;
            this.actuatorsInitBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.actuatorsInitBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.actuatorsInitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.actuatorsInitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.actuatorsInitBtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.actuatorsInitBtn.Image = global::Elvis.Fenix.Properties.Resources.actuator;
            this.actuatorsInitBtn.Location = new System.Drawing.Point(221, 21);
            this.actuatorsInitBtn.Margin = new System.Windows.Forms.Padding(5);
            this.actuatorsInitBtn.Name = "actuatorsInitBtn";
            this.actuatorsInitBtn.Padding = new System.Windows.Forms.Padding(5);
            this.actuatorsInitBtn.Size = new System.Drawing.Size(60, 60);
            this.actuatorsInitBtn.TabIndex = 26;
            this.actuatorsInitBtn.UseVisualStyleBackColor = false;
            this.actuatorsInitBtn.Visible = false;
            this.actuatorsInitBtn.Click += new System.EventHandler(this.actuatorsInitBtn_Click);
            // 
            // filenameTb
            // 
            this.filenameTb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(102)))), ((int)(((byte)(120)))));
            this.filenameTb.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.filenameTb.Font = new System.Drawing.Font("Calibri", 9F);
            this.filenameTb.ForeColor = System.Drawing.SystemColors.Window;
            this.filenameTb.Location = new System.Drawing.Point(14, 94);
            this.filenameTb.Name = "filenameTb";
            this.filenameTb.Size = new System.Drawing.Size(403, 15);
            this.filenameTb.TabIndex = 34;
            this.filenameTb.Text = "C:\\Temp\\Log Images";
            this.filenameTb.Visible = false;
            // 
            // singleRun2Btn
            // 
            this.singleRun2Btn.BackColor = System.Drawing.Color.Transparent;
            this.singleRun2Btn.BackgroundImage = global::Elvis.Fenix.Properties.Resources.single_run_2;
            this.singleRun2Btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.singleRun2Btn.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.singleRun2Btn.FlatAppearance.BorderSize = 0;
            this.singleRun2Btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.singleRun2Btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.singleRun2Btn.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.singleRun2Btn.ForeColor = System.Drawing.Color.IndianRed;
            this.singleRun2Btn.Location = new System.Drawing.Point(288, 21);
            this.singleRun2Btn.Margin = new System.Windows.Forms.Padding(0);
            this.singleRun2Btn.Name = "singleRun2Btn";
            this.singleRun2Btn.Size = new System.Drawing.Size(60, 60);
            this.singleRun2Btn.TabIndex = 26;
            this.singleRun2Btn.UseVisualStyleBackColor = false;
            this.singleRun2Btn.Visible = false;
            this.singleRun2Btn.Click += new System.EventHandler(this.singleRun2Btn_Click);
            // 
            // enFileSourceBtn
            // 
            this.enFileSourceBtn.BackColor = System.Drawing.Color.Transparent;
            this.enFileSourceBtn.FlatAppearance.BorderSize = 0;
            this.enFileSourceBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.enFileSourceBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.enFileSourceBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.enFileSourceBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.enFileSourceBtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.enFileSourceBtn.Image = global::Elvis.Fenix.Properties.Resources.file_source_icon;
            this.enFileSourceBtn.Location = new System.Drawing.Point(356, 21);
            this.enFileSourceBtn.Margin = new System.Windows.Forms.Padding(5);
            this.enFileSourceBtn.Name = "enFileSourceBtn";
            this.enFileSourceBtn.Padding = new System.Windows.Forms.Padding(5);
            this.enFileSourceBtn.Size = new System.Drawing.Size(61, 61);
            this.enFileSourceBtn.TabIndex = 32;
            this.enFileSourceBtn.UseVisualStyleBackColor = false;
            this.enFileSourceBtn.Visible = false;
            this.enFileSourceBtn.Click += new System.EventHandler(this.enFileSourceBtn_Click);
            // 
            // prevBtn
            // 
            this.prevBtn.BackColor = System.Drawing.Color.Transparent;
            this.prevBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.prevBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.prevBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.prevBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.prevBtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.prevBtn.Image = global::Elvis.Fenix.Properties.Resources.previous;
            this.prevBtn.Location = new System.Drawing.Point(84, 21);
            this.prevBtn.Margin = new System.Windows.Forms.Padding(5);
            this.prevBtn.Name = "prevBtn";
            this.prevBtn.Padding = new System.Windows.Forms.Padding(5);
            this.prevBtn.Size = new System.Drawing.Size(60, 60);
            this.prevBtn.TabIndex = 29;
            this.prevBtn.UseVisualStyleBackColor = false;
            this.prevBtn.Visible = false;
            this.prevBtn.Click += new System.EventHandler(this.prevBtn_Click);
            // 
            // NextBtn
            // 
            this.NextBtn.BackColor = System.Drawing.Color.Transparent;
            this.NextBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.NextBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.NextBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NextBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.NextBtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.NextBtn.Image = global::Elvis.Fenix.Properties.Resources.next;
            this.NextBtn.Location = new System.Drawing.Point(152, 21);
            this.NextBtn.Margin = new System.Windows.Forms.Padding(5);
            this.NextBtn.Name = "NextBtn";
            this.NextBtn.Padding = new System.Windows.Forms.Padding(5);
            this.NextBtn.Size = new System.Drawing.Size(60, 60);
            this.NextBtn.TabIndex = 28;
            this.NextBtn.Text = ">";
            this.NextBtn.UseVisualStyleBackColor = false;
            this.NextBtn.Visible = false;
            this.NextBtn.Click += new System.EventHandler(this.NextBtn_Click);
            // 
            // folderBtn
            // 
            this.folderBtn.BackColor = System.Drawing.Color.Transparent;
            this.folderBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.folderBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.folderBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.folderBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.folderBtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.folderBtn.Image = global::Elvis.Fenix.Properties.Resources.load;
            this.folderBtn.Location = new System.Drawing.Point(16, 21);
            this.folderBtn.Margin = new System.Windows.Forms.Padding(5);
            this.folderBtn.Name = "folderBtn";
            this.folderBtn.Padding = new System.Windows.Forms.Padding(5);
            this.folderBtn.Size = new System.Drawing.Size(60, 60);
            this.folderBtn.TabIndex = 26;
            this.folderBtn.UseVisualStyleBackColor = false;
            this.folderBtn.Visible = false;
            this.folderBtn.Click += new System.EventHandler(this.folderBtn_Click);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(92)))), ((int)(((byte)(110)))));
            this.panel8.BackgroundImage = global::Elvis.Fenix.Properties.Resources.ui_block_main;
            this.panel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel8.Controls.Add(this._statusIndicationLabel);
            this.panel8.Controls.Add(this.sceneNameLabel);
            this.panel8.Controls.Add(this._calcTimeLabel);
            this.panel8.Controls.Add(this._statisticEfficientLabel);
            this.panel8.Controls.Add(this._statisticNegativeLabel);
            this.panel8.Controls.Add(this._statisticGoodLabel);
            this.panel8.Controls.Add(this._statisticTotalLabel);
            this.panel8.Controls.Add(this._judgementLabel);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(8, 8);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(428, 165);
            this.panel8.TabIndex = 3;
            this.panel8.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel8_MouseDoubleClick);
            // 
            // _statusIndicationLabel
            // 
            this._statusIndicationLabel.AutoSize = true;
            this._statusIndicationLabel.BackColor = System.Drawing.Color.Transparent;
            this._statusIndicationLabel.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this._statusIndicationLabel.ForeColor = System.Drawing.Color.Gainsboro;
            this._statusIndicationLabel.Location = new System.Drawing.Point(306, 123);
            this._statusIndicationLabel.Name = "_statusIndicationLabel";
            this._statusIndicationLabel.Size = new System.Drawing.Size(97, 26);
            this._statusIndicationLabel.TabIndex = 7;
            this._statusIndicationLabel.Text = "Not ready";
            // 
            // sceneNameLabel
            // 
            this.sceneNameLabel.AutoSize = true;
            this.sceneNameLabel.BackColor = System.Drawing.Color.Transparent;
            this.sceneNameLabel.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.sceneNameLabel.ForeColor = System.Drawing.Color.Gainsboro;
            this.sceneNameLabel.Location = new System.Drawing.Point(20, 16);
            this.sceneNameLabel.Name = "sceneNameLabel";
            this.sceneNameLabel.Size = new System.Drawing.Size(73, 26);
            this.sceneNameLabel.TabIndex = 6;
            this.sceneNameLabel.Text = "Scene: ";
            // 
            // _calcTimeLabel
            // 
            this._calcTimeLabel.AutoSize = true;
            this._calcTimeLabel.BackColor = System.Drawing.Color.Transparent;
            this._calcTimeLabel.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this._calcTimeLabel.ForeColor = System.Drawing.Color.Gainsboro;
            this._calcTimeLabel.Location = new System.Drawing.Point(306, 98);
            this._calcTimeLabel.Name = "_calcTimeLabel";
            this._calcTimeLabel.Size = new System.Drawing.Size(53, 26);
            this._calcTimeLabel.TabIndex = 5;
            this._calcTimeLabel.Text = "0 ms";
            // 
            // _statisticEfficientLabel
            // 
            this._statisticEfficientLabel.AutoSize = true;
            this._statisticEfficientLabel.BackColor = System.Drawing.Color.Transparent;
            this._statisticEfficientLabel.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this._statisticEfficientLabel.ForeColor = System.Drawing.Color.Gainsboro;
            this._statisticEfficientLabel.Location = new System.Drawing.Point(20, 124);
            this._statisticEfficientLabel.Name = "_statisticEfficientLabel";
            this._statisticEfficientLabel.Size = new System.Drawing.Size(129, 26);
            this._statisticEfficientLabel.TabIndex = 4;
            this._statisticEfficientLabel.Text = "Efficiency: 0%";
            // 
            // _statisticNegativeLabel
            // 
            this._statisticNegativeLabel.AutoSize = true;
            this._statisticNegativeLabel.BackColor = System.Drawing.Color.Transparent;
            this._statisticNegativeLabel.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this._statisticNegativeLabel.ForeColor = System.Drawing.Color.Gainsboro;
            this._statisticNegativeLabel.Location = new System.Drawing.Point(20, 97);
            this._statisticNegativeLabel.Name = "_statisticNegativeLabel";
            this._statisticNegativeLabel.Size = new System.Drawing.Size(61, 26);
            this._statisticNegativeLabel.TabIndex = 3;
            this._statisticNegativeLabel.Text = "NG: 0";
            // 
            // _statisticGoodLabel
            // 
            this._statisticGoodLabel.AutoSize = true;
            this._statisticGoodLabel.BackColor = System.Drawing.Color.Transparent;
            this._statisticGoodLabel.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this._statisticGoodLabel.ForeColor = System.Drawing.Color.Gainsboro;
            this._statisticGoodLabel.Location = new System.Drawing.Point(20, 70);
            this._statisticGoodLabel.Name = "_statisticGoodLabel";
            this._statisticGoodLabel.Size = new System.Drawing.Size(59, 26);
            this._statisticGoodLabel.TabIndex = 2;
            this._statisticGoodLabel.Text = "OK: 0";
            // 
            // _statisticTotalLabel
            // 
            this._statisticTotalLabel.AutoSize = true;
            this._statisticTotalLabel.BackColor = System.Drawing.Color.Transparent;
            this._statisticTotalLabel.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this._statisticTotalLabel.ForeColor = System.Drawing.Color.Gainsboro;
            this._statisticTotalLabel.Location = new System.Drawing.Point(20, 43);
            this._statisticTotalLabel.Name = "_statisticTotalLabel";
            this._statisticTotalLabel.Size = new System.Drawing.Size(75, 26);
            this._statisticTotalLabel.TabIndex = 1;
            this._statisticTotalLabel.Text = "Total: 0";
            // 
            // _judgementLabel
            // 
            this._judgementLabel.BackColor = System.Drawing.Color.Transparent;
            this._judgementLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._judgementLabel.Font = new System.Drawing.Font("Verdana", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this._judgementLabel.ForeColor = System.Drawing.Color.LawnGreen;
            this._judgementLabel.Location = new System.Drawing.Point(277, 21);
            this._judgementLabel.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this._judgementLabel.Name = "_judgementLabel";
            this._judgementLabel.Size = new System.Drawing.Size(141, 83);
            this._judgementLabel.TabIndex = 0;
            this._judgementLabel.Text = "OK";
            this._judgementLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.mainFormChart, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(724, 850);
            this.tableLayoutPanel3.TabIndex = 11;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanel5.ColumnCount = 3;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.windowViewSelector, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(718, 24);
            this.tableLayoutPanel5.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.ForeColor = System.Drawing.Color.Gainsboro;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Image View:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // windowViewSelector
            // 
            this.windowViewSelector.BackColor = System.Drawing.Color.Black;
            this.windowViewSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowViewSelector.Font = new System.Drawing.Font("Calibri", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.windowViewSelector.ForeColor = System.Drawing.SystemColors.Window;
            this.windowViewSelector.FormattingEnabled = true;
            this.windowViewSelector.Items.AddRange(new object[] {
            "Normal",
            "Freeze"});
            this.windowViewSelector.Location = new System.Drawing.Point(103, 3);
            this.windowViewSelector.Name = "windowViewSelector";
            this.windowViewSelector.Size = new System.Drawing.Size(94, 22);
            this.windowViewSelector.TabIndex = 1;
            this.windowViewSelector.SelectedIndexChanged += new System.EventHandler(this.windowViewSelector_SelectedIndexChanged);
            // 
            // mainFormChart
            // 
            this.mainFormChart.BackColor = System.Drawing.Color.Transparent;
            this.mainFormChart.BackImageWrapMode = System.Windows.Forms.DataVisualization.Charting.ChartImageWrapMode.Scaled;
            chartArea1.AxisY.IsReversed = true;
            chartArea1.AxisY.LabelStyle.Enabled = false;
            chartArea1.BackColor = System.Drawing.Color.Transparent;
            chartArea1.BorderColor = System.Drawing.Color.DarkGray;
            chartArea1.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea1.Name = "ChartArea1";
            chartArea1.Position.Auto = false;
            chartArea1.Position.Height = 65F;
            chartArea1.Position.Width = 15F;
            chartArea1.Position.X = 13F;
            chartArea1.Position.Y = 25F;
            chartArea2.AxisY.IsReversed = true;
            chartArea2.AxisY.LabelStyle.Enabled = false;
            chartArea2.BackColor = System.Drawing.Color.Transparent;
            chartArea2.BorderColor = System.Drawing.Color.DarkGray;
            chartArea2.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea2.Name = "ChartArea2";
            chartArea2.Position.Auto = false;
            chartArea2.Position.Height = 65F;
            chartArea2.Position.Width = 15F;
            chartArea2.Position.X = 28F;
            chartArea2.Position.Y = 25F;
            chartArea3.AxisY.IsReversed = true;
            chartArea3.AxisY.LabelStyle.Enabled = false;
            chartArea3.BackColor = System.Drawing.Color.Transparent;
            chartArea3.BorderColor = System.Drawing.Color.DarkGray;
            chartArea3.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea3.Name = "ChartArea3";
            chartArea3.Position.Auto = false;
            chartArea3.Position.Height = 65F;
            chartArea3.Position.Width = 15F;
            chartArea3.Position.X = 43F;
            chartArea3.Position.Y = 25F;
            chartArea4.AxisY.IsReversed = true;
            chartArea4.AxisY.LabelStyle.Enabled = false;
            chartArea4.BackColor = System.Drawing.Color.Transparent;
            chartArea4.BorderColor = System.Drawing.Color.DarkGray;
            chartArea4.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea4.Name = "ChartArea4";
            chartArea4.Position.Auto = false;
            chartArea4.Position.Height = 65F;
            chartArea4.Position.Width = 15F;
            chartArea4.Position.X = 58F;
            chartArea4.Position.Y = 25F;
            chartArea5.AxisY.IsReversed = true;
            chartArea5.AxisY.LabelStyle.Enabled = false;
            chartArea5.BackColor = System.Drawing.Color.Transparent;
            chartArea5.BorderColor = System.Drawing.Color.DarkGray;
            chartArea5.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea5.Name = "ChartArea5";
            chartArea5.Position.Auto = false;
            chartArea5.Position.Height = 65F;
            chartArea5.Position.Width = 15F;
            chartArea5.Position.X = 73F;
            chartArea5.Position.Y = 25F;
            this.mainFormChart.ChartAreas.Add(chartArea1);
            this.mainFormChart.ChartAreas.Add(chartArea2);
            this.mainFormChart.ChartAreas.Add(chartArea3);
            this.mainFormChart.ChartAreas.Add(chartArea4);
            this.mainFormChart.ChartAreas.Add(chartArea5);
            this.mainFormChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainFormChart.Location = new System.Drawing.Point(3, 33);
            this.mainFormChart.Name = "mainFormChart";
            series1.BorderColor = System.Drawing.Color.Black;
            series1.BorderWidth = 3;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            series1.IsValueShownAsLabel = true;
            series1.IsVisibleInLegend = false;
            series1.LabelBackColor = System.Drawing.Color.Transparent;
            series1.MarkerColor = System.Drawing.Color.Black;
            series1.MarkerSize = 8;
            series1.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Diamond;
            series1.Name = "Series1";
            series1.SmartLabelStyle.MaxMovingDistance = 50D;
            series1.SmartLabelStyle.MinMovingDistance = 30D;
            series1.SmartLabelStyle.MovingDirection = System.Windows.Forms.DataVisualization.Charting.LabelAlignmentStyles.Right;
            series2.BorderWidth = 3;
            series2.ChartArea = "ChartArea2";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            series2.IsValueShownAsLabel = true;
            series2.IsVisibleInLegend = false;
            series2.MarkerColor = System.Drawing.Color.Black;
            series2.MarkerSize = 8;
            series2.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Diamond;
            series2.Name = "Series2";
            series3.BorderWidth = 3;
            series3.ChartArea = "ChartArea3";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            series3.IsValueShownAsLabel = true;
            series3.IsVisibleInLegend = false;
            series3.MarkerColor = System.Drawing.Color.Black;
            series3.MarkerSize = 8;
            series3.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Diamond;
            series3.Name = "Series3";
            series4.BorderWidth = 3;
            series4.ChartArea = "ChartArea4";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            series4.IsValueShownAsLabel = true;
            series4.IsVisibleInLegend = false;
            series4.MarkerColor = System.Drawing.Color.Black;
            series4.MarkerSize = 8;
            series4.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Diamond;
            series4.Name = "Series4";
            series5.BorderWidth = 3;
            series5.ChartArea = "ChartArea5";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            series5.IsValueShownAsLabel = true;
            series5.IsVisibleInLegend = false;
            series5.MarkerColor = System.Drawing.Color.Black;
            series5.MarkerSize = 8;
            series5.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Diamond;
            series5.Name = "Series5";
            this.mainFormChart.Series.Add(series1);
            this.mainFormChart.Series.Add(series2);
            this.mainFormChart.Series.Add(series3);
            this.mainFormChart.Series.Add(series4);
            this.mainFormChart.Series.Add(series5);
            this.mainFormChart.Size = new System.Drawing.Size(718, 814);
            this.mainFormChart.TabIndex = 12;
            // 
            // controlUpdate
            // 
            this.controlUpdate.Enabled = true;
            this.controlUpdate.Interval = 10;
            this.controlUpdate.Tick += new System.EventHandler(this.controlUpdate_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1180, 856);
            this.Controls.Add(this.tableLayoutPanel2);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fenix Mask I";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loginBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.actuatorErrorIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpioErrorIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cameraErrorIcon)).EndInit();
            this.multiPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainFormChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button runBtn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel multiPanel;
        private System.Windows.Forms.Label _judgementLabel;
        private System.Windows.Forms.Button flowBtn;
        private System.Windows.Forms.RichTextBox logMsg;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label _statisticEfficientLabel;
        private System.Windows.Forms.Label _statisticNegativeLabel;
        private System.Windows.Forms.Label _statisticGoodLabel;
        private System.Windows.Forms.Label _statisticTotalLabel;
        private System.Windows.Forms.Label _calcTimeLabel;
        private System.Windows.Forms.FolderBrowserDialog logFolderDialog;
        private System.Windows.Forms.Button settingsBtn;
        private System.Windows.Forms.Button enGpioBtn;
        private System.Windows.Forms.Button enImgLogBtn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.PictureBox gpioErrorIcon;
        private System.Windows.Forms.PictureBox cameraErrorIcon;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button folderBtn;
        private System.Windows.Forms.Button prevBtn;
        private System.Windows.Forms.Button NextBtn;
        private System.Windows.Forms.Timer controlUpdate;
        private System.Windows.Forms.OpenFileDialog openImageDialog;
        private System.Windows.Forms.Button enFileSourceBtn;
        private System.Windows.Forms.PictureBox actuatorErrorIcon;
        private System.Windows.Forms.Label sceneNameLabel;
        private System.Windows.Forms.Button actuatorsInitBtn;
        private System.Windows.Forms.Button singleRun1Btn;
        private System.Windows.Forms.Label _statusIndicationLabel;
        private System.Windows.Forms.PictureBox loginBtn;
        private System.Windows.Forms.TextBox filenameTb;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox windowViewSelector;
        private System.Windows.Forms.Button singleRun2Btn;
        private System.Windows.Forms.DataVisualization.Charting.Chart mainFormChart;
    }
}

