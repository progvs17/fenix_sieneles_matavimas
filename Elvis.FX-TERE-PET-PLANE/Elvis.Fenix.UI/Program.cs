﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;


namespace Elvis.Fenix
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            SplashForm.ShowSplashScreen();
            System.Threading.Thread.Sleep(500);
            SplashForm.Print("Loading...");
            
            try
            {
                //HalconDotNet.HSystem.GetSystem("hostids");
            }
            /*catch ()//HalconDotNet.HalconException ex)
            {
                SplashForm.Print("Checking license... Failed");
                MessageBox.Show("No Halcon license or USB dongle found! Software will close.", "License error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }*/
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, "Program error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, true);
                SplashForm.Print("Launch error.");
                Thread.Sleep(2000);
                SplashForm.Print("Software will close.");
                Thread.Sleep(2000);
                return;
            }

            //SplashForm.Print("Checking license... OK");

            MainForm mainForm = new MainForm();
            SplashForm.CloseForm();

            Application.Run(mainForm);

        }
    }
}
