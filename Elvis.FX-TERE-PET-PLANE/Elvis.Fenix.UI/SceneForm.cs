﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

using Elvis.Fenix.FunctionBlocks;


namespace Elvis.Fenix
{
    public struct Group <T1, T2>
    {
        public T1 Id { get; set; }
        public T2 Value { get; set; }
    };


    public partial class SceneForm : Form
    {
        FenixCore _core;
        List<ListViewItem> Blocks = new List<ListViewItem>();
        public int SelectedIndex { get; set; }
        private Bitmap image;

        private readonly Mutex _mutex;

        public SceneForm(MainForm form, FenixCore core)
        {
            InitializeComponent();

            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);


            _core = core;

            _mutex = new Mutex();

            DataGridViewImageColumn gridImage = new DataGridViewImageColumn();
            gridImage.Name = "Image";
            blockSelect.Columns.Add(gridImage);

            DataGridTextBoxColumn gridText = new DataGridTextBoxColumn();

            blockSelect.Columns.Add("Name", "Name");
            blockSelect.Columns.Add("Status", "Status");          
    
            
            foreach(var item in core.scene.blocks)
            {
                if (item.Icon != null)
                {
                    string judge = item.Judgement == true ? "OK" : "NG";
                    blockSelect.Rows.Add(item.Icon, item.Name, judge);                    
                    blockSelect.Columns[0].Width = 60;
                    blockSelect.Columns[2].Width = 60;
                    blockSelect.Rows[blockSelect.Rows.Count - 1].Height = 50;
                    blockSelect.Rows[blockSelect.Rows.Count - 1].Cells[1].Style.ForeColor = Color.Gray;
                }                
            }

            if (_core.scenesCount > 0)
            {
                foreach (var _scene in _core.settingsManager.FolderNames)
                {
                    sceneSelect.Items.Add(_scene);
                }

            }




            //blockSelect.Items.Add(_core.scene.Name);            
            propertyGrid1.SelectedObject = core.scene.StartBlock;

            //this.hWindow.HMouseDown += new HMouseEventHandler(((FxRotateAndGrabImage)core.scene.blocks.Find(m => m.Name == "Filter Images")).HOnMouseDown);//HolesBlobs Images
            //this.hWindow.HMouseMove += new HMouseEventHandler(((FxRotateAndGrabImage)core.scene.blocks.Find(m => m.Name == "Filter Images")).HOnMouseMove);//HolesBlobs Images
            //this.hWindow.HMouseWheel += new HMouseEventHandler(((FxRotateAndGrabImage)core.scene.blocks.Find(m => m.Name == "Filter Images")).HOnMouseWheel);//HolesBlobs Images
            //this.hWindow.HMouseUp += new HMouseEventHandler(((FxRotateAndGrabImage)core.scene.blocks.Find(m => m.Name == "Filter Images")).HOnMouseUp);//HolesBlobs Images
        }

        private void SceneForm_Load(object sender, EventArgs e)
        {
            //_core.Stop();

            Thread.Sleep(1000);

            _core.scene.IsRunMode = false;

            sceneSelect.SelectedIndex = _core.settings.LastSavedMaskIndex; // ??????????????????????????????????????????????????????????????
            _core.scene.onCompleted += new SceneCompleteHandler(UpdateHImage);

            foreach(var block in _core.scene.blocks)
            {
                block.onTaskCompleted += new TaskCompleteHandler(HandleBlockEvent);
            }

            _core.SetGrabBlocksOperation(OperationMode.SingleImage);

            _core.RunBlock();

            canUpdate = true;

        }

        void HandleBlockEvent(object sender)
        {
            FxSVMPredictor svmPredictor = sender as FxSVMPredictor;
            FxBottlePlaneData bottleData = sender as FxBottlePlaneData;

            if (svmPredictor != null || bottleData != null)
            {
                //if (canUpdate)
                UpdateHImage(sender);
            }
        }

        bool canUpdate = true;

        void UpdateHImage(Object sender)
        {
            lock (imageWindow)
            {
                if (canUpdate)
                {
                    canUpdate = false;
                    try
                    {
                        FxSVMPredictor svmPredictor = sender as FxSVMPredictor;
                        FxBottlePlaneData bottleData = sender as FxBottlePlaneData;
                        FxScene scene = sender as FxScene;
                        FxBlock block = null;

                        image = null;

                        if (svmPredictor != null)
                        {
                            block = svmPredictor;
                            image = svmPredictor.Image;
                        }
                        else if (bottleData != null)
                        {
                            image = bottleData.Image;
                        }


                        if (image != null)
                        {
                            imageWindow.Invoke(new Action(() => imageWindow.BackgroundImage = image));
                        }

                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        canUpdate = true;
                    }
                }
            }
            
        }
        
      

        void UpdateBlockList(FxScene scene)
        {
            if (blockSelect.InvokeRequired)
            {
                blockSelect.Invoke(new Action(() => UpdateBlockList(scene)));
            }
            else
            {
                

                List<FxBlock> blocks = new List<FxBlock>();

                FxBlock sceneBlock = scene.StartBlock;
                while (sceneBlock != null)
                {
                    blocks.Add(sceneBlock);
                    sceneBlock = sceneBlock.NextFxBlock;
                }


                int row = 0;
                foreach (var block in scene.blocks)
                {
                    if (blocks.Find(b => b == block) != null)
                    {
                        if (block.Icon != null)
                        {
                            Color color = block.Judgement == true ? Color.GreenYellow : Color.IndianRed;
                            string judge = block.Judgement == true ? "OK" : "NG";
                            blockSelect.Rows[row].Cells[1].Style.ForeColor = Color.Black;
                            blockSelect.Rows[row].Cells[2].Style.ForeColor = color;
                            blockSelect.Rows[row].Cells[2].Value = judge;
                        }
                    }
                    else
                    {
                        blockSelect.Rows[row].Cells[1].Style.ForeColor = Color.Gray;
                        blockSelect.Rows[row].Cells[2].Value = "";                        
                    }
                    row++;
                } 
            }
        }

        private void SceneForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            canUpdate = false;         

            foreach (var block in _core.scene.blocks)
            {
                block.SingleRun = true;
                block.Stop();
                block.onTaskCompleted -= new TaskCompleteHandler(HandleBlockEvent);
            }

            _core.AllowToStopThread();
            _core.Stop();
            _core.scene.onCompleted -= new SceneCompleteHandler(UpdateHImage);

            _core.SaveConfigData();
        }

        /// <summary>
        /// Scene changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sceneSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            _core.Stop();
            _core.Stop();
            // unassign old events
            _core.scene.onCompleted -= new SceneCompleteHandler(UpdateHImage);

            foreach (var block in _core.scene.blocks)
            {
                block.onTaskCompleted -= new TaskCompleteHandler(HandleBlockEvent);
            }

            // change settings
            if (sceneSelect.SelectedIndex >= 0)
            {
                propertyGrid1.SelectedObject = _core.scene.blocks[sceneSelect.SelectedIndex];
                _core.ChangeSettings(sceneSelect.SelectedIndex, sceneSelect.SelectedItem.ToString());
            }

            // assign new events
            _core.scene.onCompleted += new SceneCompleteHandler(UpdateHImage);

            foreach (var block in _core.scene.blocks)
            {
                block.onTaskCompleted += new TaskCompleteHandler(HandleBlockEvent);
            }

            _core.RunBlock();

        }
   

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
 //           if (!canUpdate) return;

            for (int i = 0; i < _core.scene.BlocksCount; i++)
            {
                FxBlock block_ = _core.scene.GetBlockByOrder(i);               
                if(block_ != null)
                {
                    block_.IsFocused = false;                    
                }                
            }

            FxBlock block = _core.scene.GetBlockByOrder(e.RowIndex);
            if (block != null)
            {
                block.IsFocused = true;
            } 

            propertyGrid1.SelectedObject = _core.scene.GetBlockByOrder(e.RowIndex);
            SelectedIndex = e.RowIndex;
            int  prevIndex = _core.scene.BlockIndex;
            _core.scene.BlockIndex = SelectedIndex;
            //_core.scene.blocks[prevIndex].Exit();
            _core.scene.blocks[prevIndex].Stop();

            /*var filter1 = _core.scene.GetBlockByOrder(e.RowIndex) as FxHalconFilterDemo;*/


            
            controlPanel.SuspendLayout();

            
            controlPanel.Controls.Clear();
            //controlPanel.RowStyles.Clear();

            

            var item = _core.scene.blocks[SelectedIndex];
            int ii = 0;

            //if(item.GetType() == new FxBottlePlaneData().GetType())
            //{
            //    imageWindow.Visible = false;
            //    bottleDataChart.Visible = true;
            //}
            //else
            //{
            //    imageWindow.Visible = true;
            //    bottleDataChart.Visible = false;
            //}


            System.Reflection.MethodInfo[] m = item.GetType().GetMethods();

            controlPanel.Height = 0;


            foreach (var method in item.GetType().GetMethods())
            {
                if (method.GetCustomAttributes(false).Any(a => a is ButtonControlAttribute))
                {

                    controlPanel.Height = 35 * (ii + 1);
                    Button button = new Button { Text = method.Name, Height = 30, Width = 420, FlatStyle = FlatStyle.Flat, Left = 3, Top = 3 + 33 * ii++ };
                    controlPanel.Controls.Add(button);

                    var meth = item.GetType().GetMethods().FirstOrDefault(f => f.Name == method.Name);

                    try
                    {
                        button.Click += (s, ev) => { meth.Invoke(item, null); };
                    }
                    catch(System.Reflection.TargetInvocationException ex)
                    {
                        Console.Write(ex.Message);
                    }
                    catch(Exception ex) 
                    {

                    }

                }

            }
 

        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            if(!propGridIsFocused)
                propertyGrid1.Refresh();
        }

        // reset chart scale
        private void bottleDataChart_DoubleClick(object sender, EventArgs e)
        {
            bottleDataChart.Invoke(new Action(() => bottleDataChart.ResetAutoValues()));
        }

        bool propGridIsFocused = false;

        private void propertyGrid1_Enter(object sender, EventArgs e)
        {
            propGridIsFocused = true;
        }

        private void propertyGrid1_Leave(object sender, EventArgs e)
        {
            propGridIsFocused = false;
        }

        private void SceneForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if((Keys)e.KeyChar == Keys.Enter && propGridIsFocused)
            {
                this.ActiveControl = blockSelect;
            }
        }

        private void propertyGrid1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {

        }


    }


}
