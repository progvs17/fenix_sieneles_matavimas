﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Elvis.Fenix
{
    public partial class SplashForm : Form
    {

        //Delegate for cross thread call to close
        private delegate void CloseDelegate();
        private delegate void PrintMessage(string msg);

        //The type of form to be displayed as the splash screen.
        private static SplashForm splashForm;

        public SplashForm()
        {
            InitializeComponent();
            this.label1.Text = "Build " + typeof(MainForm).Assembly.GetName().Version;
        }

        static public void ShowSplashScreen()
        {
            // Make sure it is only launched once.

            if (splashForm != null)
                return;
            Thread thread = new Thread(new ThreadStart(SplashForm.ShowForm));
            thread.IsBackground = true;
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        static private void ShowForm()
        {
            splashForm = new SplashForm();
            Application.Run(splashForm);
        }

        static public void CloseForm()
        {
            try
            {
                splashForm.Invoke(new CloseDelegate(SplashForm.CloseFormInternal));
            }
            catch (Exception ex)
            {

            }
        }

        static public void Print(string msg)
        {
            try
            {
                splashForm.Invoke(new PrintMessage(SplashForm.PrintMessageInternal), msg);
            }
            catch (Exception ex)
            {

            }
        }

        static private void PrintMessageInternal(string msg)
        {
            splashForm.msgLabel.Text = msg;
        }

        static private void CloseFormInternal()
        {
            splashForm.Close();
        }
    }
}
