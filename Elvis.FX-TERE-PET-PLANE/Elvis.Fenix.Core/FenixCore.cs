﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Threading;
using System.Linq.Expressions;
using System.Drawing;
using System.Drawing.Imaging;
using System.Xml.Serialization;
using System.Reflection;

using Elvis.Fenix.Hardware;
using Elvis.Fenix.Hardware.Gpio;
using Elvis.Fenix.General;
using Elvis.Fenix.FunctionBlocks;
using Elvis.Fenix.IO;


namespace Elvis.Fenix
{
    public delegate void SceneCompletedHandler (object sender);
    public delegate void ErrorStateChangedEventHandler(object sender, ErrorFlags err);
    public delegate void MessageEventHandler(object sender, FxMessage err);

    [Flags]
    public enum ErrorFlags
    {
        CAMERA_ERROR = 0x01,
        GPIO_ERROR = 0x02,
        LAMP_ERROR = 0x04,
        TARGET_ERROR = 0x08,
        ACTUATOR_ERROR = 0x10,
        ALL = 0xFF,
    }

    [Serializable()]
    public struct BindingsPairItem
    {
        public string SourceObj { get; set; }
        public string SourceName { get; set; }
        public string SourcePropName { get; set; }

        public string TargetObj { get; set; }
        public string TargetName { get; set; }
        public string TargetPropName { get; set; }
    }

    [Serializable()]
    public struct BlockRellationsItem
    {
        public string SourceBlock { get; set; }
        public string TargetBlock { get; set; }
    }

    [Serializable()]
    public struct HardwareRellationsItem
    {
        public string BlockName { get; set; }
        public string HardwareId { get; set; }
    }

    [Serializable()]
    public class RellationsListsClass
    {
        public RellationsListsClass() { }

        List<BindingsPairItem> _bindingsList = new List<BindingsPairItem>();
        List<HardwareRellationsItem> _hwRellationsList = new List<HardwareRellationsItem>();
        List<BlockRellationsItem> _blocksRellationsList = new List<BlockRellationsItem>();
        string _startBlock;
        private string _pos1Block;
        private string _pos2Block;
        private string _endBlock;

        public List<BindingsPairItem> BindingsList 
        {
            get { return _bindingsList; }
            set { _bindingsList = value; } 
        }

        public List<HardwareRellationsItem> HwRellationsList
        {
            get { return _hwRellationsList; }
            set { _hwRellationsList = value; }
        }

        public List<BlockRellationsItem> BlocksRellationsList
        {
            get { return _blocksRellationsList; }
            set { _blocksRellationsList = value; }
        }

        public string StartBlock
        {
            get { return _startBlock; }
            set { _startBlock = value; }
        }

        public string EndBlock
        {
            get { return _endBlock; }
            set { _endBlock = value; }
        }

        public string Position1Block
        {
            get { return _pos1Block; }
            set { _pos1Block = value; }
        }

        public string Position2Block
        {
            get { return _pos2Block; }
            set { _pos2Block = value; }
        }
    }

    public class FenixCore : IFxBindable, IDisposable
    {
        private Thread thread;
        private SynchronizationContext _context;        
        private Object sync = new FxBinder();
        private int _results;
        private long _judgement;
        private FxBinder binder = new FxBinder();
        private ErrorFlags _errorStatus;

        private bool threadStopRequest = false;
        private bool sceneStarted = false;

        private string settingsPath = @"C:\Fenix\Terekas\PET Plane Quality\App settings";

        private System.Threading.Timer errTimer;
        bool sceneChangeIsAvailable = true;
        private readonly Action<Action> _synchronousInvoker;   

        // Sources
        public FxGpio _gpio;
        public FxCamera _camera;
        public FxCamera _camera2;
        public Settings settings;
        public FxSettingsManager settingsManager;
        public FxGpio analog_board;
        
        public FxScene scene;

        public List<FxScene> scenesList;
        
        public FxBranch _condition;
        
        public FxDIBranch _diBranch;   

        public FxStartBlock _startBlock;
        public FxSaveGrabbedImagesBlock _imSaveBlock;
        public FxSVMPredictor _svmPredictor;
        public FxBottlePlaneData _bottlePlaneData;

        public FxEnd _end;

        public FxImageLog _imgLogger;
        //public FxImageLogger _imgLogger = new FxImageLogger(@"C:\Fenix\Terekas\PET Plane Quality\Images");

        
        public List<HardwareObject> HardwareList = new List<HardwareObject>();
        public List<Object> BlockList = new List<Object>();
        //public List<BindingsPairClass> BindingsList = new List<BindingsPairClass>();
        public RellationsListsClass Rellations = new RellationsListsClass();


        public bool actuatorsInitialized = false;
        public bool actuatorsInitSequenceCompleted = false;

        public int TotalCount { get; set; }
        public int NGCount { get; set; }
        public int OKCount { get; set; }
        public double Efficiency { get; set; }
        public bool disposed { get; set; }

        public bool LoadImagesFromFiles { get; set; }
        
        public event ErrorStateChangedEventHandler onErroStatusChanged;
        public event MessageEventHandler onMessage;
        public event SceneCompletedHandler onSceneCompleted;

        public FenixCore()
        {
        }

        public FenixCore(Action<Action> synchronousInvoker)
        {
            _synchronousInvoker = synchronousInvoker;            
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);   
        }

        // Protected implementation of Dispose pattern. 
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                SaveCommonSettings();
                SaveConfigData();

                errTimer.Dispose();

               if(scene.blocks != null)
               {
                   foreach(var block in scene.blocks)
                   {
                       block.Dispose();
                   }
               }

               SetGpioPort(0, 0);               
               Thread.Sleep(1000);

                HardwareDisabled();
                if(_gpio != null)
                {
                    _gpio.Close();
                }           
                
                if(analog_board != null)
                {
                    analog_board.Close();
                }   
            }

            // Free any unmanaged objects here. 
            disposed = true;
        }

        bool initCompleted = false;

        public void Init()
        {
            settingsManager = new FxSettingsManager();

            LoadCommonSettings();

            HandleInfoMessage("CORE", "Started core initialization.");
            ClearError(ErrorFlags.ALL);
            try
            {
                _gpio = new ICPDASUniDAQ();
                _gpio.EnableDI = true;
                _gpio.EnableDO = true;
                _gpio.Open();

            }
            catch (Exception ex)
            {
                SetError(ErrorFlags.GPIO_ERROR);
                HandleErrorMessage("GPIO", ex.Message);
                FxSessionTimeClass.LogErrorMessage("(GPIO) ", ex.Message);
            }


            try
            {
                analog_board = new DAQ_2205(DAQ_2205.DEVICE_MODEL.DAQ_2205, 0);
                
                if (!analog_board.IsOpen)
                {
                    analog_board.Open();
                    analog_board.SetReferenceVoltage(-1, (ushort)DAQ_2205.AD_RANGE.AD_B_5_V);
                }

                if (!analog_board.IsOpen)
                {
                    SetError(ErrorFlags.GPIO_ERROR);
                    HandleErrorMessage("GPIO", "Analog inputs board is not connected!");
                }
            }
            catch (Exception ex)
            {
                SetError(ErrorFlags.GPIO_ERROR);
                HandleErrorMessage("GPIO", ex.Message);
                FxSessionTimeClass.LogErrorMessage("(GPIO) ", ex.Message);
            }


            _context = SynchronizationContext.Current;


            if(!IsError(ErrorFlags.GPIO_ERROR))
            {
                _gpio.Open();
            }


            //LoadActuators();
            //InitActuators();

            //LoadHardwareList();
            LoadBlockList();
            
            LoadConfigData();

            LoadScene();

            foreach (FxBlock b in scene.blocks)
            {
                b.onTaskCompleted += b_onTaskCompleted;
                b.onMessage += b_onMessage;
            }

            SetBlocksRellations();

            SetDataBindings(true);

            //CheckScenePins();
            //HanldeInfoMessage("Scene: ", settings.SceneName);

            errTimer = new System.Threading.Timer(new TimerCallback(OnErrTimerElapsed), true, 100, 200); //

            HandleInfoMessage("CORE", "Finished core initialization.");

            if(_gpio != null)
            {
                EnableDI = true;
                EnableDO = true;
            }
  
            if(Errors == 0)
            {
                
            }

            ClearStatistic();

            SetGpioPin(settings.SystemFaultLightDO, true);

            initCompleted = true;

            //if(settings.EnAutoRun)
            //{
            //    Start();
            //}

            HardwareEnabled();
        }

        private Queue<int> scenePathQueue = new Queue<int>();
        
        private void AddScenePathToBuffer(int path)
        {
            scenePathQueue.Enqueue(path);
        }

        private int RetrieveScenePathFromBuffer()
        {
            if(scenePathQueue.Count > 0)
            {
                return scenePathQueue.Dequeue();
            }
            return 0;
        }


        int nextScene = 0;
 

        void LoadConfigData()
        {
            if (settingsManager == null)
                settingsManager = new FxSettingsManager();


            settingsManager.Add<FxStartBlock>(_startBlock.Name, _startBlock);
            settingsManager.Add<FxSaveGrabbedImagesBlock>(_imSaveBlock.Name, _imSaveBlock);
            settingsManager.Add<FxSVMPredictor>(_svmPredictor.Name, _svmPredictor);
            settingsManager.Add<FxBottlePlaneData>(_bottlePlaneData.Name, _bottlePlaneData);
            //settingsManager.Add<FxGrabImage>(_grabber1.Name, _grabber1);
            //settingsManager.Add<FxHContourBreak>(_contourBreak.Name, _contourBreak);

            //settingsManager.Add<FxGrabImage>(_grabber2.Name, _grabber2);
            //settingsManager.Add<FxHNeckDeformation>(_neckDeformation.Name, _neckDeformation);
            //settingsManager.Add<FxActuatorBlock>(_rotaryActuatorBlock1.Name, _rotaryActuatorBlock1);

            settingsManager.Add<FxEnd>(_end.Name, _end);

            //settingsManager.Add<RellationsListsClass>("RellationList", Rellations);


            settingsManager.GetSettingsFolders(settingsPath); // (@"C:\Fenix\Mask Configs");
            settingsManager.ApplyNewSettings(settings.LastSavedMaskIndex);

            scenesCount = settingsManager.NumberOfSettings;         
        }

        public void AnnounceSettingsSavedMessage()
        {
            HandleInfoMessage("Settings", "Saved!");
        }


        public void AnnounceDebugLogStatus(bool status)
        {
            string str = "disabled!";
            if (status) str = "enabled!";

            HandleInfoMessage("Settings", "Debug log " + str);
        }


        public int _idx = 0;
        public int scenesCount = 0;

        /// <summary>
        /// Method to change scene settings
        /// </summary>
        /// <param name="index">new scene index</param>
        /// <param name="newSceneName">new scene name</param>
        public void ChangeSettings(int index, string newSceneName)
        {
            if (settings.EnableDebugLog)
            {
                HandleInfoMessage("Core", "Current scene id: " + settings.LastSavedMaskIndex);
                HandleInfoMessage("Core", "New scene id: " + index);
            }

            if (!index.Equals(settings.LastSavedMaskIndex))
            {
                Stop();

                HandleInfoMessage("Core", "Changing scene...");
                // save previous
                //SaveConfigData(); 

                // update new
                settings.LastSavedMaskIndex = index;

                settingsManager.ApplyNewSettings(settings.LastSavedMaskIndex);
                HandleInfoMessage("Core", "Scene changed!");
               
            }

            settings.SceneName = scene.Name = newSceneName;
            //_grabber1.SceneName = newSceneName;
            //_grabber2.SceneName = newSceneName;

            OnPropertyChanged(new PropertyChangedEventArgs("SceneName"));

            LoadScene();
        }

        public void LoadCommonSettings()
        {
            settings = new Settings("Settings");

            if (System.IO.File.Exists(settingsPath + @"\" + settings.Name + ".xml"))
                settings = settingsManager.ReadSingleFileFromXml<Settings>(settingsPath + @"\" + settings.Name);
                
        }

        public void SaveCommonSettings()
        {
            settingsManager.WriteSingleFileToXml<Settings>(settingsPath + @"\" + settings.Name, settings);
        }

        /// <summary>
        /// Method to save current settings configuration
        /// </summary>
        public void SaveConfigData()
        {
            // update bindings
            GetDataBindings();

            settingsManager.SaveCurrentSettings(settings.LastSavedMaskIndex, "cfg");

            //settingsManager.WriteSingleFileToXml<Settings>(settings.Name, settings);
        }

        //void CheckCamera(FxCamera camera, bool clearErrors = false)
        //{
        //    if(camera != null)
        //    {
        //        try
        //        {
        //            camera.Connect();
        //            camera.Disconnect();
        //            if(clearErrors)
        //            {
        //                //ClearError(ErrorFlags.CAMERA_ERROR);
        //                HandleInfoMessage("CORE", "Camera init done.");
        //            }
        //        }
        //        catch(Exception ex)
        //        {
        //            HandleErrorMessage("Hardware error", "Failed to start camera");
        //            //SetError(ErrorFlags.CAMERA_ERROR);        
        //            FxSessionTimeClass.LogErrorMessage("(Camera) ", ex.Message);
           
        //        }
        //    }
        //}

        #region FX-INS-MASK 1 PINS
        
        /// <summary>
        /// Set 'MANUAL' DO pin
        /// </summary>
        public void SetRunDO()
        {
            if (_gpio != null) _gpio.SetPin(settings.HardwareEnableDOPin, 1);
        }



        /// <summary>
        /// Set 'GD' (Good) DO pin
        /// </summary>
        public void SetGD()
        {
            //if (_gpio != null) _gpio.SetPin(settings.GDDOPin, 1);
        }

        /// <summary>
        /// Clear 'GD' (Good) DO pin
        /// </summary>
        public void ClearGDDO()
        {
            //if (_gpio != null) _gpio.SetPin(settings.GDDOPin, 0);
        }

        public void ActuatorON()
        {
            if (_gpio != null) _gpio.SetPin(settings.ActuatorOnDOPin, 1);
        }

        public void ActuatorOFF()
        {
            if (_gpio != null) _gpio.SetPin(settings.ActuatorOnDOPin, 0);
        }

        public void HardwareEnabled()
        {
            if (_gpio != null) _gpio.SetPin(settings.HardwareEnableDOPin, 1);
        }

        public void HardwareDisabled()
        {
            if (_gpio != null) _gpio.SetPin(settings.HardwareEnableDOPin, 0);
        }

        /// <summary>
        /// Set 'NG' (Not Good) DO pin
        /// </summary>
        public void SetNG()
        {
            if (_gpio != null) _gpio.SetPin(settings.NGDOPin, 1);
        }

        /// <summary>
        /// Clear 'NG' (Not Good) DO pin
        /// </summary>
        public void ClearNGDO()
        {
            if (_gpio != null) _gpio.SetPin(settings.NGDOPin, 0);
        }

        public bool ManualEnabled()
        {
            if (_gpio != null)
            {
                return _gpio.GetPin(0, settings.ManualInDIPin) == 1 ? true : false;
            }
            else
            {
                return false;
            }
        }

        public bool BottleIsIN()
        {
            if (_gpio != null)
            {
                return _gpio.GetPin(0, settings.BottleInDIPin) == 1 ? true : false;
            }
            else
            {
                return false;
            }
        }

        public bool SafetyStopIsActive()
        {
            if (_gpio != null)
            {
                return _gpio.GetPin(0, settings.SafetyStopInDIPin) == 1 ? true : false;
            }

            return false;
        }
       
        public void ReleaseBottle()
        {
            if (_gpio != null) _gpio.SetPin(settings.BottleCatchDOPin, 0);
        }

        public void EnableNozzles()
        {
            if (_gpio != null)
            {
                _gpio.SetPin(settings.NozzleDOPin, 1);
            }
        }

        public void DisableNozzles()
        {
            if (_gpio != null)
            {
                _gpio.SetPin(settings.NozzleDOPin, 0);
            }
        }


        #endregion


        public void SetGpioPin(int pin, bool value)
        {
            if(_gpio != null)
            {
                _gpio.SetPin(pin, Convert.ToInt32(value));
            }
        }

        public void SetGpioPort(int port, int value)
        {
            if(_gpio != null)
            {
                _gpio.SetPort(port, value);
            }            
        }

        public void ToggleGpioPin(int pin)
        {
            if (_gpio != null)
            {
                _gpio.TogglePin(pin);
            }
        }

        void CheckGpio(FxGpio gpio, bool clearErrors = false)
        {
            if (gpio != null)
            {
                try
                {
                    gpio.Open();
                    //gpio.Close();
                    if (clearErrors)
                    {
                        ClearError(ErrorFlags.GPIO_ERROR);
                        HandleInfoMessage("CORE", "GPIO init done.");
                    }
                }
                catch (Exception ex)
                {
                    HandleErrorMessage("Hardware error", "Failed to start gpio module");
                    SetError(ErrorFlags.GPIO_ERROR);
                    FxSessionTimeClass.LogErrorMessage("(GPIO) ", ex.Message);

                }
            }            
        }

        void HandleInfoMessage(string title, string msg)
        {
            var handler = onMessage;

            if (handler != null)
            {
                handler(null, new General.FxMessage(General.FxMessageType.INFO_MSG, title, msg));
            }
        }

        void HandleErrorMessage(string title, string msg)
        {
            var handler = onMessage;
            if (handler != null)
            {
                handler(null, new General.FxMessage(General.FxMessageType.ERROR_MSG, title, msg));
            }
        }

        void HandleWarningMessage(string title, string msg)
        {
            var handler = onMessage;
            if (handler != null)
            {
                handler(null, new General.FxMessage(General.FxMessageType.WARNING_MSG, title, msg));
            }
        }

        void HandleMaskOkMessage(string title, string msg)
        {
            var handler = onMessage;
            if (handler != null)
            {
                handler(null, new General.FxMessage(General.FxMessageType.MASK_OK_MSG, title, msg));
            }
        }

        /// <summary>
        /// Core start method
        /// </summary>
        public void Start()
        {
            previousPath = 1;

            SetGrabBlocksOperation(OperationMode.MultipleImages);

            scene.CropTiledImages = settings.CropTiledImages;
            scene.CroppedImageSize = settings.CroppedImageSize;
            scene.HorizontalImagesToTile = settings.HorizontalImagesToTile;
            scene.TiledImagesBorderWidth = settings.TiledImagesBorderWidth;

            if (thread != null) Stop();

            if (thread == null)
            {

                if (_gpio != null)
                {
                    
                }

                // patikrinti del auto/manual input'o
                if (ManualEnabled() && !runSceneOnce)
                {
                    HandleInfoMessage("Hardware", "Manual switch is enabled");
                    return;
                }
                else if (!ManualEnabled() && runSceneOnce)
                {
                    HandleInfoMessage("Hardware", "Manual switch is not enabled");
                    return;
                }
                else if (SafetyStopIsActive())
                {
                    HandleErrorMessage("ERROR", "Safety stop is active!");
                    return;
                }
                else
                {
                    ActuatorON();

                    scene.IsRunMode = true;
                    thread = new Thread(new ThreadStart(scene.Run));
                    thread.Name = "SceneRun";
                    thread.Start();
                    HandleInfoMessage("Core", "Started");

                    threadStopRequest = false;
                }

                
            }

        }

        /// <summary>
        /// Core stop method
        /// </summary>
        public void Stop()
        {
            ActuatorOFF();
            // isjungti putiklius
            DisableNozzles();

            if (thread != null && thread.IsAlive)
            {
                scene.IsRunMode = false;
                scene.Stop();
                thread.Abort(); /// !!!!
                thread.Join(1000);
                HandleInfoMessage("Core", "Stopped");
            }
            thread = null;
            threadStopRequest = false;
            

            if (!runSceneOnce)
                scenePath = 0;

            nextScene = 0;
            scenePathQueue.Clear();

        }       

        /// <summary>
        /// Core status indication
        /// </summary>
        /// <returns>Status</returns>
        public string CoreStatus()
        {
            if (actuatorsInitSequenceCompleted && !IsStarted())
            {
                return "Ready";
            }
            else if(actuatorsInitSequenceCompleted && IsStarted())
            {
                return "Running";
            }
            else if(IsStarted() && _imSaveBlock.EnableImageLoader)
            {
                return "Running";
            }
            else
            {
                return "Not ready";
            }
        }

        /// <summary>
        /// Method to check if core thread is started
        /// </summary>
        /// <returns>true if alive, false if not</returns>
        public bool IsStarted()
        {
            return thread != null ? thread.IsAlive : false;
        }


        public void RunBlock()
        {
            if (thread == null)
            {
                try
                {
                    thread = new Thread(() => scene.RunBlock());
                    thread.Start();
                    HandleInfoMessage("Core", "Started");
                }
                catch
                {
                    HandleInfoMessage("Core", "Unexpected thread termination");
                    if(thread != null)
                    {
                        thread.Abort();
                        thread.Join(1000);
                    }
                    thread = null;
                }
            }
        }

        public void Run()
        {
            int i = 0;
            while (i++ < 10)
            {
                Results++;
                System.Threading.Thread.Sleep(1000);
            }
        }

        public void Run2()
        {
            int i = 0;
            while (i++ < 10)
            {
                Results++;
                System.Threading.Thread.Sleep(200);
            }
        }



        private void OnErrTimerElapsed(object state)
        {
            if (_gpio == null) // || IsStarted())
                return;

            // sviesoforo indikacija
            if (!SafetyStopIsActive() && IsStarted() && scene.IsRunMode == true)
            {
                _gpio.SetPin(settings.RunDOPin, 1);
                _gpio.SetPin(settings.SystemFaultLightDO, 0);
            } 
            else
            {
                _gpio.SetPin(settings.RunDOPin, 0);
                _gpio.SetPin(settings.SystemFaultLightDO, 1);
            }

            if (IsStarted() && _gpio.GetDOPin(settings.HardwareEnableDOPin) == 0)
            {
                _gpio.SetPin(settings.HardwareEnableDOPin, 1);
            }

            //if (!IsStarted() && _gpio.GetDOPin(settings.NozzleDOPin) == 1)
            //    DisableNozzles();

            //if (!IsStarted() && _gpio.GetDOPin(settings.ActuatorOnDOPin) == 1)
            //    ActuatorOFF();
        }

        public int Results
        {
            get
            {
                return _results;
            }
            set
            {
                _results = value;              
                _synchronousInvoker(() => InvokePropertyChanged(() => Results));                
            }
        }

        public long Judgement
        {
            get
            {
                return _judgement;
            }
            set
            {
                _judgement = value;
                _synchronousInvoker(() => InvokePropertyChanged(() => Judgement));
            }
        }

        public override event PropertyChangedEventHandler PropertyChanged = delegate { };

        private void InvokePropertyChanged<T>(Expression<Func<T>> expr)
        {
            var body = expr.Body as MemberExpression;

            if (body != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(body.Member.Name));
            }
        }

        public void SetError(ErrorFlags err)
        {
            Errors |= err;
        }

        public void ClearError(ErrorFlags err)
        {
            Errors &= ~err;
        }

        public bool IsError(ErrorFlags err)
        {
            return ((Errors & err) == err);
        }

        public ErrorFlags Errors
        {
            set
            {
                _errorStatus = value;
                ErrorStateChangedEventHandler handler = onErroStatusChanged;
                if (handler != null)
                {
                    onErroStatusChanged(null, _errorStatus);
                }
            }
            get
            {
                return _errorStatus;
            }
        }

        public int scenePath = 0;
        public bool runSceneOnce = false;
        /// <summary>
        /// Method that is invoked when scene's start block is completed
        /// </summary>
        /// <param name="sender">scene's start block</param>
        public void StartBlockCompleteAction(Object sender)
        {
            if (_startBlock != null)
            {
                scene.CountScene = _startBlock.CountResults;
            }


            if (scenePath == 0 && _imSaveBlock.EnableImageLoader)
            {
                _startBlock.Position1IsBusy = false;
                _startBlock.Position2IsBusy = false;
            }
      
        }

        int previousPath = 0;

        /// <summary>
        /// Method that is invoked when scene is started
        /// </summary>
        /// <param name="sender">scene</param>
        public void SceneStartAction(Object sender)
        {
            FxSessionTimeClass.UpdateTime();

            sceneStarted = true;
            nextScene = 1; // RetrieveScenePathFromBuffer();
            ClearNGDO();

            if (runSceneOnce && scenePath == 1)
            {
                scenePath = 1;
                _startBlock.Position1IsBusy = true;
                _startBlock.Position2IsBusy = false;
            }
            else if(runSceneOnce && scenePath == 2)
            {
                scenePath = 2;
                _startBlock.Position1IsBusy = false;
                _startBlock.Position2IsBusy = true;
            }

            if (nextScene == 1)
            {
                scenePath = 1;
                _startBlock.Position1IsBusy = true;
                _startBlock.Position2IsBusy = false;
                nextScene = 0;
            }
            else if (nextScene == 2)
            {
                scenePath = 2;
                _startBlock.Position1IsBusy = false;
                _startBlock.Position2IsBusy = true;
                nextScene = 0;
            }



            if (scenePath.Equals(1) || scenePath.Equals(2) || _imSaveBlock.EnableImageLoader)
            {
                string text = "Started new session on side ";

                if (scenePath.Equals(1)) text += "[1]: ";
                else if (scenePath.Equals(2)) text += "[2]: ";
                else text += "[NaN]: ";

                HandleInfoMessage("Core", text + FxSessionTimeClass.SessionStartTime);
            }

            if (_bottlePlaneData != null)
            {
                _bottlePlaneData.UpdateV0Values();
            }

               // HandleInfoMessage("Core", "Started new session: " + FxSessionTimeClass.SessionStartTime);
        }

        /// <summary>
        /// Method that is invoked when scene is finished
        /// </summary>
        /// <param name="sender">scene</param>
        public void SceneCompleteAction(Object sender)
        {

            if (((FxScene)sender).Judgement == true || settings.TreatAllMasksAsGood == true)
            {

            }
            else
            {
                SetNG();

                // jei ijungtas tik blogu buteliu duomenu saugojimas
                if (settings.EnNgImgLog && !_imSaveBlock.SaveGrabbedImages)
                {
                    _bottlePlaneData.SaveWallThicknessData();
                }                

            }


            if (scenePath.Equals(1) || scenePath.Equals(2) || _imSaveBlock.EnableImageLoader)
            {
                if(scene.CountScene)
                {
                    TotalCount++;
                    OKCount = ((FxScene)sender).Judgement == true ? OKCount + 1 : OKCount;
                    NGCount = ((FxScene)sender).Judgement == false ? NGCount + 1 : NGCount;
                    Efficiency = 100 * OKCount / TotalCount;

                    // paleisti buteli
                    ReleaseBottle();
                    // ijungti putiklius
                    EnableNozzles();
                }


                string text = "Session on side ";

                if (scenePath.Equals(1)) text += "[1] '";
                else if (scenePath.Equals(2)) text += "[2] '";
                else text += "[NaN] '";

                if(((FxScene)sender).Judgement == true)
                {
                    HandleMaskOkMessage("Core", text + FxSessionTimeClass.SessionStartTime + "' completed with result OK");
                }
                else
                {
                    HandleErrorMessage("Core", text + FxSessionTimeClass.SessionStartTime + "' completed with result NG");
                }
                //HandleInfoMessage("Core", "Session '" + FxSessionTimeClass.SessionStartTime + "' completed with result " + (((FxScene)sender).Judgement == true ? "OK" : "NG"));
            }

            if (!BottleIsIN() && !_startBlock.CanStart && IsStarted())
            {

                // ijungti putiklius
                EnableNozzles();   

                _startBlock.CanStart = true;

            }


            if (onSceneCompleted != null)
            {
                onSceneCompleted(sender);
            }

            sceneStarted = false;

            //if(threadStopRequest.Equals(true))
            //{
            //    Stop();
            //}

            if (scenePath == 1) _startBlock.Position1IsBusy = false;
            if (scenePath == 2) _startBlock.Position2IsBusy = false;

            scenePath = 0;


            if(((FxScene)sender).Judgement == true)
            {
                // stabdyti po gero, jei pasirinkta
                if (_gpio != null && scene.CountScene && settings.StopAfterGD)
                {
                    ActuatorOFF();
                    _gpio.SetPin(settings.BottleCatchDOPin, 0);
                    Stop();
                }
            }
            else
            {
                // stabdyti po blogo, jei pasirinkta
                if (_gpio != null && scene.CountScene && settings.StopAfterNG)
                {
                    ActuatorOFF();
                    _gpio.SetPin(settings.BottleCatchDOPin, 0);
                    Stop();
                }
            }
        } 


        public void ClearStatistic()
        {
            TotalCount = 0;
            NGCount = 0;
            OKCount = 0;
            Efficiency = 0;
        }

        public string NextImage()
        {
            return "Next";
        }

        public string PrevImage()
        {
            return "Prev";
        }

        // props

        public bool EnableDI {
            get
            {
                return _gpio != null ?_gpio.EnableDI : false;
            }
            set
            {
                if(_gpio != null)
                {
                    _gpio.EnableDI = value;
                }

                if (settings.EnableDebugLog) HandleInfoMessage("Core", value ? "DI enabled" : "DI disabled");
            }
        }
        
        public bool EnableDO
        {
            get
            {
                return _gpio != null ? _gpio.EnableDO : false;
            }
            set
            {
                if (_gpio != null)
                {
                    if (!value)
                    {
                        _gpio.SetPort(0, 0);
                    }
                    _gpio.EnableDO = value;
                }

                if (settings.EnableDebugLog) HandleInfoMessage("Core", value ? "DO enabled" : "DO disabled");
            }
        }

        public bool EnableImgLog
        {
            get
            {
                return _imSaveBlock != null ? _imSaveBlock.SaveGrabbedImages : false;
            }
            set
            {
                if (_imSaveBlock != null)
                {
                    _imSaveBlock.SaveGrabbedImages = value;
                }
                HandleInfoMessage("Core", value ? "Data log enabled" : "Data log disabled");
            }

        }

        public bool EnableNgImgLog { get; set; }
        //{
        //    get
        //    {
        //        return _imgLogger.EnableNgLogging;
        //    }
        //    set
        //    {
        //        _imgLogger.EnableNgLogging = value;
        //    }
        //}


        public string Filepath
        {
            get
            {
                return "";  
            }
            set
            {

            }
        }

        public string ImgLogPath
        {
            get;
            set;
        }

        public string FileName
        {
            get
            {
                return "";
            }
            set
            {

            }
        }

        /// <summary>
        /// Load predefined hardware items to list
        /// </summary>
        void LoadHardwareList()
        {
            if (HardwareList.Count > 0) HardwareList = new List<HardwareObject>();

            // hardware list
            HardwareList.Add(new HardwareObject { Name = _camera.Name, Obj = _camera }); 

            HardwareList.Add(new HardwareObject { Name = _camera2.Name, Obj = _camera2 });


            //foreach (HardwareObject hwObj in HardwareList)
            //{
            //    Object item = hwObj.Obj;
            //    Type tt = item.GetType();

            //    if (hwObj.Obj.GetType().Equals(typeof(FxActuator)))
            //    {
            //        FxActuator actuator = hwObj.Obj as FxActuator;
            //        actuator.onMessage += actuator_onMessage;
            //    }
            //}
            
        }


        /// <summary>
        /// Load predefined function block list
        /// </summary>
        void LoadBlockList()
        {
            try
            {
                if (BlockList.Count > 0)
                    BlockList.RemoveRange(0, BlockList.Count);

                BlockList.Add(_startBlock = new FxStartBlock("Start Block", _gpio));
                BlockList.Add(_bottlePlaneData = new FxBottlePlaneData("Bottle Plane Data", analog_board, _gpio));
                BlockList.Add(_svmPredictor = new FxSVMPredictor("SVM"));
                BlockList.Add(_end = new FxEnd("End Block"));

                BlockList.Add(_imSaveBlock = new FxSaveGrabbedImagesBlock("Save Grabbed Images"));

                BlockList.Add(settings);
                
            }
            catch(Exception ex)
            {
                FxSessionTimeClass.LogErrorMessage("(Failed to load blocklist) ", ex.Message);

            }

        }

        /// <summary>
        /// Creates hardware & blocks lists
        /// </summary>
        void LoadScene()
        {

            try
            {
                scene = new FxScene(settingsManager.FolderNames[settings.LastSavedMaskIndex]); //settings.SceneName);
                OnPropertyChanged(new PropertyChangedEventArgs("SceneName"));
            }
            catch(Exception ex)
            {
                //throw new Exception("Scene change error! Check settings folders!");
                HandleErrorMessage("(Scene change)", "Check settings folders!");
                FxSessionTimeClass.LogErrorMessage("(Scene change error) ", ex.Message);
            }

            if (scene.blocks.Count > 0) scene.blocks = new List<FxBlock>();

            // hardware binding to blocks
            foreach(object block in BlockList)
            {
                var properties = block.GetType().GetProperties();
                foreach (var prop in properties)
                {
                    // search for hardware id attribute
                    if (prop.GetCustomAttributes(false).Any(a => a is HardwareIdAtrribute))
                    {
                        string hwId = (string)prop.GetValue(block);
                        int itemIdx = HardwareList.FindIndex(x => x.Name == hwId);

                        foreach (var method in block.GetType().GetMethods())
                        {
                            // assign hardware element to block
                            if (method.GetCustomAttributes(false).Any(a => a is HardwareAtrribute))
                            {
                                var meth = block.GetType().GetMethods().FirstOrDefault(f => f.Name == method.Name);
                                try
                                {
                                    if(itemIdx >= 0)
                                        meth.Invoke(block, new object[] { HardwareList[itemIdx] });
                                }
                                catch(Exception ex)
                                {
                                    HandleErrorMessage("(Scene load error)", ex.Message);
                                    FxSessionTimeClass.LogErrorMessage("(Scene load error) ", ex.Message);
                                    //throw new Exception(ex.Message);
                                }
                                
                            }
                        }

                    }
           
                }

                try
                {
                    var testIfBlock = block.GetType().GetMethods().First(b => b.Name == "Run");
                    scene.AddBlock((FxBlock)block);
                }
                catch(Exception ex)
                {
                    // not a FxBlock
                    FxSessionTimeClass.LogErrorMessage("(Scene load error) ", ex.Message);
                }   
            }

            if(scene != null)
            {
                SetBlocksRellations();

                scene.onStarted += delegate(Object sender) { SceneStartAction(sender); };
                scene.onCompleted += delegate(Object sender) { SceneCompleteAction(sender); };
                scene.onStartBlockCompleted += delegate(Object sender) { StartBlockCompleteAction(sender); };
                
            }

            // update bindings
            SetDataBindings(true);
            OnPropertyChanged(scene, new PropertyChangedEventArgs("Name"));
        }

        void b_onMessage(object sender, string msg)
        {
            FxBlock b = sender as FxBlock;
            if (settings.EnableDebugLog) HandleInfoMessage(b.Name, msg);
        }

        void b_onTaskCompleted(object sender)
        {
            FxBlock b = sender as FxBlock;

            if (settings.EnableDebugLog)
            {
                if (b == null || b.NextFxBlock == null) return;


                // update block status if log is enabled
                if (!(b.GetType() == new FxStartBlock().GetType() && b.NextFxBlock.GetType() == new FxEnd().GetType()) && b.GetType() != new FxEnd().GetType())
                    HandleInfoMessage(b.Name, "Completed " + (b.Judgement == true ? "OK" : "NG"));
            }
        }
       

        /// <summary>
        /// Save current bindings from FxBinder to bindingList
        /// </summary>
        public void GetDataBindings()
        {
            if (Rellations.BindingsList.Count > 0) Rellations.BindingsList = new List<BindingsPairItem>();
                //Rellations.BindingsList.RemoveRange(0, Rellations.BindingsList.Count - 1);

            foreach(var item in binder.Map)
            {
                string sourceName = "";
                string targetName = "";

                foreach (var prop in item.Source.Object.GetType().GetProperties())
                {
                    if (prop.GetCustomAttributes(false).Any(a => a is BlockNameAttribute))
                    {
                        sourceName = (string)prop.GetValue(item.Source.Object);
                        break;
                    }
                }

                foreach (var prop in item.Target.Object.GetType().GetProperties())
                {
                    if (prop.GetCustomAttributes(false).Any(a => a is BlockNameAttribute))
                    {
                        targetName = (string)prop.GetValue(item.Target.Object);
                        break;
                    }
                }

                Rellations.BindingsList.Add(new BindingsPairItem { SourceObj = item.Source.Object.GetType().ToString(),
                    SourceName = sourceName, SourcePropName = item.Source.PropName, TargetObj = item.Target.Object.GetType().ToString(),
                    TargetName = targetName, TargetPropName = item.Target.PropName });
            }
        }

        /// <summary>
        /// Load bindings from bindingsList to FxBinder. If 'false' option provided or bindingsList is empty, bindingsList is created from hardcoded bindings
        /// </summary>
        /// <param name="loadFromFile">Load bindings from file or predefined ones in code</param>
        public void SetDataBindings(bool loadFromFile)
         {
            AddFirstTimeBindings();
             
         }

        /// <summary>
        /// Add first time bindings, that are not present in config file
        /// </summary>
        public void AddFirstTimeBindings()
        {
            if (binder.Map.Count > 0) binder.Map = new List<MapPair>();
            if (binder.Objects.Count > 0) binder.Objects = new List<object>(); 

            // scene name binding
            binder.AddBind(new MapPair(new MapNode(scene, "Name"), new MapNode(settings, "SceneName")));
            binder.AddBind(new MapPair(new MapNode(settings, "SceneName"), new MapNode(_imSaveBlock, "SceneName")));

            binder.AddBind(new MapPair(new MapNode(_bottlePlaneData, "BottleDataString"), new MapNode(_svmPredictor, "Problem")));
            binder.AddBind(new MapPair(new MapNode(_bottlePlaneData, "BlockOperationMode"), new MapNode(_svmPredictor, "BlockOperationMode")));
            binder.AddBind(new MapPair(new MapNode(_imSaveBlock, "FilePath"), new MapNode(_bottlePlaneData, "FilePath")));

            binder.AddBind(new MapPair(new MapNode(settings, "ActuatorOnDOPin"), new MapNode(_bottlePlaneData, "ActuatorOnDOPin")));
            binder.AddBind(new MapPair(new MapNode(_imSaveBlock, "SaveGrabbedImages"), new MapNode(_bottlePlaneData, "SaveData")));



            binder.AddBind(new MapPair(new MapNode(settings, "BottleInDIPin"), new MapNode(_startBlock, "BottleInDI")));
            binder.AddBind(new MapPair(new MapNode(settings, "BottleFlowDOPin"), new MapNode(_startBlock, "BottleFlowDO")));
            binder.AddBind(new MapPair(new MapNode(settings, "BottleCatchDOPin"), new MapNode(_startBlock, "BottleCatchDO")));
            binder.AddBind(new MapPair(new MapNode(settings, "NozzleDOPin"), new MapNode(_startBlock, "Nozzle1DO")));
            binder.AddBind(new MapPair(new MapNode(settings, "ActuatorOnDOPin"), new MapNode(_startBlock, "ActuatorOnDOPin")));

            //save them
            GetDataBindings();
        }

        /// <summary>
        /// Set blocks rellations
        /// </summary>
        public void SetBlocksRellations()
        {
            scene.StartBlock = _startBlock;

            _startBlock.IsStartBlock = true;
            _startBlock.EndBlock = _end;

            _startBlock.Position1 = _bottlePlaneData;
            _bottlePlaneData.NextFxBlock = _svmPredictor;
            _svmPredictor.NextFxBlock = _end;

            //_startBlock.Position1 = _grabber1;
            //_startBlock.Position2 = _grabber2;

            //_grabber1.NextFxBlock = _contourBreak;
            //_contourBreak.NextFxBlock = _end;

            //_grabber2.NextFxBlock = _neckDeformation;
            //_neckDeformation.NextFxBlock = _end;

            //_startBlock.Position2 = _grabImages2;
            //_startBlock.EndBlock = _end;

            //Left side
            //_grabImages.NextFxBlock = _filterHolesBlobs;
            //_actuatorMaskLinear1.NextFxBlock = _end;

            //Right side
            //_grabImages2.NextFxBlock = _filterHolesBlobs2;

            
        }



        public void SetGrabBlocksOperation(OperationMode mode)
        {
            if (BlockList != null && BlockList.Count > 0)
            {
                foreach (object block in BlockList)
                {
                    try
                    {
                        //FxRotateAndGrabImage b = new FxRotateAndGrabImage();
                        FxBottlePlaneData b = new FxBottlePlaneData();
                        //string blockName = "";

                        if (block.GetType().Equals(b.GetType()))
                        {
                            //b = block as FxRotateAndGrabImage;
                            b = block as FxBottlePlaneData;
                            b.BlockOperationMode = mode;
                            //blockName = block.Name;
                        }
                    }
                    catch
                    {
                        // not an FxBlock
                    }
                }

            }
        }


        public void AllowToStopThread()
        {
            threadStopRequest = true;
            sceneStarted = false;
        }

    }
}
