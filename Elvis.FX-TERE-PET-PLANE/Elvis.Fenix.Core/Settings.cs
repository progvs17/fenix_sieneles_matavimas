﻿using Elvis.Fenix.FunctionBlocks;
using Elvis.Fenix.General;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elvis.Fenix
{
    public class Settings : IFxBindable
    {
        #region Private Fields

        private string name;

        //general
        private bool enAutoRun;
        private bool enAuto;
        private bool enAllImgLog;
        private bool enGPIO;
        private bool enNgImgLog;
        private bool enFileSrc;
        private bool enableFileOrderByDate;

        //gpio
        private string _sceneName;// = "NaN";
        private int _lastSavedMaskIndex = 0;

        private int yellowLightDO;
        private int bottleInDiPin;
        private int hwEnDOPin;
        private int input1;

        private int position1ReadyDOPin;
        private int bottleCatchDOPin;
        private int nozzleDOPin;
        private int runDOPin;
        private int actuatorOnDOPin;
        private int ngDOPin;

 
        // image tileing
        private int _horizontalImagesToTile = 4;
        private bool _cropTiledImages = true;
        private int _tiledImagesBorderWidth = 10;
        private int _croppedImageSize = 130;
        private int widthOffset = 0;
        private int heightOffset = 0;
        private int xOffset = 0;
        private int yOffset = 0;

        
        // hardware
        private string camera1Name = "C3";
        private string camera2Name ="C4";
        private bool _enableDebugLog;
        private bool treatAllMasksAsGood;
        private int input2;
        private int input3;
        private int bottleFlowDOPin;
        private int bottleFlowDelay;
        private int noBottleDelay;
        private int manualInDiPin;
        private bool stopAfterGD;
        private bool stopAfterNG;

        #endregion

        #region Constructors

        public Settings()
        { }

        public Settings(string name)
        {
            Name = name;

            //EnAuto = true;
            enAllImgLog = false;
            enGPIO = true;
            //enNgImgLog = false;
            //enFileSrc = false;
            _enableDebugLog = false;
        }

        #endregion

        #region Public Properties


        #region Common
        [BlockNameAttribute]
        public string  Name 
        {
            get { return name; }
            set { name = value; } 
        }

        public bool EnAutoRun
        {
            get
            {
                return enAutoRun;
            }
            set
            {
                enAutoRun = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("EnAutoRun"));
            }
        }

        public bool EnableDebugLog
        {
            get
            {
                return _enableDebugLog;
            }
            set
            {
                _enableDebugLog = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("EnableDebugLog"));
            }
        }

        //public bool EnAuto
        //{
        //    get
        //    {
        //        return enAuto;
        //    }

        //    set
        //    {
        //        enAuto = value;
        //        OnPropertyChanged(this, new PropertyChangedEventArgs("EnAuto"));
        //    }
        //}

        public bool EnAllImgLog
        {
            get
            {
                return enAllImgLog;
            }

            set
            {
                enAllImgLog = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("EnAllImgLog"));
            }
        }

        public bool EnGPIO
        {
            get
            {
                return enGPIO;
            }

            set
            {
                enGPIO = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("EnGPIO"));
            }
        }

        public bool EnNgImgLog
        {
            get
            {
                return enNgImgLog;
            }

            set
            {
                enNgImgLog = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("EnNgImgLog"));
            }
        }

        public bool EnableFileSource
        {
            get
            {
                return enFileSrc;
            }
            set
            {
                enFileSrc = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("EnableFileSource"));
            }
        }

        public bool EnableFileOrderByDate
        {
            get
            {
                return enableFileOrderByDate;
            }
            set
            {
                enableFileOrderByDate = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("EnableFileOrderByDate"));
            }
        }

        public bool TreatAllMasksAsGood
        {
            get
            {
                return treatAllMasksAsGood;
            }
            set
            {
                treatAllMasksAsGood = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("TreatAllMasksAsGood"));
            }
        }


        public bool StopAfterGD
        {
            get
            {
                return stopAfterGD;
            }
            set
            {
                stopAfterGD = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("StopAfterGD"));
            }
        }

        public bool StopAfterNG
        {
            get
            {
                return stopAfterNG;
            }
            set
            {
                stopAfterNG = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("StopAfterNG"));
            }
        }

        #endregion


        #region Image Tileing
        // image tile'ing properties
        public int HorizontalImagesToTile
        {
            get
            {
                return _horizontalImagesToTile;
            }
            set
            {
                _horizontalImagesToTile = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HorizontalImagesToTile"));
            }
        }

        public bool CropTiledImages
        {
            get
            {
                return _cropTiledImages;
            }
            set
            {
                _cropTiledImages = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("CropTiledImages"));
            }
        }

        public int TiledImagesBorderWidth
        {
            get
            {
                return _tiledImagesBorderWidth;
            }
            set
            {
                _tiledImagesBorderWidth = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("TiledImagesBorderWidth"));
            }
        }

        public int CroppedImageSize
        {
            get
            {
                return _croppedImageSize;
            }
            set
            {
                _croppedImageSize = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("CroppedImageSize"));
            }
        }

        public int WidthOffset
        {
            get
            {
                return widthOffset;
            }
            set
            {
                widthOffset = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("WidthOffset"));
            }
        }

        public int HeigthOffset
        {
            get
            {
                return heightOffset;
            }
            set
            {
                heightOffset = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HeigthOffset"));
            }
        }

        public int XOffset
        {
            get
            {
                return xOffset;
            }
            set
            {
                xOffset = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("XOffset"));
            }
        }

        public int YOffset
        {
            get
            {
                return yOffset;
            }
            set
            {
                yOffset = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("YOffset"));
            }
        }
        #endregion


        #region Scene
        public int LastSavedMaskIndex 
        {
            get
            { 
                return _lastSavedMaskIndex; 
            }
            set
            {
                _lastSavedMaskIndex = value;
                //OnPropertyChanged(this, new PropertyChangedEventArgs("LastSavedMaskIndex"));
            }
        }

        public string SceneName
        {
            get
            {
                return _sceneName;
            }
            set
            {
                _sceneName = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SceneName"));
            }
        }

        

        #endregion


        #region Hardware

        public string Camera1Name
        {
            get
            {
                return camera1Name;
            }
            set
            {
                camera1Name = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Camera1Name"));
            }
        }

        public string Camera2Name
        {
            get
            {
                return camera2Name;
            }
            set
            {
                camera2Name = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Camera2Name"));
            }
        }

        #endregion
        

        #region FX-INS-MASK 1 PINS

        
        public int BottleInDIPin
        {
            get
            {
                return bottleInDiPin;
            }

            set
            {
                bottleInDiPin = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BottleInDIPin"));
            }
        }

        public int ManualInDIPin
        {
            get
            {
                return manualInDiPin;
            }

            set
            {
                manualInDiPin = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ManualInDIPin"));
            }
        }

        public int SafetyStopInDIPin
        {
            get
            {
                return input1;
            }

            set
            {
                input1 = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("SafetyStopInDIPin"));
            }
        }

        public int Input2DIPin
        {
            get
            {
                return input2;
            }

            set
            {
                input2 = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Input2DIPin"));
            }
        }

        public int Input3DIPin
        {
            get
            {
                return input3;
            }

            set
            {
                input3 = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("Input3DIPin"));
            }
        }

        public int HardwareEnableDOPin
        {
            get
            {
                return hwEnDOPin;
            }

            set
            {
                hwEnDOPin = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("HardwareEnableDOPin"));
            }
        }

        public int SystemFaultLightDO
        {
            get
            {
                return yellowLightDO;
            }

            set
            {
                yellowLightDO = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("YellowLightDO"));
            }
        }


        public int BottleFlowDOPin
        {
            get
            {
                return bottleFlowDOPin;
            }

            set
            {
                bottleFlowDOPin = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BottleFlowDOPin"));
            }
        }

        public int BottleCatchDOPin
        {
            get
            {
                return bottleCatchDOPin;
            }

            set
            {
                bottleCatchDOPin = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("BottleCatchDOPin"));
            }
        }

        public int NozzleDOPin
        {
            get
            {
                return nozzleDOPin;
            }

            set
            {
                nozzleDOPin = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("NozzleDOPin"));
            }
        }

        public int RunDOPin
        {
            get
            {
                return runDOPin;
            }

            set
            {
                runDOPin = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("RunDOPin"));
            }
        }

        public int ActuatorOnDOPin
        {
            get
            {
                return actuatorOnDOPin;
            }

            set
            {
                actuatorOnDOPin = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("ActuatorOnDOPin"));
            }
        }

        public int NGDOPin
        {
            get
            {
                return ngDOPin;
            }

            set
            {
                ngDOPin = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs("NGDOPin"));
            }
        }

        #endregion



        #endregion

    }
}
